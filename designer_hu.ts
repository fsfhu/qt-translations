<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu_HU">
<context>
    <name>AbstractFindWidget</name>
    <message>
        <location filename="../../qttools/src/shared/findwidget/abstractfindwidget.cpp" line="+129"/>
        <source>&amp;Previous</source>
        <translation>&amp;Előző</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Next</source>
        <translation>&amp;Következő </translation>
    </message>
    <message>
        <location line="+24"/>
        <source>&amp;Case sensitive</source>
        <translation>Kis- és &amp;nagybetű érzékeny</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Whole &amp;words</source>
        <translation>&amp;Teljes szavak</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&lt;img src=&quot;:/qt-project.org/shared/images/wrap.png&quot;&gt;&amp;nbsp;Search wrapped</source>
        <translation>&lt;img src=&quot;:/qt-project.org/shared/images/wrap.png&quot;&gt;&amp;nbsp;A keresés körbeért</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>&amp;Find in Text...</source>
        <translation type="unfinished">Ke&amp;resés a szövegben…</translation>
    </message>
</context>
<context>
    <name>AbstractItemEditor</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/itemlisteditor.cpp" line="+94"/>
        <source>Selectable</source>
        <translation>Kiválasztható</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Editable</source>
        <translation>Szerkeszthető</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>DragEnabled</source>
        <translation>Húzás engedélyezve</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>DropEnabled</source>
        <translation>Ejtés engedélyezve</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>UserCheckable</source>
        <translation>Felhasználó bejelölheti</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Enabled</source>
        <translation>Engedélyezve</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tristate</source>
        <translation>Három állapotú</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unchecked</source>
        <translation>Nem bejelölt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PartiallyChecked</source>
        <translation>Részlegesen bejelölt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Checked</source>
        <translation>Bejelölt</translation>
    </message>
</context>
<context>
    <name>AddLinkDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/addlinkdialog.ui"/>
        <source>Insert Link</source>
        <translation>Hivatkozás beszúrása</translation>
    </message>
    <message>
        <location/>
        <source>Title:</source>
        <translation>Cím:</translation>
    </message>
    <message>
        <location/>
        <source>URL:</source>
        <translation>URL:</translation>
    </message>
</context>
<context>
    <name>AppFontDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/designer/appfontdialog.cpp" line="+405"/>
        <source>Additional Fonts</source>
        <translation>További betűkészletek</translation>
    </message>
</context>
<context>
    <name>AppFontManager</name>
    <message>
        <location line="-267"/>
        <source>&apos;%1&apos; is not a file.</source>
        <translation>A(z) „%1” nem egy fájl.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The font file &apos;%1&apos; does not have read permissions.</source>
        <translation>A(z) „%1” betűkészletfájlon nincs olvasási jog.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The font file &apos;%1&apos; is already loaded.</source>
        <translation>A(z) „%1” betűkészletfájl már be van töltve.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The font file &apos;%1&apos; could not be loaded.</source>
        <translation>A(z) „%1” betűkészletfájlt nem sikerült betölteni.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>&apos;%1&apos; is not a valid font id.</source>
        <translation>A(z) „%1” nem érvényes betűkészlet-azonosító.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>There is no loaded font matching the id &apos;%1&apos;.</source>
        <translation>Nincs „%1” azonosítóra illeszkedő betűkészlet betöltve.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>The font &apos;%1&apos; (%2) could not be unloaded.</source>
        <translation>A(z) „%1” (%2) betűkészletet nem sikerült eltávolítani.</translation>
    </message>
</context>
<context>
    <name>AppFontWidget</name>
    <message>
        <location line="+26"/>
        <source>Fonts</source>
        <translation>Betűkészletek</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Add font files</source>
        <translation>Betűkészletfájlok hozzáadása</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Remove current font file</source>
        <translation>Jelenlegi betűkészletfájl eltávolítása</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Remove all font files</source>
        <translation>Minden betűkészletfájl eltávolítása</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Add Font Files</source>
        <translation>Betűkészletfájlok hozzáadása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Font files (*.ttf)</source>
        <translation>Betűkészletfájlok (*.ttf)</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Error Adding Fonts</source>
        <translation>Hiba a betűkészlet hozzáadásakor</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Error Removing Fonts</source>
        <translation>Hiba a betűkészlet eltávolításakor</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Remove Fonts</source>
        <translation>Betűkészletek eltávolítása</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Would you like to remove all fonts?</source>
        <translation>Az összes betűkészletet el szeretné távolítani?</translation>
    </message>
</context>
<context>
    <name>AppearanceOptionsWidget</name>
    <message>
        <location filename="../../qttools/src/designer/src/designer/qdesigner_appearanceoptions.ui"/>
        <source>Form</source>
        <translation>Űrlap</translation>
    </message>
    <message>
        <location/>
        <source>User Interface Mode</source>
        <translation>Felhasználói felület módja</translation>
    </message>
</context>
<context>
    <name>AssistantClient</name>
    <message>
        <location filename="../../qttools/src/designer/src/designer/assistantclient.cpp" line="+84"/>
        <source>Unable to send request: Assistant is not responding.</source>
        <translation>Nem küldhető el a kérés: az Asszisztens nem válaszol.</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>The binary &apos;%1&apos; does not exist.</source>
        <translation>A(z) „%1” bináris nem létezik.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Unable to launch assistant (%1).</source>
        <translation>Nem indítható el az Asszisztens (%1)</translation>
    </message>
</context>
<context>
    <name>BrushPropertyManager</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/propertyeditor/brushpropertymanager.cpp" line="+39"/>
        <source>No brush</source>
        <translation>Nincs ecset</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Solid</source>
        <translation>Tömör</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dense 1</source>
        <translation>Sűrű 1</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dense 2</source>
        <translation>Sűrű 2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dense 3</source>
        <translation>Sűrű 3</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dense 4</source>
        <translation>Sűrű 4</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dense 5</source>
        <translation>Sűrű 5</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dense 6</source>
        <translation>Sűrű 6</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dense 7</source>
        <translation>Sűrű 7</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Horizontal</source>
        <translation>Vízszintes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Vertical</source>
        <translation>Függőleges</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cross</source>
        <translation>Kereszt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Backward diagonal</source>
        <translation>Fordított átlós</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Forward diagonal</source>
        <translation>Előre átlós</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Crossing diagonal</source>
        <translation>Keresztátlós</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>Style</source>
        <translation>Stílus</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Color</source>
        <translation>Szín</translation>
    </message>
    <message>
        <location line="+105"/>
        <source>[%1, %2]</source>
        <translation>[%1, %2]</translation>
    </message>
</context>
<context>
    <name>Command</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/connectionedit.cpp" line="+129"/>
        <source>Add connection</source>
        <translation>Kapcsolat hozzáadása</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Adjust connection</source>
        <translation>Kapcsolat módosítása</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Delete connections</source>
        <translation>Kapcsolatok törlése</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Change source</source>
        <translation>Forrás módosítása</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Change target</source>
        <translation>Cél módosítása</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/formlayoutmenu.cpp" line="+443"/>
        <location filename="../../qttools/src/designer/src/components/taskmenu/button_taskmenu.cpp" line="+269"/>
        <source>Add &apos;%1&apos; to &apos;%2&apos;</source>
        <extracomment>Command description for adding buttons to a QButtonGroup</extracomment>
        <translation>„%1” hozzáadása ehhez: „%2”</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/morphmenu.cpp" line="+330"/>
        <source>Morph %1/&apos;%2&apos; into %3</source>
        <extracomment>MorphWidgetCommand description</extracomment>
        <translation>%1/„%2” átalakítása erre: %3</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_command.cpp" line="+136"/>
        <source>Insert &apos;%1&apos;</source>
        <translation>„%1” beszúrása</translation>
    </message>
    <message>
        <location line="+130"/>
        <source>Change Z-order of &apos;%1&apos;</source>
        <translation>„%1” mélységének módosítása</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Raise &apos;%1&apos;</source>
        <translation>„%1” előrébb hozása</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Lower &apos;%1&apos;</source>
        <translation>„%1” hátrébb küldése</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>Delete &apos;%1&apos;</source>
        <translation>„%1” törlése</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>Reparent &apos;%1&apos;</source>
        <translation>„%1” szülőjének módosítása</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Promote to custom widget</source>
        <translation>Előléptetés egyéni felületi elemmé</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Demote from custom widget</source>
        <translation>Lefokozás egyéni felületi elemről</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Lay out using grid</source>
        <translation>Elrendezés rács használatával</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Lay out vertically</source>
        <translation>Elrendezés függőlegesen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Lay out horizontally</source>
        <translation>Elrendezés vízszintesen</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Break layout</source>
        <translation>Elrendezés feltörése</translation>
    </message>
    <message>
        <location line="+105"/>
        <source>Simplify Grid Layout</source>
        <translation>Rács elrendezés egyszerűsítése</translation>
    </message>
    <message>
        <location line="+131"/>
        <location line="+223"/>
        <location line="+74"/>
        <source>Move Page</source>
        <translation>Oldal áthelyezése</translation>
    </message>
    <message>
        <location line="-265"/>
        <location line="+117"/>
        <location line="+178"/>
        <location line="+659"/>
        <source>Delete Page</source>
        <translation>Oldal törlése</translation>
    </message>
    <message>
        <location line="-918"/>
        <location line="+117"/>
        <source>Page</source>
        <translation>Oldal</translation>
    </message>
    <message>
        <location line="-112"/>
        <location line="+117"/>
        <location line="+176"/>
        <location line="+660"/>
        <source>Insert Page</source>
        <translation>Oldal beszúrása</translation>
    </message>
    <message>
        <location line="-640"/>
        <source>Change Tab order</source>
        <translation>Lapsorrend módosítása</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Create Menu Bar</source>
        <translation>Menüsor létrehozása</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Delete Menu Bar</source>
        <translation>Menüsor törlése</translation>
    </message>
    <message>
        <location line="+47"/>
        <source>Create Status Bar</source>
        <translation>Állapotsor létrehozása</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Delete Status Bar</source>
        <translation>Állapotsor törlése</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Add Tool Bar</source>
        <translation>Eszköztár hozzáadása</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Add Dock Window</source>
        <translation>Dokkablak hozzáadása</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Adjust Size of &apos;%1&apos;</source>
        <translation>„%1” méretének beállítása</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Change Form Layout Item Geometry</source>
        <translation>Űrlapelrendezés elem geometriájának módosítása</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>Change Layout Item Geometry</source>
        <translation>Elrendezés elem geometriájának módosítása</translation>
    </message>
    <message>
        <location line="+134"/>
        <source>Delete Subwindow</source>
        <translation>Gyermekablak törlése</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Insert Subwindow</source>
        <translation>Gyermekablak beszúrása</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Subwindow</source>
        <translation>Gyermekablak</translation>
    </message>
    <message>
        <location line="+385"/>
        <source>Change Table Contents</source>
        <translation>Táblázat tartalmának módosítása</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Change Tree Contents</source>
        <translation>Fa tartalmának módosítása</translation>
    </message>
    <message>
        <location line="+74"/>
        <location line="+147"/>
        <source>Add action</source>
        <translation>Művelet hozzáadása</translation>
    </message>
    <message>
        <location line="-121"/>
        <location line="+127"/>
        <source>Remove action</source>
        <translation>Művelet eltávolítása</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Add menu</source>
        <translation>Menü hozzáadása</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Remove menu</source>
        <translation>Menü eltávolítása</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Create submenu</source>
        <translation>Almenü létrehozása</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Delete Tool Bar</source>
        <translation>Eszköztár törlése</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_command2.cpp" line="+141"/>
        <source>Change layout of &apos;%1&apos; from %2 to %3</source>
        <translation>„%1” elrendezésének módosítása: %2 → %3</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Change layout alignment</source>
        <translation>Elrendezés igazításának módosítása</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_menu.cpp" line="+1171"/>
        <source>Set action text</source>
        <translation>Művelet szövegének beállítása</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Insert action</source>
        <translation>Művelet beszúrása</translation>
    </message>
    <message>
        <location line="+89"/>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_menubar.cpp" line="+886"/>
        <source>Move action</source>
        <translation>Művelet áthelyezése</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_menubar.cpp" line="-424"/>
        <source>Change Title</source>
        <translation>Cím módosítása</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Insert Menu</source>
        <translation>Menü beszúrása</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_propertycommand.cpp" line="+1249"/>
        <source>Changed &apos;%1&apos; of &apos;%2&apos;</source>
        <translation>A(z) „%2” objektum „%1” tulajdonsága megváltoztatva</translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>Changed &apos;%1&apos; of %n objects</source>
        <translation>
            <numerusform>%n objektum „%1” tulajdonsága megváltoztatva</numerusform>
        </translation>
    </message>
    <message>
        <location line="+96"/>
        <source>Reset &apos;%1&apos; of &apos;%2&apos;</source>
        <translation>A(z) „%2” objektum „%1” tulajdonsága visszaállítva</translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>Reset &apos;%1&apos; of %n objects</source>
        <translation>
            <numerusform>%n objektum „%1” tulajdonsága visszaállítva</numerusform>
        </translation>
    </message>
    <message>
        <location line="+83"/>
        <source>Add dynamic property &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>A(z) „%1” dinamikus tulajdonság hozzáadása a(z) „%2” objektumhoz</translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>Add dynamic property &apos;%1&apos; to %n objects</source>
        <translation>
            <numerusform>A(z) „%1” dinamikus tulajdonság hozzáadása %n objektumhoz</numerusform>
        </translation>
    </message>
    <message>
        <location line="+85"/>
        <source>Remove dynamic property &apos;%1&apos; from &apos;%2&apos;</source>
        <translation>A(z) „%1” dinamikus tulajdonság eltávolítása a(z) „%2” objektumból</translation>
    </message>
    <message numerus="yes">
        <location line="+5"/>
        <source>Remove dynamic property &apos;%1&apos; from %n objects</source>
        <translation>
            <numerusform>A(z) „%1” dinamikus tulajdonság eltávolítása %n objektumból</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/signalslotdialog.cpp" line="+191"/>
        <source>Change signals/slots</source>
        <translation>Jelzések és tárolóhelyek módosítása</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/components/signalsloteditor/signalsloteditor.cpp" line="+195"/>
        <location line="+259"/>
        <source>Change signal</source>
        <translation>Jelzés módosítása</translation>
    </message>
    <message>
        <location line="-257"/>
        <location line="+269"/>
        <source>Change slot</source>
        <translation>Tárolóhely módosítása</translation>
    </message>
    <message>
        <location line="-221"/>
        <source>Change signal-slot connection</source>
        <translation>Jelzés-tárolóhely kapcsolat módosítása</translation>
    </message>
    <message>
        <location line="+235"/>
        <source>Change sender</source>
        <translation>Küldő módosítása</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Change receiver</source>
        <translation>Fogadó módosítása</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/button_taskmenu.cpp" line="-61"/>
        <source>Create button group</source>
        <translation>Gombcsoport létrehozása</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Break button group</source>
        <translation>Gombcsoport feltörése</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Break button group &apos;%1&apos;</source>
        <translation>A(z) „%1” gombcsoport feltörése</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Add buttons to group</source>
        <translation>Gombok hozzáadása a csoporthoz</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Remove buttons from group</source>
        <translation>Gombok eltávolítása a csoportból</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Remove &apos;%1&apos; from &apos;%2&apos;</source>
        <extracomment>Command description for removing buttons from a QButtonGroup</extracomment>
        <translation>„%1” eltávolítása innen: „%2”</translation>
    </message>
</context>
<context>
    <name>ConnectDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/signalsloteditor/connectdialog.ui"/>
        <source>Configure Connection</source>
        <translation>Kapcsolatok beállítása</translation>
    </message>
    <message>
        <location/>
        <source>GroupBox</source>
        <translation>Csoporttároló doboz</translation>
    </message>
    <message>
        <location/>
        <source>Edit...</source>
        <translation>Szerkesztés…</translation>
    </message>
    <message>
        <location/>
        <source>Show signals and slots inherited from QWidget</source>
        <translation>A QWidget objektumból származtatott jelzések és tárolóhelyek megjelenítése</translation>
    </message>
</context>
<context>
    <name>ConnectionDelegate</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/signalsloteditor/signalsloteditorwindow.cpp" line="+624"/>
        <source>&lt;object&gt;</source>
        <translation>&lt;objektum&gt;</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>&lt;signal&gt;</source>
        <translation>&lt;jelzés&gt;</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&lt;slot&gt;</source>
        <translation>&lt;tárolóhely&gt;</translation>
    </message>
</context>
<context>
    <name>DPI_Chooser</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/dpi_chooser.cpp" line="+55"/>
        <source>Standard (96 x 96)</source>
        <extracomment>Embedded device standard screen resolution</extracomment>
        <translation>Szabványos (96 x 96)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Greenphone (179 x 185)</source>
        <extracomment>Embedded device screen resolution</extracomment>
        <translation>Greenphone (179 x 185)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>High (192 x 192)</source>
        <extracomment>Embedded device high definition screen resolution</extracomment>
        <translation>Nagy (192 x 192)</translation>
    </message>
</context>
<context>
    <name>Designer</name>
    <message>
        <source>Unable to launch %1.</source>
        <translation type="vanished">A(z) %1 nem indítható el.</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_utils.cpp" line="+734"/>
        <source>Unable to launch %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 timed out.</source>
        <translation>A(z) %1 túllépte az időkorlátot.</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qsimpleresource.cpp" line="+179"/>
        <source>Custom Widgets</source>
        <translation>Egyéni felületi elemek</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Promoted Widgets</source>
        <translation>Előléptetett felület elemek</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/qdesigner_resource.cpp" line="+469"/>
        <source>Qt Designer</source>
        <translation>Qt Tervező</translation>
    </message>
    <message>
        <location line="+166"/>
        <source>This file cannot be read because the extra info extension failed to load.</source>
        <translation>Ez a fájl nem olvasható, mert a további információs kiterjesztést nem sikerült betölteni.</translation>
    </message>
</context>
<context>
    <name>DesignerMetaEnum</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_utils.cpp" line="-579"/>
        <source>%1 is not a valid enumeration value of &apos;%2&apos;.</source>
        <translation>A(z) %1 nem a(z) „%2” felsorolt típus érvényes értéke.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&apos;%1&apos; could not be converted to an enumeration value of type &apos;%2&apos;.</source>
        <translation>A(z) „%1” átalakítása nem sikerült egy „%2” típusú felsorolásértékre.</translation>
    </message>
</context>
<context>
    <name>DesignerMetaFlags</name>
    <message>
        <location line="+77"/>
        <source>&apos;%1&apos; could not be converted to a flag value of type &apos;%2&apos;.</source>
        <translation>A(z) „%1” átalakítása nem sikerült egy „%2” típusú jelzőértékre.</translation>
    </message>
</context>
<context>
    <name>DeviceProfile</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/deviceprofile.cpp" line="+374"/>
        <source>&apos;%1&apos; is not a number.</source>
        <extracomment>Reading a number for an embedded device profile</extracomment>
        <translation>A(z) „%1” nem szám.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>An invalid tag &lt;%1&gt; was encountered.</source>
        <translation>Egy érvénytelen &lt;%1&gt; címke fordult elő.</translation>
    </message>
</context>
<context>
    <name>DeviceProfileDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/deviceprofiledialog.ui"/>
        <source>&amp;Family</source>
        <translation>Betűcs&amp;alád</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Point Size</source>
        <translation>&amp;Pontméret</translation>
    </message>
    <message>
        <location/>
        <source>Style</source>
        <translation>Stílus</translation>
    </message>
    <message>
        <location/>
        <source>Device DPI</source>
        <translation>Eszköz DPI</translation>
    </message>
    <message>
        <location/>
        <source>Name</source>
        <translation>Név</translation>
    </message>
</context>
<context>
    <name>DeviceSkin</name>
    <message>
        <location filename="../../qttools/src/shared/deviceskin/deviceskin.cpp" line="+77"/>
        <source>The image file &apos;%1&apos; could not be loaded.</source>
        <translation>Nem sikerült betölteni a(z) „%1” képfájlt.</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>The skin directory &apos;%1&apos; does not contain a configuration file.</source>
        <translation>A(z) „%1” felszínkönyvtár nem tartalmaz beállítófájlt.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The skin configuration file &apos;%1&apos; could not be opened.</source>
        <translation>A(z) „%1” felszínbeállító-fájlt nem sikerült megnyitni.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The skin configuration file &apos;%1&apos; could not be read: %2</source>
        <translation>A(z) „%1” felszínbeállító-fájlt nem sikerült beolvasni: %2</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Syntax error: %1</source>
        <translation>Szintaktikai hiba: %1</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>The skin &quot;up&quot; image file &apos;%1&apos; does not exist.</source>
        <translation>A felszín „fel” képfájlja („%1”) nem létezik.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The skin &quot;down&quot; image file &apos;%1&apos; does not exist.</source>
        <translation>A felszín „le” képfájlja („%1”) nem létezik.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>The skin &quot;closed&quot; image file &apos;%1&apos; does not exist.</source>
        <translation>A felszín „lezárt” képfájlja („%1”) nem létezik.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>The skin cursor image file &apos;%1&apos; does not exist.</source>
        <translation>A felszín kurzor képfájlja („%1”) nem létezik.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Syntax error in area definition: %1</source>
        <translation>Szintaktikai hiba a terület-meghatározásban: %1</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Mismatch in number of areas, expected %1, got %2.</source>
        <translation>Eltérés a területek számában: a várt érték %1, a kapott %2.</translation>
    </message>
</context>
<context>
    <name>EmbeddedOptionsControl</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/embeddedoptionspage.cpp" line="+298"/>
        <source>&lt;html&gt;&lt;table&gt;&lt;tr&gt;&lt;td&gt;&lt;b&gt;Font&lt;/b&gt;&lt;/td&gt;&lt;td&gt;%1, %2&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;b&gt;Style&lt;/b&gt;&lt;/td&gt;&lt;td&gt;%3&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;b&gt;Resolution&lt;/b&gt;&lt;/td&gt;&lt;td&gt;%4 x %5&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/html&gt;</source>
        <extracomment>Format embedded device profile description</extracomment>
        <translation>&lt;html&gt;&lt;table&gt;&lt;tr&gt;&lt;td&gt;&lt;b&gt;Betűkészlet&lt;/b&gt;&lt;/td&gt;&lt;td&gt;%1, %2&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;b&gt;Stílus&lt;/b&gt;&lt;/td&gt;&lt;td&gt;%3&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;b&gt;Felbontás&lt;/b&gt;&lt;/td&gt;&lt;td&gt;%4 x %5&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>EmbeddedOptionsPage</name>
    <message>
        <location line="+103"/>
        <source>Embedded Design</source>
        <extracomment>Tab in preferences dialog</extracomment>
        <translation>Beágyazott terv</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Device Profiles</source>
        <extracomment>EmbeddedOptionsControl group box&quot;</extracomment>
        <translation>Eszközprofilok</translation>
    </message>
</context>
<context>
    <name>FontPanel</name>
    <message>
        <location filename="../../qttools/src/shared/fontpanel/fontpanel.cpp" line="+61"/>
        <source>Font</source>
        <translation>Betűkészlet</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Writing system</source>
        <translation>Írás&amp;rendszer</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Family</source>
        <translation>Betűcs&amp;alád</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Style</source>
        <translation>&amp;Stílus</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Point size</source>
        <translation>&amp;Pontméret</translation>
    </message>
</context>
<context>
    <name>FontPropertyManager</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/propertyeditor/fontpropertymanager.cpp" line="+49"/>
        <source>PreferDefault</source>
        <translation>Alapértelmezés előnyben részesítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>NoAntialias</source>
        <translation>Nincs élsimítás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PreferAntialias</source>
        <translation>Élsimítás előnyben részesítése</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>Antialiasing</source>
        <translation>Élsimítás</translation>
    </message>
</context>
<context>
    <name>FormBuilder</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/uilib/formbuilderextra.cpp" line="+414"/>
        <source>Invalid stretch value for &apos;%1&apos;: &apos;%2&apos;</source>
        <extracomment>Parsing layout stretch values</extracomment>
        <translation>Érvénytelen nyújtás a(z) „%1” objektumnál: „%2”</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Invalid minimum size for &apos;%1&apos;: &apos;%2&apos;</source>
        <extracomment>Parsing grid layout minimum size values</extracomment>
        <translation>Érvénytelen minimális méret a(z) „%1” objektumnál: „%2”</translation>
    </message>
</context>
<context>
    <name>FormEditorOptionsPage</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/formeditor_optionspage.cpp" line="+76"/>
        <source>%1 %</source>
        <extracomment>Zoom percentage</extracomment>
        <translation>%1 %</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Preview Zoom</source>
        <translation>Nagyítás előnézete</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Default Zoom</source>
        <translation>Alapértelmezett nagyítás</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Forms</source>
        <extracomment>Tab in preferences dialog</extracomment>
        <translation>Űrlapok</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Default Grid</source>
        <translation>Alapértelmezett rács</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Object Naming Convention</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Naming convention used for generating action object names from their text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Camel Case</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Underscore</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FormLayoutRowDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/formlayoutrowdialog.ui"/>
        <source>Add Form Layout Row</source>
        <translation>Új űrlapelrendezés-sor hozzáadása</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Label text:</source>
        <translation>&amp;Címkeszöveg:</translation>
    </message>
    <message>
        <location/>
        <source>Field &amp;type:</source>
        <translation>Mező&amp;típus:</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Field name:</source>
        <translation>&amp;Mezőnév:</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Buddy:</source>
        <translation>&amp;Partner:</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Row:</source>
        <translation>&amp;Sor:</translation>
    </message>
    <message>
        <location/>
        <source>Label &amp;name:</source>
        <translation>Címke&amp;név:</translation>
    </message>
</context>
<context>
    <name>FormWindow</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/formwindow.cpp" line="+1757"/>
        <source>Unexpected element &lt;%1&gt;</source>
        <translation>Váratlan &lt;%1&gt; elem</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Error while pasting clipboard contents at line %1, column %2: %3</source>
        <translation>Hiba a vágólap tartalmának beillesztésekor a(z) %1. sor %2. oszlopában: %3</translation>
    </message>
</context>
<context>
    <name>FormWindowSettings</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/formwindowsettings.ui"/>
        <source>Form Settings</source>
        <translation>Űrlap beállítások</translation>
    </message>
    <message>
        <location/>
        <source>Layout &amp;Default</source>
        <translation>&amp;Alapértelmezett elrendezése</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Spacing:</source>
        <translation>&amp;Térköz:</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Margin:</source>
        <translation>&amp;Margó:</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Layout Function</source>
        <translation>&amp;Elrendezési függvény</translation>
    </message>
    <message>
        <location/>
        <source>Ma&amp;rgin:</source>
        <translation>Ma&amp;rgó:</translation>
    </message>
    <message>
        <location/>
        <source>Spa&amp;cing:</source>
        <translation>Tér&amp;köz:</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Pixmap Function</source>
        <translation>&amp;Kép függvény</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Include Hints</source>
        <translation>&amp;Felvételi tippek</translation>
    </message>
    <message>
        <location/>
        <source>Grid</source>
        <translation>Rács</translation>
    </message>
    <message>
        <location/>
        <source>Embedded Design</source>
        <translation>Beágyazott terv</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Author</source>
        <translation>&amp;Szerző</translation>
    </message>
    <message>
        <location/>
        <source>Translations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>ID-based</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Connections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Connect slots by name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IconSelector</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/iconselector.cpp" line="+306"/>
        <source>The pixmap file &apos;%1&apos; cannot be read.</source>
        <translation>A(z) „%1” képfájl nem olvasható.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The file &apos;%1&apos; does not appear to be a valid pixmap file: %2</source>
        <translation>A(z) „%1” fájl nem tűnik érvényes képfájlnak: %2</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>The file &apos;%1&apos; could not be read: %2</source>
        <translation>A(z) %1 fájlt nem sikerült beolvasni: %2</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>All Pixmaps (</source>
        <translation>Összes képfájl (</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Choose a Pixmap</source>
        <translation>Válasszon egy képfájlt</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Pixmap Read Error</source>
        <translation>Képolvasási hiba</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>...</source>
        <translation>…</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Normal Off</source>
        <translation>Normál ki</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Normal On</source>
        <translation>Normál be</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Disabled Off</source>
        <translation>Letiltott ki</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Disabled On</source>
        <translation>Letiltott be</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Active Off</source>
        <translation>Aktív ki</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Active On</source>
        <translation>Aktív be</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Selected Off</source>
        <translation>Kijelölt ki</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Selected On</source>
        <translation>Kijelölt be</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Choose Resource...</source>
        <translation>Erőforrás kiválasztása…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Choose File...</source>
        <translation>Fájl kiválasztása…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Reset</source>
        <translation>Visszaállítás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Reset All</source>
        <translation>Összes visszaállítása</translation>
    </message>
</context>
<context>
    <name>ItemPropertyBrowser</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/itemlisteditor.cpp" line="-54"/>
        <source>XX Icon Selected off</source>
        <extracomment>Sample string to determinate the width for the first column of the list item property browser</extracomment>
        <translation>XX ikon kijelölese ki</translation>
    </message>
</context>
<context>
    <name>MainWindowBase</name>
    <message>
        <location filename="../../qttools/src/designer/src/designer/mainwindow.cpp" line="+108"/>
        <source>Main</source>
        <extracomment>Not currently used (main tool bar)</extracomment>
        <translation>Fő</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>File</source>
        <translation>Fájl</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit</source>
        <translation>Szerkesztés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tools</source>
        <translation>Eszközök</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Form</source>
        <translation>Űrlap</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Qt Designer</source>
        <translation>Qt Tervező</translation>
    </message>
</context>
<context>
    <name>NewForm</name>
    <message>
        <location filename="../../qttools/src/designer/src/designer/newform.cpp" line="+61"/>
        <source>Show this Dialog on Startup</source>
        <translation>Jelenlen meg ez az ablak indításkor</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>C&amp;reate</source>
        <translation>&amp;Létrehozás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Recent</source>
        <translation>Legutóbbi</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>New Form</source>
        <translation>Új űrlap</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>&amp;Close</source>
        <translation>&amp;Bezárás</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Open...</source>
        <translation>&amp;Megnyitás…</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Recent Forms</source>
        <translation>&amp;Legutóbbi űrlapok</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Read error</source>
        <translation>Olvasási hiba</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>A temporary form file could not be created in %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The temporary form file %1 could not be written: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A temporary form file could not be created in %1.</source>
        <translation type="vanished">Egy átmeneti űrlapfájlt nem sikerült létrehozni ebben: %1.</translation>
    </message>
    <message>
        <source>The temporary form file %1 could not be written.</source>
        <translation type="vanished">A(z) %1 átmeneti űrlapfájlt nem sikerült kiírni.</translation>
    </message>
</context>
<context>
    <name>ObjectInspectorModel</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/objectinspector/objectinspectormodel.cpp" line="+332"/>
        <source>Object</source>
        <translation>Objektum</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Class</source>
        <translation>Osztály</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>separator</source>
        <translation>elválasztó</translation>
    </message>
    <message>
        <location line="+98"/>
        <source>&lt;noname&gt;</source>
        <translation>&lt;névtelen&gt;</translation>
    </message>
</context>
<context>
    <name>ObjectNameDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_taskmenu.cpp" line="+116"/>
        <source>Change Object Name</source>
        <translation>Objektumnév módosítása</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Object Name</source>
        <translation>Objektumnév</translation>
    </message>
</context>
<context>
    <name>PluginDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/plugindialog.ui"/>
        <source>Plugin Information</source>
        <translation>Bővítményinformációk</translation>
    </message>
    <message>
        <location/>
        <source>1</source>
        <translation>1</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/designer/preferencesdialog.ui"/>
        <source>Preferences</source>
        <translation>Beállítások</translation>
    </message>
</context>
<context>
    <name>PreviewConfigurationWidget</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/previewconfigurationwidget.ui"/>
        <source>Form</source>
        <translation>Űrlap</translation>
    </message>
    <message>
        <location/>
        <source>Print/Preview Configuration</source>
        <translation>Nyomtatási és előnézeti beállítások</translation>
    </message>
    <message>
        <location/>
        <source>Style</source>
        <translation>Stílus</translation>
    </message>
    <message>
        <location/>
        <source>Style sheet</source>
        <translation>Stíluslap</translation>
    </message>
    <message>
        <location/>
        <source>...</source>
        <translation>…</translation>
    </message>
    <message>
        <location/>
        <source>Device skin</source>
        <translation>Eszközfelszín</translation>
    </message>
</context>
<context>
    <name>PromotionModel</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/promotionmodel.cpp" line="+98"/>
        <source>Not used</source>
        <extracomment>Usage of promoted widgets</extracomment>
        <translation>Nem használt</translation>
    </message>
</context>
<context>
    <name>QAbstractFormBuilder</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/uilib/formbuilderextra.cpp" line="-361"/>
        <source>An error has occurred while reading the UI file at line %1, column %2: %3</source>
        <translation>Hiba történt a felhasználói felület fájl olvasása közben a(z) %1. sor %2. oszlopában: %3</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>This file was created using Designer from Qt-%1 and cannot be read.</source>
        <translation>Ezt a fájlt a Qt-%1 verziójában lévő Tervező használatával hozták létre, és nem lehet beolvasni.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>This file cannot be read because it was created using %1.</source>
        <translation>Ezt a fájlt nem lehet beolvasni, mert a(z) %1 használatával hozták létre.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Invalid UI file: The root element &lt;ui&gt; is missing.</source>
        <translation>Érvénytelen felhasználói felület fájl: az &lt;ui&gt; gyökérelem hiányzik.</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Invalid UI file</source>
        <translation>Érvénytelen felhasználói felület fájl</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/lib/uilib/abstractformbuilder.cpp" line="+289"/>
        <source>The creation of a widget of the class &apos;%1&apos; failed.</source>
        <translation>A(z) „%1” osztályú felületi elem létrehozása sikertelen.</translation>
    </message>
    <message>
        <location line="+286"/>
        <source>Attempt to add child that is not of class QWizardPage to QWizard.</source>
        <translation>Egy olyan gyermek hozzáadásának kísérlete a QWizard objektumhoz, amely nem QWizardPage osztályú.</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Attempt to add a layout to a widget &apos;%1&apos; (%2) which already has a layout of non-box type %3.
This indicates an inconsistency in the ui-file.</source>
        <translation>Egy elrendezés hozzáadásának kísérlet egy olyan „%1” (%2) felületi elemhez, amelynek már van egy %3 nem doboz típusú elrendezése.
Ez következetlenséget jelez a felhasználói felület fájlban.</translation>
    </message>
    <message>
        <location line="+212"/>
        <source>Empty widget item in %1 &apos;%2&apos;.</source>
        <translation>Üres felületi elem a következőben: %1 „%2”.</translation>
    </message>
    <message>
        <location line="+598"/>
        <source>Flags property are not supported yet.</source>
        <translation>A jelző tulajdonságok még nem támogatottak.</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>While applying tab stops: The widget &apos;%1&apos; could not be found.</source>
        <translation>A bejárási pontok alkalmazásakor: a(z) „%1” felületi elem nem található.</translation>
    </message>
    <message>
        <location line="+741"/>
        <source>Invalid QButtonGroup reference &apos;%1&apos; referenced by &apos;%2&apos;.</source>
        <translation>A(z) „%2” érvénytelen „%1” QButtonGroup hivatkozásra hivatkozik.</translation>
    </message>
</context>
<context>
    <name>QAxWidgetPlugin</name>
    <message>
        <location filename="../../qttools/src/designer/src/plugins/activeqt/qaxwidgetplugin.cpp" line="+61"/>
        <source>ActiveX control</source>
        <translation>ActiveX-vezérlő</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>ActiveX control widget</source>
        <translation>ActiveX-vezérlő felületi elem</translation>
    </message>
</context>
<context>
    <name>QAxWidgetTaskMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/plugins/activeqt/qaxwidgettaskmenu.cpp" line="+108"/>
        <source>Set Control</source>
        <translation>Vezérlő beállítása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Reset Control</source>
        <translation>Vezérlő visszaállítása</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Licensed Control</source>
        <translation>Licencelt vezérlő</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The control requires a design-time license</source>
        <translation>A vezérlő egy tervezésidejű licencet igényel</translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_promotion.cpp" line="+69"/>
        <source>%1 is not a promoted class.</source>
        <translation>A(z) %1 nem egy előléptetett osztály.</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>The base class %1 is invalid.</source>
        <translation>A(z) %1 alaposztály érvénytelen.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The class %1 already exists.</source>
        <translation>A(z) %1 osztály már létezik.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Promoted Widgets</source>
        <translation>Előléptetett felület elemek</translation>
    </message>
    <message>
        <location line="+126"/>
        <source>The class %1 cannot be removed</source>
        <translation>A(z) %1 osztályt nem lehet eltávolítani</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The class %1 cannot be removed because it is still referenced.</source>
        <translation>A(z) %1 osztályt nem lehet eltávolítani, mert még hivatkoznak rá.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>The class %1 cannot be renamed</source>
        <translation>A(z) %1 osztályt nem lehet átnevezni</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The class %1 cannot be renamed to an empty name.</source>
        <translation>A(z) %1 osztályt nem lehet átnevezni üres névre.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>There is already a class named %1.</source>
        <translation>Már van egy %1 nevű osztály.</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Cannot set an empty include file.</source>
        <translation>Nem lehet beállítani egy üres felvételi fájlt.</translation>
    </message>
</context>
<context>
    <name>QDesigner</name>
    <message>
        <location filename="../../qttools/src/designer/src/designer/qdesigner.cpp" line="+126"/>
        <source>%1 - warning</source>
        <translation>%1 – figyelmeztetés</translation>
    </message>
</context>
<context>
    <name>QDesignerActions</name>
    <message>
        <location filename="../../qttools/src/designer/src/designer/qdesigner_actions.cpp" line="+131"/>
        <source>Saved %1.</source>
        <translation>A(z) %1 elmentve.</translation>
    </message>
    <message>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation type="vanished">A(z) %1 már létezik.
Szeretné lecserélni?</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Edit Widgets</source>
        <translation>Felületi elemek szerkesztése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;New...</source>
        <translation>Ú&amp;j…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Open...</source>
        <translation>&amp;Megnyitás…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Save</source>
        <translation>Menté&amp;s</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save &amp;As...</source>
        <translation>Mentés má&amp;sként…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save A&amp;ll</source>
        <translation>&amp;Összes mentése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save As &amp;Template...</source>
        <translation>Mentés s&amp;ablonként…</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+877"/>
        <source>&amp;Close</source>
        <translation>&amp;Bezárás</translation>
    </message>
    <message>
        <location line="-876"/>
        <source>Save &amp;Image...</source>
        <translation>Ké&amp;p mentése…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Print...</source>
        <translation>&amp;Nyomtatás…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Quit</source>
        <translation>&amp;Kilépés</translation>
    </message>
    <message>
        <source>View &amp;Code...</source>
        <translation type="vanished">&amp;Kód megtekintése…</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Minimize</source>
        <translation>&amp;Kis méret</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Bring All to Front</source>
        <translation>Összes előtérbe hozása</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Preferences...</source>
        <translation>Beállítások…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Additional Fonts...</source>
        <translation>További betűkészletek…</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>ALT+CTRL+S</source>
        <translation>ALT+CTRL+S</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>CTRL+SHIFT+S</source>
        <translation>CTRL+SHIFT+S</translation>
    </message>
    <message>
        <location line="+127"/>
        <source>CTRL+R</source>
        <translation>CTRL+R</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>CTRL+M</source>
        <translation>CTRL+M</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Qt Designer &amp;Help</source>
        <translation>Qt Tervező &amp;súgó</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Current Widget Help</source>
        <translation>A jelenlegi felületi elem súgója</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>About Plugins</source>
        <translation>Bővítmények névjegye</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+545"/>
        <source>About Qt Designer</source>
        <translation>A Qt Tervező névjegye</translation>
    </message>
    <message>
        <location line="-539"/>
        <source>About Qt</source>
        <translation>A Qt névjegye</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Clear &amp;Menu</source>
        <translation>&amp;Menü törlése</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Recent Forms</source>
        <translation>&amp;Legutóbbi űrlapok</translation>
    </message>
    <message>
        <location line="+77"/>
        <location line="+185"/>
        <source>Open Form</source>
        <translation>Űrlap megnyitása</translation>
    </message>
    <message>
        <location line="-644"/>
        <source>Designer UI files (*.%1);;All Files (*)</source>
        <translation>Tervező felületi elem fájlok (*.%1);;Összes fájl (*)</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Save Form As</source>
        <translation>Űrlap mentése másként</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>View &amp;C++ Code...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>View &amp;Python Code...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+550"/>
        <source>Designer</source>
        <translation>Tervező</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Feature not implemented yet!</source>
        <translation>A funkció még nincs megvalósítva!</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Code generation failed</source>
        <translation>A kódelőállítás sikertelen</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Read error</source>
        <translation>Olvasási hiba</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1
Do you want to update the file location or generate a new form?</source>
        <translation>%1
Frissíteni szeretné a fájl helyét, vagy előállít egy új űrlapot?</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Update</source>
        <translation>&amp;Frissítés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;New Form</source>
        <translation>Ú&amp;j űrlap</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Qt Designer</source>
        <translation>Qt Tervező</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Save Form?</source>
        <translation>Menti az űrlapot?</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Could not open file</source>
        <translation>Nem sikerült megnyitni a fájlt</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The file %1 could not be opened.
Reason: %2
Would you like to retry or select a different file?</source>
        <translation>A(z) %1 fájlt nem sikerült megnyitni.
Ok: %2
Szeretné megpróbálni újra, vagy választ egy másik fájlt?</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Select New File</source>
        <translation>Másik fájl választása</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Save Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Could not write file</source>
        <translation>Nem sikerült írni a fájlt</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>It was not possible to write the file %1 to disk.
Reason: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>It was not possible to write the entire file %1 to disk.
Reason:%2
Would you like to retry?</source>
        <translation type="vanished">Nem volt lehetséges a teljes %1 fájlt kiírni a lemezre.
Ok: %2
Szeretné megpróbálni újra?</translation>
    </message>
    <message>
        <location line="+144"/>
        <location line="+32"/>
        <source>Assistant</source>
        <translation>Asszisztens</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Close Preview</source>
        <translation>Előnézet &amp;bezárása</translation>
    </message>
    <message>
        <location line="+40"/>
        <location line="+17"/>
        <source>The backup file %1 could not be written.</source>
        <translation>A(z) %1 biztonsági mentés fájlt nem sikerült kiírni.</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>The backup directory %1 could not be created.</source>
        <translation>A(z) %1 biztonsági mentési könyvtárat nem sikerül létrehozni.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The temporary backup directory %1 could not be created.</source>
        <translation>A(z) %1 átmeneti biztonsági másolat könyvtárat nem sikerült létrehozni.</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Preview failed</source>
        <translation>Az előnézet sikertelen</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Image files (*.%1)</source>
        <translation>Képfájlok (*.%1)</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+22"/>
        <source>Save Image</source>
        <translation>Kép mentése</translation>
    </message>
    <message>
        <location line="-4"/>
        <source>Saved image %1.</source>
        <translation>A(z) %1 kép elmentve.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The file %1 could not be written.</source>
        <translation>A(z) %1 fájlt nem sikerült kiírni.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Please close all forms to enable the loading of additional fonts.</source>
        <translation>Zárja be az összes űrlapot a további betűkészletek betöltésének engedélyezéséhez.</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Printed %1.</source>
        <translation>A(z) %1 kinyomtatva.</translation>
    </message>
</context>
<context>
    <name>QDesignerAppearanceOptionsPage</name>
    <message>
        <location filename="../../qttools/src/designer/src/designer/qdesigner_appearanceoptions.cpp" line="+119"/>
        <source>Appearance</source>
        <extracomment>Tab in preferences dialog</extracomment>
        <translation>Megjelenés</translation>
    </message>
</context>
<context>
    <name>QDesignerAppearanceOptionsWidget</name>
    <message>
        <location line="-53"/>
        <source>Docked Window</source>
        <translation>Dokkolt ablak</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Multiple Top-Level Windows</source>
        <translation>Több felsőszintű ablak</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Toolwindow Font</source>
        <translation>Eszközablak betűkészlete</translation>
    </message>
</context>
<context>
    <name>QDesignerAxWidget</name>
    <message>
        <location filename="../../qttools/src/designer/src/plugins/activeqt/qaxwidgettaskmenu.cpp" line="-72"/>
        <source>Reset control</source>
        <translation>Vezérlő visszaállítása</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Set control</source>
        <translation>Vezérlő beállítása</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/plugins/activeqt/qdesigneraxwidget.cpp" line="+164"/>
        <source>Control loaded</source>
        <translation>Vezérlő betöltve</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>A COM exception occurred when executing a meta call of type %1, index %2 of &quot;%3&quot;.</source>
        <translation>Egy COM kivétel történt a(z) %1 típusú metahíváskor a(z) „%3” objektum %2 indexénél.</translation>
    </message>
</context>
<context>
    <name>QDesignerFormBuilder</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_formbuilder.cpp" line="+336"/>
        <source>The preview failed to build.</source>
        <translation>Az előnézet összeállítása sikertelen.</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Designer</source>
        <translation>Tervező</translation>
    </message>
</context>
<context>
    <name>QDesignerFormWindow</name>
    <message>
        <location filename="../../qttools/src/designer/src/designer/qdesigner_formwindow.cpp" line="+208"/>
        <source>%1 - %2[*]</source>
        <translation>%1 – %2[*]</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Save Form?</source>
        <translation>Menti az űrlapot?</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Do you want to save the changes to this document before closing?</source>
        <translation>Szeretné menteni a változtatásokat ebbe a dokumentumba a bezárás előtt?</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>If you don&apos;t save, your changes will be lost.</source>
        <translation>Ha nem menti, akkor a változtatások el fognak veszni.</translation>
    </message>
</context>
<context>
    <name>QDesignerMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_menu.cpp" line="-1167"/>
        <source>Type Here</source>
        <translation>Írjon ide</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Add Separator</source>
        <translation>Elválasztó hozzáadása</translation>
    </message>
    <message>
        <location line="+370"/>
        <source>Insert separator</source>
        <translation>Elválasztó beszúrása</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Remove separator</source>
        <translation>Elválasztó eltávolítása</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Remove action &apos;%1&apos;</source>
        <translation>„%1” művelet eltávolítása</translation>
    </message>
    <message>
        <location line="+25"/>
        <location line="+637"/>
        <source>Add separator</source>
        <translation>Elválasztó hozzáadása</translation>
    </message>
    <message>
        <location line="-340"/>
        <source>Insert action</source>
        <translation>Művelet beszúrása</translation>
    </message>
</context>
<context>
    <name>QDesignerMenuBar</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_menubar.cpp" line="-374"/>
        <source>Type Here</source>
        <translation>Írjon ide</translation>
    </message>
    <message>
        <location line="+297"/>
        <source>Remove Menu &apos;%1&apos;</source>
        <translation>„%1” menü eltávolítása</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Remove Menu Bar</source>
        <translation>Menüsor eltávolítása</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Menu</source>
        <translation>Menü</translation>
    </message>
</context>
<context>
    <name>QDesignerPluginManager</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/pluginmanager.cpp" line="+265"/>
        <source>An XML error was encountered when parsing the XML of the custom widget %1: %2</source>
        <translation>XML hiba történt a(z) %1 egyéni felületi elem XML kódjának feldolgozásakor: %2</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>A required attribute (&apos;%1&apos;) is missing.</source>
        <translation>Egy szükséges attribútum („%1”) hiányzik.</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>&apos;%1&apos; is not a valid string property specification.</source>
        <translation>A(z) „%1” nem érvényes karakterlánc-tulajdonság specifikáció.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>An invalid property specification (&apos;%1&apos;) was encountered. Supported types: %2</source>
        <translation>Egy érvénytelen tulajdonság specifikáció („%1”) fordult elő. Támogatott típusok: %2</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>The XML of the custom widget %1 does not contain any of the elements &lt;widget&gt; or &lt;ui&gt;.</source>
        <translation>A(z) %1 felületi elem XML kódja nem tartalmaz sem &lt;widget&gt;, sem &lt;ui&gt; elemet.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>The class attribute for the class %1 is missing.</source>
        <translation>Az osztályattribútum hiányzik a(z) %1 osztálynál.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The class attribute for the class %1 does not match the class name %2.</source>
        <translation>Az osztályattribútum a(z) %1 osztálynál nem egyezik a(z) %2 osztálynévvel.</translation>
    </message>
</context>
<context>
    <name>QDesignerPropertySheet</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_propertysheet.cpp" line="+790"/>
        <source>Dynamic Properties</source>
        <translation>Dinamikus tulajdonságok</translation>
    </message>
</context>
<context>
    <name>QDesignerResource</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/qdesigner_resource.cpp" line="+445"/>
        <source>The layout type &apos;%1&apos; is not supported, defaulting to grid.</source>
        <translation>A(z) „%1” elrendezéstípus nem támogatott, visszatérés rácsra.</translation>
    </message>
    <message>
        <location line="+206"/>
        <source>The container extension of the widget &apos;%1&apos; (%2) returned a widget not managed by Designer &apos;%3&apos; (%4) when queried for page #%5.
Container pages should only be added by specifying them in XML returned by the domXml() method of the custom widget.</source>
        <translation>A(z) „%1” (%2) felületi elem konténerkiterjesztése egy olyan felületi elemet adott vissza a(z) %5. oldal lekérdezésekor, amelyet nem a(z) „%3” (%4) Tervező kezel.
Konténeroldalak csak az egyéni felületi elem domXml() metódusa által visszaadott XML-ben történő meghatározással adhatók hozzá.</translation>
    </message>
    <message>
        <location line="+507"/>
        <source>Unexpected element &lt;%1&gt;</source>
        <extracomment>Parsing clipboard contents</extracomment>
        <translation>Váratlan &lt;%1&gt; elem</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Error while pasting clipboard contents at line %1, column %2: %3</source>
        <extracomment>Parsing clipboard contents</extracomment>
        <translation>Hiba a vágólap tartalmának beillesztésekor a(z) %1. sor %2. oszlopában: %3</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Error while pasting clipboard contents: The root element &lt;ui&gt; is missing.</source>
        <extracomment>Parsing clipboard contents</extracomment>
        <translation>Hiba a vágólap tartalmának beillesztésekor: az &lt;ui&gt; gyökérelem hiányzik.</translation>
    </message>
</context>
<context>
    <name>QDesignerSharedSettings</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/shared_settings.cpp" line="+77"/>
        <source>The template path %1 could not be created.</source>
        <translation>A(z) %1 sablonútvonalat nem sikerült létrehozni.</translation>
    </message>
    <message>
        <location line="+198"/>
        <source>An error has been encountered while parsing device profile XML: %1</source>
        <translation>Hiba történt az eszközprofil XML feldolgozásakor: %1</translation>
    </message>
</context>
<context>
    <name>QDesignerTaskMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/selectsignaldialog.cpp" line="+143"/>
        <source>no signals available</source>
        <translation type="unfinished">nincsenek elérhető jelzések</translation>
    </message>
</context>
<context>
    <name>QDesignerToolWindow</name>
    <message>
        <location filename="../../qttools/src/designer/src/designer/qdesigner_toolwindow.cpp" line="+170"/>
        <source>Property Editor</source>
        <translation>Tulajdonság-szerkesztő</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>Action Editor</source>
        <translation>Műveletszerkesztő</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Object Inspector</source>
        <translation>Objektumvizsgáló</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Resource Browser</source>
        <translation>Erőforrás-tallózó</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Signal/Slot Editor</source>
        <translation>Jelzés és tárolóhely szerkesztő</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Widget Box</source>
        <translation>Felületi elem doboz</translation>
    </message>
</context>
<context>
    <name>QDesignerWorkbench</name>
    <message>
        <location filename="../../qttools/src/designer/src/designer/qdesigner_workbench.cpp" line="+184"/>
        <source>&amp;File</source>
        <translation>&amp;Fájl</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Edit</source>
        <translation>S&amp;zerkesztés</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>F&amp;orm</source>
        <translation>Ű&amp;rlap</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Preview in</source>
        <translation>Előnézet ebben</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;View</source>
        <translation>&amp;Nézet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Settings</source>
        <translation>&amp;Beállítások</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Window</source>
        <translation>&amp;Ablak</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Help</source>
        <translation>&amp;Súgó</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Toolbars</source>
        <translation>Eszköztárak</translation>
    </message>
    <message>
        <location line="+177"/>
        <source>Widget Box</source>
        <translation>Felületi elem doboz</translation>
    </message>
    <message>
        <location line="+299"/>
        <source>Save Forms?</source>
        <translation>Menti az űrlapokat?</translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>There are %n forms with unsaved changes. Do you want to review these changes before quitting?</source>
        <translation>
            <numerusform>%n mentetlen változtatással rendelkező űrlap van. Szeretné átnézni ezeket a változtatásokat a kilépés előtt?</numerusform>
        </translation>
    </message>
    <message>
        <location line="+3"/>
        <source>If you do not review your documents, all your changes will be lost.</source>
        <translation>Ha nem nézi át a dokumentumokat, akkor minden változtatás el fog veszni.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Discard Changes</source>
        <translation>Változtatások eldobása</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Review Changes</source>
        <translation>Változtatások átnézése</translation>
    </message>
    <message>
        <location line="+91"/>
        <source>Backup Information</source>
        <translation>Biztonsági mentés információk</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The last session of Designer was not terminated correctly. Backup files were left behind. Do you want to load them?</source>
        <translation>A Tervező utolsó munkamenete nem megfelelően fejeződött be. Biztonsági mentés fájlok maradtak utána. Szeretné betölteni ezeket?</translation>
    </message>
    <message>
        <location line="+106"/>
        <source>The file &lt;b&gt;%1&lt;/b&gt; could not be opened: %2</source>
        <translation>A(z) &lt;b&gt;%1&lt;/b&gt; fájlt nem sikerült megnyitni: %2</translation>
    </message>
</context>
<context>
    <name>QFormBuilder</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/uilib/formbuilder.cpp" line="+171"/>
        <source>An empty class name was passed on to %1 (object name: &apos;%2&apos;).</source>
        <extracomment>Empty class name passed to widget factory method</extracomment>
        <translation>Egy üres osztálynév lett átadva a(z) %1 metódusnak (objektumnév: „%2”).</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>QFormBuilder was unable to create a custom widget of the class &apos;%1&apos;; defaulting to base class &apos;%2&apos;.</source>
        <translation>A QFormBuilder nem tudott „%1” osztályú egyéni felületi elemet létrehozni. Visszatérés a(z) „%2” alaposztályra.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>QFormBuilder was unable to create a widget of the class &apos;%1&apos;.</source>
        <translation>A QFormBuilder nem tudott „%1” osztályú felületi elemet létrehozni.</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>The layout type `%1&apos; is not supported.</source>
        <translation>A(z) „%1” elrendezéstípus nem támogatott.</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/lib/uilib/properties.cpp" line="+115"/>
        <source>The set-type property %1 could not be read.</source>
        <translation>A(z) %1 halmaz típusú tulajdonságot nem sikerült beolvasni.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>The enumeration-type property %1 could not be read.</source>
        <translation>A(z) %1 felsorolás típusú tulajdonságot nem sikerült beolvasni.</translation>
    </message>
    <message>
        <location line="+193"/>
        <source>Reading properties of the type %1 is not supported yet.</source>
        <translation>A(z) %1 típusú tulajdonságok olvasása még nem támogatott.</translation>
    </message>
    <message>
        <location line="+266"/>
        <source>The property %1 could not be written. The type %2 is not supported yet.</source>
        <translation>A(z) %1 tulajdonságot nem sikerült kiírni. A(z) %2 típus még nem támogatott.</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/lib/uilib/properties_p.h" line="+141"/>
        <source>The enumeration-value &apos;%1&apos; is invalid. The default value &apos;%2&apos; will be used instead.</source>
        <translation>A(z) „%1” felsorolás érték érvénytelen. Helyette a(z) „%2” alapértelmezett érték lesz használva.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>The flag-value &apos;%1&apos; is invalid. Zero will be used instead.</source>
        <translation>A(z) „%1” jelzőérték érvénytelen. Helyette nulla lesz használva.</translation>
    </message>
</context>
<context>
    <name>QStackedWidgetEventFilter</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_stackedbox.cpp" line="+184"/>
        <source>Previous Page</source>
        <translation>Előző oldal</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Next Page</source>
        <translation>Következő oldal</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Before Current Page</source>
        <translation>A jelenlegi oldal elé</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>After Current Page</source>
        <translation>A jelenlegi oldal után</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Change Page Order...</source>
        <translation>Oldalsorrend módosítása…</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>Change Page Order</source>
        <translation>Oldalsorrend módosítása</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Page %1 of %2</source>
        <translation>%1/%2 oldal</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+4"/>
        <source>Insert Page</source>
        <translation>Oldal beszúrása</translation>
    </message>
</context>
<context>
    <name>QStackedWidgetPreviewEventFilter</name>
    <message>
        <location line="-156"/>
        <source>Go to previous page of %1 &apos;%2&apos; (%3/%4).</source>
        <translation>Ugrás a(z) %1 „%2” előző oldalára (%3/%4).</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Go to next page of %1 &apos;%2&apos; (%3/%4).</source>
        <translation>Ugrás a(z) %1 „%2” következő oldalára (%3/%4).</translation>
    </message>
</context>
<context>
    <name>QTabWidgetEventFilter</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_tabwidget.cpp" line="+78"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Before Current Page</source>
        <translation>A jelenlegi oldal elé</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>After Current Page</source>
        <translation>A jelenlegi oldal után</translation>
    </message>
    <message>
        <location line="+280"/>
        <source>Page %1 of %2</source>
        <translation>%1/%2 oldal</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+4"/>
        <source>Insert Page</source>
        <translation>Oldal beszúrása</translation>
    </message>
</context>
<context>
    <name>QToolBoxHelper</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_toolbox.cpp" line="+50"/>
        <source>Delete Page</source>
        <translation>Oldal törlése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Before Current Page</source>
        <translation>A jelenlegi oldal elé</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>After Current Page</source>
        <translation>A jelenlegi oldal után</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Change Page Order...</source>
        <translation>Oldalsorrend módosítása…</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Change Page Order</source>
        <translation>Oldalsorrend módosítása</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Page %1 of %2</source>
        <translation>%1/%2 oldal</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Insert Page</source>
        <translation>Oldal beszúrása</translation>
    </message>
</context>
<context>
    <name>QtBoolEdit</name>
    <message>
        <location filename="../../qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="+253"/>
        <location line="+10"/>
        <location line="+25"/>
        <source>True</source>
        <translation>Igaz</translation>
    </message>
    <message>
        <location line="-25"/>
        <location line="+25"/>
        <source>False</source>
        <translation>Hamis</translation>
    </message>
</context>
<context>
    <name>QtBoolPropertyManager</name>
    <message>
        <location filename="../../qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="+1484"/>
        <source>True</source>
        <translation>Igaz</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>False</source>
        <translation>Hamis</translation>
    </message>
</context>
<context>
    <name>QtCharEdit</name>
    <message>
        <location filename="../../qttools/src/shared/qtpropertybrowser/qteditorfactory.cpp" line="+1558"/>
        <source>Clear Char</source>
        <translation>Karakter törlése</translation>
    </message>
</context>
<context>
    <name>QtColorEditWidget</name>
    <message>
        <location line="+601"/>
        <source>...</source>
        <translation>…</translation>
    </message>
</context>
<context>
    <name>QtColorPropertyManager</name>
    <message>
        <location filename="../../qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="+4719"/>
        <source>Red</source>
        <translation>Vörös</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Green</source>
        <translation>Zöld</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Blue</source>
        <translation>Kék</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Alpha</source>
        <translation>Alfa</translation>
    </message>
</context>
<context>
    <name>QtCursorDatabase</name>
    <message>
        <location filename="../../qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="-234"/>
        <source>Arrow</source>
        <translation>Nyíl</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Up Arrow</source>
        <translation>Felfelé nyíl</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cross</source>
        <translation>Kereszt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Wait</source>
        <translation>Várakozás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>IBeam</source>
        <translation>Hiányjel</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Size Vertical</source>
        <translation>Függőleges méretezés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Size Horizontal</source>
        <translation>Vízszintes méretezés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Size Backslash</source>
        <translation>Fordított perjel méretezés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Size Slash</source>
        <translation>Perjel méretezés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Size All</source>
        <translation>Összes méretezése</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Blank</source>
        <translation>Üres</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Split Vertical</source>
        <translation>Függőleges felosztás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Split Horizontal</source>
        <translation>Vízszintes felosztás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Pointing Hand</source>
        <translation>Mutató kéz</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Forbidden</source>
        <translation>Tiltott</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open Hand</source>
        <translation>Nyitott kéz</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Closed Hand</source>
        <translation>Csukott kéz</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>What&apos;s This</source>
        <translation>Mi ez</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Busy</source>
        <translation>Foglalt</translation>
    </message>
</context>
<context>
    <name>QtFontEditWidget</name>
    <message>
        <location filename="../../qttools/src/shared/qtpropertybrowser/qteditorfactory.cpp" line="+197"/>
        <source>...</source>
        <translation>…</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Select Font</source>
        <translation>Betűkészlet kiválasztása</translation>
    </message>
</context>
<context>
    <name>QtFontPropertyManager</name>
    <message>
        <location filename="../../qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="-351"/>
        <source>Family</source>
        <translation>Betűcsalád</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Point Size</source>
        <translation>Pontméret</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Bold</source>
        <translation>Félkövér</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Italic</source>
        <translation>Dőlt</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Underline</source>
        <translation>Aláhúzott</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Strikeout</source>
        <translation>Áthúzott</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Kerning</source>
        <translation>Alávágás</translation>
    </message>
</context>
<context>
    <name>QtGradientDialog</name>
    <message>
        <location filename="../../qttools/src/shared/qtgradienteditor/qtgradientdialog.ui"/>
        <source>Edit Gradient</source>
        <translation>Színátmenet szerkesztése</translation>
    </message>
</context>
<context>
    <name>QtGradientEditor</name>
    <message>
        <location filename="../../qttools/src/shared/qtgradienteditor/qtgradienteditor.ui"/>
        <source>Form</source>
        <translation>Űrlap</translation>
    </message>
    <message>
        <location/>
        <source>Gradient Editor</source>
        <translation>Színátmenet szerkesztő</translation>
    </message>
    <message>
        <location/>
        <source>This area shows a preview of the gradient being edited. It also allows you to edit parameters specific to the gradient&apos;s type such as start and final point, radius, etc. by drag &amp; drop.</source>
        <translation>Ez a terület a szerkesztett színátmenet előnézetét jeleníti meg. Lehetővé teszi továbbá a színátmenet típusára jellemző paraméterek fogd és vidd módon történő szerkesztését, mint például a kezdő és befejező pont, sugár, stb.</translation>
    </message>
    <message>
        <location/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location/>
        <source>2</source>
        <translation>2</translation>
    </message>
    <message>
        <location/>
        <source>3</source>
        <translation>3</translation>
    </message>
    <message>
        <location/>
        <source>4</source>
        <translation>4</translation>
    </message>
    <message>
        <location/>
        <source>5</source>
        <translation>5</translation>
    </message>
    <message>
        <location/>
        <source>Gradient Stops Editor</source>
        <translation>Színátmeneti végpont szerkesztő</translation>
    </message>
    <message>
        <location/>
        <source>This area allows you to edit gradient stops. Double click on the existing stop handle to duplicate it. Double click outside of the existing stop handles to create a new stop. Drag &amp; drop the handle to reposition it. Use right mouse button to popup context menu with extra actions.</source>
        <translation>Ez a terület lehetővé teszi a színátmeneti határok szerkesztését. Kettőzéshez kattintson duplán a meglévő határfogantyúra. A meglévő határfogantyúkon kívülre történő dupla kattintással új határt hozhat létre. Fogd és vidd módon a fogantyú áthelyezhető. A jobb egérgomb használatával további műveleteket tartalmazó helyi menüt jeleníthet meg.</translation>
    </message>
    <message>
        <location/>
        <source>Zoom</source>
        <translation>Nagyítás</translation>
    </message>
    <message>
        <location/>
        <source>Reset Zoom</source>
        <translation>Nagyítás visszaállítása</translation>
    </message>
    <message>
        <location/>
        <source>Position</source>
        <translation>Helyzet</translation>
    </message>
    <message>
        <location/>
        <source>Hue</source>
        <translation>Árny</translation>
    </message>
    <message>
        <location/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location/>
        <source>Saturation</source>
        <translation>Telítettség</translation>
    </message>
    <message>
        <location/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location/>
        <source>Sat</source>
        <translation>Tel</translation>
    </message>
    <message>
        <location/>
        <source>Value</source>
        <translation>Érték</translation>
    </message>
    <message>
        <location/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location/>
        <source>Val</source>
        <translation>Ért</translation>
    </message>
    <message>
        <location/>
        <source>Alpha</source>
        <translation>Alfa</translation>
    </message>
    <message>
        <location/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location/>
        <source>Type</source>
        <translation>Típus</translation>
    </message>
    <message>
        <location/>
        <source>Spread</source>
        <translation>Hízlalás</translation>
    </message>
    <message>
        <location/>
        <source>Color</source>
        <translation>Szín</translation>
    </message>
    <message>
        <location/>
        <source>Current stop&apos;s color</source>
        <translation>Jelenlegi határ színe</translation>
    </message>
    <message>
        <location/>
        <source>Show HSV specification</source>
        <translation>HSV specifikáció megjelenítése</translation>
    </message>
    <message>
        <location/>
        <source>HSV</source>
        <translation>HSV</translation>
    </message>
    <message>
        <location/>
        <source>Show RGB specification</source>
        <translation>RGB specifikáció megjelenítése</translation>
    </message>
    <message>
        <location/>
        <source>RGB</source>
        <translation>RGB</translation>
    </message>
    <message>
        <location/>
        <source>Current stop&apos;s position</source>
        <translation>Jelenlegi határ helyzete</translation>
    </message>
    <message>
        <location/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location/>
        <source>Zoom In</source>
        <translation>Nagyítás</translation>
    </message>
    <message>
        <location/>
        <source>Zoom Out</source>
        <translation>Kicsinyítés</translation>
    </message>
    <message>
        <location/>
        <source>Toggle details extension</source>
        <translation>Részletek kiterjesztés be- és kikapcsolása</translation>
    </message>
    <message>
        <location/>
        <source>&gt;</source>
        <translation>&gt;</translation>
    </message>
    <message>
        <location/>
        <source>Linear Type</source>
        <translation>Lineáris típus</translation>
    </message>
    <message>
        <location/>
        <source>...</source>
        <translation>…</translation>
    </message>
    <message>
        <location/>
        <source>Radial Type</source>
        <translation>Sugáriranyú típus</translation>
    </message>
    <message>
        <location/>
        <source>Conical Type</source>
        <translation>Kúpos típus</translation>
    </message>
    <message>
        <location/>
        <source>Pad Spread</source>
        <translation>Hizlalás kitöltése</translation>
    </message>
    <message>
        <location/>
        <source>Repeat Spread</source>
        <translation>Hizlalás ismétlése</translation>
    </message>
    <message>
        <location/>
        <source>Reflect Spread</source>
        <translation>Hizlalás tükröződése</translation>
    </message>
    <message>
        <location filename="../../qttools/src/shared/qtgradienteditor/qtgradienteditor.cpp" line="+431"/>
        <source>Start X</source>
        <translation>Kezdő X</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Start Y</source>
        <translation>Kezdő Y</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Final X</source>
        <translation>Végső X</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Final Y</source>
        <translation>Végső Y</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+24"/>
        <source>Central X</source>
        <translation>Közép X</translation>
    </message>
    <message>
        <location line="-20"/>
        <location line="+24"/>
        <source>Central Y</source>
        <translation>Közép Y</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Focal X</source>
        <translation>Fókusz X</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Focal Y</source>
        <translation>Fókusz Y</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Radius</source>
        <translation>Sugár</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Angle</source>
        <translation>Szög</translation>
    </message>
    <message>
        <location line="+288"/>
        <source>Linear</source>
        <translation>Lineáis</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Radial</source>
        <translation>Sugárirányú</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Conical</source>
        <translation>Kúpos</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Pad</source>
        <translation>Kitöltés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Repeat</source>
        <translation>Ismétlés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Reflect</source>
        <translation>Tükröződés</translation>
    </message>
</context>
<context>
    <name>QtGradientStopsWidget</name>
    <message>
        <location filename="../../qttools/src/shared/qtgradienteditor/qtgradientstopswidget.cpp" line="+928"/>
        <source>New Stop</source>
        <translation>Új határ</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Flip All</source>
        <translation>Összes tükrözése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Select All</source>
        <translation>Összes kijelölése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom In</source>
        <translation>Nagyítás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Zoom Out</source>
        <translation>Kicsinyítés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Reset Zoom</source>
        <translation>Nagyítás visszaállítása</translation>
    </message>
</context>
<context>
    <name>QtGradientView</name>
    <message>
        <location filename="../../qttools/src/shared/qtgradienteditor/qtgradientview.ui"/>
        <source>Gradient View</source>
        <translation>Színátmenet nézet</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/shared/qtgradienteditor/qtgradientview.cpp" line="+207"/>
        <source>New...</source>
        <translation>Új…</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/shared/qtgradienteditor/qtgradientview.cpp" line="+1"/>
        <source>Edit...</source>
        <translation>Szerkesztés…</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/shared/qtgradienteditor/qtgradientview.cpp" line="+1"/>
        <source>Rename</source>
        <translation>Átnevezés</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/shared/qtgradienteditor/qtgradientview.cpp" line="+1"/>
        <source>Remove</source>
        <translation>Eltávolítás</translation>
    </message>
    <message>
        <location filename="../../qttools/src/shared/qtgradienteditor/qtgradientview.cpp" line="-104"/>
        <source>Grad</source>
        <translation>Színátmenet</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Remove Gradient</source>
        <translation>Színátmenet eltávolítása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Are you sure you want to remove the selected gradient?</source>
        <translation>Biztosan el szeretné távolítani a kijelölt színátmenetet?</translation>
    </message>
</context>
<context>
    <name>QtGradientViewDialog</name>
    <message>
        <location filename="../../qttools/src/shared/qtgradienteditor/qtgradientviewdialog.ui"/>
        <location filename="../../qttools/src/shared/qtgradienteditor/qtgradientviewdialog.h" line="+60"/>
        <source>Select Gradient</source>
        <translation>Színátmenet kiválasztása</translation>
    </message>
</context>
<context>
    <name>QtLocalePropertyManager</name>
    <message>
        <location filename="../../qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="-3541"/>
        <source>&lt;Invalid&gt;</source>
        <translation>&lt;érvénytelen&gt;</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>%1, %2</source>
        <translation>%1, %2</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Language</source>
        <translation>Nyelv</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Country</source>
        <translation>Ország</translation>
    </message>
</context>
<context>
    <name>QtPointFPropertyManager</name>
    <message>
        <location line="+410"/>
        <source>(%1, %2)</source>
        <translation>(%1, %2)</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
</context>
<context>
    <name>QtPointPropertyManager</name>
    <message>
        <location line="-319"/>
        <source>(%1, %2)</source>
        <translation>(%1, %2)</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
</context>
<context>
    <name>QtPropertyBrowserUtils</name>
    <message>
        <location filename="../../qttools/src/shared/qtpropertybrowser/qtpropertybrowserutils.cpp" line="+93"/>
        <source>[%1, %2, %3] (%4)</source>
        <translation>[%1, %2, %3] (%4)</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>[%1, %2]</source>
        <translation>[%1, %2]</translation>
    </message>
</context>
<context>
    <name>QtRectFPropertyManager</name>
    <message>
        <location filename="../../qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="+1701"/>
        <source>[(%1, %2), %3 x %4]</source>
        <translation>[(%1, %2), %3 x %4]</translation>
    </message>
    <message>
        <location line="+156"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Width</source>
        <translation>Szélesség</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Height</source>
        <translation>Magasság</translation>
    </message>
</context>
<context>
    <name>QtRectPropertyManager</name>
    <message>
        <location line="-611"/>
        <source>[(%1, %2), %3 x %4]</source>
        <translation>[(%1, %2), %3 x %4]</translation>
    </message>
    <message>
        <location line="+120"/>
        <source>X</source>
        <translation>X</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Y</source>
        <translation>Y</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Width</source>
        <translation>Szélesség</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Height</source>
        <translation>Magasság</translation>
    </message>
</context>
<context>
    <name>QtResourceEditorDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qtresourceeditordialog.ui"/>
        <source>Dialog</source>
        <translation>Párbeszédablak</translation>
    </message>
    <message>
        <location/>
        <source>New File</source>
        <translation>Új fájl</translation>
    </message>
    <message>
        <location/>
        <source>N</source>
        <translation>N</translation>
    </message>
    <message>
        <location/>
        <source>Remove File</source>
        <translation>Fájl eltávolítása</translation>
    </message>
    <message>
        <location/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location/>
        <source>I</source>
        <translation>I</translation>
    </message>
    <message>
        <location/>
        <source>New Resource</source>
        <translation>Új erőforrás</translation>
    </message>
    <message>
        <location/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location/>
        <source>Remove Resource or File</source>
        <translation>Erőforrás vagy fájl eltávolítása</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qtresourceeditordialog.cpp" line="+64"/>
        <source>%1 already exists.
Do you want to replace it?</source>
        <translation>A(z) %1 már létezik.
Szeretné lecserélni?</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The file does not appear to be a resource file; element &apos;%1&apos; was found where &apos;%2&apos; was expected.</source>
        <translation>A fájl nem tűnik erőforrásfájlnak: a(z) „%1” elem volt megtalálható, ahol „%2” várt.</translation>
    </message>
    <message>
        <location line="+840"/>
        <source>%1 [read-only]</source>
        <translation>%1 [csak olvasható]</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+198"/>
        <source>%1 [missing]</source>
        <translation>%1 [hiányzik]</translation>
    </message>
    <message>
        <location line="-72"/>
        <source>&lt;no prefix&gt;</source>
        <translation>&lt;nincs előtag&gt;</translation>
    </message>
    <message>
        <location line="+315"/>
        <location line="+569"/>
        <source>New Resource File</source>
        <translation>Új erőforrásfájl</translation>
    </message>
    <message>
        <location line="-567"/>
        <location line="+25"/>
        <source>Resource files (*.qrc)</source>
        <translation>Erőforrásfájlok (*.qrc)</translation>
    </message>
    <message>
        <location line="-2"/>
        <source>Import Resource File</source>
        <translation>Erőforrásfájl importálása</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>newPrefix</source>
        <translation>új előtag</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; The file&lt;/p&gt;&lt;p&gt;%1&lt;/p&gt;&lt;p&gt;is outside of the current resource file&apos;s parent directory.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Figyelmeztetés:&lt;/b&gt; a megadott&lt;/p&gt;&lt;p&gt;%1 fájl&lt;/p&gt;a jelenlegi erőforrásfájl szülőkönyvtárán kívül van.&lt;p&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&lt;p&gt;To resolve the issue, press:&lt;/p&gt;&lt;table&gt;&lt;tr&gt;&lt;th align=&quot;left&quot;&gt;Copy&lt;/th&gt;&lt;td&gt;to copy the file to the resource file&apos;s parent directory.&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;th align=&quot;left&quot;&gt;Copy As...&lt;/th&gt;&lt;td&gt;to copy the file into a subdirectory of the resource file&apos;s parent directory.&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;th align=&quot;left&quot;&gt;Keep&lt;/th&gt;&lt;td&gt;to use its current location.&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</source>
        <translation>&lt;p&gt;A probléma megoldásához válasszon a következők közül:&lt;/p&gt;&lt;table&gt;&lt;tr&gt;&lt;th align=&quot;left&quot;&gt;Másolás&lt;/th&gt;&lt;td&gt;a fájl másolása az erőforrásfájl szülőkönyvtárába.&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;th align=&quot;left&quot;&gt;Másolás másként…&lt;/th&gt;&lt;td&gt;a fájl másolása az erőforrásfájl szülőkönyvtárának egyik alkönyvtárba.&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;th align=&quot;left&quot;&gt;Megtartás&lt;/th&gt;&lt;td&gt;a fájl jelenlegi helyének használata.&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Add Files</source>
        <translation>Fájlok hozzáadása</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Incorrect Path</source>
        <translation>Hibás útvonal</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+19"/>
        <location line="+210"/>
        <location line="+7"/>
        <source>Copy</source>
        <translation>Másolás</translation>
    </message>
    <message>
        <location line="-234"/>
        <source>Copy As...</source>
        <translation>Másolás másként…</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Keep</source>
        <translation>Megtartás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Skip</source>
        <translation>Kihagyás</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Clone Prefix</source>
        <translation>Előtag klónozása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Enter the suffix which you want to add to the names of the cloned files.
This could for example be a language extension like &quot;_de&quot;.</source>
        <translation>Adja meg az utótagot, amelyet a klónozott fájlok neveihez szeretne hozzáadni.
Ez lehet például egy nyelvi kiterjesztés, úgymint „_hu”.</translation>
    </message>
    <message>
        <location line="+111"/>
        <location line="+4"/>
        <source>Copy As</source>
        <translation>Másolás másként</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;p&gt;The selected file:&lt;/p&gt;&lt;p&gt;%1&lt;/p&gt;&lt;p&gt;is outside of the current resource file&apos;s directory:&lt;/p&gt;&lt;p&gt;%2&lt;/p&gt;&lt;p&gt;Please select another path within this directory.&lt;p&gt;</source>
        <translation>&lt;p&gt;A kiválasztott fájl:&lt;/p&gt;&lt;p&gt;%1&lt;/p&gt;&lt;p&gt;a jelenlegi erőforrásfájl könyvtárán kívül van:&lt;/p&gt;&lt;p&gt;%2&lt;/p&gt;&lt;p&gt;Válasszon egy másik útvonalat ezen a könyvtáron belül.&lt;p&gt;</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Could not overwrite %1.</source>
        <translation>Nem sikerült a(z) %1 felülírása.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Could not copy
%1
to
%2</source>
        <translation>Nem másolható
%1
ide:
%2</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>A parse error occurred at line %1, column %2 of %3:
%4</source>
        <translation>Feldolgozási hiba történt a(z) %3 %1. sorának %2. oszlopában:
%4</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Save Resource File</source>
        <translation>Erőforrásfájl mentése</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Could not write %1: %2</source>
        <translation>Nem sikerült a(z) %1 kiírása: %2</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Edit Resources</source>
        <translation>Erőforrások szerkesztése</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>New...</source>
        <translation>Új…</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Open...</source>
        <translation>Megnyitás…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open Resource File</source>
        <translation>Erőforrásfájl megnyitása</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+11"/>
        <source>Remove</source>
        <translation>Eltávolítás</translation>
    </message>
    <message>
        <location line="-10"/>
        <location line="+11"/>
        <source>Move Up</source>
        <translation>Mozgatás fel</translation>
    </message>
    <message>
        <location line="-10"/>
        <location line="+11"/>
        <source>Move Down</source>
        <translation>Mozgatás le</translation>
    </message>
    <message>
        <location line="-9"/>
        <location line="+1"/>
        <source>Add Prefix</source>
        <translation>Előtag hozzáadása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Files...</source>
        <translation>Fájlok hozzáadása…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Change Prefix</source>
        <translation>Előtag módosítása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Change Language</source>
        <translation>Nyelv módosítása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Change Alias</source>
        <translation>Álnév módosítása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Clone Prefix...</source>
        <translation>Előtag klónozása…</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Prefix / Path</source>
        <translation>Előtag / útvonal</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Language / Alias</source>
        <translation>Nyelv / álnév</translation>
    </message>
    <message>
        <location line="+117"/>
        <source>&lt;html&gt;&lt;p&gt;&lt;b&gt;Warning:&lt;/b&gt; There have been problems while reloading the resources:&lt;/p&gt;&lt;pre&gt;%1&lt;/pre&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;p&gt;&lt;b&gt;Figyelmeztetés:&lt;/b&gt; hibák történtek az erőforrás újratöltése közben:&lt;/p&gt;&lt;pre&gt;%1&lt;/pre&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Resource Warning</source>
        <translation>Erőforrás figyelmeztetés</translation>
    </message>
</context>
<context>
    <name>QtResourceView</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qtresourceview.cpp" line="+537"/>
        <source>Size: %1 x %2
%3</source>
        <translation>Méret: %1 x %2
%3</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Edit Resources...</source>
        <translation>Erőforrások szerkesztése…</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Reload</source>
        <translation>Újratöltés</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Copy Path</source>
        <translation>Útvonal másolása</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Filter</source>
        <translation>Szűrő</translation>
    </message>
</context>
<context>
    <name>QtResourceViewDialog</name>
    <message>
        <location line="+250"/>
        <source>Select Resource</source>
        <translation>Erőforrás kiválasztása</translation>
    </message>
</context>
<context>
    <name>QtSizeFPropertyManager</name>
    <message>
        <location filename="../../qttools/src/shared/qtpropertybrowser/qtpropertymanager.cpp" line="-534"/>
        <source>%1 x %2</source>
        <translation>%1 x %2</translation>
    </message>
    <message>
        <location line="+130"/>
        <source>Width</source>
        <translation>Szélesség</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Height</source>
        <translation>Magasság</translation>
    </message>
</context>
<context>
    <name>QtSizePolicyPropertyManager</name>
    <message>
        <location line="+1707"/>
        <location line="+1"/>
        <source>&lt;Invalid&gt;</source>
        <translation>&lt;Érvénytelen&gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>[%1, %2, %3, %4]</source>
        <translation>[%1, %2, %3, %4]</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Horizontal Policy</source>
        <translation>Vízszintes irányelv</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Vertical Policy</source>
        <translation>Függőleges irányelv</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Horizontal Stretch</source>
        <translation>Vízszintes nyújtás</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Vertical Stretch</source>
        <translation>Függőleges nyújtás</translation>
    </message>
</context>
<context>
    <name>QtSizePropertyManager</name>
    <message>
        <location line="-2283"/>
        <source>%1 x %2</source>
        <translation>%1 x %2</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>Width</source>
        <translation>Szélesség</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Height</source>
        <translation>Magasság</translation>
    </message>
</context>
<context>
    <name>QtToolBarDialog</name>
    <message>
        <location filename="../../qttools/src/shared/qttoolbardialog/qttoolbardialog.ui"/>
        <source>Customize Toolbars</source>
        <translation>Eszköztárak személyre szabása</translation>
    </message>
    <message>
        <location/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location/>
        <source>Actions</source>
        <translation>Műveletek</translation>
    </message>
    <message>
        <location/>
        <source>Toolbars</source>
        <translation>Eszköztárak</translation>
    </message>
    <message>
        <location/>
        <source>Add new toolbar</source>
        <translation>Új eszköztár hozzáadása</translation>
    </message>
    <message>
        <location/>
        <source>New</source>
        <translation>Új</translation>
    </message>
    <message>
        <location/>
        <source>Remove selected toolbar</source>
        <translation>Kijelölt eszköztár eltávolítása</translation>
    </message>
    <message>
        <location/>
        <source>Remove</source>
        <translation>Eltávolítás</translation>
    </message>
    <message>
        <location/>
        <source>Rename toolbar</source>
        <translation>Eszköztár átnevezése</translation>
    </message>
    <message>
        <location/>
        <source>Rename</source>
        <translation>Átnevezés</translation>
    </message>
    <message>
        <location/>
        <source>Move action up</source>
        <translation>Művelet mozgatása fel</translation>
    </message>
    <message>
        <location/>
        <source>Up</source>
        <translation>Fel</translation>
    </message>
    <message>
        <location/>
        <source>Remove action from toolbar</source>
        <translation>Művelet eltávolítása az eszköztárról</translation>
    </message>
    <message>
        <location/>
        <source>&lt;-</source>
        <translation>&lt;-</translation>
    </message>
    <message>
        <location/>
        <source>Add action to toolbar</source>
        <translation>Művelet hozzáadása az eszköztárhoz</translation>
    </message>
    <message>
        <location/>
        <source>-&gt;</source>
        <translation>-&gt;</translation>
    </message>
    <message>
        <location/>
        <source>Move action down</source>
        <translation>Művelet mozgatása le</translation>
    </message>
    <message>
        <location/>
        <source>Down</source>
        <translation>Le</translation>
    </message>
    <message>
        <location/>
        <source>Current Toolbar Actions</source>
        <translation>Jelenlegi eszköztár műveletek</translation>
    </message>
    <message>
        <location filename="../../qttools/src/shared/qttoolbardialog/qttoolbardialog.cpp" line="+1189"/>
        <source>Custom Toolbar</source>
        <translation>Egyéni eszköztár</translation>
    </message>
    <message>
        <location line="+530"/>
        <source>&lt; S E P A R A T O R &gt;</source>
        <translation>&lt; E L V Á L A S Z T Ó &gt;</translation>
    </message>
</context>
<context>
    <name>QtTreePropertyBrowser</name>
    <message>
        <location filename="../../qttools/src/shared/qtpropertybrowser/qttreepropertybrowser.cpp" line="+439"/>
        <source>Property</source>
        <translation>Tulajdonság</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Value</source>
        <translation>Érték</translation>
    </message>
</context>
<context>
    <name>SaveFormAsTemplate</name>
    <message>
        <location filename="../../qttools/src/designer/src/designer/saveformastemplate.ui"/>
        <source>Save Form As Template</source>
        <translation>Űrlap mentése sablonként</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Name:</source>
        <translation>&amp;Név:</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Category:</source>
        <translation>&amp;Kategória:</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/designer/saveformastemplate.cpp" line="+59"/>
        <source>Add path...</source>
        <translation>Útvonal hozzáadása…</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Template Exists</source>
        <translation>A sablon létezik</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>A template with the name %1 already exists.
Do you want overwrite the template?</source>
        <translation>Egy %1 nevű sablon már létezik.
Szeretné felülírni a sablont?</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Overwrite Template</source>
        <translation>Sablon felülírása</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Open Error</source>
        <translation>Megnyitási hiba</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error opening template %1 for writing. Reason: %2</source>
        <translation>Hiba történt a(z) %1 sablon írásra való megnyitása során. Ok: %2</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Write Error</source>
        <translation>Írási hiba</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>There was an error writing the template %1 to disk. Reason: %2</source>
        <translation>Hiba történt a(z) %1 sablon lemezre írása során. Ok: %2</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Pick a directory to save templates in</source>
        <translation>Válasszon egy könyvtárat, amelybe a sablonokat menti</translation>
    </message>
</context>
<context>
    <name>SelectSignalDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/selectsignaldialog.ui"/>
        <source>Go to slot</source>
        <translation>Ugrás a tárolóhelyhez</translation>
    </message>
    <message>
        <location/>
        <source>Select signal</source>
        <translation>Válasszon jelzést</translation>
    </message>
    <message>
        <location/>
        <source>signal</source>
        <translation>jelzés</translation>
    </message>
    <message>
        <location/>
        <source>class</source>
        <translation>osztály</translation>
    </message>
</context>
<context>
    <name>SignalSlotConnection</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/signalsloteditor/signalsloteditor.cpp" line="-359"/>
        <source>SENDER(%1), SIGNAL(%2), RECEIVER(%3), SLOT(%4)</source>
        <translation>KÜLDŐ(%1), JELZÉS(%2), FOGADÓ(%3), TÁROLÓHELY(%4)</translation>
    </message>
</context>
<context>
    <name>SignalSlotDialogClass</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/signalslotdialog.ui"/>
        <source>Signals and slots</source>
        <translation>Jelzések és tárolóhelyek</translation>
    </message>
    <message>
        <location/>
        <source>Slots</source>
        <translation>Tárolóhelyek</translation>
    </message>
    <message>
        <location/>
        <source>Add</source>
        <translation>Hozzáadás</translation>
    </message>
    <message>
        <location/>
        <source>...</source>
        <translation>…</translation>
    </message>
    <message>
        <location/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location/>
        <source>Signals</source>
        <translation>Jelzések</translation>
    </message>
</context>
<context>
    <name>Spacer</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/spacer_widget.cpp" line="+256"/>
        <source>Horizontal Spacer &apos;%1&apos;, %2 x %3</source>
        <translation>Vízszintes helyőrző: „%1”, %2 x %3</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Vertical Spacer &apos;%1&apos;, %2 x %3</source>
        <translation>Függőleges helyőrző: „%1”, %2 x %3</translation>
    </message>
</context>
<context>
    <name>TemplateOptionsPage</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/templateoptionspage.cpp" line="+144"/>
        <source>Template Paths</source>
        <extracomment>Tab in preferences dialog</extracomment>
        <translation>Sablonok útvonalai</translation>
    </message>
</context>
<context>
    <name>ToolBarManager</name>
    <message>
        <location filename="../../qttools/src/designer/src/designer/mainwindow.cpp" line="+89"/>
        <source>Configure Toolbars...</source>
        <translation>Eszköztárak személyre szabása…</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Window</source>
        <translation>Ablak</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Help</source>
        <translation>Súgó</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Style</source>
        <translation>Stílus</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dock views</source>
        <translation>Dokknézetek</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>File</source>
        <translation>Fájl</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit</source>
        <translation>Szerkesztés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tools</source>
        <translation>Eszközök</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Form</source>
        <translation>Űrlap</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Toolbars</source>
        <translation>Eszköztárak</translation>
    </message>
</context>
<context>
    <name>VersionDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/designer/versiondialog.cpp" line="+157"/>
        <source>&lt;h3&gt;%1&lt;/h3&gt;&lt;br/&gt;&lt;br/&gt;Version %2</source>
        <translation>&lt;h3&gt;%1&lt;/h3&gt;&lt;br/&gt;&lt;br/&gt;%2. verzió</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Qt Designer</source>
        <translation>Qt Tervező</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;br/&gt;Qt Designer is a graphical user interface designer for Qt applications.&lt;br/&gt;</source>
        <translation>&lt;br/&gt;A Qt Tervező egy grafikus felhasználói felület tervező a Qt alkalmazásokhoz.&lt;br/&gt;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1&lt;br/&gt;Copyright (C) %2 The Qt Company Ltd.</source>
        <translation>%1&lt;br/&gt;Copyright (C) %2 The Qt Company Ltd.</translation>
    </message>
</context>
<context>
    <name>WidgetDataBase</name>
    <message>
        <source>A custom widget plugin whose class name (%1) matches that of an existing class has been found.</source>
        <translation type="vanished">Egy egyéni felületi elem bővítmény, amely osztályneve (%1) megegyezik azzal, hogy egy létező osztály található.</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/widgetdatabase.cpp" line="+788"/>
        <source>The file contains a custom widget &apos;%1&apos; whose base class (%2) differs from the current entry in the widget database (%3). The widget database is left unchanged.</source>
        <translation>A fájl egy egyéni felületi elemet tartalmaz („%1”), amelynek alaposztálya (%2) eltér a jelenlegi bejegyzéstől a felületi elem adatbázisban (%3). A felületi elem adatbázis változatlanul lett hagyva.</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/widgetfactory.cpp" line="+396"/>
        <source>%1 Widget</source>
        <translation>%1 felületi elem</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::ActionEditor</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/actioneditor.cpp" line="+111"/>
        <source>New...</source>
        <translation>Új…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Edit...</source>
        <translation>Szerkesztés…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Go to slot...</source>
        <translation>Ugrás a tárolóhelyhez…</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copy</source>
        <translation>Másolás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cut</source>
        <translation>Kivágás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Paste</source>
        <translation>Beillesztés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Select all</source>
        <translation>Összes kijelölése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Actions</source>
        <translation>Műveletek</translation>
    </message>
    <message>
        <location line="+51"/>
        <source>Configure Action Editor</source>
        <translation>Műveletszerkesztő beállítása</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Icon View</source>
        <translation>Ikon nézet</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Detailed View</source>
        <translation>Részletes nézet</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Filter</source>
        <translation>Szűrő</translation>
    </message>
    <message>
        <location line="+246"/>
        <source>New action</source>
        <translation>Új művelet</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Edit action</source>
        <translation>Művelet szerkesztése</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Remove action &apos;%1&apos;</source>
        <translation>„%1” művelet eltávolítása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove actions</source>
        <translation>Műveletek eltávolítása</translation>
    </message>
    <message>
        <location line="+224"/>
        <source>Used In</source>
        <translation>Használat helye</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::ActionModel</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/actionrepository.cpp" line="+75"/>
        <source>Name</source>
        <translation>Név</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Used</source>
        <translation>Használatban</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Text</source>
        <translation>Szöveg</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Shortcut</source>
        <translation>Gyorsbillentyű</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Checkable</source>
        <translation>Bejelölhető</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ToolTip</source>
        <translation>Buboréksúgó</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::BuddyEditor</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/buddyeditor/buddyeditor.cpp" line="+234"/>
        <source>Add buddy</source>
        <translation>Partner hozzáadása</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>Remove buddies</source>
        <translation>Partnerek eltávolítása</translation>
    </message>
    <message numerus="yes">
        <location line="+24"/>
        <source>Remove %n buddies</source>
        <translation>
            <numerusform>%n partner eltávolítása</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+51"/>
        <source>Add %n buddies</source>
        <translation>
            <numerusform>%n partner hozzáadása</numerusform>
        </translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Set automatically</source>
        <translation>Automatikus beállítás</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::BuddyEditorPlugin</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/buddyeditor/buddyeditor_plugin.cpp" line="+55"/>
        <source>Edit Buddies</source>
        <translation>Partnerek szerkesztése</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::BuddyEditorTool</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/buddyeditor/buddyeditor_tool.cpp" line="+43"/>
        <source>Edit Buddies</source>
        <translation>Partnerek szerkesztése</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::ButtonGroupMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/button_taskmenu.cpp" line="+7"/>
        <source>Select members</source>
        <translation>Tagok kijelölése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Break</source>
        <translation>Feltörés</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::ButtonTaskMenu</name>
    <message>
        <location line="+118"/>
        <source>Assign to button group</source>
        <translation>Hozzárendelés gombcsoporthoz</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Button group</source>
        <translation>Gombcsoport</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>New button group</source>
        <translation>Új gombcsoport</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Change text...</source>
        <translation>Szöveg módosítása…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>None</source>
        <translation>Nincs</translation>
    </message>
    <message>
        <location line="+101"/>
        <source>Button group &apos;%1&apos;</source>
        <translation>„%1” gombcsoport</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::CodeDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/codedialog.cpp" line="+86"/>
        <source>Save...</source>
        <translation>Mentés…</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Copy All</source>
        <translation>Összes másolása</translation>
    </message>
    <message>
        <source>&amp;Find in Text...</source>
        <translation type="vanished">Ke&amp;resés a szövegben…</translation>
    </message>
    <message>
        <location line="+83"/>
        <source>A temporary form file could not be created in %1.</source>
        <translation>Egy átmeneti űrlapfájlt nem sikerült létrehozni ebben: %1.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The temporary form file %1 could not be written.</source>
        <translation>A(z) %1 átmeneti űrlapfájlt nem sikerült kiírni.</translation>
    </message>
    <message>
        <source>%1 - [Code]</source>
        <translation type="vanished">%1 – [Kód]</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Save Code</source>
        <translation>Kód mentése</translation>
    </message>
    <message>
        <source>Header Files (*.%1)</source>
        <translation type="vanished">Fejlécfájlok (*.%1)</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>%1 - [%2 Code]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+32"/>
        <source>The file %1 could not be opened: %2</source>
        <translation>A(z) %1 fájlt nem sikerült megnyitni: %2</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The file %1 could not be written: %2</source>
        <translation>A(z) %1 fájlt nem sikerült kiírni: %2</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1 - Error</source>
        <translation>%1 – Hiba</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::ColorAction</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/richtexteditor.cpp" line="+331"/>
        <source>Text Color</source>
        <translation>Szövegszín</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::ComboBoxTaskMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/combobox_taskmenu.cpp" line="+55"/>
        <source>Edit Items...</source>
        <translation>Elemek szerkesztése…</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Change Combobox Contents</source>
        <translation>Legördülő menü tartalmának módosítása</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::CommandLinkButtonTaskMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/button_taskmenu.cpp" line="+156"/>
        <source>Change description...</source>
        <translation>Leírás módosítása…</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::ConnectionEdit</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/connectionedit.cpp" line="+1310"/>
        <source>Select All</source>
        <translation>Összes kijelölése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Deselect All</source>
        <translation>Kijelölés megszüntetése</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::ConnectionModel</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/signalsloteditor/signalsloteditorwindow.cpp" line="-463"/>
        <source>Sender</source>
        <translation>Küldő</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Signal</source>
        <translation>Jelzés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Receiver</source>
        <translation>Fogadó</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Slot</source>
        <translation>Tárolóhely</translation>
    </message>
    <message>
        <location line="+99"/>
        <source>&lt;sender&gt;</source>
        <translation>&lt;küldő&gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;signal&gt;</source>
        <translation>&lt;jelzés&gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;receiver&gt;</source>
        <translation>&lt;fogadó&gt;</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&lt;slot&gt;</source>
        <translation>&lt;tárolóhely&gt;</translation>
    </message>
    <message>
        <location line="+100"/>
        <source>The connection already exists!&lt;br&gt;%1</source>
        <translation>A kapcsolat már létezik!&lt;br&gt;%1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Signal and Slot Editor</source>
        <translation>Jelzés és tárolóhely szerkesztő</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::ContainerWidgetTaskMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/containerwidget_taskmenu.cpp" line="+67"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Insert</source>
        <translation>Beszúrás</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Insert Page Before Current Page</source>
        <translation>Oldal beszúrása a jelenlegi oldal elé</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Insert Page After Current Page</source>
        <translation>Oldal beszúrása a jelenlegi oldal után</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Add Subwindow</source>
        <translation>Gyermekablak hozzáadása</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Subwindow</source>
        <translation>Gyermekablak</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Page</source>
        <translation>Oldal</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Page %1 of %2</source>
        <translation>%1/%2 oldal</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::DPI_Chooser</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/dpi_chooser.cpp" line="+27"/>
        <source>System (%1 x %2)</source>
        <extracomment>System resolution</extracomment>
        <translation>Rendszer (%1 × %2)</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>User defined</source>
        <translation>Felhasználó által megadott</translation>
    </message>
    <message>
        <location line="+19"/>
        <source> x </source>
        <extracomment>DPI X/Y separator</extracomment>
        <translation> x </translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::DesignerPropertyManager</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/propertyeditor/designerpropertymanager.cpp" line="+106"/>
        <source>translatable</source>
        <translation>lefordítható</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>disambiguation</source>
        <translation>egyértelműsítés</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>comment</source>
        <translation>megjegyzés</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+845"/>
        <location line="+6"/>
        <source>AlignLeft</source>
        <translation>Igazítás balra</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>AlignHCenter</source>
        <translation>Igazítás vízszintesen középre</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>AlignRight</source>
        <translation>Igazítás jobbra</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>AlignJustify</source>
        <translation>Igazítás sorkizártan</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>AlignTop</source>
        <translation>Igazítás felülre</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+4"/>
        <source>AlignVCenter</source>
        <translation>Igazítás függőlegesen középre</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>AlignBottom</source>
        <translation>Igazítás alulra</translation>
    </message>
    <message>
        <location line="+546"/>
        <source>%1, %2</source>
        <translation>%1, %2</translation>
    </message>
    <message numerus="yes">
        <location line="+7"/>
        <source>Customized (%n roles)</source>
        <translation>
            <numerusform>Személyre szabott (%n szerep)</numerusform>
        </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Inherited</source>
        <translation>Öröklött</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>[Theme] %1</source>
        <translation>[Téma] %1</translation>
    </message>
    <message>
        <location line="+501"/>
        <source>Horizontal</source>
        <translation>Vízszintes</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Vertical</source>
        <translation>Függőleges</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Theme</source>
        <translation>Téma</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Normal Off</source>
        <translation>Normál ki</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Normal On</source>
        <translation>Normál be</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Disabled Off</source>
        <translation>Letiltott ki</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Disabled On</source>
        <translation>Letiltott be</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Active Off</source>
        <translation>Aktív ki</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Active On</source>
        <translation>Aktív be</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Selected Off</source>
        <translation>Kijelölt ki</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Selected On</source>
        <translation>Kijelölt be</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::DeviceProfileDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/deviceprofiledialog.cpp" line="+51"/>
        <source>Device Profiles (*.%1)</source>
        <translation>Eszközprofilok (*.%1)</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Default</source>
        <translation>Alapértelmezett</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Save Profile</source>
        <translation>Profil mentése</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Save Profile - Error</source>
        <translation>Profil mentése – Hiba</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Unable to open the file &apos;%1&apos; for writing: %2</source>
        <translation>Nem nyitható meg a(z) „%1” fájl írásra: %2</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Open profile</source>
        <translation>Profil megnyitása</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+6"/>
        <source>Open Profile - Error</source>
        <translation>Profil megnyitása – Hiba</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>Unable to open the file &apos;%1&apos; for reading: %2</source>
        <translation>Nem nyitható meg a(z) „%1” fájl olvasásra: %2</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&apos;%1&apos; is not a valid profile: %2</source>
        <translation>A(z) „%1” nem érvényes profil: %2</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::Dialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/propertyeditor/stringlisteditor.ui"/>
        <source>Dialog</source>
        <translation>Párbeszédablak</translation>
    </message>
    <message>
        <location/>
        <source>StringList</source>
        <translation>Karakterlánclista</translation>
    </message>
    <message>
        <location/>
        <source>New String</source>
        <translation>Új karakterlánc</translation>
    </message>
    <message>
        <location/>
        <source>&amp;New</source>
        <translation>Ú&amp;j</translation>
    </message>
    <message>
        <location/>
        <source>Delete String</source>
        <translation>Karakterlánc törlése</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Delete</source>
        <translation>&amp;Törlés</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Value:</source>
        <translation>&amp;Érték:</translation>
    </message>
    <message>
        <location/>
        <source>Move String Up</source>
        <translation>Karakterlánc mozgatása fel</translation>
    </message>
    <message>
        <location/>
        <source>Up</source>
        <translation>Fel</translation>
    </message>
    <message>
        <location/>
        <source>Move String Down</source>
        <translation>Karakterlánc mozgatása le</translation>
    </message>
    <message>
        <location/>
        <source>Down</source>
        <translation>Le</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::EmbeddedOptionsControl</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/embeddedoptionspage.cpp" line="-264"/>
        <source>None</source>
        <translation>Nincs</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Add a profile</source>
        <translation>Egy profil hozzáadása</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Edit the selected profile</source>
        <translation>A kijelölt profil szerkesztése</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Delete the selected profile</source>
        <translation>A kijelölt profil törlése</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Add Profile</source>
        <translation>Profil hozzáadása</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>New profile</source>
        <translation>Új profil</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Edit Profile</source>
        <translation>Profil szerkesztése</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Delete Profile</source>
        <translation>Profil törlése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Would you like to delete the profile &apos;%1&apos;?</source>
        <translation>Törölni szeretné a következő profilt: „%1”?</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Default</source>
        <translation>Alapértelmezett</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::FormEditor</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/formeditor.cpp" line="+164"/>
        <source>Resource File Changed</source>
        <translation>Az erőforrásfájl megváltozott</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The file &quot;%1&quot; has changed outside Designer. Do you want to reload it?</source>
        <translation>A(z) „%1” fájlt a Tervezőn kívül módosították. Szeretné újratölteni?</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::FormLayoutMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/formlayoutmenu.cpp" line="+24"/>
        <source>Add form layout row...</source>
        <translation>Űrlapelrendezés-sor hozzáadása…</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::FormWindow</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/formwindow.cpp" line="-1326"/>
        <source>Edit contents</source>
        <translation>Tartalom szerkesztése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location line="+777"/>
        <source>Insert widget &apos;%1&apos;</source>
        <translation>„%1” felületi elem beszúrása</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Resize</source>
        <translation>Átméretezés</translation>
    </message>
    <message>
        <location line="+243"/>
        <source>Key Resize</source>
        <translation>Átméretezés billentyűvel</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Key Move</source>
        <translation>Áthelyezés billentyűvel</translation>
    </message>
    <message numerus="yes">
        <location line="+262"/>
        <source>Paste %n action(s)</source>
        <translation>
            <numerusform>%n művelet beillesztése</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+2"/>
        <source>Paste %n widget(s)</source>
        <translation>
            <numerusform>%n felületi elem beillesztése</numerusform>
        </translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Paste (%1 widgets, %2 actions)</source>
        <translation>Beillesztés (%1 felületi elem, %2 művelet)</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Cannot paste widgets. Designer could not find a container without a layout to paste into.</source>
        <translation>Nem lehet felületi elemeket beilleszteni. A Tervező nem talál elrendezés nélküli konténert a beillesztéshez.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Break the layout of the container you want to paste into, select this container and then paste again.</source>
        <translation>Törje fel annak a konténernek az elrendezését, amelybe be szeretné illeszteni, jelölje ki a konténert, és illessze be ismét.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Paste error</source>
        <translation>Beillesztési hiba</translation>
    </message>
    <message>
        <location line="+182"/>
        <source>Raise widgets</source>
        <translation>Felületi elemek előrébb hozása</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Lower widgets</source>
        <translation>Felületi elemek hátrébb küldése</translation>
    </message>
    <message>
        <location line="+227"/>
        <source>Select Ancestor</source>
        <translation>Ős kiválasztása</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>Lay out</source>
        <translation>Elrendezés</translation>
    </message>
    <message>
        <location line="+478"/>
        <location line="+55"/>
        <source>Drop widget</source>
        <translation>Felületi elem eldobása</translation>
    </message>
    <message>
        <location line="-13"/>
        <source>A QMainWindow-based form does not contain a central widget.</source>
        <translation>Egy QMainWindow alapú űrlap nem tartalmaz központi felületi elemet.</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::FormWindowBase</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/formwindowbase.cpp" line="+449"/>
        <source>Delete &apos;%1&apos;</source>
        <translation>„%1” törlése</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
    <message>
        <location line="+106"/>
        <source>Invalid form</source>
        <translation>Érvénytelen űrlap</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;p&gt;This file contains top level spacers.&lt;br/&gt;They will &lt;b&gt;not&lt;/b&gt; be saved.&lt;/p&gt;&lt;p&gt;Perhaps you forgot to create a layout?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Ez a fájl felsőszintű helyőrzőket tartalmaz.&lt;br/&gt;Ezek &lt;b&gt;nem&lt;/b&gt; lesznek mentve.&lt;/p&gt;&lt;p&gt;Talán elfelejtett elrendezést létrehozni?&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::FormWindowManager</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/formwindowmanager.cpp" line="+368"/>
        <source>Cu&amp;t</source>
        <translation>&amp;Kivágás</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cuts the selected widgets and puts them on the clipboard</source>
        <translation>Kivágja a kijelölt felületi elemeket, és beteszi azokat a vágólapra</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Copy</source>
        <translation>&amp;Másolás</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Copies the selected widgets to the clipboard</source>
        <translation>Lemásolja a kijelölt felületi elemeket a vágólapra</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Paste</source>
        <translation>&amp;Beillesztés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Pastes the clipboard&apos;s contents</source>
        <translation>Beilleszti a vágólap tartalmát</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Delete</source>
        <translation>&amp;Törlés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Deletes the selected widgets</source>
        <translation>Törli a kijelölt felületi elemeket</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Select &amp;All</source>
        <translation>&amp;Összes kijelölése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Selects all widgets</source>
        <translation>Kijelöli az összes felületi elemet</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Bring to &amp;Front</source>
        <translation>&amp;Előtérbe hozás</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+1"/>
        <source>Raises the selected widgets</source>
        <translation>Előre hozza a kijelölt felületi elemeket</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Send to &amp;Back</source>
        <translation>&amp;Háttérbe küldés</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+1"/>
        <source>Lowers the selected widgets</source>
        <translation>Hátraküldi a kijelölt felületi elemeket</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Adjust &amp;Size</source>
        <translation>&amp;Méret igazítása</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Adjusts the size of the selected widget</source>
        <translation>Beállítja a kijelölt felületi elemek méretét</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Lay Out &amp;Horizontally</source>
        <translation>Elrendezés &amp;vízszintesen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Lays out the selected widgets horizontally</source>
        <translation>Vízszintesen rendezi el a kijelölt felületi elemeket</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Lay Out &amp;Vertically</source>
        <translation>Elrendezés &amp;függőlegesen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Lays out the selected widgets vertically</source>
        <translation>Függőlegesen rendezi el a kijelölt felületi elemeket</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Lay Out in a &amp;Form Layout</source>
        <translation>Elrendezés egy űrl&amp;apelrendezésen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Lays out the selected widgets in a form layout</source>
        <translation>Űrlapszerűen rendezi el a kijelölt felületi elemeket</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Lay Out in a &amp;Grid</source>
        <translation>Elrendezés egy &amp;rácson</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Lays out the selected widgets in a grid</source>
        <translation>Rácsba rendezi a kijelölt felületi elemeket</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Lay Out Horizontally in S&amp;plitter</source>
        <translation>Elrendezés vízszintesen egy &amp;szétválasztóban</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Lays out the selected widgets horizontally in a splitter</source>
        <translation>Vízszintesen rendezi el a kijelölt felületi elemeket egy szétválasztóban</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Lay Out Vertically in Sp&amp;litter</source>
        <translation>Elrendezés függőlegesen egy s&amp;zétválasztóban</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Lays out the selected widgets vertically in a splitter</source>
        <translation>Függőlegesen rendezi el a kijelölt felületi elemeket egy szétválasztóban</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Break Layout</source>
        <translation>Elrendezés fel&amp;törése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Breaks the selected layout</source>
        <translation>Feltöri a kijelölt elrendezést</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Si&amp;mplify Grid Layout</source>
        <translation>Rács elrendezés &amp;egyszerűsítése</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Removes empty columns and rows</source>
        <translation>Eltávolítja az üres oszlopokat és sorokat</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Preview...</source>
        <translation>&amp;Előnézet…</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Preview current form</source>
        <translation>A jelenlegi űrlap előnézete</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Form &amp;Settings...</source>
        <translation>Űrlap&amp;beállítások…</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Break Layout</source>
        <translation>Elrendezés feltörése</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Adjust Size</source>
        <translation>Méret igazítása</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Could not create form preview</source>
        <comment>Title of warning message box</comment>
        <translation>Nem sikerült létrehozni az űrlap előnézetét</translation>
    </message>
    <message>
        <location line="+316"/>
        <source>Form Settings - %1</source>
        <translation>Űrlapbeállítások – %1</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::FormWindowSettings</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/formwindowsettings.cpp" line="+183"/>
        <source>None</source>
        <translation>Nincs</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Device Profile: %1</source>
        <translation>Eszközprofil: %1</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::GridPanel</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/gridpanel.ui"/>
        <source>Form</source>
        <translation>Űrlap</translation>
    </message>
    <message>
        <location/>
        <source>Grid</source>
        <translation>Rács</translation>
    </message>
    <message>
        <location/>
        <source>Visible</source>
        <translation>Látható</translation>
    </message>
    <message>
        <location/>
        <source>Grid &amp;X</source>
        <translation>&amp;X rács</translation>
    </message>
    <message>
        <location/>
        <source>Snap</source>
        <translation>Illesztés</translation>
    </message>
    <message>
        <location/>
        <source>Reset</source>
        <translation>Visszaállítás</translation>
    </message>
    <message>
        <location/>
        <source>Grid &amp;Y</source>
        <translation>&amp;Y rács</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::GroupBoxTaskMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/groupbox_taskmenu.cpp" line="+69"/>
        <source>Change title...</source>
        <translation>Cím módosítása…</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::HtmlTextEdit</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/richtexteditor.cpp" line="-57"/>
        <source>Insert HTML entity</source>
        <translation>HTML entitás beszúrása</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::IconThemeDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/propertyeditor/designerpropertymanager.cpp" line="-1587"/>
        <source>Set Icon From Theme</source>
        <translation>Ikon beállítása témából</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Input icon name from the current theme:</source>
        <translation>Adja meg az ikon nevét a jelenlegi témából:</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::ItemListEditor</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/itemlisteditor.ui"/>
        <source>Items List</source>
        <translation>Elemek listája</translation>
    </message>
    <message>
        <location/>
        <source>New Item</source>
        <translation>Új elem</translation>
    </message>
    <message>
        <location/>
        <source>&amp;New</source>
        <translation>Ú&amp;j</translation>
    </message>
    <message>
        <location/>
        <source>Delete Item</source>
        <translation>Elem törlése</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Delete</source>
        <translation>&amp;Törlés</translation>
    </message>
    <message>
        <location/>
        <source>Move Item Up</source>
        <translation>Elem mozgatása fel</translation>
    </message>
    <message>
        <location/>
        <source>U</source>
        <translation>F</translation>
    </message>
    <message>
        <location/>
        <source>Move Item Down</source>
        <translation>Elem mozgatása le</translation>
    </message>
    <message>
        <location/>
        <source>D</source>
        <translation>L</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/designer/src/components/taskmenu/itemlisteditor.cpp" line="+350"/>
        <source>Properties &amp;&gt;&gt;</source>
        <translation>Tulajdonságok &amp;&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/itemlisteditor.cpp" line="+0"/>
        <source>Properties &amp;&lt;&lt;</source>
        <translation>Tulajdonságok &amp;&lt;&lt;</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::LabelTaskMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/label_taskmenu.cpp" line="+72"/>
        <source>Change rich text...</source>
        <translation>Formázott szöveg módosítása…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Change plain text...</source>
        <translation>Egyszerű szöveg módosítása…</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::LanguageResourceDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/iconselector.cpp" line="-330"/>
        <source>Choose Resource</source>
        <translation>Erőforrás kiválasztása</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::LineEditTaskMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/lineedit_taskmenu.cpp" line="+67"/>
        <source>Change text...</source>
        <translation>Szöveg módosítása…</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::ListWidgetEditor</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/listwidgeteditor.cpp" line="+56"/>
        <source>New Item</source>
        <translation>Új elem</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Edit List Widget</source>
        <translation>Lista felületi elem szerkesztése</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Edit Combobox</source>
        <translation>Legördülő menü szerkesztése</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::ListWidgetTaskMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/listwidget_taskmenu.cpp" line="+54"/>
        <source>Edit Items...</source>
        <translation>Elemek szerkesztése…</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Change List Contents</source>
        <translation>Listatartalom módosítása</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::MdiContainerWidgetTaskMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/containerwidget_taskmenu.cpp" line="+113"/>
        <source>Next Subwindow</source>
        <translation>Következő gyermekablak</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Previous Subwindow</source>
        <translation>Előző gyermekablak</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tile</source>
        <translation>Csempe</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cascade</source>
        <translation>Lépcsőzetes</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::MenuTaskMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/menutaskmenu.cpp" line="+43"/>
        <source>Remove</source>
        <translation>Eltávolítás</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::MorphMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/morphmenu.cpp" line="+264"/>
        <source>Morph into</source>
        <translation>Átalakítás erre</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::NewActionDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/newactiondialog.ui"/>
        <source>New Action...</source>
        <translation>Új művelet…</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Text:</source>
        <translation>&amp;Szöveg:</translation>
    </message>
    <message>
        <location/>
        <source>Object &amp;name:</source>
        <translation>Objektum &amp;neve:</translation>
    </message>
    <message>
        <location/>
        <source>T&amp;oolTip:</source>
        <translation>&amp;Buboréksúgó:</translation>
    </message>
    <message>
        <location/>
        <source>...</source>
        <translation>…</translation>
    </message>
    <message>
        <location/>
        <source>Icon th&amp;eme:</source>
        <translation>Ikon &amp;téma:</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Icon:</source>
        <translation>&amp;Ikon:</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Checkable:</source>
        <translation>&amp;Bejelölhető:</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Shortcut:</source>
        <translation>&amp;Gyorsbillentyű:</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::NewDynamicPropertyDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/propertyeditor/newdynamicpropertydialog.ui"/>
        <source>Create Dynamic Property</source>
        <translation>Dinamikus tulajdonság létrehozása</translation>
    </message>
    <message>
        <location/>
        <source>Property Name</source>
        <translation>Tulajdonságnév</translation>
    </message>
    <message>
        <location/>
        <source>horizontalSpacer</source>
        <translation>Vízszintes helyőrző</translation>
    </message>
    <message>
        <location/>
        <source>Property Type</source>
        <translation>Tulajdonságtípus</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/components/propertyeditor/newdynamicpropertydialog.cpp" line="+121"/>
        <source>Set Property Name</source>
        <translation>Tulajdonságnév beállítása</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>The current object already has a property named &apos;%1&apos;.
Please select another, unique one.</source>
        <translation>A jelenlegi objektumnak már van egy „%1” nevű tulajdonsága.
Válasszon egy másik, egyedi nevet.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>The &apos;_q_&apos; prefix is reserved for the Qt library.
Please select another name.</source>
        <translation>A „_q_” előtag a Qt függvénykönyvtár számára van fenntartva.
Válasszon egy másik nevet.</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::NewFormWidget</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/newformwidget.ui"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location/>
        <source>Choose a template for a preview</source>
        <translation>Válasszon sablont az előnézethez</translation>
    </message>
    <message>
        <location/>
        <source>Embedded Design</source>
        <translation>Beágyazott terv</translation>
    </message>
    <message>
        <location/>
        <source>Device:</source>
        <translation>Eszköz:</translation>
    </message>
    <message>
        <location/>
        <source>Screen Size:</source>
        <translation>Képernyőméret:</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/newformwidget.cpp" line="+93"/>
        <source>Default size</source>
        <translation>Alapértelmezett méret</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>QVGA portrait (240x320)</source>
        <translation>QVGA álló (240x320)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>QVGA landscape (320x240)</source>
        <translation>QVGA fekvő (320x240)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>VGA portrait (480x640)</source>
        <translation>VGA álló (480x640)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>VGA landscape (640x480)</source>
        <translation>VGA fekvő (640x480)</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Widgets</source>
        <extracomment>New Form Dialog Categories</extracomment>
        <translation>Felületi elemek</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Custom Widgets</source>
        <translation>Egyéni felületi elemek</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>None</source>
        <translation>Nincs</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Error loading form</source>
        <translation>Hiba az űrlap beöltésekor</translation>
    </message>
    <message>
        <location line="+249"/>
        <source>Unable to open the form template file &apos;%1&apos;: %2</source>
        <translation>Nem nyitható meg a(z) „%1” űrlapsablonfájl: %2</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>Internal error: No template selected.</source>
        <translation>Belső hiba: nem lett kiválasztva sablon.</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::NewPromotedClassPanel</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_promotiondialog.cpp" line="+78"/>
        <source>Add</source>
        <translation>Hozzáadás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New Promoted Class</source>
        <translation>Új előléptetett osztály</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Base class name:</source>
        <translation>Alaposztály neve:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Promoted class name:</source>
        <translation>Előléptetett osztály neve:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Header file:</source>
        <translation>Fejlécfájl:</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Global include</source>
        <translation>Globális felvétel</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Reset</source>
        <translation>Visszaállítás</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::ObjectInspector</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/objectinspector/objectinspector.cpp" line="+245"/>
        <source>Filter</source>
        <translation type="unfinished">Szűrő</translation>
    </message>
    <message>
        <location line="+74"/>
        <source>Change Current Page</source>
        <translation>Jelenlegi oldal módosítása</translation>
    </message>
    <message>
        <source>&amp;Find in Text...</source>
        <translation type="vanished">Ke&amp;resés a szövegben…</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::OrderDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/orderdialog.ui"/>
        <source>Change Page Order</source>
        <translation>Oldalsorrend módosítása</translation>
    </message>
    <message>
        <location/>
        <source>Page Order</source>
        <translation>Oldalsorrend</translation>
    </message>
    <message>
        <location/>
        <source>Move page up</source>
        <translation>Oldal mozgatása fel</translation>
    </message>
    <message>
        <location/>
        <source>Move page down</source>
        <translation>Oldal mozgatása le</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/orderdialog.cpp" line="+97"/>
        <source>Index %1 (%2)</source>
        <translation>Index %1 (%2)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::PaletteEditor</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/propertyeditor/paletteeditor.ui"/>
        <source>Edit Palette</source>
        <translation>Paletta szerkesztése</translation>
    </message>
    <message>
        <location/>
        <source>Tune Palette</source>
        <translation>Paletta hangolása</translation>
    </message>
    <message>
        <location/>
        <source>Show Details</source>
        <translation>Részletek megjelenítése</translation>
    </message>
    <message>
        <location/>
        <source>Compute Details</source>
        <translation>Részletek kiszámítása</translation>
    </message>
    <message>
        <location/>
        <source>Quick</source>
        <translation>Gyors</translation>
    </message>
    <message>
        <location/>
        <source>Preview</source>
        <translation>Előnézet</translation>
    </message>
    <message>
        <location/>
        <source>Disabled</source>
        <translation>Letiltva</translation>
    </message>
    <message>
        <location/>
        <source>Inactive</source>
        <translation>Inaktív</translation>
    </message>
    <message>
        <location/>
        <source>Active</source>
        <translation>Aktív</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/components/propertyeditor/paletteeditor.cpp" line="+71"/>
        <source>Save...</source>
        <translation type="unfinished">Mentés…</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Load...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+185"/>
        <source>Lighter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Darker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Copy color %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>QPalette UI file (*.xml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot open %1 for writing: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Cannot write %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Cannot read palette from %1:%2:%3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot open %1 for reading: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Invalid element &quot;%1&quot;, expected &quot;palette&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Save Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error Writing Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Load Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Error Reading Palette</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::PaletteEditorButton</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/propertyeditor/paletteeditorbutton.cpp" line="+44"/>
        <source>Change Palette</source>
        <translation>Paletta módosítása</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::PaletteModel</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/propertyeditor/paletteeditor.cpp" line="+147"/>
        <source>Color Role</source>
        <translation>Színszabály</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Active</source>
        <translation>Aktív</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Inactive</source>
        <translation>Inaktív</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Disabled</source>
        <translation>Letiltva</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::PixmapEditor</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/propertyeditor/designerpropertymanager.cpp" line="+83"/>
        <source>Choose Resource...</source>
        <translation>Erőforrás kiválasztása…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Choose File...</source>
        <translation>Fájl kiválasztása…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Set Icon From Theme...</source>
        <translation>Ikon beállítása témából…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Copy Path</source>
        <translation>Útvonal másolása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Paste Path</source>
        <translation>Útvonal beillesztése</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+18"/>
        <source>...</source>
        <translation>…</translation>
    </message>
    <message>
        <location line="+54"/>
        <source>[Theme] %1</source>
        <translation>[Téma] %1</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::PlainTextEditorDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/plaintexteditor.cpp" line="+52"/>
        <source>Edit text</source>
        <translation>Szöveg szerkesztése</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::PluginDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/plugindialog.cpp" line="+61"/>
        <source>Components</source>
        <translation>Összetevők</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Plugin Information</source>
        <translation>Bővítményinformációk</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Refresh</source>
        <translation>Frissítés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scan for newly installed custom widget plugins.</source>
        <translation>Újonnan telepített egyéni felületi elem bővítmények keresése.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Loaded Plugins</source>
        <translation>Betöltött bővítmények</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Failed Plugins</source>
        <translation>Hibás bővítmények</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Qt Designer couldn&apos;t find any plugins</source>
        <translation>A Qt Tervező nem talált semmilyen bővítményt</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Qt Designer found the following plugins</source>
        <translation>A Qt Tervező a következő bővítményeket találta</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>New custom widget plugins have been found.</source>
        <translation>Új egyéni felületi elem bővítmények találhatók.</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::PreviewActionGroup</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/previewactiongroup.cpp" line="+82"/>
        <source>%1 Style</source>
        <translation>%1 stílus</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::PreviewConfigurationWidget</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/previewconfigurationwidget.cpp" line="+122"/>
        <source>Default</source>
        <translation>Alapértelmezett</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>None</source>
        <translation>Nincs</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Browse...</source>
        <translation>Tallózás…</translation>
    </message>
    <message>
        <location line="+119"/>
        <source>Load Custom Device Skin</source>
        <translation>Egyéni eszközfelszín betöltése</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>All QVFB Skins (*.%1)</source>
        <translation>Összes QVFB felszín (*.%1)</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>%1 - Duplicate Skin</source>
        <translation>%1 – Kettőzött felszín</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The skin &apos;%1&apos; already exists.</source>
        <translation>A(z) %1 felszín már létezik.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>%1 - Error</source>
        <translation>%1 – Hiba</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>%1 is not a valid skin directory:
%2</source>
        <translation>A(z) %1 nem érvényes felszínkönyvtár:
%2</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::PreviewDeviceSkin</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/previewmanager.cpp" line="+248"/>
        <source>&amp;Portrait</source>
        <translation>&amp;Álló</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Landscape (&amp;CCW)</source>
        <extracomment>Rotate form preview counter-clockwise</extracomment>
        <translation>Fekvő (&amp;balra forgatva)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Landscape (CW)</source>
        <extracomment>Rotate form preview clockwise</extracomment>
        <translation>&amp;Fekvő (jobbra forgatva)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Close</source>
        <translation>&amp;Bezárás</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::PreviewManager</name>
    <message>
        <location line="+425"/>
        <source>%1 - [Preview]</source>
        <translation>%1 – [Előnézet]</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::PreviewMdiArea</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/propertyeditor/previewframe.cpp" line="+59"/>
        <source>The moose in the noose
ate the goose who was loose.</source>
        <extracomment>Palette editor background</extracomment>
        <translation>A gyapjas jak apja gyapja
gyakran gyatra gyapjas jak apa gyapja.</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::PreviewWidget</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/propertyeditor/previewwidget.ui"/>
        <source>Preview Window</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation>Előnézeti ablak</translation>
    </message>
    <message>
        <location/>
        <source>LineEdit</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation>Sorszerkesztés</translation>
    </message>
    <message>
        <location/>
        <source>ComboBox</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation>Legördülő menü</translation>
    </message>
    <message>
        <location/>
        <source>PushButton</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation>Nyomógomb</translation>
    </message>
    <message>
        <source>ButtonGroup2</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation type="vanished">2. gombcsoport</translation>
    </message>
    <message>
        <location/>
        <source>CheckBox1</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation>1. jelölőnégyzet</translation>
    </message>
    <message>
        <source>CheckBox2</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation type="vanished">2. jelölőnégyzet</translation>
    </message>
    <message>
        <source>ButtonGroup</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation type="vanished">Gombcsoport</translation>
    </message>
    <message>
        <location/>
        <source>RadioButton1</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation>1. választógomb</translation>
    </message>
    <message>
        <location/>
        <source>RadioButton2</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation>2. választógomb</translation>
    </message>
    <message>
        <location/>
        <source>RadioButton3</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation>3. választógomb</translation>
    </message>
    <message>
        <location/>
        <source>Buttons</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Tristate CheckBox</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>ToggleButton</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>ToolButton</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Menu</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation type="unfinished">Menü</translation>
    </message>
    <message>
        <location/>
        <source>Item Views</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Column 1</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Top Level 1</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Nested Item 1</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Nested Item 2</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Nested Item 3</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Simple Input Widgets</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Item1</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Item2</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Display Widgets</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>QLabel</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>QLabel with frame</source>
        <extracomment>Palette Editor Preview Widget</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/components/propertyeditor/previewwidget.cpp" line="+48"/>
        <source>Option 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Checkable</source>
        <translation type="unfinished">Bejelölhető</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::PromotionModel</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/promotionmodel.cpp" line="+17"/>
        <source>Name</source>
        <translation>Név</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Header file</source>
        <translation>Fejlécfájl</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Global include</source>
        <translation>Globális felvétel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Usage</source>
        <translation>Használat</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::PromotionTaskMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/promotiontaskmenu.cpp" line="+70"/>
        <source>Promoted widgets...</source>
        <translation>Előléptetett felületi elemek…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Promote to ...</source>
        <translation>Előléptetés erre…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Change signals/slots...</source>
        <translation>Jelzések és tárolóhelyek módosítása…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Promote to</source>
        <translation>Előléptetés erre</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Demote to %1</source>
        <translation>Lefokozás erre: %1</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::PropertyEditor</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/propertyeditor/propertyeditor.cpp" line="+208"/>
        <source>Add Dynamic Property...</source>
        <translation>Dinamikus tulajdonság hozzáadása…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove Dynamic Property</source>
        <translation>Dinamikus tulajdonság eltávolítása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sorting</source>
        <translation>Rendezés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Color Groups</source>
        <translation>Csoportok színezése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tree View</source>
        <translation>Fa nézet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Drop Down Button View</source>
        <translation>Legördülő gomb nézet</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>String...</source>
        <translation>Karakterlánc…</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Bool...</source>
        <translation>Logikai…</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Other...</source>
        <translation>Egyéb…</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Configure Property Editor</source>
        <translation>Tulajdonság-szerkesztő beállításai</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Filter</source>
        <translation>Szűrő</translation>
    </message>
    <message>
        <location line="+481"/>
        <source>Object: %1
Class: %2</source>
        <translation>Objektum: %1
Osztály: %2</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::PropertyLineEdit</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/propertylineedit.cpp" line="+74"/>
        <source>Insert line break</source>
        <translation>Sortörés beszúrása</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::QDesignerPromotionDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_promotiondialog.cpp" line="+90"/>
        <source>Promoted Widgets</source>
        <translation>Előléptetett felület elemek</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Promoted Classes</source>
        <translation>Előléptetett osztályok</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>Promote</source>
        <translation>Előléptetés</translation>
    </message>
    <message>
        <location line="+151"/>
        <source>Change signals/slots...</source>
        <translation>Jelzések és tárolóhelyek módosítása…</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>%1 - Error</source>
        <translation>%1 – Hiba</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::QDesignerResource</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/qdesigner_resource.cpp" line="+251"/>
        <source>Loading qrc file</source>
        <translation>Egy qrc fájl betöltése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The specified qrc file &lt;p&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/p&gt;&lt;p&gt;could not be found. Do you want to update the file location?&lt;/p&gt;</source>
        <translation>A megadott qrc fájl &lt;p&gt;&lt;b&gt;%1&lt;/b&gt;&lt;/p&gt;&lt;p&gt; nem található. Szeretné frissíteni a fájl helyét?&lt;/p&gt;</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>New location for %1</source>
        <translation>A(z) %1 új útvonala</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Resource files (*.qrc)</source>
        <translation>Erőforrásfájlok (*.qrc)</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::QDesignerTaskMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_taskmenu.cpp" line="+62"/>
        <source>Layout Alignment</source>
        <translation>Elrendezés igazítása</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>No Horizontal Alignment</source>
        <translation>Nincs vízszintes igazítás</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+178"/>
        <source>Left</source>
        <translation>Balra</translation>
    </message>
    <message>
        <location line="-177"/>
        <source>Center Horizontally</source>
        <translation>Vízszintesen középre</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+178"/>
        <source>Right</source>
        <translation>Jobbra</translation>
    </message>
    <message>
        <location line="-176"/>
        <source>No Vertical Alignment</source>
        <translation>Nincs függőleges igazítás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Top</source>
        <translation>Fentre</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Center Vertically</source>
        <translation>Függőlegesen középre</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+175"/>
        <source>Bottom</source>
        <translation>Lentre</translation>
    </message>
    <message>
        <location line="-68"/>
        <source>Change objectName...</source>
        <translation>Objektumnév módosítása…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Change toolTip...</source>
        <translation>Buboréksúgó módosítása…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Change whatsThis...</source>
        <translation>„Mi ez?” módosítása…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Change styleSheet...</source>
        <translation>Stíluslap módosítása…</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Create Menu Bar</source>
        <translation>Menüsor létrehozása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Tool Bar</source>
        <translation>Eszköztár hozzáadása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Tool Bar to Other Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Create Status Bar</source>
        <translation>Állapotsor létrehozása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Remove Status Bar</source>
        <translation>Állapotsor eltávolítása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Change signals/slots...</source>
        <translation>Jelzések és tárolóhelyek módosítása…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Go to slot...</source>
        <translation>Ugrás a tárolóhelyhez…</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Size Constraints</source>
        <translation>Méretkorlátozások</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Set Minimum Width</source>
        <translation>Legkisebb szélesség beállítása</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Set Minimum Height</source>
        <translation>Legkisebb magasság beállítása</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Set Minimum Size</source>
        <translation>Legkisebb méret beállítása</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Set Maximum Width</source>
        <translation>Legnagyobb szélesség beállítása</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Set Maximum Height</source>
        <translation>Legnagyobb magasság beállítása</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Set Maximum Size</source>
        <translation>Legnagyobb méret beállítása</translation>
    </message>
    <message>
        <location line="+242"/>
        <source>Edit ToolTip</source>
        <translation>Buboréksúgó szerkesztése</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Edit WhatsThis</source>
        <translation>„Mi ez?” szerkesztése</translation>
    </message>
    <message>
        <source>no signals available</source>
        <translation type="vanished">nincsenek elérhető jelzések</translation>
    </message>
    <message numerus="yes">
        <location line="+87"/>
        <source>Set size constraint on %n widget(s)</source>
        <translation>
            <numerusform>Méretkorlátozás beállítása %n felületi elemen</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::QDesignerWidgetBox</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_widgetbox.cpp" line="+195"/>
        <location line="+13"/>
        <source>Unexpected element &lt;%1&gt;</source>
        <translation>Váratlan &lt;%1&gt; elem</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>A parse error occurred at line %1, column %2 of the XML code specified for the widget %3: %4
%5</source>
        <translation>Feldolgozási hiba történt a(z) %3 felületi elemhez megadott XML kód %1. sorának %2. oszlopában: %4
%5</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The XML code specified for the widget %1 does not contain any widget elements.
%2</source>
        <translation>A(z) %1 felületi elemhez megadott XML kód nem tartalmaz egyetlen felületi elemet sem.
%2</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/components/widgetbox/widgetboxtreewidget.cpp" line="+345"/>
        <source>An error has been encountered at line %1 of %2: %3</source>
        <translation>Hiba történt a(z) %2 %1. sorában: %3</translation>
    </message>
    <message>
        <location line="+138"/>
        <source>Unexpected element &lt;%1&gt; encountered when parsing for &lt;widget&gt; or &lt;ui&gt;</source>
        <translation>Váratlan &lt;%1&gt; elem fordult elő a &lt;widget&gt; vagy az &lt;ui&gt; címkék feldolgozásakor</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Unexpected end of file encountered when parsing widgets.</source>
        <translation>Váratlan fájlvég fordult elő a felületi elemek feldolgozásakor.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>A widget element could not be found.</source>
        <translation>Egy felületi elem nem található.</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::QtGradientStopsController</name>
    <message>
        <location filename="../../qttools/src/shared/qtgradienteditor/qtgradientstopscontroller.cpp" line="+171"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>V</source>
        <translation>V</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+4"/>
        <source>Hue</source>
        <translation>Árny</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>Sat</source>
        <translation>Tel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Val</source>
        <translation>Ért</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Saturation</source>
        <translation>Telítettség</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Value</source>
        <translation>Érték</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>R</source>
        <translation>R</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>G</source>
        <translation>G</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Red</source>
        <translation>Vörös</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Green</source>
        <translation>Zöld</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Blue</source>
        <translation>Kék</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::RichTextEditorDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/richtexteditor.cpp" line="+487"/>
        <source>Edit text</source>
        <translation>Szöveg szerkesztése</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Rich Text</source>
        <translation>Formázott szöveg</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Source</source>
        <translation>Forrás</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;OK</source>
        <translation>&amp;OK</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Mégse</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::RichTextEditorToolBar</name>
    <message>
        <location line="-361"/>
        <source>Bold</source>
        <translation>Félkövér</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CTRL+B</source>
        <translation>CTRL+B</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Italic</source>
        <translation>Dőlt</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CTRL+I</source>
        <translation>CTRL+I</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Underline</source>
        <translation>Aláhúzott</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>CTRL+U</source>
        <translation>CTRL+U</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Left Align</source>
        <translation>Balra igazítás</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Center</source>
        <translation>Középre igazítás</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Right Align</source>
        <translation>Jobbra igazítás</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Justify</source>
        <translation>Sorkizárás</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Right to Left</source>
        <translation>Jobbról balra</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Superscript</source>
        <translation>Felső index</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Subscript</source>
        <translation>Alsó index</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Insert &amp;Link</source>
        <translation>&amp;Hivatkozás beszúrása</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Insert &amp;Image</source>
        <translation>&amp;Kép beszúrása</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Simplify Rich Text</source>
        <translation>Formázott szöveg egyszerűsítése</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::SignalSlotDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/signalslotdialog.cpp" line="+201"/>
        <source>There is already a slot with the signature &apos;%1&apos;.</source>
        <translation>Már létezik egy tárolóhely a(z) „%1” aláírással.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>There is already a signal with the signature &apos;%1&apos;.</source>
        <translation>Már létezik egy jelzés a(z) „%1” aláírással.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>%1 - Duplicate Signature</source>
        <translation>%1 – Kettőzött aláírás</translation>
    </message>
    <message>
        <location line="+21"/>
        <location line="+76"/>
        <source>Signals/Slots of %1</source>
        <translation>%1 jelzései és tárolóhelyei</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::SignalSlotEditorPlugin</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/signalsloteditor/signalsloteditor_plugin.cpp" line="+54"/>
        <source>Edit Signals/Slots</source>
        <translation>Jelzések és tárolóhelyek szerkesztése</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>F4</source>
        <translation>F4</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::SignalSlotEditorTool</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/signalsloteditor/signalsloteditor_tool.cpp" line="+45"/>
        <source>Edit Signals/Slots</source>
        <translation>Jelzések és tárolóhelyek szerkesztése</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::StatusBarTaskMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/toolbar_taskmenu.cpp" line="+68"/>
        <source>Remove</source>
        <translation>Eltávolítás</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::StringListEditorButton</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/propertyeditor/stringlisteditorbutton.cpp" line="+43"/>
        <source>Change String List</source>
        <translation>Karakterlánclista módosítása</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::StyleSheetEditorDialog</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/stylesheeteditor.cpp" line="+82"/>
        <location line="+295"/>
        <source>Valid Style Sheet</source>
        <translation>Érvényes stíluslap</translation>
    </message>
    <message>
        <location line="-293"/>
        <source>Add Resource...</source>
        <translation>Erőforrás hozzáadása…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Gradient...</source>
        <translation>Színátmenet hozzáadása…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Color...</source>
        <translation>Szín hozzáadása…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Add Font...</source>
        <translation>Betűkészlet hozzáadása…</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Edit Style Sheet</source>
        <translation>Stíluslap szerkesztése</translation>
    </message>
    <message>
        <location line="+291"/>
        <source>Invalid Style Sheet</source>
        <translation>Érvénytelen stíluslap</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::TabOrderEditor</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/tabordereditor/tabordereditor.cpp" line="+349"/>
        <source>Start from Here</source>
        <translation>Kezdés innen</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Restart</source>
        <translation>Újrakezdés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Tab Order List...</source>
        <translation>Lapsorrend lista…</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Tab Order List</source>
        <translation>Lapsorrend lista</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tab Order</source>
        <translation>Lapsorrend</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::TabOrderEditorPlugin</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/tabordereditor/tabordereditor_plugin.cpp" line="+55"/>
        <source>Edit Tab Order</source>
        <translation>Lapsorrend szerkesztése</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::TabOrderEditorTool</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/tabordereditor/tabordereditor_tool.cpp" line="+44"/>
        <source>Edit Tab Order</source>
        <translation>Lapsorrend szerkesztése</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::TableWidgetEditor</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/tablewidgeteditor.ui"/>
        <source>Edit Table Widget</source>
        <translation>Táblázat felületi elem szerkesztése</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Items</source>
        <translation>&amp;Elemek</translation>
    </message>
    <message>
        <location/>
        <source>Table Items</source>
        <translation>Táblázatelemek</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/designer/src/components/taskmenu/tablewidgeteditor.cpp" line="+235"/>
        <source>Properties &amp;&gt;&gt;</source>
        <translation>Tulajdonságok &amp;&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/tablewidgeteditor.cpp" line="-181"/>
        <source>New Column</source>
        <translation>Új oszlop</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>New Row</source>
        <translation>Új sor</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Columns</source>
        <translation>&amp;Oszlopok</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Rows</source>
        <translation>&amp;Sorok</translation>
    </message>
    <message>
        <location line="+169"/>
        <source>Properties &amp;&lt;&lt;</source>
        <translation>Tulajdonságok &amp;&lt;&lt;</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::TableWidgetTaskMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/tablewidget_taskmenu.cpp" line="+51"/>
        <source>Edit Items...</source>
        <translation>Elemek szerkesztése…</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::TemplateOptionsWidget</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/templateoptionspage.ui"/>
        <source>Form</source>
        <translation>Űrlap</translation>
    </message>
    <message>
        <location/>
        <source>Additional Template Paths</source>
        <translation>További sablonútvonalak</translation>
    </message>
    <message>
        <location/>
        <source>...</source>
        <translation>…</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/templateoptionspage.cpp" line="-18"/>
        <source>Pick a directory to save templates in</source>
        <translation>Válasszon egy könyvtárat, amelybe a sablonokat menti</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::TextEditTaskMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/textedit_taskmenu.cpp" line="+45"/>
        <source>Edit HTML</source>
        <translation>HTML szerkesztése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Change HTML...</source>
        <translation>HTML módosítása…</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Edit Text</source>
        <translation>Szöveg szerkesztése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Change Plain Text...</source>
        <translation>Egyszerű szöveg módosítása…</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::TextEditor</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/propertyeditor/designerpropertymanager.cpp" line="-337"/>
        <source>Choose Resource...</source>
        <translation>Erőforrás kiválasztása…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Choose File...</source>
        <translation>Fájl kiválasztása…</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>...</source>
        <translation>…</translation>
    </message>
    <message>
        <location line="+136"/>
        <source>Choose a File</source>
        <translation>Válasszon egy fájlt</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::ToolBarEventFilter</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/qdesigner_toolbar.cpp" line="+133"/>
        <source>Insert Separator before &apos;%1&apos;</source>
        <translation>Elválasztó beszúrása a(z) „%1” elé</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Append Separator</source>
        <translation>Elválasztó hozzáfűzése</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Remove action &apos;%1&apos;</source>
        <translation>„%1” művelet eltávolítása</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Remove Toolbar &apos;%1&apos;</source>
        <translation>„%1” eszköztár eltávolítása</translation>
    </message>
    <message>
        <location line="+58"/>
        <source>Insert Separator</source>
        <translation>Elválasztó beszúrása</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::TreeWidgetEditor</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/treewidgeteditor.ui"/>
        <source>Edit Tree Widget</source>
        <translation>Fa felületi elem szerkesztése</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Items</source>
        <translation>&amp;Elemek</translation>
    </message>
    <message>
        <location/>
        <source>Tree Items</source>
        <translation>Faelemek</translation>
    </message>
    <message>
        <location/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/designer/src/components/taskmenu/treewidgeteditor.cpp" line="+217"/>
        <source>New Item</source>
        <translation>Új elem</translation>
    </message>
    <message>
        <location/>
        <source>&amp;New</source>
        <translation>Ú&amp;j</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/designer/src/components/taskmenu/treewidgeteditor.cpp" line="+19"/>
        <source>New Subitem</source>
        <translation>Új alelem</translation>
    </message>
    <message>
        <location/>
        <source>New &amp;Subitem</source>
        <translation>Új &amp;alelem</translation>
    </message>
    <message>
        <location/>
        <source>Delete Item</source>
        <translation>Elem törlése</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Delete</source>
        <translation>&amp;Törlés</translation>
    </message>
    <message>
        <location/>
        <source>Move Item Left (before Parent Item)</source>
        <translation>Elem mozgatása balra (a szülőelem elé)</translation>
    </message>
    <message>
        <location/>
        <source>L</source>
        <translation>B</translation>
    </message>
    <message>
        <location/>
        <source>Move Item Right (as a First Subitem of the Next Sibling Item)</source>
        <translation>Elem mozgatása jobbra (a következő testvérelem első alelemeként)</translation>
    </message>
    <message>
        <location/>
        <source>R</source>
        <translation>J</translation>
    </message>
    <message>
        <location/>
        <source>Move Item Up</source>
        <translation>Elem mozgatása fel</translation>
    </message>
    <message>
        <location/>
        <source>U</source>
        <translation>F</translation>
    </message>
    <message>
        <location/>
        <source>Move Item Down</source>
        <translation>Elem mozgatása le</translation>
    </message>
    <message>
        <location/>
        <source>D</source>
        <translation>L</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/designer/src/components/taskmenu/treewidgeteditor.cpp" line="+176"/>
        <source>Properties &amp;&gt;&gt;</source>
        <translation>Tulajdonságok &amp;&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/treewidgeteditor.cpp" line="-357"/>
        <source>New Column</source>
        <translation>Új oszlop</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&amp;Columns</source>
        <translation>&amp;Oszlopok</translation>
    </message>
    <message>
        <location line="+94"/>
        <source>Per column properties</source>
        <translation>Oszloponkénti tulajdonságok</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Common properties</source>
        <translation>Közös tulajdonságok</translation>
    </message>
    <message>
        <location line="+254"/>
        <source>Properties &amp;&lt;&lt;</source>
        <translation>Tulajdonságok &amp;&lt;&lt;</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::TreeWidgetTaskMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/treewidget_taskmenu.cpp" line="+50"/>
        <source>Edit Items...</source>
        <translation>Elemek szerkesztése…</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::WidgetBox</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/widgetbox/widgetbox.cpp" line="+106"/>
        <source>Filter</source>
        <translation>Szűrő</translation>
    </message>
    <message>
        <location filename="../../qttools/src/designer/src/components/widgetbox/widgetbox_dnditem.cpp" line="+102"/>
        <source>Warning: Widget creation failed in the widget box. This could be caused by invalid custom widget XML.</source>
        <translation>Figyelmeztetés: a felületi elem létrehozása sikertelen a felületi elem dobozban. Ezt egy érvénytelen egyéni felületi elem XML okozhatta.</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::WidgetBoxTreeWidget</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/widgetbox/widgetboxtreewidget.cpp" line="-271"/>
        <source>Scratchpad</source>
        <translation>Jegyzettömb</translation>
    </message>
    <message>
        <location line="+371"/>
        <source>Custom Widgets</source>
        <translation>Egyéni felületi elemek</translation>
    </message>
    <message>
        <location line="+262"/>
        <source>Expand all</source>
        <translation>Összes kinyitása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Collapse all</source>
        <translation>Összes összecsukása</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>List View</source>
        <translation>Lista nézet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Icon View</source>
        <translation>Ikon nézet</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Remove</source>
        <translation>Eltávolítás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Edit name</source>
        <translation>Név szerkesztése</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::WidgetDataBase</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/widgetdatabase.cpp" line="-405"/>
        <source>A custom widget plugin whose class name (%1) matches that of an existing class has been found.</source>
        <translation type="unfinished">Egy egyéni felületi elem bővítmény, amely osztályneve (%1) megegyezik azzal, hogy egy létező osztály található.</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::WidgetEditorTool</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/formeditor/tool_widgeteditor.cpp" line="+54"/>
        <source>Edit Widgets</source>
        <translation>Felületi elemek szerkesztése</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::WidgetFactory</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/widgetfactory.cpp" line="-131"/>
        <source>The custom widget factory registered for widgets of class %1 returned 0.</source>
        <translation>A(z) %1 osztályú felületi elemekhez regisztrált egyéni felületi elem gyár 0-val tért vissza.</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>A class name mismatch occurred when creating a widget using the custom widget factory registered for widgets of class %1. It returned a widget of class %2.</source>
        <translation>Osztálynév eltérés történt a felületi elem létrehozásánál a(z) %1 osztályú felületi elemekhez regisztrált egyéni felületi elem gyár használatakor. A gyár %2 osztályú felületi elemet adott vissza.</translation>
    </message>
    <message>
        <location line="+186"/>
        <source>The current page of the container &apos;%1&apos; (%2) could not be determined while creating a layout.This indicates an inconsistency in the ui-file, probably a layout being constructed on a container widget.</source>
        <translation>A(z) „%1” (%2) konténer jelenlegi oldalát nem sikerült meghatározni az elrendezés létrehozása során. Ez a felhasználói felület fájlban lévő következetlenséget jelzi, valószínűleg egy elrendezés hoztak létre egy konténer felületi elemen.</translation>
    </message>
    <message>
        <location line="+38"/>
        <source>Attempt to add a layout to a widget &apos;%1&apos; (%2) which already has an unmanaged layout of type %3.
This indicates an inconsistency in the ui-file.</source>
        <translation>Egy olyan „%1” (%2) felületi elemhez próbál elrendezést hozzáadni, amely már tartalmaz egy %3 típusú kezeletlen elrendezést.
Ez a felhasználói felület fájlban lévő következetlenséget jelzi.</translation>
    </message>
    <message>
        <location line="+201"/>
        <source>Cannot create style &apos;%1&apos;.</source>
        <translation>Nem hozható létre a(z) „%1” stílus.</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::WizardContainerWidgetTaskMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/components/taskmenu/containerwidget_taskmenu.cpp" line="-39"/>
        <source>Next</source>
        <translation>Következő</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Back</source>
        <translation>Vissza</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::ZoomMenu</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/zoomwidget.cpp" line="+61"/>
        <source>%1 %</source>
        <extracomment>Zoom factor</extracomment>
        <translation>%1 %</translation>
    </message>
</context>
<context>
    <name>qdesigner_internal::ZoomablePreviewDeviceSkin</name>
    <message>
        <location filename="../../qttools/src/designer/src/lib/shared/previewmanager.cpp" line="-269"/>
        <source>&amp;Zoom</source>
        <translation>&amp;Nagyítás</translation>
    </message>
</context>
</TS>
