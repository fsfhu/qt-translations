<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu_HU">
<context>
    <name>AudioContainerControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/audiocapture/audiocontainercontrol.cpp" line="+75"/>
        <source>RAW (headerless) file format</source>
        <translation>RAW (fejléc nélküli) fájlformátum</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>WAV file format</source>
        <translation>WAV fájlformátum</translation>
    </message>
</context>
<context>
    <name>AudioEncoderControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/audiocapture/audioencodercontrol.cpp" line="+99"/>
        <source>Linear PCM audio data</source>
        <translation>Lineáris PCM hangadatok</translation>
    </message>
</context>
<context>
    <name>BbCameraAudioEncoderSettingsControl</name>
    <message>
        <source>No compression</source>
        <translation type="vanished">Nincs tömörítés</translation>
    </message>
    <message>
        <source>AAC compression</source>
        <translation type="vanished">AAC tömörítés</translation>
    </message>
    <message>
        <source>PCM uncompressed</source>
        <translation type="vanished">PCM tömörítetlen</translation>
    </message>
</context>
<context>
    <name>BbCameraMediaRecorderControl</name>
    <message>
        <source>Unable to retrieve mute status</source>
        <translation type="vanished">Nem lehet lekérni a némítási állapotot</translation>
    </message>
    <message>
        <source>Unable to retrieve audio input volume</source>
        <translation type="vanished">Nem lehet lekérni a hangbemenet hangerejét</translation>
    </message>
    <message>
        <source>Unable to set mute status</source>
        <translation type="vanished">Nem állítható be a némítási állapot</translation>
    </message>
    <message>
        <source>Unable to set audio input volume</source>
        <translation type="vanished">Nem állítható be a hangbemenet hangereje</translation>
    </message>
</context>
<context>
    <name>BbCameraSession</name>
    <message>
        <source>Camera provides image in unsupported format</source>
        <translation type="vanished">A kamera nem támogatott formátumban szolgáltatja a képet</translation>
    </message>
    <message>
        <source>Could not load JPEG data from frame</source>
        <translation type="vanished">Nem sikerült betölteni a JPEG adatokat a keretből</translation>
    </message>
    <message>
        <source>Camera not ready</source>
        <translation type="vanished">A kamera nem áll készen</translation>
    </message>
    <message>
        <source>Unable to apply video settings</source>
        <translation type="vanished">Nem alkalmazhatók a videobeállítások</translation>
    </message>
    <message>
        <source>Could not open destination file:
%1</source>
        <translation type="vanished">Nem sikerült megnyitni a célfájlt:
%1</translation>
    </message>
    <message>
        <source>Unable to open camera</source>
        <translation type="vanished">Nem nyitható meg a kamera</translation>
    </message>
    <message>
        <source>Unable to retrieve native camera orientation</source>
        <translation type="vanished">Nem lehet lekérni a natív kameratájolást</translation>
    </message>
    <message>
        <source>Unable to close camera</source>
        <translation type="vanished">Nem zárható be a kamera</translation>
    </message>
    <message>
        <source>Unable to start video recording</source>
        <translation type="vanished">Nem indítható el a videorögzítés</translation>
    </message>
    <message>
        <source>Unable to stop video recording</source>
        <translation type="vanished">Nem állítható le a videorögzítés</translation>
    </message>
</context>
<context>
    <name>BbCameraVideoEncoderSettingsControl</name>
    <message>
        <source>No compression</source>
        <translation type="vanished">Nincs tömörítés</translation>
    </message>
    <message>
        <source>AVC1 compression</source>
        <translation type="vanished">AVC1 tömörítés</translation>
    </message>
    <message>
        <source>H264 compression</source>
        <translation type="vanished">H264 tömörítés</translation>
    </message>
</context>
<context>
    <name>BbImageEncoderControl</name>
    <message>
        <source>JPEG image</source>
        <translation type="vanished">JPEG-kép</translation>
    </message>
</context>
<context>
    <name>BbVideoDeviceSelectorControl</name>
    <message>
        <source>Front Camera</source>
        <translation type="vanished">Előlapi kamera</translation>
    </message>
    <message>
        <source>Rear Camera</source>
        <translation type="vanished">Hátsó kamera</translation>
    </message>
    <message>
        <source>Desktop Camera</source>
        <translation type="vanished">Asztali kamera</translation>
    </message>
</context>
<context>
    <name>CameraBinImageCapture</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/camerabin/camerabinimagecapture.cpp" line="+92"/>
        <source>Camera not ready</source>
        <translation>A kamera nem áll készen</translation>
    </message>
</context>
<context>
    <name>CameraBinImageEncoder</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/camerabin/camerabinimageencoder.cpp" line="+72"/>
        <source>JPEG image</source>
        <translation>JPEG-kép</translation>
    </message>
</context>
<context>
    <name>CameraBinRecorder</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/camerabin/camerabinrecorder.cpp" line="+253"/>
        <source>QMediaRecorder::pause() is not supported by camerabin2.</source>
        <translation>A camerabin2 nem támogatja a QMediaRecorder::pause() függvényt.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Service has not been started</source>
        <translation>A szolgáltatás nem indult el</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Recording permissions are not available</source>
        <translation>A felvételi jogosultságok nem érhetők el</translation>
    </message>
</context>
<context>
    <name>CameraBinSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/camerabin/camerabinsession.cpp" line="+1068"/>
        <source>Camera error</source>
        <translation>Kamerahiba</translation>
    </message>
</context>
<context>
    <name>DSCameraSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/directshow/camera/dscamerasession.cpp" line="+453"/>
        <source>Failed to configure preview format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Failed to connect graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+39"/>
        <source>Failed to get stream control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-32"/>
        <source>Failed to start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Failed to stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Camera not ready for capture</source>
        <translation>A kamera nem áll készen a felvételhez</translation>
    </message>
    <message>
        <location line="+107"/>
        <source>Could not save image to file.</source>
        <translation>Nem sikerült elmenteni a képet fájlba.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Failed to create filter graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Failed to create graph builder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Failed to connect capture graph and filter graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+69"/>
        <source>No capture device found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Failed to create null renderer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MFPlayerSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/wmf/player/mfplayersession.cpp" line="+209"/>
        <source>Invalid stream source.</source>
        <translation>Érvénytelen adatfolyamforrás.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Attempting to play invalid Qt resource.</source>
        <translation>Érvénytelen Qt erőforrás lejátszásának kísérlete.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The system cannot find the file specified.</source>
        <translation>A rendszer nem találja a megadott fájlt.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>The specified server could not be found.</source>
        <translation>A megadott kiszolgáló nem található.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Unsupported media type.</source>
        <translation>Nem támogatott médiatípus.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Failed to load source.</source>
        <translation>A forrás betöltése sikertelen.</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Cannot create presentation descriptor.</source>
        <translation>Nem lehet létrehozni bemutatóleírót.</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Failed to get stream count.</source>
        <translation>Az adatfolyam darabszám lekérése sikertelen.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Failed to create topology.</source>
        <translation>A topológia létrehozása sikertelen.</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Unable to play any stream.</source>
        <translation>Nem lehet lejátszani semmilyen adatfolyamot.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Unable to play.</source>
        <translation>Nem játszható le.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Failed to set topology.</source>
        <translation>A topológia beállítása sikertelen.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Unknown stream type.</source>
        <translation>Ismeretlen adatfolyamtípus.</translation>
    </message>
    <message>
        <location line="+553"/>
        <source>Failed to stop.</source>
        <translation>A leállítás sikertelen.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>failed to start playback</source>
        <translation>a lejátszás indítása sikertelen</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Failed to pause.</source>
        <translation>A szüneteltetés sikertelen.</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Unable to create mediasession.</source>
        <translation>Nem hozható létre média-munkamenet.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to pull session events.</source>
        <translation>Nem kérhetők le a munkamenet-események.</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>Failed to seek.</source>
        <translation>A tekerés sikertelen.</translation>
    </message>
    <message>
        <location line="+393"/>
        <source>Media session non-fatal error.</source>
        <translation>Nem végzetes média-munkamenet hiba.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Media session serious error.</source>
        <translation>Komoly média-munkamenet hiba.</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Unsupported media, a codec is missing.</source>
        <translation>Nem támogatott média, egy kodek hiányzik.</translation>
    </message>
</context>
<context>
    <name>QAndroidAudioEncoderSettingsControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/android/src/mediacapture/qandroidaudioencodersettingscontrol.cpp" line="+60"/>
        <source>Adaptive Multi-Rate Narrowband (AMR-NB) audio codec</source>
        <translation>Alkalmazkodó többes gyakoriságú keskeny sávú (AMR-NB) hangkodek</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Adaptive Multi-Rate Wideband (AMR-WB) audio codec</source>
        <translation>Alkalmazkodó többes gyakoriságú széles sávú (AMR-WB) hangkodek</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>AAC Low Complexity (AAC-LC) audio codec</source>
        <translation>AAC alacsony bonyolultságú (AAC-LC) hangkodek</translation>
    </message>
</context>
<context>
    <name>QAndroidCameraSession</name>
    <message>
        <source>Camera cannot be started without a viewfinder.</source>
        <translation type="vanished">A kamerát nem lehet elindítani a fényképező keresője nélkül.</translation>
    </message>
    <message>
        <location filename="../../qtmultimedia/src/plugins/android/src/mediacapture/qandroidcamerasession.cpp" line="+678"/>
        <source>Camera not ready</source>
        <translation>A kamera nem áll készen</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Drive mode not supported</source>
        <extracomment>Drive mode is the camera&apos;s shutter mode, for example single shot, continuos exposure, etc.</extracomment>
        <translation>A meghajtómód nem támogatott</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Failed to capture image</source>
        <translation>A kép rögzítése sikertelen</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Camera preview failed to start.</source>
        <translation>A kamera előnézetének indítása sikertelen.</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>Could not open destination file: %1</source>
        <translation>Nem sikerült megnyitni a célfájlt: %1</translation>
    </message>
</context>
<context>
    <name>QAndroidImageEncoderControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/android/src/mediacapture/qandroidimageencodercontrol.cpp" line="+63"/>
        <source>JPEG image</source>
        <translation>JPEG-kép</translation>
    </message>
</context>
<context>
    <name>QAndroidMediaContainerControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/android/src/mediacapture/qandroidmediacontainercontrol.cpp" line="+73"/>
        <source>MPEG4 media file format</source>
        <translation>MPEG4 médiafájl-formátum</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>3GPP media file format</source>
        <translation>3GPP médiafájl-formátum</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>AMR NB file format</source>
        <translation>AMR NB fájlformátum</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>AMR WB file format</source>
        <translation>AMR WB fájlformátum</translation>
    </message>
</context>
<context>
    <name>QAndroidVideoEncoderSettingsControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/android/src/mediacapture/qandroidvideoencodersettingscontrol.cpp" line="+78"/>
        <source>H.263 compression</source>
        <translation>H.263 tömörítés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>H.264 compression</source>
        <translation>H.264 tömörítés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>MPEG-4 SP compression</source>
        <translation>MPEG-4 SP tömörítés</translation>
    </message>
</context>
<context>
    <name>QAudioDecoder</name>
    <message>
        <location filename="../../qtmultimedia/src/multimedia/audio/qaudiodecoder.cpp" line="+153"/>
        <location line="+58"/>
        <source>The QAudioDecoder object does not have a valid service</source>
        <translation>A QAudioDecoder objektumnak nincs érvényes szolgáltatása</translation>
    </message>
</context>
<context>
    <name>QCamera</name>
    <message>
        <location filename="../../qtmultimedia/src/multimedia/camera/qcamera.cpp" line="+121"/>
        <location line="+97"/>
        <location line="+160"/>
        <source>The camera service is missing</source>
        <translation>A kameraszolgáltatás hiányzik</translation>
    </message>
</context>
<context>
    <name>QCameraImageCapture</name>
    <message>
        <location filename="../../qtmultimedia/src/multimedia/camera/qcameraimagecapture.cpp" line="+547"/>
        <location line="+22"/>
        <source>Device does not support images capture.</source>
        <translation>Az eszköz nem támogatja a képek rögzítését.</translation>
    </message>
</context>
<context>
    <name>QDeclarativeAudio</name>
    <message>
        <location filename="../../qtmultimedia/src/imports/multimedia/qdeclarativeaudio.cpp" line="+531"/>
        <source>volume should be between 0.0 and 1.0</source>
        <translation>a hangerőnek 0,0 és 1,0 között kell lennie</translation>
    </message>
</context>
<context>
    <name>QGstreamerAudioDecoderSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/audiodecoder/qgstreameraudiodecodersession.cpp" line="+227"/>
        <source>Cannot play stream of type: &lt;unknown&gt;</source>
        <translation>Nem lehet lejátszani az ilyen típusú adatfolyamot: &lt;ismeretlen&gt;</translation>
    </message>
</context>
<context>
    <name>QGstreamerAudioEncode</name>
    <message>
        <source>Raw PCM audio</source>
        <translation type="vanished">Nyers PCM hang</translation>
    </message>
</context>
<context>
    <name>QGstreamerAudioInputSelector</name>
    <message>
        <location filename="../../qtmultimedia/src/gsttools/qgstreameraudioinputselector.cpp" line="+108"/>
        <source>System default device</source>
        <translation>A rendszer alapértelmezett eszköze</translation>
    </message>
</context>
<context>
    <name>QGstreamerCameraControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediacapture/qgstreamercameracontrol.cpp" line="+126"/>
        <source>State not supported.</source>
        <translation>Az állapot nem támogatott.</translation>
    </message>
</context>
<context>
    <name>QGstreamerCaptureSession</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediacapture/qgstreamercapturesession.cpp" line="+232"/>
        <source>Could not create an audio source element</source>
        <translation>Nem sikerült létrehozni egy hangforrás elemet</translation>
    </message>
    <message>
        <location line="+402"/>
        <source>Failed to build media capture pipeline.</source>
        <translation>A médiarögzítés csővezeték összeállítása sikertelen.</translation>
    </message>
</context>
<context>
    <name>QGstreamerImageCaptureControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediacapture/qgstreamerimagecapturecontrol.cpp" line="+74"/>
        <source>Not ready to capture</source>
        <translation>Nem áll készen a rögzítésre</translation>
    </message>
</context>
<context>
    <name>QGstreamerImageEncode</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediacapture/qgstreamerimageencode.cpp" line="+72"/>
        <source>JPEG image encoder</source>
        <translation>JPEG-kép kódoló</translation>
    </message>
</context>
<context>
    <name>QGstreamerPlayerControl</name>
    <message>
        <location filename="../../qtmultimedia/src/gsttools/qgstreamerplayercontrol.cpp" line="+378"/>
        <source>Attempting to play invalid user stream</source>
        <translation>Érvénytelen felhasználói adatfolyam lejátszásának kísérlete</translation>
    </message>
</context>
<context>
    <name>QGstreamerPlayerSession</name>
    <message>
        <location filename="../../qtmultimedia/src/gsttools/qgstreamerplayersession.cpp" line="+1288"/>
        <location line="+125"/>
        <source>Cannot play stream of type: &lt;unknown&gt;</source>
        <translation>Nem lehet lejátszani az ilyen típusú adatfolyamot: &lt;ismeretlen&gt;</translation>
    </message>
    <message>
        <location line="-11"/>
        <source>UDP source timeout</source>
        <translation>UDP forrás időtúllépés</translation>
    </message>
    <message>
        <source>Media is loaded as a playlist</source>
        <translation type="vanished">A média betöltve lejátszólistaként</translation>
    </message>
</context>
<context>
    <name>QGstreamerRecorderControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/gstreamer/mediacapture/qgstreamerrecordercontrol.cpp" line="+169"/>
        <location line="+21"/>
        <source>Service has not been started</source>
        <translation>A szolgáltatás nem indult el</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>Not compatible codecs and container format.</source>
        <translation>Nem összeegyeztethető kodekek és konténerformátum.</translation>
    </message>
</context>
<context>
    <name>QGstreamerVideoInputDeviceControl</name>
    <message>
        <location filename="../../qtmultimedia/src/gsttools/qgstreamervideoinputdevicecontrol_p.h" line="+79"/>
        <source>Main camera</source>
        <translation>Fő kamera</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Front camera</source>
        <translation>Előlapi kamera</translation>
    </message>
</context>
<context>
    <name>QMediaPlayer</name>
    <message>
        <location filename="../../qtmultimedia/src/multimedia/playback/qmediaplayer.cpp" line="+364"/>
        <source>Attempting to play invalid Qt resource</source>
        <translation>Érvénytelen Qt erőforrás lejátszásának kísérlete</translation>
    </message>
    <message>
        <location line="+568"/>
        <source>The QMediaPlayer object does not have a valid service</source>
        <translation>A QMediaPlayer objektumnak nincs érvényes szolgáltatása</translation>
    </message>
</context>
<context>
    <name>QMediaPlaylist</name>
    <message>
        <location filename="../../qtmultimedia/src/multimedia/playback/qmediaplaylist.cpp" line="+537"/>
        <location line="+62"/>
        <source>Could not add items to read only playlist.</source>
        <translation>Nem sikerült elemeket hozzáadni egy csak olvasható lejátszólistához.</translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+62"/>
        <source>Playlist format is not supported</source>
        <translation>A lejátszólista formátuma nem támogatott</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>The file could not be accessed.</source>
        <translation>A fájlt nem sikerült elérni.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Playlist format is not supported.</source>
        <translation>A lejátszólista formátuma nem támogatott.</translation>
    </message>
</context>
<context>
    <name>QMultimediaDeclarativeModule</name>
    <message>
        <location filename="../../qtmultimedia/src/imports/multimedia/multimedia.cpp" line="+96"/>
        <location line="+48"/>
        <source>CameraCapture is provided by Camera</source>
        <translation>A CameraCapture értéket a kamera biztosítja</translation>
    </message>
    <message>
        <location line="-46"/>
        <source>CameraRecorder is provided by Camera</source>
        <translation>A CameraRecorder értéket a kamera biztosítja</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+54"/>
        <source>CameraExposure is provided by Camera</source>
        <translation>A CameraExposure értéket a kamera biztosítja</translation>
    </message>
    <message>
        <location line="-52"/>
        <location line="+50"/>
        <source>CameraFocus is provided by Camera</source>
        <translation>A CameraFocus értéket a kamera biztosítja</translation>
    </message>
    <message>
        <location line="-48"/>
        <location line="+42"/>
        <source>CameraFlash is provided by Camera</source>
        <translation>A CameraFlash értéket a kamera biztosítja</translation>
    </message>
    <message>
        <location line="-40"/>
        <location line="+17"/>
        <location line="+12"/>
        <location line="+21"/>
        <source>CameraImageProcessing is provided by Camera</source>
        <translation>A CameraImageProcessing értéket a kamera biztosítja</translation>
    </message>
    <message>
        <location line="-36"/>
        <source>CameraViewfinder is provided by Camera</source>
        <translation>A CameraViewfinder értéket a kamera biztosítja</translation>
    </message>
</context>
<context>
    <name>QPlaylistFileParser</name>
    <message>
        <location filename="../../qtmultimedia/src/multimedia/playback/qplaylistfileparser.cpp" line="+333"/>
        <source>%1 playlist type is unknown</source>
        <translation>%1 lejátszólista-típus ismeretlen</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>invalid line in playlist file</source>
        <translation>érvénytelen sor a lejátszólista fájlban</translation>
    </message>
    <message>
        <location line="+122"/>
        <source>Invalid stream</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Empty file provided</source>
        <translation>Üres fájl lett megadva</translation>
    </message>
    <message>
        <location line="-45"/>
        <source>%1 does not exist</source>
        <translation>%1 nem létezik</translation>
    </message>
</context>
<context>
    <name>QWinRTCameraImageCaptureControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/winrt/qwinrtcameraimagecapturecontrol.cpp" line="+166"/>
        <source>Camera not ready</source>
        <translation>A kamera nem áll készen</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>Invalid photo data length.</source>
        <translation>Érvénytelen fényképadathossz.</translation>
    </message>
    <message>
        <location line="+75"/>
        <source>Image saving failed</source>
        <translation>A képmentés sikertelen</translation>
    </message>
</context>
<context>
    <name>QWinRTImageEncoderControl</name>
    <message>
        <location filename="../../qtmultimedia/src/plugins/winrt/qwinrtimageencodercontrol.cpp" line="+63"/>
        <source>JPEG image</source>
        <translation>JPEG-kép</translation>
    </message>
</context>
</TS>
