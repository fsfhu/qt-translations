<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu_HU">
<context>
    <name>QQuickPlatformDialog</name>
    <message>
        <location filename="../../qtquickcontrols2/src/imports/platform/qtlabsplatformplugin.cpp" line="+83"/>
        <source>Dialog is an abstract base class</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Cannot create an instance of StandardButton</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QtLabsMaterialStylePlugin</name>
    <message>
        <source>Material is an attached property</source>
        <translation type="vanished">Az anyag egy csatolt tulajdonság</translation>
    </message>
</context>
<context>
    <name>QtLabsUniversalStylePlugin</name>
    <message>
        <source>Universal is an attached property</source>
        <translation type="vanished">Az univerzális egy csatolt tulajdonság</translation>
    </message>
</context>
<context>
    <name>QtQuickControls2ImagineStylePlugin</name>
    <message>
        <location filename="../../qtquickcontrols2/src/imports/controls/imagine/qtquickcontrols2imaginestyleplugin.cpp" line="+71"/>
        <source>Imagine is an attached property</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QtQuickControls2MaterialStylePlugin</name>
    <message>
        <location filename="../../qtquickcontrols2/src/imports/controls/material/qtquickcontrols2materialstyleplugin.cpp" line="+72"/>
        <source>Material is an attached property</source>
        <translation type="unfinished">Az anyag egy csatolt tulajdonság</translation>
    </message>
</context>
<context>
    <name>QtQuickControls2UniversalStylePlugin</name>
    <message>
        <location filename="../../qtquickcontrols2/src/imports/controls/universal/qtquickcontrols2universalstyleplugin.cpp" line="+69"/>
        <source>Universal is an attached property</source>
        <translation type="unfinished">Az univerzális egy csatolt tulajdonság</translation>
    </message>
</context>
</TS>
