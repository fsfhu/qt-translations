#!/bin/sh
for f in *_hu.ts; do
  file=`basename $f .ts`
  project=`basename $file _hu`
  ts2po $file.ts -o $file.po
  sed -i "s/Project-Id-Version: PACKAGE VERSION/Project-Id-Version: ${project} 5.15/g" $file.po
  sed -i "s/Last-Translator: FULL NAME <EMAIL@ADDRESS>/Last-Translator: Meskó Balázs <mesko.balazs@fsf.hu>/g" $file.po
  sed -i "s/Language-Team: LANGUAGE <LL@li.org>/Language-Team: hu <hu@l10n-kde.org>/g" $file.po
done
