<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu_HU">
<context>
    <name>QQuickXmlListModel</name>
    <message>
        <location filename="../../qtxmlpatterns/src/imports/xmllistmodel/qqmlxmllistmodel.cpp" line="+608"/>
        <source>&quot;%1&quot; duplicates a previous role name and will be disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+559"/>
        <location line="+4"/>
        <source>invalid query: &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQuickXmlListModelRole</name>
    <message>
        <location filename="../../qtxmlpatterns/src/imports/xmllistmodel/qqmlxmllistmodel_p.h" line="+182"/>
        <source>An XmlRole query must not start with &apos;/&apos;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQuickXmlRoleList</name>
    <message>
        <location filename="../../qtxmlpatterns/src/imports/xmllistmodel/qqmlxmllistmodel.cpp" line="-313"/>
        <source>An XmlListModel query must start with &apos;/&apos; or &quot;//&quot;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QXmlPatternistCLI</name>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/api/qcoloringmessagehandler.cpp" line="+85"/>
        <source>Warning in %1, at line %2, column %3: %4</source>
        <translation>Figyelmeztetés a(z) %1 %2. sorának %3. oszlopában: %4</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Warning in %1: %2</source>
        <translation>Figyelmeztetés a(z) %1 fájlban: %2</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Unknown location</source>
        <translation>Ismeretlen hely</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Error %1 in %2, at line %3, column %4: %5</source>
        <translation>%1 hiba a(z) %2 fájl %3. sorának %4. oszlopában: %5</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Error %1 in %2: %3</source>
        <translation>%1 hiba a(z) %2 fájlban: %3</translation>
    </message>
</context>
<context>
    <name>QtXmlPatterns</name>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/acceltree/qacceltreebuilder_tpl_p.h" line="+225"/>
        <source>An %1-attribute with value %2 has already been declared.</source>
        <translation>Egy %2 értékkel rendelkező %1-attribútum már deklarálva van.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>An %1-attribute must have a valid %2 as value, which %3 isn&apos;t.</source>
        <translation>Egy %1-attribútumnak érvényes %2 értékkel kell rendelkeznie, azonban %3 nem az.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/acceltree/qacceltreeresourceloader.cpp" line="+339"/>
        <source>%1 is an unsupported encoding.</source>
        <translation>A(z) %1 egy nem támogatott kódolás.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>%1 contains octets which are disallowed in the requested encoding %2.</source>
        <translation>A(z) %1 olyan okteteket tartalmaz, amelyek nem megengedettek a kért %2 kódolásban.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>The codepoint %1, occurring in %2 using encoding %3, is an invalid XML character.</source>
        <translation>A(z) %3 kódolást használó %2 fájlban előforduló %1 kódpont egy érvénytelen XML karakter.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/api/qiodevicedelegate.cpp" line="+82"/>
        <source>Network timeout.</source>
        <translation>Hálózati időtúllépés.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/api/qxmlserializer.cpp" line="+319"/>
        <source>Element %1 can&apos;t be serialized because it appears outside the document element.</source>
        <translation>A(z) %1 elemet nem lehet sorosítani, mert a dokumentumelemen kívül szerepel.</translation>
    </message>
    <message>
        <location line="+60"/>
        <source>Attribute %1 can&apos;t be serialized because it appears at the top level.</source>
        <translation>A(z) %1 attribútumot nem lehet sorosítani, mert legfelső szinten szerepel.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/data/qabstractdatetime.cpp" line="+81"/>
        <source>Year %1 is invalid because it begins with %2.</source>
        <translation>A(z) %1 év érvénytelen, mert %2 értékkel kezdődik.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Day %1 is outside the range %2..%3.</source>
        <translation>A(z) %1. nap nem esik %2 és %3 közé.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Month %1 is outside the range %2..%3.</source>
        <translation>A(z) %1. hónap nem esik %2 és %3 közé.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Overflow: Can&apos;t represent date %1.</source>
        <translation>Túlcsordulás: a(z) %1 dátumot nem lehet ábrázolni.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Day %1 is invalid for month %2.</source>
        <translation>A(z) %1. nap érvénytelen a(z) %2. hónapban.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Time 24:%1:%2.%3 is invalid. Hour is 24, but minutes, seconds, and milliseconds are not all 0; </source>
        <translation>A 24:%1:%2.%3 időpont érvénytelen. Az óra értéke 24, de a perc, másodperc és ezredmásodperc nem mind 0; </translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Time %1:%2:%3.%4 is invalid.</source>
        <translation>A(z) %1:%2:%3.%4 időpont érvénytelen.</translation>
    </message>
    <message>
        <location line="+115"/>
        <source>Overflow: Date can&apos;t be represented.</source>
        <translation>Túlcsordulás: a dátumot nem lehet ábrázolni.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/data/qabstractduration.cpp" line="+97"/>
        <location line="+15"/>
        <source>At least one component must be present.</source>
        <translation>Legalább egy összetevőnek jelen kell lenni.</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>At least one time component must appear after the %1-delimiter.</source>
        <translation>Legalább egy időösszetevőnek szerepelnie kell a(z) %1 elválasztó után.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/data/qabstractfloatmathematician_tpl_p.h" line="+73"/>
        <source>No operand in an integer division, %1, can be %2.</source>
        <translation>Nincs operandus egy egész osztásnál (%1), %2 is lehet.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>The first operand in an integer division, %1, cannot be infinity (%2).</source>
        <translation>Az első operandus egy egész osztásban (%1) nem lehet végtelen (%2).</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The second operand in a division, %1, cannot be zero (%2).</source>
        <translation>A második operandus egy osztásban (%1) nem lehet nulla (%2).</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/data/qanyuri_p.h" line="+128"/>
        <source>%1 is not a valid value of type %2.</source>
        <translation>A(z) %1 nem érvényes %2 típusú érték.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/data/qatomiccasters_p.h" line="+219"/>
        <source>When casting to %1 from %2, the source value cannot be %3.</source>
        <translation>A(z) %2 → %1 típusváltáskor a forrásérték nem lehet %3.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/data/qatomicmathematicians.cpp" line="+63"/>
        <source>Integer division (%1) by zero (%2) is undefined.</source>
        <translation>A nullával (%2) való egész osztás (%1) nincs értelmezve.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Division (%1) by zero (%2) is undefined.</source>
        <translation>A nullával (%2) való osztás (%1) nincs értelmezve.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Modulus division (%1) by zero (%2) is undefined.</source>
        <translation>A nullával (%2) való maradékos osztás (%1) nincs értelmezve.</translation>
    </message>
    <message>
        <location line="+122"/>
        <location line="+32"/>
        <source>Dividing a value of type %1 by %2 (not-a-number) is not allowed.</source>
        <translation>Egy %1 típusú érték osztása %2 (nem szám) értékkel nem megengedett.</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>Dividing a value of type %1 by %2 or %3 (plus or minus zero) is not allowed.</source>
        <translation>Egy %1 típusú érték osztása %2 vagy %3 (pozitív vagy negatív nulla) értékkel nem megengedett.</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Multiplication of a value of type %1 by %2 or %3 (plus or minus infinity) is not allowed.</source>
        <translation>Egy %1 típusú érték szorzása %2 vagy %3 (pozitív vagy negatív végtelen) értékkel nem megengedett.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/data/qatomicvalue.cpp" line="+77"/>
        <source>A value of type %1 cannot have an Effective Boolean Value.</source>
        <translation>Egy %1 típusú értéknek nem lehet tényleges logikai értéke.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/data/qboolean.cpp" line="+76"/>
        <source>Effective Boolean Value cannot be calculated for a sequence containing two or more atomic values.</source>
        <translation>Tényleges logikai értéket nem lehet kiszámítani kettő vagy több atomi értéket tartalmazó sorozatnál.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/data/qderivedinteger_p.h" line="+400"/>
        <source>Value %1 of type %2 exceeds maximum (%3).</source>
        <translation>A(z) %2 típusú %1 érték meghaladja a maximumot (%3).</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Value %1 of type %2 is below minimum (%3).</source>
        <translation>A(z) %2 típusú %1 érték a minimum (%3) alatt van.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/data/qhexbinary.cpp" line="+89"/>
        <source>A value of type %1 must contain an even number of digits. The value %2 does not.</source>
        <translation>Egy %1 típusú értéknek páros számú számjegyet kell tartalmaznia. A(z) %2 érték nem párosat tartalmaz.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>%1 is not valid as a value of type %2.</source>
        <translation>A(z) %1 nem érvényes egy %2 típusú értékként.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/expr/qapplytemplate.cpp" line="+117"/>
        <source>Ambiguous rule match.</source>
        <translation>Nem egyértelmű szabály illeszkedés.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/expr/qarithmeticexpression.cpp" line="+205"/>
        <source>Operator %1 cannot be used on type %2.</source>
        <translation>A(z) %1 művelet nem használható %2 típuson.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Operator %1 cannot be used on atomic values of type %2 and %3.</source>
        <translation>A(z) %1 művelet nem használható %2 és %3 típusú atomi értékeken.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/expr/qattributenamevalidator.cpp" line="+64"/>
        <source>The namespace URI in the name for a computed attribute cannot be %1.</source>
        <translation>A nevében szereplő névtér URI egy számított attribútumnál nem lehet %1.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The name for a computed attribute cannot have the namespace URI %1 with the local name %2.</source>
        <translation>A név egy számított attribútumnál nem rendelkezhet a helyi %2 névvel ellátott %1 névtér URI-val.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/expr/qcastas.cpp" line="+86"/>
        <source>Type error in cast, expected %1, received %2.</source>
        <translation>Típushiba a típusváltás során: %1 várt, %2 kapott.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>When casting to %1 or types derived from it, the source value must be of the same type, or it must be a string literal. Type %2 is not allowed.</source>
        <translation>%1 vagy az abból származó típusokra való típusváltáskor a forrásértéknek ugyanolyan típusúnak vagy karakterlánc konstansértéknek kell lennie. A(z) %2 típus nem megengedett.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/expr/qcastingplatform_tpl_p.h" line="+145"/>
        <source>No casting is possible with %1 as the target type.</source>
        <translation>Nem lehetséges a típusváltás a(z) %1 típussal, mint a céltípusként.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>It is not possible to cast from %1 to %2.</source>
        <translation>Nem lehetséges %1 típusról %2 típusra váltani.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Casting to %1 is not possible because it is an abstract type, and can therefore never be instantiated.</source>
        <translation>A(z) %1 típusra való váltás nem lehetséges, mert ez egy absztrakt típus, és emiatt soha sem példányosítható.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>It&apos;s not possible to cast the value %1 of type %2 to %3</source>
        <translation>Nem lehetséges a(z) %2 típusú %1 értékről %3 típusra váltani.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Failure when casting from %1 to %2: %3</source>
        <translation>Sikertelen a(z) %1 típusról %2 típusra váltani: %3</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/expr/qcommentconstructor.cpp" line="+65"/>
        <source>A comment cannot contain %1</source>
        <translation>Egy megjegyzés nem tartalmazhatja a következőt: %1</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>A comment cannot end with a %1.</source>
        <translation>Egy megjegyzés nem végződhet a következőre: %1.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/expr/qcomparisonplatform_tpl_p.h" line="+175"/>
        <source>No comparisons can be done involving the type %1.</source>
        <translation>Nem lehet összehasonlításokat végezni a(z) %1 típus bevonásával.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Operator %1 is not available between atomic values of type %2 and %3.</source>
        <translation>A(z) %1 művelet nem érhető el %2 és %3 típusú atomi értékek között.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/expr/qcomputednamespaceconstructor.cpp" line="+67"/>
        <source>In a namespace constructor, the value for a namespace cannot be an empty string.</source>
        <translation>Egy névtér konstruktorában a névtér értéke nem lehet üres karakterlánc.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>The prefix must be a valid %1, which %2 is not.</source>
        <translation>Az előtag érvényes %1 kell legyen, de %2 nem az.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>The prefix %1 cannot be bound.</source>
        <translation>A(z) %1 előtag nem köthető.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Only the prefix %1 can be bound to %2 and vice versa.</source>
        <translation>Csak a(z) %1 előtag köthető %2 kifejezéshez és fordítva.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/expr/qdocumentcontentvalidator.cpp" line="+84"/>
        <source>An attribute node cannot be a child of a document node. Therefore, the attribute %1 is out of place.</source>
        <translation>Egy attribútum csomópont nem lehet egy dokumentum csomópont gyermeke. Emiatt a(z) %1 attribútum nem ide való.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/expr/qevaluationcache_tpl_p.h" line="+126"/>
        <source>Circularity detected</source>
        <translation>Körkörösség észlelhető</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/expr/qexpressionfactory.cpp" line="+160"/>
        <source>A library module cannot be evaluated directly. It must be imported from a main module.</source>
        <translation>Egy programkönyvtár modult nem lehet közvetlenül kiértékelni. Egy fő modulból kell importálni.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>No template by name %1 exists.</source>
        <translation>Nem létezik %1 nevű sablon.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/expr/qgenericpredicate.cpp" line="+104"/>
        <source>A value of type %1 cannot be a predicate. A predicate must have either a numeric type or an Effective Boolean Value type.</source>
        <translation>Egy %1 típusú érték nem lehet predikátum. Egy predikátumnak szám típusúnak vagy tényleges logikai érték típusúnak kell lennie.</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>A positional predicate must evaluate to a single numeric value.</source>
        <translation>Egy helyzetbeállító predikátumnak egyetlen numerikus értékké kell kiértékelődnie.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/expr/qncnameconstructor_p.h" line="+109"/>
        <source>The target name in a processing instruction cannot be %1 in any combination of upper and lower case. Therefore, %2 is invalid.</source>
        <translation>A célnév egy feldolgozási utasításban nem lehet %1 semmilyen kis- és nagybetűs formában. Emiatt a(z) %2 érvénytelen.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>%1 is not a valid target name in a processing instruction. It must be a %2 value, e.g. %3.</source>
        <translation>A(z) %1 nem érvényes célnév egy feldolgozási utasításban. Egy %2 értéknek kell lennie, például %3.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/expr/qpath.cpp" line="+112"/>
        <source>The last step in a path must contain either nodes or atomic values. It cannot be a mixture between the two.</source>
        <translation>Egy útvonal utolsó lépésének vagy csomópontokat, vagy atomi értékeket kell tartalmaznia. Nem lehet a kettő közötti keverék.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/expr/qprocessinginstructionconstructor.cpp" line="+82"/>
        <source>The data of a processing instruction cannot contain the string %1</source>
        <translation>Egy feldolgozási utasítás adatai nem tartalmazhatják a(z) %1 karakterláncot.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/expr/qqnameconstructor.cpp" line="+80"/>
        <source>No namespace binding exists for the prefix %1</source>
        <translation>Nem létezik névtér kötés a(z) %1 előtaghoz.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/expr/qqnameconstructor_p.h" line="+152"/>
        <source>No namespace binding exists for the prefix %1 in %2</source>
        <translation>Nem létezik névtér kötés a(z) %1 előtaghoz itt: %2</translation>
    </message>
    <message>
        <location line="+12"/>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/functions/qqnamefns.cpp" line="+67"/>
        <source>%1 is an invalid %2</source>
        <translation>A(z) %1 egy érvénytelen %2</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/expr/qtemplate.cpp" line="+77"/>
        <source>The parameter %1 is passed, but no corresponding %2 exists.</source>
        <translation>A(z) %1 paraméter át lett adva, de nem létezik megfelelő %2.</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>The parameter %1 is required, but no corresponding %2 is supplied.</source>
        <translation>A(z) %1 paraméter kötelező, de nincs megfelelő %2 megadva.</translation>
    </message>
    <message numerus="yes">
        <location filename="../../qtxmlpatterns/src/xmlpatterns/functions/qabstractfunctionfactory.cpp" line="+75"/>
        <source>%1 takes at most %n argument(s). %2 is therefore invalid.</source>
        <translation>
            <numerusform>A(z) %1 legfeljebb %n argumentumot fogad el. Ebből következően %2 érvénytelen.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+11"/>
        <source>%1 requires at least %n argument(s). %2 is therefore invalid.</source>
        <translation>
            <numerusform>A(z) %1 legalább %n argumentumot igényel. Ebből következően %2 érvénytelen.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/functions/qaggregatefns.cpp" line="+118"/>
        <source>The first argument to %1 cannot be of type %2. It must be a numeric type, xs:yearMonthDuration or xs:dayTimeDuration.</source>
        <translation>A(z) %1 első paramétere nem lehet %2 típusú. Csak numerikus típus, xs:yearMonthDuration vagy dayTimeDuration lehet.</translation>
    </message>
    <message>
        <location line="+74"/>
        <source>The first argument to %1 cannot be of type %2. It must be of type %3, %4, or %5.</source>
        <translation>A(z) %1 első argumentuma nem lehet %2 típusú. Csak %3, %4 vagy %5 típusú lehet.</translation>
    </message>
    <message>
        <location line="+91"/>
        <source>The second argument to %1 cannot be of type %2. It must be of type %3, %4, or %5.</source>
        <translation>A(z) %1 második paramétere nem lehet %2 típusú. Csak %3, %4 vagy %5 típusú lehet.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/functions/qassemblestringfns.cpp" line="+86"/>
        <source>%1 is not a valid XML 1.0 character.</source>
        <translation>A(z) %1 nem érvényes XML 1.0 karakter.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/functions/qcomparingaggregator_tpl_p.h" line="+206"/>
        <source>The first argument to %1 cannot be of type %2.</source>
        <translation>A(z) %1 első argumentuma nem lehet %2 típusú.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/functions/qcontextnodechecker.cpp" line="+52"/>
        <source>The root node of the second argument to function %1 must be a document node. %2 is not a document node.</source>
        <translation>A(z) %1 függvény második argumentumának gyökércsomópontja dokumentum típusú kell legyen. A(z) %2 nem dokumentumcsomópont.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/functions/qdatetimefn.cpp" line="+84"/>
        <source>If both values have zone offsets, they must have the same zone offset. %1 and %2 are not the same.</source>
        <translation>Ha mindkét értéknek van zónaeltolása, akkor ugyanolyan zónaeltolásnak kell lennie. %1 és %2 nem ugyanaz.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/functions/qerrorfn.cpp" line="+59"/>
        <source>%1 was called.</source>
        <translation>%1 lett meghívva.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/functions/qpatternmatchingfns.cpp" line="+92"/>
        <source>%1 must be followed by %2 or %3, not at the end of the replacement string.</source>
        <translation>A(z) %1 után %2 vagy %3 kell kövezzen, nem a helyettesítő karakterlánc végén.</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>In the replacement string, %1 must be followed by at least one digit when not escaped.</source>
        <translation>A helyettesítő karakterláncban a(z) %1 után legalább egy számjegynek kell következnie, ha nincs elfedve.</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>In the replacement string, %1 can only be used to escape itself or %2, not %3</source>
        <translation>A helyettesítő karakterláncban a(z) %1 csak önmaga vagy %2 elfedésére használható, %3 elfedésére nem.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/functions/qpatternplatform.cpp" line="+90"/>
        <source>%1 matches newline characters</source>
        <translation>A(z) %1 illeszkedik az új sor karakterekre</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>%1 and %2 match the start and end of a line.</source>
        <translation>A(z) %1 és %2 illeszkedik a sor elejére és a végére.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Matches are case insensitive</source>
        <translation>Az illeszkedések nem érzékenyek a kis- és nagybetűkre</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Whitespace characters are removed, except when they appear in character classes</source>
        <translation>Az üres karakterek eltávolításra kerülnek, kivéve ha karakterosztályokban szerepelnek</translation>
    </message>
    <message>
        <location line="+100"/>
        <source>%1 is an invalid regular expression pattern: %2</source>
        <translation>A(z) %1 érvénytelen reguláris kifejezés minta: %2</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>%1 is an invalid flag for regular expressions. Valid flags are:</source>
        <translation>A(z) %1 érvénytelen jelző a reguláris kifejezésekhez. Az érvényes jelzők a következők:</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/functions/qqnamefns.cpp" line="+17"/>
        <source>If the first argument is the empty sequence or a zero-length string (no namespace), a prefix cannot be specified. Prefix %1 was specified.</source>
        <translation>Ha az első argumentum az üres sorozat vagy egy nulla hosszúságú karakterlánc (névtér nélkül), akkor előtag nem adható meg. A(z) %1 előtag lett megadva.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/functions/qsequencefns.cpp" line="+344"/>
        <source>It will not be possible to retrieve %1.</source>
        <translation>Nem lesz lehetséges a(z) %1 lekérése.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/functions/qsequencegeneratingfns.cpp" line="+264"/>
        <source>The default collection is undefined</source>
        <translation>Az alapértelmezett gyűjtemény nincs meghatározva</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>%1 cannot be retrieved</source>
        <translation>A(z) %1 nem kérhető le</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/functions/qstringvaluefns.cpp" line="+250"/>
        <source>The normalization form %1 is unsupported. The supported forms are %2, %3, %4, and %5, and none, i.e. the empty string (no normalization).</source>
        <translation>A(z) %1 normalizált alak nem támogatott. A támogatott alakok: %2, %3, %4 és %5, valamint semmi, azaz üres karakterlánc (normalizálás nélkül).</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/functions/qtimezonefns.cpp" line="+85"/>
        <source>A zone offset must be in the range %1..%2 inclusive. %3 is out of range.</source>
        <translation>Egy zónaeltolásnak a(z) %1 és %2 nyílt tartományban kell lennie. A(z) %3 kívül esik a tartományon.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1 is not a whole number of minutes.</source>
        <translation>A(z) %1 nem egész számú perc.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/functions/qunparsedtextfn.cpp" line="+63"/>
        <source>The URI cannot have a fragment</source>
        <translation>Az URI nem rendelkezhet töredékkel</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/janitors/qcardinalityverifier.cpp" line="+56"/>
        <source>Required cardinality is %1; got cardinality %2.</source>
        <translation>A szükséges számosság %1, %2 számosság kapott.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/janitors/qitemverifier.cpp" line="+65"/>
        <source>The item %1 did not match the required type %2.</source>
        <translation>A(z) %1 elem nem egyezett a szükséges %2 típussal.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/parser/qmaintainingreader_tpl_p.h" line="+193"/>
        <source>Attribute %1 cannot appear on the element %2. Only the standard attributes can appear.</source>
        <translation>A(z) %1 attribútum nem szerepelhet a(z) %2 elemben. Csak a szabványos attribútumok szerepelhetnek.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Attribute %1 cannot appear on the element %2. Only %3 is allowed, and the standard attributes.</source>
        <translation>A(z) %1 attribútum nem szerepelhet a(z) %2 elemben. Csak a(z) %3 és a szabványos attribútumok engedélyezettek.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Attribute %1 cannot appear on the element %2. Allowed is %3, %4, and the standard attributes.</source>
        <translation>A(z) %1 attribútum nem szerepelhet a(z) %2 elemben. Csak a(z) %3, %4 és a szabványos attribútumok engedélyezettek.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Attribute %1 cannot appear on the element %2. Allowed is %3, and the standard attributes.</source>
        <translation>A(z) %1 attribútum nem szerepelhet a(z) %2 elemben. Csak a(z) %3 és a szabványos attribútumok engedélyezettek.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>XSL-T attributes on XSL-T elements must be in the null namespace, not in the XSL-T namespace which %1 is.</source>
        <translation>Az XSL-T elemekhez tartozó XSL-T attribútumoknak a null névtérben kell lenniük, nem abban az XSL-T névtérben, amely %1.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>The attribute %1 must appear on element %2.</source>
        <translation>A(z) %1 attribútumnak a(z) %2 elemben kell szerepelnie.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The element with local name %1 does not exist in XSL-T.</source>
        <translation>A(z) %1 helyi nevű elem nem létezik az XSL-T-ben.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/parser/qparsercontext.cpp" line="+91"/>
        <source>The variable %1 is unused</source>
        <translation>A(z) %1 változó nincs használva</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/parser/qquerytransformparser.cpp" line="+357"/>
        <source>W3C XML Schema identity constraint selector</source>
        <translation>W3C XML séma azonosító megszorítás kiválasztó</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>W3C XML Schema identity constraint field</source>
        <translation>W3C XML séma azonosító megszorítás mező</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>A construct was encountered which is disallowed in the current language(%1).</source>
        <translation>Egy olyan szerkezet volt megtalálható, amely nem megengedett a jelenlegi nyelvben (%1).</translation>
    </message>
    <message>
        <location line="+50"/>
        <location line="+7111"/>
        <source>%1 is an unknown schema type.</source>
        <translation>A(z) %1 egy ismeretlen sématípus.</translation>
    </message>
    <message>
        <location line="-7042"/>
        <source>A template with name %1 has already been declared.</source>
        <translation>Egy %1 nevű sablon már deklarálva van.</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>%1 is not a valid numeric literal.</source>
        <translation>A(z) %1 nem érvényes numerikus konstansérték.</translation>
    </message>
    <message>
        <location line="+187"/>
        <source>Only one %1 declaration can occur in the query prolog.</source>
        <translation>Csak egy %1 deklaráció szerepelhet a lekérdezés bevezetőjében.</translation>
    </message>
    <message>
        <location line="+188"/>
        <source>The initialization of variable %1 depends on itself</source>
        <translation>A(z) %1 változó előkészítése önmagától függ</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>No variable with name %1 exists</source>
        <translation>Nem létezik %1 nevű változó</translation>
    </message>
    <message>
        <location line="+2629"/>
        <source>Version %1 is not supported. The supported XQuery version is 1.0.</source>
        <translation>A(z) %1 verzió nem támogatott. A támogatott XQuery verzió az 1.0.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>The encoding %1 is invalid. It must contain Latin characters only, must not contain whitespace, and must match the regular expression %2.</source>
        <translation>A(z) %1 kódolás érvénytelen. Csak Latin karaktereket tartalmazhat, nem tartalmazhat üres karaktereket, és illeszkednie kell a(z) %2 reguláris kifejezésre.</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>No function with signature %1 is available</source>
        <translation>Nem érhető el %1 aláírással rendelkező függvény</translation>
    </message>
    <message>
        <location line="+72"/>
        <location line="+10"/>
        <source>A default namespace declaration must occur before function, variable, and option declarations.</source>
        <translation>Egy alapértelmezett névtér deklarációnak a függvény-, változó- és kapcsolódeklarációk előtt kell szerepelnie.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Namespace declarations must occur before function, variable, and option declarations.</source>
        <translation>A névtér deklarációknak a függvény-, változó- és kapcsolódeklarációk előtt kell szerepelnie.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Module imports must occur before function, variable, and option declarations.</source>
        <translation>A modulimportálásoknak a függvény-, változó- és kapcsolódeklarációk előtt kell szerepelniük.</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>The keyword %1 cannot occur with any other mode name.</source>
        <translation>A(z) %1 kulcsszó nem szerepelhet együtt semmilyen más módnévvel.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>The value of attribute %1 must be of type %2, which %3 isn&apos;t.</source>
        <translation>A(z) %1 attribútum értékének %2 típusúnak kell lennie, %3 nem az.</translation>
    </message>
    <message>
        <location line="+69"/>
        <source>It is not possible to redeclare prefix %1.</source>
        <translation>Nem lehetséges a(z) %1 előtag újradeklarálása.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>The prefix %1 can not be bound. By default, it is already bound to the namespace %2.</source>
        <translation>A(z) %1 előtagot nem lehet kötni. Alapértelmezetten már kötve van a(z) %2 névtérhez.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Prefix %1 is already declared in the prolog.</source>
        <translation>A(z) %1 előtag már deklarálva van a bevezetőben.</translation>
    </message>
    <message>
        <location line="+95"/>
        <source>The name of an option must have a prefix. There is no default namespace for options.</source>
        <translation>Egy kapcsoló nevének előtaggal kell rendelkeznie. Nincs alapértelmezett névtér a kapcsolókhoz.</translation>
    </message>
    <message>
        <location line="+171"/>
        <source>The Schema Import feature is not supported, and therefore %1 declarations cannot occur.</source>
        <translation>A sémaimportálási szolgáltatás nem támogatott, és emiatt %1 deklarációk nem szerepelhetnek.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>The target namespace of a %1 cannot be empty.</source>
        <translation>Egy %1 célnévtere nem lehet üres.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>The module import feature is not supported</source>
        <translation>A modulimportálási szolgáltatás nem támogatott</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>A variable with name %1 has already been declared.</source>
        <translation>Egy %1 nevű változó már deklarálva van.</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>No value is available for the external variable with name %1.</source>
        <translation>Nem érhető el érték a(z) %1 nevű külső változóhoz.</translation>
    </message>
    <message>
        <location line="+96"/>
        <source>A stylesheet function must have a prefixed name.</source>
        <translation>Egy stíluslap függvénynek előtaggal ellátott névvel kell rendelkeznie.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The namespace for a user defined function cannot be empty (try the predefined prefix %1 which exists for cases like this)</source>
        <translation>Egy felhasználó által meghatározott függvény névtere nem lehet üres (próbálja meg az előre meghatározott %1 előtagot, amely az ilyen esetekhez létezik)</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>The namespace %1 is reserved; therefore user defined functions may not use it. Try the predefined prefix %2, which exists for these cases.</source>
        <translation>A(z) %1 névtér foglalt, ezért a felhasználó által meghatározott függvények nem használhatják. Próbálja meg az előre meghatározott %2 előtagot, amely az ilyen esetekhez létezik.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>The namespace of a user defined function in a library module must be equivalent to the module namespace. In other words, it should be %1 instead of %2</source>
        <translation>Egy függvénykönyvtár modulban lévő felhasználó által meghatározott függvény névterének egyenértékűnek kell lennie a modul névterével. Más szóval csak %1 lehet %2 helyett.</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>A function already exists with the signature %1.</source>
        <translation>Már létezik egy függvény a(z) „%1” aláírással.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>No external functions are supported. All supported functions can be used directly, without first declaring them as external</source>
        <translation>A külső függvények nem támogatottak. Minden támogatott függvény közvetlenül használható anélkül, hogy először külsőként kellene deklarálni azokat.</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>An argument with name %1 has already been declared. Every argument name must be unique.</source>
        <translation>Egy %1 nevű argumentum már deklarálva van. Minden argumentumnévnek egyedinek kell lennie.</translation>
    </message>
    <message>
        <location line="+179"/>
        <source>When function %1 is used for matching inside a pattern, the argument must be a variable reference or a string literal.</source>
        <translation>Ha a(z) %1 függvényt használják az illesztéshez egy mintán belül, akkor az argumentumnak változóhivatkozásnak vagy karakterlánc konstansértéknek kell lennie.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>In an XSL-T pattern, the first argument to function %1 must be a string literal, when used for matching.</source>
        <translation>Egy XSL-T mintában a(z) %1 függvény első argumentumának karakterlánc konstansértéknek kell lennie, ha illesztéshez használják.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>In an XSL-T pattern, the first argument to function %1 must be a literal or a variable reference, when used for matching.</source>
        <translation>Egy XSL-T mintában a(z) %1 függvény első argumentumának konstansértéknek vagy változóhivatkozásnak kell lennie, ha illesztéshez használják.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>In an XSL-T pattern, function %1 cannot have a third argument.</source>
        <translation>Egy XSL-T mintában a(z) %1 függvénynek nem lehet harmadik argumentuma.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>In an XSL-T pattern, only function %1 and %2, not %3, can be used for matching.</source>
        <translation>Egy XSL-T mintában csak a(z) %1 és a(z) %2 függvények használhatók illesztéshez, a(z) %3 nem.</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>In an XSL-T pattern, axis %1 cannot be used, only axis %2 or %3 can.</source>
        <translation>Egy XSL-T mintában a(z) %1 tengely nem használható, csak a(z) %2 vagy a(z) %3 tengely használható.</translation>
    </message>
    <message>
        <location line="+126"/>
        <source>%1 is an invalid template mode name.</source>
        <translation>A(z) %1 egy érvénytelen sablonmódnév.</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>The name of a variable bound in a for-expression must be different from the positional variable. Hence, the two variables named %1 collide.</source>
        <translation>Egy for-kifejezésben kötött változó nevének különbözőnek kell lennie a helyzetbeállító változótól. Ezért a két %1 nevű változó ütközik.</translation>
    </message>
    <message>
        <location line="+778"/>
        <source>The Schema Validation Feature is not supported. Hence, %1-expressions may not be used.</source>
        <translation>A sémaellenőrzési szolgáltatás nem támogatott. Ezért a(z) %1-kifejezések nem használhatók.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>None of the pragma expressions are supported. Therefore, a fallback expression must be present</source>
        <translation>A pragma kifejezések egyike sem támogatott. Ezért egy tartalék kifejezésnek kell jelen lennie.</translation>
    </message>
    <message>
        <location line="+269"/>
        <source>Each name of a template parameter must be unique; %1 is duplicated.</source>
        <translation>Egy sablonparaméter minden egyes nevének egyedinek kell lennie. A(z) %1 kettőzött.</translation>
    </message>
    <message>
        <location line="+129"/>
        <source>The %1-axis is unsupported in XQuery</source>
        <translation>A(z) %1-tengely nem támogatott XQuery-ben</translation>
    </message>
    <message>
        <location line="+333"/>
        <source>No function with name %1 is available.</source>
        <translation>Nem érhető el %1 nevű függvény.</translation>
    </message>
    <message>
        <location line="+102"/>
        <source>The namespace URI cannot be the empty string when binding to a prefix, %1.</source>
        <translation>A névtér URI nem lehet az üres karakterlánc, amikor egy előtaghoz kötik, %1.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>%1 is an invalid namespace URI.</source>
        <translation>A(z) %1 egy érvénytelen névtér URI.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>It is not possible to bind to the prefix %1</source>
        <translation>Nem lehetséges a(z) %1 előtaghoz kötni</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Namespace %1 can only be bound to %2 (and it is, in either case, pre-declared).</source>
        <translation>A(z) %1 névtér csak ehhez köthető: %2 (és ez minden esetben előre deklarált).</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Prefix %1 can only be bound to %2 (and it is, in either case, pre-declared).</source>
        <translation>A(z) %1 előtag csak ehhez köthető: %2 (és ez minden esetben előre deklarált).</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Two namespace declaration attributes have the same name: %1.</source>
        <translation>Két névtér-deklaráció attribútumnak ugyanaz a neve: %1.</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>The namespace URI must be a constant and cannot use enclosed expressions.</source>
        <translation>Egy névtér URI csak konstans lehet, és nem használhat bezárt kifejezéseket.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>An attribute with name %1 has already appeared on this element.</source>
        <translation>Egy %1 nevű attribútum már szerepelt ebben az elemben.</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>A direct element constructor is not well-formed. %1 is ended with %2.</source>
        <translation>Egy közvetlen elem konstruktor nem jól formázott. A(z) %1 ezzel végződik: %2.</translation>
    </message>
    <message>
        <location line="+458"/>
        <source>The name %1 does not refer to any schema type.</source>
        <translation>A(z) %1 név nem hivatkozik semmilyen sématípusra sem.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 is an complex type. Casting to complex types is not possible. However, casting to atomic types such as %2 works.</source>
        <translation>A(z) %1 egy összetett típus. Összetett típusokra való típusváltás nem lehetséges. Azonban az atomi típusokra való típusváltás működik, mint például %2 típusra.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1 is not an atomic type. Casting is only possible to atomic types.</source>
        <translation>A(z) %1 nem atomi típus. Típusváltás csak atomi típusokra lehetséges.</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>%1 is not a valid name for a processing-instruction.</source>
        <translation>A(z) %1 nem érvényes név egy feldolgozási utasításhoz.</translation>
    </message>
    <message>
        <location line="+69"/>
        <location line="+71"/>
        <source>%1 is not in the in-scope attribute declarations. Note that the schema import feature is not supported.</source>
        <translation>A(z) %1 nem a hatáskörön belüli attribútum deklarációkban van. Vegye figyelembe, hogy a sémaimportálási szolgáltatás nem támogatott.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The name of an extension expression must be in a namespace.</source>
        <translation>Egy kiterjesztés kifejezés nevének egy névtérben kell lennie.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/parser/qxslttokenizer.cpp" line="+518"/>
        <source>Element %1 is not allowed at this location.</source>
        <translation>A(z) %1 elem nem megengedett ezen a helyen.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Text nodes are not allowed at this location.</source>
        <translation>Szövegcsomópontok nem megengedettek ezen a helyen.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Parse error: %1</source>
        <translation>Feldolgozási hiba: %1</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>The value of the XSL-T version attribute must be a value of type %1, which %2 isn&apos;t.</source>
        <translation>Az XSL-T verzióattribútum értékének %1 típusúnak kell lennie, %2 nem az.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Running an XSL-T 1.0 stylesheet with a 2.0 processor.</source>
        <translation>Egy XSL-T 1.0 stíluslap futtatása 2.0 verziójú feldolgozóval.</translation>
    </message>
    <message>
        <location line="+103"/>
        <source>Unknown XSL-T attribute %1.</source>
        <translation>Ismeretlen XSLT attribútum: %1.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Attribute %1 and %2 are mutually exclusive.</source>
        <translation>A(z) %1 és %2 attribútumok kölcsönösen kizárják egymást.</translation>
    </message>
    <message>
        <location line="+166"/>
        <source>In a simplified stylesheet module, attribute %1 must be present.</source>
        <translation>Egy egyszerűsített stíluslap modulban a(z) %1 attribútumnak jelen kell lennie.</translation>
    </message>
    <message>
        <location line="+72"/>
        <source>If element %1 has no attribute %2, it cannot have attribute %3 or %4.</source>
        <translation>Ha a(z) %1 elemnek nincs %2 attribútuma, akkor nem lehet %3 vagy %4 attribútuma sem.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Element %1 must have at least one of the attributes %2 or %3.</source>
        <translation>A(z) %1 elemnek rendelkeznie kell a(z) %2 vagy %3 attribútumok legalább egyikével.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>At least one mode must be specified in the %1-attribute on element %2.</source>
        <translation>Legalább egy módot meg kell adni a(z) %2 elemben lévő %1-attribútumban.</translation>
    </message>
    <message>
        <location line="+123"/>
        <source>Element %1 must come last.</source>
        <translation>A(z) %1 elemnek utolsónak kell lennie.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>At least one %1-element must occur before %2.</source>
        <translation>Legalább egy %1-elemnek szerepelnie kell a(z) %2 előtt.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Only one %1-element can appear.</source>
        <translation>Csak egy %1-elem szerepelhet.</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>At least one %1-element must occur inside %2.</source>
        <translation>Legalább egy %1-elemnek elő kell fordulnia a(z) %2 belsejében.</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>When attribute %1 is present on %2, a sequence constructor cannot be used.</source>
        <translation>Ha a(z) %1 szerepel a(z) %2 elemben, akkor egy sorozatkonstruktor nem használható.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Element %1 must have either a %2-attribute or a sequence constructor.</source>
        <translation>A(z) %1 elemnek rendelkeznie kell vagy egy %2-attribútummal, vagy sorozatkonstruktorral.</translation>
    </message>
    <message>
        <location line="+125"/>
        <source>When a parameter is required, a default value cannot be supplied through a %1-attribute or a sequence constructor.</source>
        <translation>Ha egy paraméter szükséges, akkor egy alapértelmezett értéket nem lehet egy  %1-attribútumon vagy sorozatkonstruktoron keresztül megadni.</translation>
    </message>
    <message>
        <location line="+270"/>
        <source>Element %1 cannot have children.</source>
        <translation>A(z) %1 elemnek nem lehetnek gyermekei.</translation>
    </message>
    <message>
        <location line="+430"/>
        <source>Element %1 cannot have a sequence constructor.</source>
        <translation>A(z) %1 elemnek nem lehet sorozatkonstruktora.</translation>
    </message>
    <message>
        <location line="+86"/>
        <location line="+9"/>
        <source>The attribute %1 cannot appear on %2, when it is a child of %3.</source>
        <translation>A(z) %1 attribútum nem szerepelhet a(z) %2 elemben, ha az egy %3 gyermeke.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>A parameter in a function cannot be declared to be a tunnel.</source>
        <translation>Egy paraméter egy függvényben nem deklarálható úgy, hogy alagút legyen.</translation>
    </message>
    <message>
        <location line="+148"/>
        <source>This processor is not Schema-aware and therefore %1 cannot be used.</source>
        <translation>Ez a feldolgozó nem sémaalapú, és ezért a(z) %1 nem használható.</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Top level stylesheet elements must be in a non-null namespace, which %1 isn&apos;t.</source>
        <translation>A felső szintű stíluslap elemeknek egy nem üres névtérben kell lenniük, %1 nem az.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>The value for attribute %1 on element %2 must either be %3 or %4, not %5.</source>
        <translation>A(z) %1 attribútum értéke a(z) %2 elemben vagy %3, vagy %4 lehet csak, %5 nem.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Attribute %1 cannot have the value %2.</source>
        <translation>A(z) %1 attribútumnak nem lehet %2 értéke.</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>The attribute %1 can only appear on the first %2 element.</source>
        <translation>A(z) %1 attribútum csak az első %2 elemben szerepelhet.</translation>
    </message>
    <message>
        <location line="+98"/>
        <source>At least one %1 element must appear as child of %2.</source>
        <translation>Legalább egy %1 elemnek szerepelnie kell a(z) %2 gyermekeként.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/schema/qxsdparticlechecker.cpp" line="+163"/>
        <source>Empty particle cannot be derived from non-empty particle.</source>
        <translation>Üres részecske nem származtatható nem üres részecskéből.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Derived particle is missing element %1.</source>
        <translation>Származtatott részecskéből hiányzik a(z) %1 elem.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Derived element %1 is missing value constraint as defined in base particle.</source>
        <translation>A(z) %1 származtatott elemből hiányzik az értékmegkötés, ahogy az alaprészecskében meg van határozva.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Derived element %1 has weaker value constraint than base particle.</source>
        <translation>A(z) %1 származtatott elemnek gyengébb értékmegkötése van az alaprészecskéénél.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Fixed value constraint of element %1 differs from value constraint in base particle.</source>
        <translation>A(z) %1 elem rögzített értékmegkötése különbözik az alaprészecskében lévő értékmegkötéstől.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Derived element %1 cannot be nillable as base element is not nillable.</source>
        <translation>A(z) %1 származtatott elem nem lehet nullázható, mivel az alapelem sem nullázható.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Block constraints of derived element %1 must not be more weaker than in the base element.</source>
        <translation>A(z) %1 származtatott elem blokkmegkötései nem lehetnek sokkal gyengébbek az alapelemben lévőknél.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Simple type of derived element %1 cannot be validly derived from base element.</source>
        <translation>A(z) %1 származtatott elem egyszerű típusa nem lehet érvényesen származtatott az alapelemből.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Complex type of derived element %1 cannot be validly derived from base element.</source>
        <translation>A(z) %1 származtatott elem összetett típusa nem lehet érvényesen származtatott az alapelemből.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Element %1 is missing in derived particle.</source>
        <translation>A(z) %1 elem hiányzik a származtatott részecskéből.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Element %1 does not match namespace constraint of wildcard in base particle.</source>
        <translation>A(z) %1 elem nem illeszkedik az alaprészecskében lévő helyettesítő elem névtérmegkötésére.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Wildcard in derived particle is not a valid subset of wildcard in base particle.</source>
        <translation>A származtatott részecskében lévő helyettesítő elem nem érvényes részhalmaza az alaprészecskében lévő helyettesítő elemnek.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>processContent of wildcard in derived particle is weaker than wildcard in base particle.</source>
        <translation>A származtatott részecskében lévő helyettesítő elem tartalomfeldolgozási értéke gyengébb az alaprészecske helyettesítő eleménél.</translation>
    </message>
    <message>
        <location line="+255"/>
        <source>Derived particle allows content that is not allowed in the base particle.</source>
        <translation>A származtatott részecske olyan tartalmat is megenged, amely nem megengedett az alaprészecskében.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/schema/qxsdschemachecker.cpp" line="+225"/>
        <source>%1 has inheritance loop in its base type %2.</source>
        <translation>A(z) %1 öröklődési hurkot tartalmaz annak %2 alaptípusában.</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+24"/>
        <source>Circular inheritance of base type %1.</source>
        <translation>A(z) %1 alaptípus körkörös öröklődése.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Circular inheritance of union %1.</source>
        <translation>A(z) %1 unió körkörös öröklődése.</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>%1 is not allowed to derive from %2 by restriction as the latter defines it as final.</source>
        <translation>A(z) %1 nem származhat a(z) %2 osztályból megkötés szerint, mivel az utóbbi végsőként határozza meg.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 is not allowed to derive from %2 by extension as the latter defines it as final.</source>
        <translation>A(z) %1 nem származhat a(z) %2 osztályból kiterjesztés szerint, mivel az utóbbi végsőként határozza meg.</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Base type of simple type %1 cannot be complex type %2.</source>
        <translation>A(z) %1 egyszerű típus alaptípusa nem lehet %2 összetett típus.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Simple type %1 cannot have direct base type %2.</source>
        <translation>A(z) %1 egyszerű típusnak nem lehet %2 közvetlen alaptípusa.</translation>
    </message>
    <message>
        <location line="+33"/>
        <location line="+9"/>
        <source>Simple type %1 is not allowed to have base type %2.</source>
        <translation>A(z) %1 egyszerű típusnak nem megengedett, hogy %2 alaptípusa legyen.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Simple type %1 can only have simple atomic type as base type.</source>
        <translation>A(z) %1 egyszerű típusnak csak egyszerű atomi típusa lehet alaptípusként.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Simple type %1 cannot derive from %2 as the latter defines restriction as final.</source>
        <translation>A(z) %1 egyszerű típus nem származhat a(z) %2 osztályból, mivel az utóbbi végsőként határozza meg a megkötést.</translation>
    </message>
    <message>
        <location line="+13"/>
        <location line="+478"/>
        <source>Variety of item type of %1 must be either atomic or union.</source>
        <translation>A(z) %1 elemtípusának választéka csak atomi vagy unió lehet.</translation>
    </message>
    <message>
        <source>Variety of member types of %1 must be atomic.</source>
        <translation type="vanished">A(z) %1 tagtípusainak választéka csak atomi lehet.</translation>
    </message>
    <message>
        <location line="-468"/>
        <location line="+477"/>
        <source>Variety of member types of %1 must be atomic or union.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-464"/>
        <location line="+445"/>
        <source>%1 is not allowed to derive from %2 by list as the latter defines it as final.</source>
        <translation>A(z) %1 nem származhat a(z) %2 osztályból lista szerint, mivel az utóbbi végsőként határozza meg.</translation>
    </message>
    <message>
        <location line="-427"/>
        <source>Simple type %1 is only allowed to have %2 facet.</source>
        <translation>A(z) %1 egyszerű típusnak csak %2 aspektusa lehet.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Base type of simple type %1 must have variety of type list.</source>
        <translation>A(z) %1 egyszerű típus alaptípusának rendelkeznie kell típuslista választékkal.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Base type of simple type %1 has defined derivation by restriction as final.</source>
        <translation>A(z) %1 egyszerű típus alaptípusa a megkötés szerinti származtatást végsőként határozta meg.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Item type of base type does not match item type of %1.</source>
        <translation>Az alaptípusa elemtípusa nem egyezik a(z) %1 elemtípusával.</translation>
    </message>
    <message>
        <location line="+24"/>
        <location line="+91"/>
        <source>Simple type %1 contains not allowed facet type %2.</source>
        <translation>A(z) %1 egyszerű típus nem megengedett %2 aspektustípust tartalmaz.</translation>
    </message>
    <message>
        <location line="-70"/>
        <location line="+412"/>
        <source>%1 is not allowed to derive from %2 by union as the latter defines it as final.</source>
        <translation>A(z) %1 nem származhat a(z) %2 osztályból unió szerint, mivel az utóbbi végsőként határozza meg.</translation>
    </message>
    <message>
        <location line="-403"/>
        <source>%1 is not allowed to have any facets.</source>
        <translation>A(z) %1 nem rendelkezhet egyetlen aspektussal sem.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Base type %1 of simple type %2 must have variety of union.</source>
        <translation>A(z) %2 egyszerű típus %1 alaptípusának rendelkeznie kell unióválasztékkal.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Base type %1 of simple type %2 is not allowed to have restriction in %3 attribute.</source>
        <translation>A(z) %2 egyszerű típus %1 alaptípusának nem megengedett, hogy megkötése legyen a(z) %3 attribútumban.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Member type %1 cannot be derived from member type %2 of %3&apos;s base type %4.</source>
        <translation>A(z) %1 tagtípus nem származhat a(z) %3 %4 alaptípusának %2 tagtípusából.</translation>
    </message>
    <message>
        <location line="+63"/>
        <source>Derivation method of %1 must be extension because the base type %2 is a simple type.</source>
        <translation>A(z) %1 származtatási módszerének kiterjesztésnek kell lennie, mert a(z) %2 alaptípus egy egyszerű típus.</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Complex type %1 has duplicated element %2 in its content model.</source>
        <translation>A(z) %1 összetett típusnak kettőzött %2 eleme van a tartalommodelljében.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Complex type %1 has non-deterministic content.</source>
        <translation>A(z) %1 összetett típusnak nem determinisztikus tartalma van.</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Attributes of complex type %1 are not a valid extension of the attributes of base type %2: %3.</source>
        <translation>A(z) %1 összetett típus attribútumai nem érvényes kiterjesztései a(z) %2 alaptípus attribútumainak: %3.</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Content model of complex type %1 is not a valid extension of content model of %2.</source>
        <translation>A(z) %1 összetett típus tartalommodellje nem érvényes kiterjesztése a(z) %2 tartalommodellnek.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Complex type %1 must have simple content.</source>
        <translation>A(z) %1 összetett típusnak rendelkeznie kell egyszerű tartalommal.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Complex type %1 must have the same simple type as its base class %2.</source>
        <translation>A(z) %1 összetett típusnak ugyanazzal az egyszerű típussal kell rendelkeznie, mint a(z) %2 alaposztályának.</translation>
    </message>
    <message>
        <location line="+67"/>
        <source>Complex type %1 cannot be derived from base type %2%3.</source>
        <translation>A(z) %1 összetett típus nem származhat a(z) %2%3 alaptípusból.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Attributes of complex type %1 are not a valid restriction from the attributes of base type %2: %3.</source>
        <translation>A(z) %1 összetett típus attribútumai nem érvényes megkötések a(z) %2 alaptípus attribútumaiból: %3.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Complex type %1 with simple content cannot be derived from complex base type %2.</source>
        <translation>Az egyszerű tartalommal rendelkező %1 összetett típus nem származhat a(z) %2 összetett alaptípusból.</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Item type of simple type %1 cannot be a complex type.</source>
        <translation>A(z) %1 egyszerű típus elemtípusa nem lehet összetett típus.</translation>
    </message>
    <message>
        <location line="+45"/>
        <source>Member type of simple type %1 cannot be a complex type.</source>
        <translation>A(z) %1 egyszerű típus tagtípusa nem lehet összetett típus.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>%1 is not allowed to have a member type with the same name as itself.</source>
        <translation>A(z) %1 nem rendelkezhet saját magával megegyező nevű tagtípussal.</translation>
    </message>
    <message>
        <location line="+83"/>
        <location line="+29"/>
        <location line="+34"/>
        <source>%1 facet collides with %2 facet.</source>
        <translation>A(z) %1 aspektus ütközik a(z) %2 aspektussal.</translation>
    </message>
    <message>
        <location line="-20"/>
        <source>%1 facet must have the same value as %2 facet of base type.</source>
        <translation>A(z) %1 aspektusnak ugyanazzal az értékkel kell rendelkeznie, mint amilyen az alaptípus %2 aspektusának van.</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>%1 facet must be equal or greater than %2 facet of base type.</source>
        <translation>A(z) %1 aspektusnak egyenlőnek vagy nagyobbnak kell lennie az alaptípus %2 aspektusánál.</translation>
    </message>
    <message>
        <location line="+19"/>
        <location line="+125"/>
        <location line="+55"/>
        <location line="+12"/>
        <location line="+91"/>
        <location line="+58"/>
        <location line="+34"/>
        <location line="+35"/>
        <source>%1 facet must be less than or equal to %2 facet of base type.</source>
        <translation>A(z) %1 aspektusnak kisebbnek vagy egyenlőnek kell lennie az alaptípus %2 aspektusával.</translation>
    </message>
    <message>
        <location line="-389"/>
        <source>%1 facet contains invalid regular expression</source>
        <translation>A(z) %1 aspektus érvénytelen reguláris kifejezést tartalmaz.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Unknown notation %1 used in %2 facet.</source>
        <translation>Ismeretlen %1 jelölés van használatban a(z) %2 aspektusban.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>%1 facet contains invalid value %2: %3.</source>
        <translation>A(z) %1 aspektus érvénytelen %2 értéket tartalmaz: %3.</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>%1 facet cannot be %2 or %3 if %4 facet of base type is %5.</source>
        <translation>A(z) %1 aspektus nem lehet %2 vagy %3, ha az alaptípus %4 aspektusa %5.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>%1 facet cannot be %2 if %3 facet of base type is %4.</source>
        <translation>A(z) %1 aspektus nem lehet %2, ha az alaptípus %3 aspektusa %4.</translation>
    </message>
    <message>
        <location line="+20"/>
        <location line="+55"/>
        <location line="+230"/>
        <source>%1 facet must be less than or equal to %2 facet.</source>
        <translation>A(z) %1 aspektusnak kisebbnek vagy egyenlőnek kell lennie a(z) %2 aspektussal.</translation>
    </message>
    <message>
        <location line="-257"/>
        <location line="+134"/>
        <location line="+82"/>
        <source>%1 facet must be less than %2 facet of base type.</source>
        <translation>A(z) %1 aspektusnak kisebbnek kell lennie az alaptípus %2 aspektusánál.</translation>
    </message>
    <message>
        <location line="-201"/>
        <location line="+79"/>
        <source>%1 facet and %2 facet cannot appear together.</source>
        <translation>A(z) %1 aspektus és a(z) %2 aspektus nem szerepelhetnek együtt.</translation>
    </message>
    <message>
        <location line="-27"/>
        <location line="+12"/>
        <location line="+113"/>
        <source>%1 facet must be greater than %2 facet of base type.</source>
        <translation>A(z) %1 aspektusnak nagyobbnak kell lennie az alaptípus %2 aspektusánál.</translation>
    </message>
    <message>
        <location line="-86"/>
        <location line="+58"/>
        <source>%1 facet must be less than %2 facet.</source>
        <translation>A(z) %1 aspektusnak kisebbnek kell lennie a(z) %2 aspektusnál.</translation>
    </message>
    <message>
        <location line="-42"/>
        <location line="+58"/>
        <source>%1 facet must be greater than or equal to %2 facet of base type.</source>
        <translation>A(z) %1 aspektusnak nagyobbnak vagy egyenlőnek kell lennie az alaptípus %2 aspektusával.</translation>
    </message>
    <message>
        <location line="+116"/>
        <source>Simple type contains not allowed facet %1.</source>
        <translation>Az egyszerű típus nem megengedett %1 aspektust tartalmaz.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>%1, %2, %3, %4, %5 and %6 facets are not allowed when derived by list.</source>
        <translation>A(z) %1, %2, %3, %4, %5 és %6 aspektusok nem megengedettek, ha lista szerint származtak.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Only %1 and %2 facets are allowed when derived by union.</source>
        <translation>Csak %1 és %2 aspektusok megengedettek, ha unió szerint származtak.</translation>
    </message>
    <message>
        <location line="+21"/>
        <location line="+16"/>
        <source>%1 contains %2 facet with invalid data: %3.</source>
        <translation>A(z) %1 érvénytelen adatokkal rendelkező %2 aspektust tartalmaz: %3.</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Attribute group %1 contains attribute %2 twice.</source>
        <translation>A(z) %1 attribútumcsoport kétszer tartalmazza a(z) %2 attribútumot.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Attribute group %1 contains two different attributes that both have types derived from %2.</source>
        <translation>A(z) %1 attribútumcsoport két különböző attribútumot tartalmaz, amelyek mindegyike rendelkezik a(z) %2 osztályból származtatott típussal.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Attribute group %1 contains attribute %2 that has value constraint but type that inherits from %3.</source>
        <translation>A(z) %1 attribútumcsoport olyan %2 attribútumot tartalmaz, amelynek értékmegkötése van, kivéve a típust, amely a(z) %3 osztályból örököl.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Complex type %1 contains attribute %2 twice.</source>
        <translation>A(z) %1 összetett típus kétszer tartalmazza a(z) %2 attribútumot.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Complex type %1 contains two different attributes that both have types derived from %2.</source>
        <translation>A(z) %1 összetett típus két különböző attribútumot tartalmaz, amelyek mindegyike rendelkezik a(z) %2 osztályból származtatott típussal.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Complex type %1 contains attribute %2 that has value constraint but type that inherits from %3.</source>
        <translation>A(z) %1 összetett típus olyan %2 attribútumot tartalmaz, amelynek értékmegkötése van, kivéve a típust, amely a(z) %3 osztályból örököl.</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Element %1 is not allowed to have a value constraint if its base type is complex.</source>
        <translation>A(z) %1 elemnek nem megengedett, hogy értékmegkötése legyen, ha az alaptípusa összetett.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Element %1 is not allowed to have a value constraint if its type is derived from %2.</source>
        <translation>A(z) %1 elemnek nem megengedett, hogy értékmegkötése legyen, ha a típusa a(z) %2 osztályból származik.</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+11"/>
        <source>Value constraint of element %1 is not of elements type: %2.</source>
        <translation>A(z) %1 elem értékmegkötése nem a következő elemtípusú: %2.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Element %1 is not allowed to have substitution group affiliation as it is no global element.</source>
        <translation>A(z) %1 elemnek nem megengedett, hogy helyettesítő csoport kapcsolata legyen, mivel nem globális elem.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Type of element %1 cannot be derived from type of substitution group affiliation.</source>
        <translation>A(z) %1 elem típusa nem származhat helyettesítő csoport kapcsolatának típusából.</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Value constraint of attribute %1 is not of attributes type: %2.</source>
        <translation>A(z) %1 attribútum értékmegkötése nem a következő attribútumtípusú: %2.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Attribute %1 has value constraint but has type derived from %2.</source>
        <translation>A(z) %1 attribútumnak értékmegkötése van, de a(z) %2 osztályból származtatott típusa van.</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>%1 attribute in derived complex type must be %2 like in base type.</source>
        <translation>A származtatott összetett típusban lévő %1 attribútumnak %2 értékűnek kell lennie, mint az alaptípusban.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Attribute %1 in derived complex type must have %2 value constraint like in base type.</source>
        <translation>A származtatott összetett típusban lévő %1 attribútumnak %2 értékmegkötéssel kell rendelkeznie, mint az alaptípusban.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Attribute %1 in derived complex type must have the same %2 value constraint like in base type.</source>
        <translation>A származtatott összetett típusban lévő %1 attribútumnak ugyanolyan %2 értékmegkötéssel kell rendelkeznie, mint az alaptípusban.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Attribute %1 in derived complex type must have %2 value constraint.</source>
        <translation>A származtatott összetett típusban lévő %1 attribútumnak %2 értékmegkötéssel kell rendelkeznie.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>processContent of base wildcard must be weaker than derived wildcard.</source>
        <translation>Az alap helyettesítő elem tartalomfeldolgozási értékének gyengébbnek kell lennie a származtatott helyettesítő elemnél.</translation>
    </message>
    <message>
        <location line="+39"/>
        <location line="+15"/>
        <source>Element %1 exists twice with different types.</source>
        <translation>A(z) %1 elem kétszer létezik különböző típussal.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Particle contains non-deterministic wildcards.</source>
        <translation>A részecske nem determinisztikus helyettesítő elemeket tartalmaz.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/schema/qxsdschemahelper.cpp" line="+676"/>
        <location line="+63"/>
        <source>Base attribute %1 is required but derived attribute is not.</source>
        <translation>A(z) %1 alapattribútum kötelező, de a származtatott attribútum nem.</translation>
    </message>
    <message>
        <location line="-57"/>
        <source>Type of derived attribute %1 cannot be validly derived from type of base attribute.</source>
        <translation>A(z) %1 származtatott attribútum típusa nem lehet érvényesen származtatott az alapattribútum típusából.</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Value constraint of derived attribute %1 does not match value constraint of base attribute.</source>
        <translation>A(z) %1 származtatott attribútum értékmegkötése nem egyezik az alapattribútum értékmegkötésével.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Derived attribute %1 does not exist in the base definition.</source>
        <translation>A(z) %1 származtatott attribútum nem létezik az alap-meghatározásban.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Derived attribute %1 does not match the wildcard in the base definition.</source>
        <translation>A(z) %1 származtatott attribútum nem egyezik az alap-meghatározásban lévő helyettesítő elemmel.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Base attribute %1 is required but missing in derived definition.</source>
        <translation>A(z) %1 alapattribútum kötelező, de hiányzik a származtatott meghatározásból.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Derived definition contains an %1 element that does not exists in the base definition</source>
        <translation>A származtatott meghatározás egy olyan %1 elemet tartalmaz, amely nem létezik az alap-meghatározásban.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Derived wildcard is not a subset of the base wildcard.</source>
        <translation>A származtatott helyettesítő elem nem az alap helyettesítő elem részhalmaza.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 of derived wildcard is not a valid restriction of %2 of base wildcard</source>
        <translation>A származtatott helyettesítő elem %1 része nem érvényes megkötése az alap helyettesítő elem %2 részének.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Attribute %1 from base type is missing in derived type.</source>
        <translation>Az alaptípusban lévő %1 attribútum hiányzik a származtatott típusban.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Type of derived attribute %1 differs from type of base attribute.</source>
        <translation>A(z) %1 származtatott attribútum típusa eltér az alapattribútum típusától.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Base definition contains an %1 element that is missing in the derived definition</source>
        <translation>Az alap-meghatározás tartalmaz egy olyan %1 elemet, amely hiányzik a származtatott meghatározásból.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/schema/qxsdschemaparser.cpp" line="+168"/>
        <source>Can not process unknown element %1, expected elements are: %2.</source>
        <translation>Nem lehet feldolgozni az ismeretlen %1 elemet, a várt elemek: %2.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Element %1 is not allowed in this scope, possible elements are: %2.</source>
        <translation>A(z) %1 elem nem megengedett ebben a hatáskörben, a lehetséges elemek: %2.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Child element is missing in that scope, possible child elements are: %1.</source>
        <translation>A gyermekelem hiányzik abból a hatáskörből, a lehetséges gyermekelemek: %1.</translation>
    </message>
    <message>
        <location line="+144"/>
        <source>Document is not a XML schema.</source>
        <translation>A dokumentum nem egy XML séma.</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>%1 attribute of %2 element contains invalid content: {%3} is not a value of type %4.</source>
        <translation>A(z) %2 elem %1 attribútuma érvénytelen tartalmat tartalmaz: {%3} nem %4 típusú érték.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>%1 attribute of %2 element contains invalid content: {%3}.</source>
        <translation>A(z) %2 elem %1 attribútuma érvénytelen tartalmat tartalmaz: {%3}.</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Target namespace %1 of included schema is different from the target namespace %2 as defined by the including schema.</source>
        <translation>A tartalmazott séma %1 célnévtere különbözik attól a(z) %2 célnévtértől, ahogy a tartalmazó séma meghatározza.</translation>
    </message>
    <message>
        <location line="+14"/>
        <location line="+11"/>
        <source>Target namespace %1 of imported schema is different from the target namespace %2 as defined by the importing schema.</source>
        <translation>Az importált séma %1 célnévtere különbözik attól a(z) %2 célnévtértől, ahogy az importáló séma meghatározza.</translation>
    </message>
    <message>
        <location line="+243"/>
        <source>%1 element is not allowed to have the same %2 attribute value as the target namespace %3.</source>
        <translation>A(z) %1 elemnek nem megengedett, hogy ugyanaz a(z) %2 attribútumértéke legyen, mint a(z) %3 célnévtérnek.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>%1 element without %2 attribute is not allowed inside schema without target namespace.</source>
        <translation>A(z) %2 attribútum nélküli %1 elem nem megengedett egy célnévtér nélküli sémán belül.</translation>
    </message>
    <message>
        <location line="+851"/>
        <location line="+158"/>
        <source>%1 element is not allowed inside %2 element if %3 attribute is present.</source>
        <translation>A(z) %1 elem nem megengedett a(z) %2 elemen belül, ha a(z) %3 attribútum jelen van.</translation>
    </message>
    <message>
        <location line="-97"/>
        <location line="+119"/>
        <location line="+89"/>
        <source>%1 element has neither %2 attribute nor %3 child element.</source>
        <translation>A(z) %1 elemnek nincs sem %2 attribútuma, sem %3 gyermekeleme.</translation>
    </message>
    <message>
        <location line="+835"/>
        <location line="+1474"/>
        <location line="+232"/>
        <location line="+7"/>
        <location line="+260"/>
        <location line="+17"/>
        <location line="+258"/>
        <location line="+6"/>
        <location line="+17"/>
        <location line="+6"/>
        <location line="+17"/>
        <location line="+11"/>
        <location line="+11"/>
        <location line="+11"/>
        <source>%1 element with %2 child element must not have a %3 attribute.</source>
        <translation>A(z) %2 gyermekelemmel rendelkező %1 elemnek nem lehet %3 attribútuma.</translation>
    </message>
    <message>
        <location line="-1325"/>
        <source>%1 attribute of %2 element must be %3 or %4.</source>
        <translation>A(z) %2 elem %1 attribútuma %3 vagy %4 kell legyen.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>%1 attribute of %2 element must have a value of %3.</source>
        <translation>A(z) %2 elem %1 attribútumának rendelkeznie kell egy %3 értékével.</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+34"/>
        <source>%1 attribute of %2 element must have a value of %3 or %4.</source>
        <translation>A(z) %2 elem %1 attribútumának rendelkezni kell egy %3 vagy %4 értékével.</translation>
    </message>
    <message>
        <location line="+319"/>
        <location line="+129"/>
        <location line="+9"/>
        <location line="+7"/>
        <location line="+7"/>
        <location line="+327"/>
        <location line="+203"/>
        <location line="+6"/>
        <location line="+6"/>
        <location line="+6"/>
        <location line="+6"/>
        <location line="+6"/>
        <location line="+6"/>
        <location line="+77"/>
        <source>%1 element must not have %2 and %3 attribute together.</source>
        <translation>A(z) %1 elemnek nem lehet %2 és %3 attribútuma együtt.</translation>
    </message>
    <message>
        <location line="-768"/>
        <location line="+222"/>
        <source>Content of %1 attribute of %2 element must not be from namespace %3.</source>
        <translation>A(z) %2 elem %1 attribútumának tartalma nem lehet a(z) %3 névtérből való.</translation>
    </message>
    <message>
        <location line="-215"/>
        <location line="+222"/>
        <source>%1 attribute of %2 element must not be %3.</source>
        <translation>A(z) %2 elem %1 attribútuma nem lehet %3.</translation>
    </message>
    <message>
        <location line="-64"/>
        <source>%1 attribute of %2 element must have the value %3 because the %4 attribute is set.</source>
        <translation>A(z) %2 elem %1 attribútumának rendelkeznie kell a(z) %3 értékkel, mert a(z) %4 attribútum be van állítva.</translation>
    </message>
    <message>
        <location line="+187"/>
        <source>Specifying use=&apos;prohibited&apos; inside an attribute group has no effect.</source>
        <translation>A use=&apos;prohibited&apos; megadása egy attribútumcsoporton belül hatástalan.</translation>
    </message>
    <message>
        <location line="+353"/>
        <source>%1 element must have either %2 or %3 attribute.</source>
        <translation>A(z) %1 elemnek vagy %2, vagy %3 attribútummal kell rendelkeznie.</translation>
    </message>
    <message>
        <location line="+554"/>
        <source>%1 element must have either %2 attribute or %3 or %4 as child element.</source>
        <translation>A(z) %1 elemnek vagy %2 attribútummal, vagy %3, vagy %4 gyermekelemmel kell rendelkeznie.</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>%1 element requires either %2 or %3 attribute.</source>
        <translation>A(z) %1 elem vagy %2, vagy %3 attribútumot igényel.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Text or entity references not allowed inside %1 element</source>
        <translation>Szöveg- vagy entitáshivatkozások nem megengedettek egy %1 elemen belül.</translation>
    </message>
    <message>
        <location line="+42"/>
        <location line="+113"/>
        <source>%1 attribute of %2 element must contain %3, %4 or a list of URIs.</source>
        <translation>A(z) %2 elem %1 attribútumának tartalma %3, %4 vagy URI-k listája kell legyen.</translation>
    </message>
    <message>
        <location line="+126"/>
        <source>%1 element is not allowed in this context.</source>
        <translation>A(z) %1 elem nem megengedett ebben a környezetben.</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>%1 attribute of %2 element has larger value than %3 attribute.</source>
        <translation>A(z) %2 elem %1 attribútuma nagyobb értékkel rendelkezik a(z) %3 attribútuménál.</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Prefix of qualified name %1 is not defined.</source>
        <translation>A(z) %1 minősített név előtagja nincs meghatározva.</translation>
    </message>
    <message>
        <location line="+65"/>
        <location line="+62"/>
        <source>%1 attribute of %2 element must either contain %3 or the other values.</source>
        <translation>A(z) %2 elem %1 attribútumának tartalma vagy %3, vagy a többi érték kell legyen.</translation>
    </message>
    <message>
        <location line="+132"/>
        <source>Component with ID %1 has been defined previously.</source>
        <translation>A(z) %1 azonosítójú összetevő már korábban meg lett határozva.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Element %1 already defined.</source>
        <translation>A(z) %1 elem már meg van határozva.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Attribute %1 already defined.</source>
        <translation>A(z) %1 attribútum már meg van határozva.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Type %1 already defined.</source>
        <translation>A(z) %1 típus már meg van határozva.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Attribute group %1 already defined.</source>
        <translation>A(z) %1 attribútumcsoport már meg van határozva.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Element group %1 already defined.</source>
        <translation>A(z) %1 elemcsoport már meg van határozva.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Notation %1 already defined.</source>
        <translation>A(z) %1 jelölés már meg van határozva.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Identity constraint %1 already defined.</source>
        <translation>A(z) %1 identitás-megkötés már meg van határozva.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Duplicated facets in simple type %1.</source>
        <translation>Kettőzött aspektus a(z) %1 egyszerű típusban.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/schema/qxsdschemaresolver.cpp" line="+352"/>
        <source>%1 references unknown %2 or %3 element %4.</source>
        <translation>A(z) %1 ismeretlen %2 vagy %3 %4 elemre hivatkozik.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 references identity constraint %2 that is no %3 or %4 element.</source>
        <translation>A(z) %1 olyan %2 identitás-megkötésre hivatkozik, amely nem %3 vagy %4 elem.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>%1 has a different number of fields from the identity constraint %2 that it references.</source>
        <translation>A(z) %1 eltérő számú mezővel rendelkezik abból a(z) %2 identitás-megkötésből, amelyre hivatkozik.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Base type %1 of %2 element cannot be resolved.</source>
        <translation>A(z) %2 elem %1 alaptípusát nem lehet feloldani.</translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Item type %1 of %2 element cannot be resolved.</source>
        <translation>A(z) %2 elem %1 elemtípusát nem lehet feloldani.</translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Member type %1 of %2 element cannot be resolved.</source>
        <translation>A(z) %2 elem %1 tagtípusát nem lehet feloldani.</translation>
    </message>
    <message>
        <location line="+28"/>
        <location line="+415"/>
        <location line="+30"/>
        <source>Type %1 of %2 element cannot be resolved.</source>
        <translation>A(z) %2 elem %1 típusát nem lehet feloldani.</translation>
    </message>
    <message>
        <location line="-423"/>
        <source>Base type %1 of complex type cannot be resolved.</source>
        <translation>Az összetett típus %1 alaptípusát nem lehet feloldani.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>%1 cannot have complex base type that has a %2.</source>
        <translation>A(z) %1 nem rendelkezhet olyan összetett alaptípussal, amelynek %2 tartalma van.</translation>
    </message>
    <message>
        <location line="+286"/>
        <source>Content model of complex type %1 contains %2 element so it cannot be derived by extension from a non-empty type.</source>
        <translation>A(z) %1 összetett típus tartalommodellje %2 elemet tartalmaz, így nem lehet kiterjesztés szerint származtatni egy nem üres típusból.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Complex type %1 cannot be derived by extension from %2 as the latter contains %3 element in its content model.</source>
        <translation>A(z) %1 összetett típus nem származhat kiterjesztés szerint a(z) %2 típusból, mivel az utóbbi %3 elemet tartalmaz a tartalommodelljében.</translation>
    </message>
    <message>
        <location line="+101"/>
        <source>Type of %1 element must be a simple type, %2 is not.</source>
        <translation>A(z) %1 elem típusának egyszerű típusnak kell lennie, %2 nem az.</translation>
    </message>
    <message>
        <location line="+62"/>
        <source>Substitution group %1 of %2 element cannot be resolved.</source>
        <translation>A(z) %2 elem %1 helyettesítő csoportját nem lehet feloldani.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Substitution group %1 has circular definition.</source>
        <translation>A(z) %1 helyettesítő csoportnak körkörös meghatározása van.</translation>
    </message>
    <message>
        <location line="+120"/>
        <location line="+7"/>
        <source>Duplicated element names %1 in %2 element.</source>
        <translation>Kettőzött %1 elemnevek a(z) %2 elemben.</translation>
    </message>
    <message>
        <location line="+29"/>
        <location line="+52"/>
        <location line="+71"/>
        <location line="+28"/>
        <source>Reference %1 of %2 element cannot be resolved.</source>
        <translation>A(z) %2 elem %1 hivatkozását nem lehet feloldani.</translation>
    </message>
    <message>
        <location line="-138"/>
        <source>Circular group reference for %1.</source>
        <translation>Körkörös csoporthivatkozás ennél: %1.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>%1 element is not allowed in this scope</source>
        <translation>A(z) %1 elem nem megengedett ebben a hatáskörben.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>%1 element cannot have %2 attribute with value other than %3.</source>
        <translation>A(z) %1 elemnek nem lehet a(z) %3 értéktől eltérő értékkel rendelkező %2 attribútuma.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>%1 element cannot have %2 attribute with value other than %3 or %4.</source>
        <translation>A(z) %1 elemnek nem lehet a(z) %3 vagy %4 értéktől eltérő értékkel rendelkező %2 attribútuma.</translation>
    </message>
    <message>
        <location line="+91"/>
        <source>%1 or %2 attribute of reference %3 does not match with the attribute declaration %4.</source>
        <translation>A(z) %3 hivatkozás %1 vagy %2 attribútuma nem egyezik a(z) %4 attribútum-meghatározással.</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Attribute group %1 has circular reference.</source>
        <translation>A(z) %1 attribútumcsoportnak körkörös hivatkozása van.</translation>
    </message>
    <message>
        <location line="+131"/>
        <source>%1 attribute in %2 must have %3 use like in base type %4.</source>
        <translation>A(z) %1 attribútumnak a(z) %2 típusban %3 használattal kell rendelkeznie, mint a(z) %4 alaptípusban.</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>Attribute wildcard of %1 is not a valid restriction of attribute wildcard of base type %2.</source>
        <translation>A(z) %1 attribútum helyettesítő eleme nem érvényes megkötése a(z) %2 alaptípus attribútum helyettesítő elemének.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>%1 has attribute wildcard but its base type %2 has not.</source>
        <translation>A(z) %1 rendelkezik attribútum helyettesítő elemmel, de annak %2 alaptípusa nem rendelkezik vele.</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Union of attribute wildcard of type %1 and attribute wildcard of its base type %2 is not expressible.</source>
        <translation>A(z) %1 típus attribútum helyettesítő elemének és a(z) %2 alaptípusának attribútum helyettesítő elemének uniója nem kifejezhető.</translation>
    </message>
    <message>
        <location line="+48"/>
        <source>Enumeration facet contains invalid content: {%1} is not a value of type %2.</source>
        <translation>A felsorolás aspektus érvénytelen tartalmat tartalmaz: {%1} nem %2 típusú érték.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Namespace prefix of qualified name %1 is not defined.</source>
        <translation>A(z) %1 minősített név névtér előtagja nincs meghatározva.</translation>
    </message>
    <message>
        <location line="+51"/>
        <location line="+18"/>
        <source>%1 element %2 is not a valid restriction of the %3 element it redefines: %4.</source>
        <translation>A(z) %1 %2 eleme nem érvényes megkötése annak a(z) %3 elemnek, amit újra meghatároz: %4.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/schema/qxsdtypechecker.cpp" line="+229"/>
        <location line="+7"/>
        <location line="+21"/>
        <source>%1 is not valid according to %2.</source>
        <translation>A(z) %1 nem érvényes a(z) %2 alapján.</translation>
    </message>
    <message>
        <location line="+167"/>
        <source>String content does not match the length facet.</source>
        <translation>A karakterlánc tartalom nem egyezik a hossz aspektussal.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>String content does not match the minLength facet.</source>
        <translation>A karakterlánc tartalom nem egyezik a legkisebb hossz aspektussal.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>String content does not match the maxLength facet.</source>
        <translation>A karakterlánc tartalom nem egyezik a legnagyobb hossz aspektussal.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>String content does not match pattern facet.</source>
        <translation>A karakterlánc tartalom nem egyezik a minta aspektussal.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>String content is not listed in the enumeration facet.</source>
        <translation>A karakterlánc tartalom nincs felsorolva a felsorolás aspektusban.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Signed integer content does not match the maxInclusive facet.</source>
        <translation>Az előjeles egész szám tartalom nem egyezik a legnagyobb befoglaló aspektussal.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Signed integer content does not match the maxExclusive facet.</source>
        <translation>Az előjeles egész szám tartalom nem egyezik a legnagyobb kizáró aspektussal.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Signed integer content does not match the minInclusive facet.</source>
        <translation>Az előjeles egész szám tartalom nem egyezik a legkisebb befoglaló aspektussal.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Signed integer content does not match the minExclusive facet.</source>
        <translation>Az előjeles egész szám tartalom nem egyezik a legkisebb kizáró aspektussal.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Signed integer content is not listed in the enumeration facet.</source>
        <translation>Az előjeles egész szám tartalom nincs felsorolva a felsorolás aspektusban.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Signed integer content does not match pattern facet.</source>
        <translation>Az előjeles egész szám tartalom nem egyezik a minta aspektussal.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Signed integer content does not match in the totalDigits facet.</source>
        <translation>Az előjeles egész szám tartalom nem egyezik az összes számjegy aspektussal.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Unsigned integer content does not match the maxInclusive facet.</source>
        <translation>Az előjel nélküli egész szám tartalom nem egyezik a legnagyobb befoglaló aspektussal.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Unsigned integer content does not match the maxExclusive facet.</source>
        <translation>Az előjel nélküli egész szám tartalom nem egyezik a legnagyobb kizáró aspektussal.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Unsigned integer content does not match the minInclusive facet.</source>
        <translation>Az előjel nélküli egész szám tartalom nem egyezik a legkisebb befoglaló aspektussal.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Unsigned integer content does not match the minExclusive facet.</source>
        <translation>Az előjel nélküli egész szám tartalom nem egyezik a legkisebb kizáró aspektussal.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Unsigned integer content is not listed in the enumeration facet.</source>
        <translation>Az előjel nélküli egész szám tartalom nincs felsorolva a felsorolás aspektusban.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Unsigned integer content does not match pattern facet.</source>
        <translation>Az előjel nélküli egész szám tartalom nem egyezik a minta aspektussal.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Unsigned integer content does not match in the totalDigits facet.</source>
        <translation>Az előjel nélküli egész szám tartalom nem egyezik az összes számjegy aspektussal.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Double content does not match the maxInclusive facet.</source>
        <translation>A dupla pontosságú szám tartalom nem egyezik a legnagyobb befoglaló aspektussal.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Double content does not match the maxExclusive facet.</source>
        <translation>A dupla pontosságú szám tartalom nem egyezik a legnagyobb kizáró aspektussal.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Double content does not match the minInclusive facet.</source>
        <translation>A dupla pontosságú szám tartalom nem egyezik a legkisebb befoglaló aspektussal.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Double content does not match the minExclusive facet.</source>
        <translation>A dupla pontosságú szám tartalom nem egyezik a legkisebb kizáró aspektussal.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Double content is not listed in the enumeration facet.</source>
        <translation>A dupla pontosságú szám tartalom nincs felsorolva a felsorolás aspektusban.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Double content does not match pattern facet.</source>
        <translation>A dupla pontosságú szám tartalom nem egyezik a minta aspektussal.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Decimal content does not match in the fractionDigits facet.</source>
        <translation>A decimális tartalom nem egyezik a törtszámjegyek aspektusban.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Decimal content does not match in the totalDigits facet.</source>
        <translation>A decimális tartalom nem egyezik az összes számjegy aspektusban.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Date time content does not match the maxInclusive facet.</source>
        <translation>A dátum-idő tartalom nem egyezik a legnagyobb befoglaló aspektussal.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Date time content does not match the maxExclusive facet.</source>
        <translation>A dátum-idő tartalom nem egyezik a legnagyobb kizáró aspektussal.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Date time content does not match the minInclusive facet.</source>
        <translation>A dátum-idő tartalom nem egyezik a legkisebb befoglaló aspektussal.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Date time content does not match the minExclusive facet.</source>
        <translation>A dátum-idő tartalom nem egyezik a legkisebb kizáró aspektussal.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Date time content is not listed in the enumeration facet.</source>
        <translation>A dátum-idő tartalom nincs felsorolva a felsorolás aspektusban.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Date time content does not match pattern facet.</source>
        <translation>A dátum-idő tartalom nem egyezik a minta aspektussal.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Duration content does not match the maxInclusive facet.</source>
        <translation>Az időtartam tartalom nem egyezik a legnagyobb befoglaló aspektussal.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Duration content does not match the maxExclusive facet.</source>
        <translation>Az időtartam tartalom nem egyezik a legnagyobb kizáró aspektussal.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Duration content does not match the minInclusive facet.</source>
        <translation>Az időtartam tartalom nem egyezik a legkisebb befoglaló aspektussal.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Duration content does not match the minExclusive facet.</source>
        <translation>Az időtartam tartalom nem egyezik a legkisebb kizáró aspektussal.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Duration content is not listed in the enumeration facet.</source>
        <translation>Az időtartam tartalom nincs felsorolva a felsorolás aspektusban.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Duration content does not match pattern facet.</source>
        <translation>Az időtartam tartalom nem egyezik a minta aspektussal.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Boolean content does not match pattern facet.</source>
        <translation>A logikai érték tartalom nem egyezik a minta aspektussal.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Binary content does not match the length facet.</source>
        <translation>A bináris tartalom nem egyezik a hossz aspektussal.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Binary content does not match the minLength facet.</source>
        <translation>A bináris tartalom nem egyezik a legkisebb hossz aspektussal.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Binary content does not match the maxLength facet.</source>
        <translation>A bináris tartalom nem egyezik a legnagyobb hossz aspektussal.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Binary content is not listed in the enumeration facet.</source>
        <translation>A bináris tartalom nincs felsorolva a felsorolás aspektusban.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Invalid QName content: %1.</source>
        <translation>Érvénytelen Q-név tartalom: %1.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>QName content is not listed in the enumeration facet.</source>
        <translation>A Q-név tartalom nincs felsorolva a felsorolás aspektusban.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>QName content does not match pattern facet.</source>
        <translation>A Q-név tartalom nem egyezik a minta aspektussal.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Notation content is not listed in the enumeration facet.</source>
        <translation>A jelölés tartalom nincs felsorolva a felsorolás aspektusban.</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>List content does not match length facet.</source>
        <translation>A lista tartalom nem egyezik a hossz aspektussal.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>List content does not match minLength facet.</source>
        <translation>A lista tartalom nem egyezik a legkisebb hossz aspektussal.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>List content does not match maxLength facet.</source>
        <translation>A lista tartalom nem egyezik a legnagyobb hossz aspektussal.</translation>
    </message>
    <message>
        <location line="+90"/>
        <source>List content is not listed in the enumeration facet.</source>
        <translation>A lista tartalom nincs felsorolva a felsorolás aspektusban.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>List content does not match pattern facet.</source>
        <translation>A lista tartalom nem egyezik a minta aspektussal.</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Union content is not listed in the enumeration facet.</source>
        <translation>Az unió tartalom nincs felsorolva a felsorolás aspektusban.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Union content does not match pattern facet.</source>
        <translation>Az unió tartalom nem egyezik a minta aspektussal.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Data of type %1 are not allowed to be empty.</source>
        <translation>A(z) %1 típus adatának nem megengedett, hogy üres legyen.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/schema/qxsdvalidatinginstancereader.cpp" line="+158"/>
        <source>Element %1 is missing child element.</source>
        <translation>A(z) %1 elemből hiányzik a gyermekelem.</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>There is one IDREF value with no corresponding ID: %1.</source>
        <translation>Van egy IDREF érték megfelelő azonosító nélkül: %1.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Loaded schema file is invalid.</source>
        <translation>A betöltött sémafájl érvénytelen.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>%1 contains invalid data.</source>
        <translation>A(z) %1 érvénytelen adatot tartalmaz.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>xsi:schemaLocation namespace %1 has already appeared earlier in the instance document.</source>
        <translation>A(z) %1 xsi:schemaLocation névtér már szerepelt korábban a példánydokumentumban.</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>xsi:noNamespaceSchemaLocation cannot appear after the first no-namespace element or attribute.</source>
        <translation>Az xsi:noNamespaceSchemaLocation nem szerepelhet az első nem névtérbeli elem vagy attribútum után.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>No schema defined for validation.</source>
        <translation>Nincs séma megadva az ellenőrzéshez.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>No definition for element %1 available.</source>
        <translation>Nem érhető el meghatározás a(z) %1 elemhez.</translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+49"/>
        <location line="+142"/>
        <source>Specified type %1 is not known to the schema.</source>
        <translation>A megadott %1 típus nem ismert a séma számára.</translation>
    </message>
    <message>
        <location line="-176"/>
        <source>Element %1 is not defined in this scope.</source>
        <translation>A(z) %1 elem nincs meghatározva ebben a hatáskörben.</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Declaration for element %1 does not exist.</source>
        <translation>A(z) %1 elem deklarációja nem létezik.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Element %1 contains invalid content.</source>
        <translation>A(z) %1 elem érvénytelen tartalmat tartalmaz.</translation>
    </message>
    <message>
        <location line="+73"/>
        <source>Element %1 is declared as abstract.</source>
        <translation>A(z) %1 elem absztraktként van deklarálva.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Element %1 is not nillable.</source>
        <translation>A(z) %1 elem nem nullázható.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Attribute %1 contains invalid data: %2</source>
        <translation>A(z) %1 attribútum érvénytelen adatot tartalmaz: %2</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Element contains content although it is nillable.</source>
        <translation>Az elem tartalmaz ugyan tartalmat, habár az nullázható.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Fixed value constraint not allowed if element is nillable.</source>
        <translation>A rögzített értékmegkötés nem megengedett, ha az elem nullázható.</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>Specified type %1 is not validly substitutable with element type %2.</source>
        <translation>A megadott %1 típus nem helyettesíthető érvényesen %2 elemtípussal.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>Complex type %1 is not allowed to be abstract.</source>
        <translation>A(z) %1 összetett típusnak nem megengedett, hogy absztrakt legyen.</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Element %1 contains not allowed attributes.</source>
        <translation>A(z) %1 elem nem megengedett attribútumokat tartalmaz.</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+97"/>
        <source>Element %1 contains not allowed child element.</source>
        <translation>A(z) %1 elem nem megengedett gyermekelemet tartalmaz.</translation>
    </message>
    <message>
        <location line="-76"/>
        <location line="+93"/>
        <source>Content of element %1 does not match its type definition: %2.</source>
        <translation>A(z) %1 elem tartalma nem egyezik annak típus-meghatározásával: %2.</translation>
    </message>
    <message>
        <location line="-85"/>
        <location line="+92"/>
        <location line="+41"/>
        <source>Content of element %1 does not match defined value constraint.</source>
        <translation>A(z) %1 elem tartalma nem egyezik a meghatározott értékmegkötéssel.</translation>
    </message>
    <message>
        <location line="-73"/>
        <source>Element %1 contains not allowed child content.</source>
        <translation>A(z) %1 elem nem megengedett gyermektartalmat tartalmaz.</translation>
    </message>
    <message>
        <location line="+41"/>
        <source>Element %1 contains not allowed text content.</source>
        <translation>A(z) %1 elem nem megengedett szövegtartalmat tartalmaz.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Element %1 cannot contain other elements, as it has a fixed content.</source>
        <translation>A(z) %1 elem nem tartalmazhat más elemeket, mivel rögzített tartalma van.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Element %1 is missing required attribute %2.</source>
        <translation>A(z) %1 elemből hiányzik a kötelező %2 attribútum.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Attribute %1 does not match the attribute wildcard.</source>
        <translation>A(z) %1 attribútum nem egyezik az attribútum helyettesítő elemével.</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Declaration for attribute %1 does not exist.</source>
        <translation>A(z) %1 attribútum deklarációja nem létezik.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Element %1 contains two attributes of type %2.</source>
        <translation>A(z) %1 elem két %2 típusú attribútumot tartalmaz.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Attribute %1 contains invalid content.</source>
        <translation>A(z) %1 attribútum érvénytelen tartalmat tartalmaz.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Element %1 contains unknown attribute %2.</source>
        <translation>A(z) %1 elem ismeretlen %2 attribútumot tartalmaz.</translation>
    </message>
    <message>
        <location line="+40"/>
        <location line="+46"/>
        <source>Content of attribute %1 does not match its type definition: %2.</source>
        <translation>A(z) %1 attribútum tartalma nem egyezik annak típus-meghatározásával: %2.</translation>
    </message>
    <message>
        <location line="-38"/>
        <location line="+46"/>
        <source>Content of attribute %1 does not match defined value constraint.</source>
        <translation>A(z) %1 attribútum tartalma nem egyezik a meghatározott értékmegkötéssel.</translation>
    </message>
    <message>
        <location line="+80"/>
        <source>Non-unique value found for constraint %1.</source>
        <translation>Nem egyedi érték található a(z) %1 megkötéshez.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Key constraint %1 contains absent fields.</source>
        <translation>A(z) %1 kulcsmegkötés hiányzó mezőket tartalmaz.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Key constraint %1 contains references nillable element %2.</source>
        <translation>A(z) %1 kulcsmegkötés hivatkozásokat tartalmaz a(z) %2 nullázható elemre.</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>No referenced value found for key reference %1.</source>
        <translation>Nem található hivatkozott érték a(z) %1 kulcshivatkozáshoz.</translation>
    </message>
    <message>
        <location line="+64"/>
        <source>More than one value found for field %1.</source>
        <translation>Egynél több érték található a(z) %1 mezőhöz.</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Field %1 has no simple type.</source>
        <translation>A(z) %1 mezőnek nincs egyszerű típusa.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Field %1 is missing its simple type.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+63"/>
        <source>ID value &apos;%1&apos; is not unique.</source>
        <translation>A(z) „%1” azonosítóérték nem egyedi.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>&apos;%1&apos; attribute contains invalid QName content: %2.</source>
        <translation>A(z) „%1” attribútum érvénytelen Q-név tartalmat tartalmaz: %2.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/type/qcardinality.cpp" line="+53"/>
        <source>empty</source>
        <translation>üres</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>zero or one</source>
        <translation>nulla vagy egy</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>exactly one</source>
        <translation>pontosan egy</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>one or more</source>
        <translation>egy vagy több</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>zero or more</source>
        <translation>nulla vagy több</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/type/qtypechecker.cpp" line="+61"/>
        <source>Required type is %1, but %2 was found.</source>
        <translation>A szükséges típus %1, de %2 található.</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Promoting %1 to %2 may cause loss of precision.</source>
        <translation>A(z) %1 előléptetése %2 típusra a pontosság elvesztését okozhatja.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>The focus is undefined.</source>
        <translation>A fókusz nincs meghatározva.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/utils/qoutputvalidator.cpp" line="+84"/>
        <source>It&apos;s not possible to add attributes after any other kind of node.</source>
        <translation>Nem lehetséges attribútumokat hozzáadni semmilyen fajta csomópont után.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>An attribute by name %1 has already been created.</source>
        <translation>Egy %1 nevű attribútum már létre lett hozva.</translation>
    </message>
    <message>
        <location filename="../../qtxmlpatterns/src/xmlpatterns/utils/qxpathhelper_p.h" line="+116"/>
        <source>Only the Unicode Codepoint Collation is supported(%1). %2 is unsupported.</source>
        <translation>Csak a Unicode kódpont összevetés támogatott (%1). A(z) %2 nem támogatott.</translation>
    </message>
</context>
</TS>
