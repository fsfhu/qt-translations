<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu_HU">
<context>
    <name>FilterNameDialogClass</name>
    <message>
        <location filename="../../qttools/src/assistant/help/qfilternamedialog.ui"/>
        <source>Add Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Filter Name:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QCLuceneResultWidget</name>
    <message>
        <source>Search Results</source>
        <translation type="vanished">Keresési eredmények</translation>
    </message>
    <message>
        <source>Note:</source>
        <translation type="vanished">Megjegyzés:</translation>
    </message>
    <message>
        <source>The search results may not be complete since the documentation is still being indexed.</source>
        <translation type="vanished">A keresési eredmények esetleg nem teljesek, mivel a dokumentáció még indexelés alatt van.</translation>
    </message>
    <message>
        <source>Your search did not match any documents.</source>
        <translation type="vanished">A keresés nem illeszkedett egyetlen dokumentumra sem.</translation>
    </message>
    <message>
        <source>(The reason for this might be that the documentation is still being indexed.)</source>
        <translation type="vanished">(Ez amiatt lehet, hogy a dokumentáció még indexelés alatt van.)</translation>
    </message>
</context>
<context>
    <name>QHelp</name>
    <message>
        <location filename="../../qttools/src/assistant/help/qhelp_global.cpp" line="+60"/>
        <source>Untitled</source>
        <translation>Névtelen</translation>
    </message>
</context>
<context>
    <name>QHelpCollectionHandler</name>
    <message>
        <source>The collection file &apos;%1&apos; is not set up yet.</source>
        <translation type="vanished">A(z) „%1” gyűjteményfájl még nincs beállítva.</translation>
    </message>
    <message>
        <location filename="../../qttools/src/assistant/help/qhelpcollectionhandler.cpp" line="+110"/>
        <source>The collection file &quot;%1&quot; is not set up yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Cannot load sqlite database driver.</source>
        <translation>Nem lehet betölteni az sqlite adatbázis-meghajtót.</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+222"/>
        <source>Cannot open collection file: %1</source>
        <translation>Nem lehet megnyitni a gyűjteményfájlt: %1</translation>
    </message>
    <message>
        <location line="-203"/>
        <source>Cannot create tables in file %1.</source>
        <translation>Nem lehet táblákat létrehozni a(z) %1 fájlban.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Cannot create index tables in file %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Cannot register index tables in file %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Cannot unregister index tables in file %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+100"/>
        <source>The collection file &quot;%1&quot; already exists.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+416"/>
        <source>Unknown filter &quot;%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+142"/>
        <source>Invalid documentation file &quot;%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+954"/>
        <source>Cannot register namespace &quot;%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Cannot register virtual folder &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+106"/>
        <source>Version %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The collection file &apos;%1&apos; already exists.</source>
        <translation type="vanished">A(z) „%1” gyűjteményfájl már létezik.</translation>
    </message>
    <message>
        <location line="-1633"/>
        <source>Cannot create directory: %1</source>
        <translation>Nem lehet létrehozni a könyvtárat: %1</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Cannot copy collection file: %1</source>
        <translation>Nem lehet másolni a gyűjteményfájlt: %1</translation>
    </message>
    <message>
        <source>Unknown filter &apos;%1&apos;.</source>
        <translation type="vanished">Ismeretlen szűrő: „%1”.</translation>
    </message>
    <message>
        <location line="+438"/>
        <source>Cannot register filter %1.</source>
        <translation>Nem lehet regisztrálni a(z) %1 szűrőt.</translation>
    </message>
    <message>
        <location line="+82"/>
        <source>Cannot open documentation file %1.</source>
        <translation>Nem lehet megnyitni a(z) %1 dokumentációfájlt.</translation>
    </message>
    <message>
        <source>Invalid documentation file &apos;%1&apos;.</source>
        <translation type="vanished">Érvénytelen dokumentációfájl: „%1”.</translation>
    </message>
    <message>
        <location line="+39"/>
        <location line="+16"/>
        <source>The namespace %1 was not registered.</source>
        <translation>A(z) %1 névtér nem lett regisztrálva.</translation>
    </message>
    <message>
        <location line="+890"/>
        <source>Namespace %1 already exists.</source>
        <translation>A(z) %1 névtér már létezik.</translation>
    </message>
    <message>
        <source>Cannot register namespace &apos;%1&apos;.</source>
        <translation type="vanished">Nem lehet regisztrálni a(z) „%1” névteret.</translation>
    </message>
    <message>
        <source>Cannot open database &apos;%1&apos; to optimize.</source>
        <translation type="vanished">Nem lehet megnyitni a(z) „%1” adatbázist optimalizáláshoz.</translation>
    </message>
</context>
<context>
    <name>QHelpDBReader</name>
    <message>
        <source>Cannot open database &apos;%1&apos; &apos;%2&apos;: %3</source>
        <extracomment>The placeholders are: %1 - The name of the database which cannot be opened %2 - The unique id for the connection %3 - The actual error string</extracomment>
        <translation type="vanished">Nem lehet megnyitni a(z) „%1” „%2” adatbázist: %3</translation>
    </message>
    <message>
        <location filename="../../qttools/src/assistant/help/qhelpdbreader.cpp" line="+103"/>
        <source>Cannot open database &quot;%1&quot; &quot;%2&quot;: %3</source>
        <extracomment>The placeholders are: %1 - The name of the database which cannot be opened %2 - The unique id for the connection %3 - The actual error string</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QHelpEngineCore</name>
    <message>
        <source>Cannot open documentation file %1: %2.</source>
        <translation type="vanished">Nem lehet megnyitni a(z) %1 dokumentációfájlt: %2.</translation>
    </message>
    <message>
        <source>The specified namespace does not exist.</source>
        <translation type="vanished">A megadott névtér nem létezik.</translation>
    </message>
</context>
<context>
    <name>QHelpFilterSettingsWidget</name>
    <message>
        <location filename="../../qttools/src/assistant/help/qhelpfiltersettingswidget.ui"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Versions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Add...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Rename...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qttools/src/assistant/help/qhelpfiltersettingswidget.cpp" line="+177"/>
        <source>Add Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>New Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Rename Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Remove Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Are you sure you want to remove the &quot;%1&quot; filter?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Filter Exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The filter &quot;%1&quot; already exists.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+103"/>
        <source>No Component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid Component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>No Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid Version</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QHelpGenerator</name>
    <message>
        <source>Invalid help data.</source>
        <translation type="vanished">Érvénytelen súgóadatok.</translation>
    </message>
    <message>
        <source>No output file name specified.</source>
        <translation type="vanished">Nincs kimeneti fájlnév megadva.</translation>
    </message>
    <message>
        <source>The file %1 cannot be overwritten.</source>
        <translation type="vanished">A(z) %1 fájlt nem lehet felülírni.</translation>
    </message>
    <message>
        <source>Building up file structure...</source>
        <translation type="vanished">Fájlszerkezet felépítése…</translation>
    </message>
    <message>
        <source>Cannot open data base file %1.</source>
        <translation type="vanished">Nem lehet megnyitni a(z) %1 adatbázisfájlt.</translation>
    </message>
    <message>
        <source>Cannot register namespace %1.</source>
        <translation type="vanished">Nem lehet regisztrálni a(z) %1 névteret.</translation>
    </message>
    <message>
        <source>Insert custom filters...</source>
        <translation type="vanished">Egyéni szűrők beszúrása…</translation>
    </message>
    <message>
        <source>Insert help data for filter section (%1 of %2)...</source>
        <translation type="vanished">Súgóadatok beszúrása a szűrőszakaszhoz (%1 / %2)…</translation>
    </message>
    <message>
        <source>Documentation successfully generated.</source>
        <translation type="vanished">A dokumentáció sikeresen előállítva.</translation>
    </message>
    <message>
        <source>Some tables already exist.</source>
        <translation type="vanished">Néhány tábla már létezik.</translation>
    </message>
    <message>
        <source>Cannot create tables.</source>
        <translation type="vanished">Nem lehet táblákat létrehozni.</translation>
    </message>
    <message>
        <source>Cannot register virtual folder.</source>
        <translation type="vanished">Nem lehet regisztrálni a virtuális mappát.</translation>
    </message>
    <message>
        <source>Insert files...</source>
        <translation type="vanished">Fájlok beszúrása…</translation>
    </message>
    <message>
        <source>The file %1 does not exist! Skipping it.</source>
        <translation type="vanished">A(z) %1 fájl nem létezik! A fájl kihagyása.</translation>
    </message>
    <message>
        <source>Cannot open file %1! Skipping it.</source>
        <translation type="vanished">Nem lehet megnyitni a(z) %1 fájlt! A fájl kihagyása.</translation>
    </message>
    <message>
        <source>The filter %1 is already registered.</source>
        <translation type="vanished">A(z) %1 szűrő már regisztrálva van.</translation>
    </message>
    <message>
        <source>Cannot register filter %1.</source>
        <translation type="vanished">Nem lehet regisztrálni a(z) %1 szűrőt.</translation>
    </message>
    <message>
        <source>Insert indices...</source>
        <translation type="vanished">Indexek beszúrása…</translation>
    </message>
    <message>
        <source>Insert contents...</source>
        <translation type="vanished">Tartalmak beszúrása…</translation>
    </message>
    <message>
        <source>Cannot insert contents.</source>
        <translation type="vanished">Nem lehet beszúrni a tartalmakat.</translation>
    </message>
    <message>
        <source>Cannot register contents.</source>
        <translation type="vanished">Nem lehet regisztrálni a tartalmakat.</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; does not exist.</source>
        <translation type="vanished">A(z) „%1” fájl nem létezik.</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; cannot be opened.</source>
        <translation type="vanished">A(z) „%1” fájl nem nyitható meg.</translation>
    </message>
    <message>
        <source>File &apos;%1&apos; contains an invalid link to file &apos;%2&apos;</source>
        <translation type="vanished">A(z) „%1” fájl egy érvénytelen hivatkozást tartalmaz a(z) „%2” fájlra</translation>
    </message>
    <message>
        <source>Invalid links in HTML files.</source>
        <translation type="vanished">Érvénytelen hivatkozások a HTML fájlokban.</translation>
    </message>
</context>
<context>
    <name>QHelpProject</name>
    <message>
        <source>Unknown token in file &quot;%1&quot;.</source>
        <translation type="vanished">Ismeretlen vezérjel a(z) „%1” fájlban.</translation>
    </message>
    <message>
        <source>Unknown token. Expected &quot;QtHelpProject&quot;.</source>
        <translation type="vanished">Ismeretlen vezérjel. „QtHelpProject” várt.</translation>
    </message>
    <message>
        <source>Error in line %1: %2</source>
        <translation type="vanished">Hiba a(z) %1. sorban: %2</translation>
    </message>
    <message>
        <source>Virtual folder has invalid syntax in file: &quot;%1&quot;</source>
        <translation type="vanished">A virtuális mappának érvénytelen szintaxisa van a fájlban: „%1”</translation>
    </message>
    <message>
        <source>Namespace &quot;%1&quot; has invalid syntax in file: &quot;%2&quot;</source>
        <translation type="vanished">A(z) „%1” névtérnek érvénytelen szintaxisa van a fájlban: „%2”</translation>
    </message>
    <message>
        <source>Missing namespace in QtHelpProject file: &quot;%1&quot;</source>
        <translation type="vanished">Hiányzó névtér a QtHelpProject fájlban: „%1”</translation>
    </message>
    <message>
        <source>Missing virtual folder in QtHelpProject file: &quot;%1&quot;</source>
        <translation type="vanished">Hiányzó virtuális mappa a QtHelpProject fájlban: „%1”</translation>
    </message>
    <message>
        <source>The input file %1 could not be opened.</source>
        <translation type="vanished">A(z) %1 bemeneti fájlt nem sikerült megnyitni.</translation>
    </message>
</context>
<context>
    <name>QHelpSearchQueryWidget</name>
    <message>
        <location filename="../../qttools/src/assistant/help/qhelpsearchquerywidget.cpp" line="+113"/>
        <source>Search for:</source>
        <translation>Keresés erre:</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Previous search</source>
        <translation>Előző keresés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Next search</source>
        <translation>Következő keresés</translation>
    </message>
    <message>
        <location line="-3"/>
        <source>Search</source>
        <translation>Keresés</translation>
    </message>
    <message>
        <source>Advanced search</source>
        <translation type="vanished">Speciális keresés</translation>
    </message>
    <message>
        <source>words &lt;B&gt;similar&lt;/B&gt; to:</source>
        <translation type="vanished">szavak, amelyek &lt;B&gt;hasonlóak&lt;/B&gt; ehhez:</translation>
    </message>
    <message>
        <source>&lt;B&gt;without&lt;/B&gt; the words:</source>
        <translation type="vanished">ezen szavak &lt;B&gt;nélkül&lt;/B&gt;:</translation>
    </message>
    <message>
        <source>with &lt;B&gt;exact phrase&lt;/B&gt;:</source>
        <translation type="vanished">&lt;B&gt;pontos kifejezéssel&lt;/B&gt;:</translation>
    </message>
    <message>
        <source>with &lt;B&gt;all&lt;/B&gt; of the words:</source>
        <translation type="vanished">ezen szavak &lt;B&gt;mindegyikével&lt;/B&gt;:</translation>
    </message>
    <message>
        <source>with &lt;B&gt;at least one&lt;/B&gt; of the words:</source>
        <translation type="vanished">a következő szavak &lt;B&gt;legalább egyikével&lt;/B&gt;:</translation>
    </message>
</context>
<context>
    <name>QHelpSearchResultWidget</name>
    <message numerus="yes">
        <location filename="../../qttools/src/assistant/help/qhelpsearchresultwidget.cpp" line="+212"/>
        <source>%1 - %2 of %n Hits</source>
        <translation>
            <numerusform>%1 - %2 / %n találat</numerusform>
        </translation>
    </message>
    <message>
        <location line="+59"/>
        <source>0 - 0 of 0 Hits</source>
        <translation>0 - 0 / 0 találat</translation>
    </message>
</context>
<context>
    <name>QOptionsWidget</name>
    <message>
        <location filename="../../qttools/src/assistant/help/qoptionswidget.cpp" line="+89"/>
        <source>No Option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid Option</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QResultWidget</name>
    <message>
        <location filename="../../qttools/src/assistant/help/qhelpsearchresultwidget.cpp" line="-194"/>
        <source>Search Results</source>
        <translation type="unfinished">Keresési eredmények</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Note:</source>
        <translation type="unfinished">Megjegyzés:</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>The search results may not be complete since the documentation is still being indexed.</source>
        <translation type="unfinished">A keresési eredmények esetleg nem teljesek, mivel a dokumentáció még indexelés alatt van.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Your search did not match any documents.</source>
        <translation type="unfinished">A keresés nem illeszkedett egyetlen dokumentumra sem.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>(The reason for this might be that the documentation is still being indexed.)</source>
        <translation type="unfinished">(Ez amiatt lehet, hogy a dokumentáció még indexelés alatt van.)</translation>
    </message>
</context>
<context>
    <name>fulltextsearch::qt::QHelpSearchIndexWriter</name>
    <message>
        <location filename="../../qttools/src/assistant/help/qhelpsearchindexwriter_default.cpp" line="+78"/>
        <source>Cannot open database &quot;%1&quot; using connection &quot;%2&quot;: %3</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
