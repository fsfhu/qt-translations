<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu_HU">
<context>
    <name>GeoServiceProviderFactoryEsri</name>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/esri/geoserviceproviderfactory_esri.cpp" line="+71"/>
        <source>Esri plugin requires a &apos;esri.token&apos; parameter.
Please visit https://developers.arcgis.com/authentication/accessing-arcgis-online-services/</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlaceSearchReplyEsri</name>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/esri/placesearchreply_esri.cpp" line="+118"/>
        <location line="+7"/>
        <source>Response parse error</source>
        <translation type="unfinished">Válaszfeldolgozási hiba</translation>
    </message>
</context>
<context>
    <name>QDeclarativeGeoMap</name>
    <message>
        <location filename="../../qtlocation/src/location/declarativemaps/qdeclarativegeomap.cpp" line="+204"/>
        <location line="+1"/>
        <location line="+459"/>
        <location line="+1"/>
        <source>No Map</source>
        <translation>Nincs térkép</translation>
    </message>
    <message>
        <location line="-252"/>
        <source>Plugin does not support mapping.</source>
        <translation>A bővítmény nem támogatja a térképhasználatot.</translation>
    </message>
</context>
<context>
    <name>QDeclarativeGeoRouteModel</name>
    <message>
        <location filename="../../qtlocation/src/location/declarativemaps/qdeclarativegeoroutemodel.cpp" line="+372"/>
        <source>Plugin does not support routing.</source>
        <translation>A bővítmény nem támogatja az útvonaltervezést.</translation>
    </message>
    <message>
        <location line="+246"/>
        <source>Cannot route, plugin not set.</source>
        <translation>Nem lehet útvonalat tervezni, nincs bővítmény beállítva.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Cannot route, route manager not set.</source>
        <translation>Nem lehet útvonalat tervezni, nincs útvonalkezelő beállítva.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cannot route, valid query not set.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Not enough waypoints for routing.</source>
        <translation>Nincs elég útvonalpont az útvonaltervezéshez.</translation>
    </message>
</context>
<context>
    <name>QDeclarativeGeocodeModel</name>
    <message>
        <location filename="../../qtlocation/src/location/declarativemaps/qdeclarativegeocodemodel.cpp" line="+155"/>
        <source>Cannot geocode, plugin not set.</source>
        <translation>Nem lehet földrajzi koordinátákkal ellátni, nincs bővítmény beállítva.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Cannot geocode, geocode manager not set.</source>
        <translation>Nem lehet földrajzi koordinátákkal ellátni, nincs földrajzi koordináta kezelő beállítva.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cannot geocode, valid query not set.</source>
        <translation>Nem lehet földrajzi koordinátákkal ellátni, nincs érvényes lekérdezés beállítva.</translation>
    </message>
    <message>
        <location line="+146"/>
        <source>Plugin does not support (reverse) geocoding.</source>
        <translation>A bővítmény nem támogatja a (fordított) földrajzi koordináták megadását.</translation>
    </message>
</context>
<context>
    <name>QDeclarativeNavigator</name>
    <message>
        <location filename="../../qtlocation/src/location/labs/qdeclarativenavigator.cpp" line="+505"/>
        <source>Plugin does not support navigation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Failed to create a navigator object.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QGeoCodeReplyMapbox</name>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/mapbox/qgeocodereplymapbox.cpp" line="+84"/>
        <source>Response parse error</source>
        <translation type="unfinished">Válaszfeldolgozási hiba</translation>
    </message>
</context>
<context>
    <name>QGeoMapMapboxGL</name>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/mapboxgl/qgeomapmapboxgl.cpp" line="+503"/>
        <source>Development access token, do not use in production.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QGeoMappingManagerEngineItemsOverlay</name>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/itemsoverlay/qgeomappingmanagerengineitemsoverlay.cpp" line="+65"/>
        <source>Empty Map</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QGeoMappingManagerEngineMapboxGL</name>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/mapboxgl/qgeomappingmanagerenginemapboxgl.cpp" line="+88"/>
        <source>China Streets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>China Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>China Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Streets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Basic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Bright</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Outdoors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Satellite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Satellite Streets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Navigation Preview Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Navigation Preview Night</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Navigation Guidance Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Navigation Guidance Night</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>User provided style</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QGeoRouteParserOsrmV4</name>
    <message>
        <location filename="../../qtlocation/src/location/maps/qgeorouteparserosrmv4.cpp" line="+139"/>
        <source>Go straight.</source>
        <translation type="unfinished">Haladjon egyenesen.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Go straight onto %1.</source>
        <translation type="unfinished">Haladjon egyenesen erre: %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Turn slightly right.</source>
        <translation type="unfinished">Forduljon kissé jobbra.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Turn slightly right onto %1.</source>
        <translation type="unfinished">Forduljon kissé jobbra erre: %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Turn right.</source>
        <translation type="unfinished">Forduljon jobbra.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Turn right onto %1.</source>
        <translation type="unfinished">Forduljon jobbra erre: %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Make a sharp right.</source>
        <translation type="unfinished">Forduljon élesen jobbra.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Make a sharp right onto %1.</source>
        <translation type="unfinished">Forduljon élesen jobbra erre: %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>When it is safe to do so, perform a U-turn.</source>
        <translation type="unfinished">Amikor biztonságos, forduljon vissza.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Make a sharp left.</source>
        <translation type="unfinished">Forduljon élesen balra.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Make a sharp left onto %1.</source>
        <translation type="unfinished">Forduljon élesen balra erre: %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Turn left.</source>
        <translation type="unfinished">Forduljon balra.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Turn left onto %1.</source>
        <translation type="unfinished">Forduljon balra erre: %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Turn slightly left.</source>
        <translation type="unfinished">Forduljon kissé balra.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Turn slightly left onto %1.</source>
        <translation type="unfinished">Forduljon kissé balra erre: %1.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reached waypoint.</source>
        <translation type="unfinished">Útvonalpont elérve.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Head on.</source>
        <translation type="unfinished">Menjen előre.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Head onto %1.</source>
        <translation type="unfinished">Menjen előre erre: %1.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enter the roundabout.</source>
        <translation type="unfinished">Hajtson be a körforgalomba.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the roundabout take the first exit.</source>
        <translation type="unfinished">A körforgalomnál válassza a első kijáratot.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout take the first exit onto %1.</source>
        <translation type="unfinished">A körforgalomnál válassza a első kijáratot erre: %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the roundabout take the second exit.</source>
        <translation type="unfinished">A körforgalomnál válassza a második kijáratot.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout take the second exit onto %1.</source>
        <translation type="unfinished">A körforgalomnál válassza a második kijáratot erre: %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the roundabout take the third exit.</source>
        <translation type="unfinished">A körforgalomnál válassza a harmadik kijáratot.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout take the third exit onto %1.</source>
        <translation type="unfinished">A körforgalomnál válassza a harmadik kijáratot erre: %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the roundabout take the fourth exit.</source>
        <translation type="unfinished">A körforgalomnál válassza a negyedik kijáratot.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout take the fourth exit onto %1.</source>
        <translation type="unfinished">A körforgalomnál válassza a negyedik kijáratot erre: %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the roundabout take the fifth exit.</source>
        <translation type="unfinished">A körforgalomnál válassza az ötödik kijáratot.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout take the fifth exit onto %1.</source>
        <translation type="unfinished">A körforgalomnál válassza az ötödik kijáratot erre: %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the roundabout take the sixth exit.</source>
        <translation type="unfinished">A körforgalomnál válassza a hatodik kijáratot.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout take the sixth exit onto %1.</source>
        <translation type="unfinished">A körforgalomnál válassza a hatodik kijáratot erre: %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the roundabout take the seventh exit.</source>
        <translation type="unfinished">A körforgalomnál válassza a hetedik kijáratot.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout take the seventh exit onto %1.</source>
        <translation type="unfinished">A körforgalomnál válassza a hetedik kijáratot erre: %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the roundabout take the eighth exit.</source>
        <translation type="unfinished">A körforgalomnál válassza a nyolcadik kijáratot.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout take the eighth exit onto %1.</source>
        <translation type="unfinished">A körforgalomnál válassza a nyolcadik kijáratot erre: %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the roundabout take the ninth exit.</source>
        <translation type="unfinished">A körforgalomnál válassza a kilencedik kijáratot.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout take the ninth exit onto %1.</source>
        <translation type="unfinished">A körforgalomnál válassza a kilencedik kijáratot erre: %1.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Leave the roundabout.</source>
        <translation type="unfinished">Hagyja el a körforgalmat.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Leave the roundabout onto %1.</source>
        <translation type="unfinished">Hagyja el a körforgalmat erre: %1.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Stay on the roundabout.</source>
        <translation type="unfinished">Maradjon a körforgalomban.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Start at the end of the street.</source>
        <translation type="unfinished">Induljon az utca végén.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Start at the end of %1.</source>
        <translation type="unfinished">Induljon ennek a végén: %1.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>You have reached your destination.</source>
        <translation type="unfinished">Megérkezett a célhoz.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Don&apos;t know what to say for &apos;%1&apos;</source>
        <translation type="unfinished">Nem tudom, mit mondjak erre: „%1”</translation>
    </message>
</context>
<context>
    <name>QGeoRouteParserOsrmV5</name>
    <message>
        <location filename="../../qtlocation/src/location/maps/qgeorouteparserosrmv5.cpp" line="+104"/>
        <source>North</source>
        <extracomment>Translations exist at https://github.com/Project-OSRM/osrm-text-instructions. Always used in &quot;Head %1 [onto &lt;street name&gt;]&quot;</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>East</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>South</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>West</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>first</source>
        <comment>roundabout exit</comment>
        <extracomment>always used in &quot; and take the %1 exit [onto &lt;street name&gt;]&quot;</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>second</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>third</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>fourth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>fifth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>sixth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>seventh</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>eighth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ninth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>tenth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>eleventh</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>twelfth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>thirteenth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>fourteenth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>fifteenth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>sixteenth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>seventeenth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>eighteenth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>nineteenth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>twentieth</source>
        <comment>roundabout exit</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source> and take the %1 exit</source>
        <extracomment>Always appended to one of the following strings: - &quot;Enter the roundabout&quot; - &quot;Enter the rotary&quot; - &quot;Enter the rotary &lt;rotaryname&gt;&quot;</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source> and take the %1 exit onto %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>You have arrived at your destination, straight ahead</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>You have arrived at your destination, on the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>You have arrived at your destination, on the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>You have arrived at your destination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+252"/>
        <location line="+218"/>
        <source>Continue straight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-468"/>
        <source>Continue straight on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Continue left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Continue left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+213"/>
        <source>Continue slightly left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-211"/>
        <source>Continue slightly left on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Continue right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Continue right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+217"/>
        <source>Continue slightly right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-215"/>
        <source>Continue slightly right on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+113"/>
        <location line="+104"/>
        <location line="+196"/>
        <source>Make a U-turn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-411"/>
        <location line="+113"/>
        <location line="+104"/>
        <location line="+196"/>
        <source>Make a U-turn onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-410"/>
        <location line="+222"/>
        <location line="+30"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-250"/>
        <location line="+252"/>
        <source>Continue on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-242"/>
        <source>Head %1</source>
        <extracomment>%1 is &quot;North&quot;, &quot;South&quot;, &quot;East&quot; or &quot;West&quot;</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Head %1 onto %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Depart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Depart onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>At the end of the road, turn left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the end of the road, turn left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>At the end of the road, turn right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the end of the road, turn right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>At the end of the road, make a U-turn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the end of the road, make a U-turn onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the end of the road, continue straight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the end of the road, continue straight onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the end of the road, continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the end of the road, continue onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Take the ferry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>At the fork, take a sharp left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the fork, take a sharp left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the fork, turn left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the fork, turn left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>At the fork, keep left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the fork, keep left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the fork, take a sharp right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the fork, take a sharp right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the fork, turn right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the fork, turn right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>At the fork, keep right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the fork, keep right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>At the fork, continue straight ahead</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the fork, continue straight ahead onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the fork, continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the fork, continue onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Merge sharply left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Merge sharply left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Merge left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Merge left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Merge slightly left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Merge slightly left on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Merge sharply right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Merge sharply right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Merge right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Merge right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Merge slightly right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Merge slightly right on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Merge straight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Merge straight on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Merge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Merge onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Take a sharp left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Take a sharp left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+200"/>
        <source>Turn left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-198"/>
        <location line="+200"/>
        <source>Turn left onto %1</source>
        <translation type="unfinished">Forduljon balra erre: %1. {1?}</translation>
    </message>
    <message>
        <location line="-194"/>
        <source>Continue slightly left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Take a sharp right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Take a sharp right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+196"/>
        <source>Turn right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-194"/>
        <location line="+196"/>
        <source>Turn right onto %1</source>
        <translation type="unfinished">Forduljon jobbra erre: %1. {1?}</translation>
    </message>
    <message>
        <location line="-190"/>
        <source>Continue slightly right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+218"/>
        <source>Continue straight onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-213"/>
        <source>Continue onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Continue on the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Continue on the left on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Continue on the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Continue on the right on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Take the ramp on the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Take the ramp on the left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Take the ramp on the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Take the ramp on the right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Take the ramp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Take the ramp onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Get off the bike and push</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Get off the bike and push onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Enter the rotary</source>
        <extracomment>This string will be prepended to &quot; and take the &lt;nth&gt; exit [onto &lt;streetname&gt;]</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Enter the roundabout</source>
        <extracomment>This string will be prepended to &quot; and take the &lt;nth&gt; exit [onto &lt;streetname&gt;]</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>At the roundabout, continue straight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout, continue straight on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>At the roundabout, turn left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout, turn left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>At the roundabout, turn right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout, turn right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>At the roundabout, turn around</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout, turn around onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>At the roundabout, continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>At the roundabout, continue onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Take the train</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Take the train [%1]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Go straight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Go straight onto %1</source>
        <translation type="unfinished">Haladjon egyenesen erre: %1. {1?}</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Turn slightly left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Turn slightly left onto %1</source>
        <translation type="unfinished">Forduljon kissé balra erre: %1. {1?}</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Turn slightly right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Turn slightly right onto %1</source>
        <translation type="unfinished">Forduljon kissé jobbra erre: %1. {1?}</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Turn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Turn onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+29"/>
        <source> and continue straight</source>
        <extracomment>This string will be prepended with lane instructions. E.g., &quot;Use the left or the right lane and continue straight&quot;</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source> and continue straight onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source> and make a sharp left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source> and make a sharp left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source> and turn left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source> and turn left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source> and make a slight left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source> and make a slight left onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source> and make a sharp right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source> and make a sharp right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source> and turn right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source> and turn right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source> and make a slight right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source> and make a slight right onto %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source> and make a U-turn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source> and make a U-turn onto %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QGeoRouteReplyOsm</name>
    <message>
        <source>Go straight.</source>
        <translation type="vanished">Haladjon egyenesen.</translation>
    </message>
    <message>
        <source>Go straight onto %1.</source>
        <translation type="vanished">Haladjon egyenesen erre: %1.</translation>
    </message>
    <message>
        <source>Turn slightly right.</source>
        <translation type="vanished">Forduljon kissé jobbra.</translation>
    </message>
    <message>
        <source>Turn slightly right onto %1.</source>
        <translation type="vanished">Forduljon kissé jobbra erre: %1.</translation>
    </message>
    <message>
        <source>Turn right.</source>
        <translation type="vanished">Forduljon jobbra.</translation>
    </message>
    <message>
        <source>Turn right onto %1.</source>
        <translation type="vanished">Forduljon jobbra erre: %1.</translation>
    </message>
    <message>
        <source>Make a sharp right.</source>
        <translation type="vanished">Forduljon élesen jobbra.</translation>
    </message>
    <message>
        <source>Make a sharp right onto %1.</source>
        <translation type="vanished">Forduljon élesen jobbra erre: %1.</translation>
    </message>
    <message>
        <source>When it is safe to do so, perform a U-turn.</source>
        <translation type="vanished">Amikor biztonságos, forduljon vissza.</translation>
    </message>
    <message>
        <source>Make a sharp left.</source>
        <translation type="vanished">Forduljon élesen balra.</translation>
    </message>
    <message>
        <source>Make a sharp left onto %1.</source>
        <translation type="vanished">Forduljon élesen balra erre: %1.</translation>
    </message>
    <message>
        <source>Turn left.</source>
        <translation type="vanished">Forduljon balra.</translation>
    </message>
    <message>
        <source>Turn left onto %1.</source>
        <translation type="vanished">Forduljon balra erre: %1.</translation>
    </message>
    <message>
        <source>Turn slightly left.</source>
        <translation type="vanished">Forduljon kissé balra.</translation>
    </message>
    <message>
        <source>Turn slightly left onto %1.</source>
        <translation type="vanished">Forduljon kissé balra erre: %1.</translation>
    </message>
    <message>
        <source>Reached waypoint.</source>
        <translation type="vanished">Útvonalpont elérve.</translation>
    </message>
    <message>
        <source>Head on.</source>
        <translation type="vanished">Menjen előre.</translation>
    </message>
    <message>
        <source>Head onto %1.</source>
        <translation type="vanished">Menjen előre erre: %1.</translation>
    </message>
    <message>
        <source>Enter the roundabout.</source>
        <translation type="vanished">Hajtson be a körforgalomba.</translation>
    </message>
    <message>
        <source>At the roundabout take the first exit.</source>
        <translation type="vanished">A körforgalomnál válassza a első kijáratot.</translation>
    </message>
    <message>
        <source>At the roundabout take the first exit onto %1.</source>
        <translation type="vanished">A körforgalomnál válassza a első kijáratot erre: %1.</translation>
    </message>
    <message>
        <source>At the roundabout take the second exit.</source>
        <translation type="vanished">A körforgalomnál válassza a második kijáratot.</translation>
    </message>
    <message>
        <source>At the roundabout take the second exit onto %1.</source>
        <translation type="vanished">A körforgalomnál válassza a második kijáratot erre: %1.</translation>
    </message>
    <message>
        <source>At the roundabout take the third exit.</source>
        <translation type="vanished">A körforgalomnál válassza a harmadik kijáratot.</translation>
    </message>
    <message>
        <source>At the roundabout take the third exit onto %1.</source>
        <translation type="vanished">A körforgalomnál válassza a harmadik kijáratot erre: %1.</translation>
    </message>
    <message>
        <source>At the roundabout take the fourth exit.</source>
        <translation type="vanished">A körforgalomnál válassza a negyedik kijáratot.</translation>
    </message>
    <message>
        <source>At the roundabout take the fourth exit onto %1.</source>
        <translation type="vanished">A körforgalomnál válassza a negyedik kijáratot erre: %1.</translation>
    </message>
    <message>
        <source>At the roundabout take the fifth exit.</source>
        <translation type="vanished">A körforgalomnál válassza az ötödik kijáratot.</translation>
    </message>
    <message>
        <source>At the roundabout take the fifth exit onto %1.</source>
        <translation type="vanished">A körforgalomnál válassza az ötödik kijáratot erre: %1.</translation>
    </message>
    <message>
        <source>At the roundabout take the sixth exit.</source>
        <translation type="vanished">A körforgalomnál válassza a hatodik kijáratot.</translation>
    </message>
    <message>
        <source>At the roundabout take the sixth exit onto %1.</source>
        <translation type="vanished">A körforgalomnál válassza a hatodik kijáratot erre: %1.</translation>
    </message>
    <message>
        <source>At the roundabout take the seventh exit.</source>
        <translation type="vanished">A körforgalomnál válassza a hetedik kijáratot.</translation>
    </message>
    <message>
        <source>At the roundabout take the seventh exit onto %1.</source>
        <translation type="vanished">A körforgalomnál válassza a hetedik kijáratot erre: %1.</translation>
    </message>
    <message>
        <source>At the roundabout take the eighth exit.</source>
        <translation type="vanished">A körforgalomnál válassza a nyolcadik kijáratot.</translation>
    </message>
    <message>
        <source>At the roundabout take the eighth exit onto %1.</source>
        <translation type="vanished">A körforgalomnál válassza a nyolcadik kijáratot erre: %1.</translation>
    </message>
    <message>
        <source>At the roundabout take the ninth exit.</source>
        <translation type="vanished">A körforgalomnál válassza a kilencedik kijáratot.</translation>
    </message>
    <message>
        <source>At the roundabout take the ninth exit onto %1.</source>
        <translation type="vanished">A körforgalomnál válassza a kilencedik kijáratot erre: %1.</translation>
    </message>
    <message>
        <source>Leave the roundabout.</source>
        <translation type="vanished">Hagyja el a körforgalmat.</translation>
    </message>
    <message>
        <source>Leave the roundabout onto %1.</source>
        <translation type="vanished">Hagyja el a körforgalmat erre: %1.</translation>
    </message>
    <message>
        <source>Stay on the roundabout.</source>
        <translation type="vanished">Maradjon a körforgalomban.</translation>
    </message>
    <message>
        <source>Start at the end of the street.</source>
        <translation type="vanished">Induljon az utca végén.</translation>
    </message>
    <message>
        <source>Start at the end of %1.</source>
        <translation type="vanished">Induljon ennek a végén: %1.</translation>
    </message>
    <message>
        <source>You have reached your destination.</source>
        <translation type="vanished">Megérkezett a célhoz.</translation>
    </message>
    <message>
        <source>Don&apos;t know what to say for &apos;%1&apos;</source>
        <translation type="vanished">Nem tudom, mit mondjak erre: „%1”</translation>
    </message>
</context>
<context>
    <name>QGeoServiceProviderFactoryMapbox</name>
    <message>
        <source>Mapbox plugin requires &apos;mapbox.map_id&apos; and &apos;mapbox.access_token&apos; parameters.
Please visit https://www.mapbox.com</source>
        <translation type="vanished">A Mapbox bővítménynek a „mapbox.map_id” és a „mapbox.access_token” paraméterekre van szüksége. Látogassa meg a https://www.mapbox.com oldalt.</translation>
    </message>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/mapbox/qgeoserviceproviderpluginmapbox.cpp" line="+49"/>
        <source>Mapbox plugin requires a &apos;mapbox.access_token&apos; parameter.
Please visit https://www.mapbox.com</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QGeoTileFetcherNokia</name>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/nokia/qgeotilefetcher_nokia.cpp" line="+112"/>
        <source>Mapping manager no longer exists</source>
        <translation>A térképkezelő többé nem létezik</translation>
    </message>
</context>
<context>
    <name>QGeoTiledMapOsm</name>
    <message>
        <source>Tiles Courtesy of &lt;a href=&apos;http://www.mapquest.com/&apos;&gt;MapQuest&lt;/a&gt;&lt;br/&gt;Data &amp;copy; &lt;a href=&apos;http://www.openstreetmap.org/copyright&apos;&gt;OpenStreetMap&lt;/a&gt; contributors</source>
        <translation type="vanished">Csempék a &lt;a href=&apos;http://www.mapquest.com/&apos;&gt;MapQuest&lt;/a&gt; jóvoltából.&lt;br/&gt;Adatok: &amp;copy; &lt;a href=&apos;http://www.openstreetmap.org/copyright&apos;&gt;OpenStreetMap&lt;/a&gt; közreműködők</translation>
    </message>
    <message>
        <source>Maps &amp;copy; &lt;a href=&apos;http://www.thunderforest.com/&apos;&gt;Thunderforest&lt;/a&gt;&lt;br/&gt;Data &amp;copy; &lt;a href=&apos;http://www.openstreetmap.org/copyright&apos;&gt;OpenStreetMap&lt;/a&gt; contributors</source>
        <translation type="vanished">Térképek: &amp;copy; &lt;a href=&apos;http://www.thunderforest.com/&apos;&gt;Thunderforest&lt;/a&gt;&lt;br/&gt;Adatok: &amp;copy; &lt;a href=&apos;http://www.openstreetmap.org/copyright&apos;&gt;OpenStreetMap&lt;/a&gt; közreműködők</translation>
    </message>
    <message>
        <source>&amp;copy; &lt;a href=&apos;http://www.openstreetmap.org/copyright&apos;&gt;OpenStreetMap&lt;/a&gt; contributors</source>
        <translation type="vanished">&amp;copy; &lt;a href=&apos;http://www.openstreetmap.org/copyright&apos;&gt;OpenStreetMap&lt;/a&gt; közreműködők</translation>
    </message>
</context>
<context>
    <name>QGeoTiledMappingManagerEngineMapbox</name>
    <message>
        <source>Custom</source>
        <translation type="vanished">Egyéni</translation>
    </message>
    <message>
        <source>Mapbox custom map</source>
        <translation type="vanished">Mapbox egyéni térkép</translation>
    </message>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/mapbox/qgeotiledmappingmanagerenginemapbox.cpp" line="+83"/>
        <source>Street</source>
        <extracomment>Noun describing map type &apos;Street map&apos;</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Light</source>
        <extracomment>Noun describing type of a map using light colors (weak contrast)</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Dark</source>
        <extracomment>Noun describing type of a map using dark colors</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Satellite</source>
        <extracomment>Noun describing type of a map created by satellite</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Streets Satellite</source>
        <extracomment>Noun describing type of a street map created by satellite</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Wheatpaste</source>
        <extracomment>Noun describing type of a map using wheat paste colors</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Streets Basic</source>
        <extracomment>Noun describing type of a basic street map</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Comic</source>
        <extracomment>Noun describing type of a map using cartoon-style fonts</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Outdoors</source>
        <extracomment>Noun describing type of a map for outdoor activities</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Run Bike Hike</source>
        <extracomment>Noun describing type of a map for sports</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Pencil</source>
        <extracomment>Noun describing type of a map drawn by pencil</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Pirates</source>
        <extracomment>Noun describing type of a treasure map with pirate boat watermark</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Emerald</source>
        <extracomment>Noun describing type of a map using emerald colors</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>High Contrast</source>
        <extracomment>Noun describing type of a map with high contrast</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QGeoTiledMappingManagerEngineNokia</name>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/nokia/qgeotiledmappingmanagerengine_nokia.cpp" line="+96"/>
        <source>Street Map</source>
        <translation>Utcatérkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <location line="+11"/>
        <source>Normal map view in daylight mode</source>
        <translation>Normál térképnézet nappali módban</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Satellite Map</source>
        <translation>Műholdas térkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Satellite map view in daylight mode</source>
        <translation>Műholdas térképnézet nappali módban</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Terrain Map</source>
        <translation>Domborzati térkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Terrain map view in daylight mode</source>
        <translation>Domborzati térképnézet nappali módban</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hybrid Map</source>
        <translation>Hibrid térkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Satellite map view with streets in daylight mode</source>
        <translation>Műholdas térképnézet utcákkal nappali módban</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transit Map</source>
        <translation>Forgalmi térkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Color-reduced map view with public transport scheme in daylight mode</source>
        <translation>Színcsökkentett térképnézet tömegközlekedési adatokkal nappali módban</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Gray Street Map</source>
        <translation>Szürke utcatérkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Color-reduced map view in daylight mode</source>
        <translation>Színcsökkentett térképnézet nappali módban</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mobile Street Map</source>
        <translation>Mobil utcatérkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Mobile normal map view in daylight mode</source>
        <translation>Mobil normál térképnézet nappali módban</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mobile Terrain Map</source>
        <translation>Mobil domborzati térkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Mobile terrain map view in daylight mode</source>
        <translation>Mobil domborzati térképnézet nappali módban</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mobile Hybrid Map</source>
        <translation>Mobil hibrid térkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Mobile satellite map view with streets in daylight mode</source>
        <translation>Mobil műholdas térképnézet utcákkal nappali módban</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mobile Transit Map</source>
        <translation>Mobil forgalmi térkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Mobile color-reduced map view with public transport scheme in daylight mode</source>
        <translation>Mobil színcsökkentett térképnézet tömegközlekedési adatokkal nappali módban</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mobile Gray Street Map</source>
        <translation>Mobil szürke utcatérkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Mobile color-reduced map view in daylight mode</source>
        <translation>Mobil színcsökkentett térképnézet nappali módban</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Custom Street Map</source>
        <translation>Egyéni utcatérkép</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Night Street Map</source>
        <translation>Éjszakai utcatérkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Normal map view in night mode</source>
        <translation>Normál térképnézet éjszakai módban</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mobile Night Street Map</source>
        <translation>Mobil éjszakai utcatérkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Mobile normal map view in night mode</source>
        <translation>Mobil normál térképnézet éjszakai módban</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Gray Night Street Map</source>
        <translation>Szürke éjszakai utcatérkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Color-reduced map view in night mode (especially used for background maps)</source>
        <translation>Színcsökkentett térképnézet éjszakai módban (főleg háttértérképekhez használható)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mobile Gray Night Street Map</source>
        <translation>Mobil szürke éjszakai utcatérkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Mobile color-reduced map view in night mode (especially used for background maps)</source>
        <translation>Mobil színcsökkentett térképnézet éjszakai módban (főleg háttértérképekhez használható)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pedestrian Street Map</source>
        <translation>Gyalogos utcatérkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Pedestrian map view in daylight mode</source>
        <translation>Gyalogos térképnézet nappali módban</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mobile Pedestrian Street Map</source>
        <translation>Mobil gyalogos utcatérkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Mobile pedestrian map view in daylight mode for mobile usage</source>
        <translation>Mobil gyalogos térképnézet nappali módban mobil használathoz</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pedestrian Night Street Map</source>
        <translation>Gyalogos éjszakai utcatérkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Pedestrian map view in night mode</source>
        <translation>Gyalogos térképnézet éjszakai módban</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Mobile Pedestrian Night Street Map</source>
        <translation>Mobil gyalogos éjszakai utcatérkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Mobile pedestrian map view in night mode for mobile usage</source>
        <translation>Mobil gyalogos térképnézet éjszakai módban mobil használathoz</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Car Navigation Map</source>
        <translation>Autónavigációs térkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Normal map view in daylight mode for car navigation</source>
        <translation>Normál térképnézet nappali módban autónavigációhoz</translation>
    </message>
</context>
<context>
    <name>QGeoTiledMappingManagerEngineOsm</name>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/osm/qgeotiledmappingmanagerengineosm.cpp" line="+180"/>
        <source>Street Map</source>
        <translation>Utcatérkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Street map view in daylight mode</source>
        <translation>Utcai térképnézet nappali módban</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Satellite Map</source>
        <translation>Műholdas térkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Satellite map view in daylight mode</source>
        <translation>Műholdas térkép nappali módban</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cycle Map</source>
        <translation>Kerékpáros térkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Cycle map view in daylight mode</source>
        <translation>Kerékpáros térképnézet nappali módban</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Transit Map</source>
        <translation>Forgalmi térkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Public transit map view in daylight mode</source>
        <translation>Tömegközlekedési térképnézet nappali módban</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Night Transit Map</source>
        <translation>Éjszakai forgalmi térkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Public transit map view in night mode</source>
        <translation>Tömegközlekedési térképnézet éjszakai módban</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Terrain Map</source>
        <translation>Domborzati térkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Terrain map view</source>
        <translation>Domborzati térképnézet</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Hiking Map</source>
        <translation>Túratérkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Hiking map view</source>
        <translation>Túratérkép nézet</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Custom URL Map</source>
        <translation>Egyéni URL térkép</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Custom url map view set via urlprefix parameter</source>
        <translation>Egyéni URL térképnézet az urlprefix paraméteren keresztül megadva</translation>
    </message>
</context>
<context>
    <name>QPlaceManagerEngineOsm</name>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/osm/qplacemanagerengineosm.cpp" line="+62"/>
        <source>Aeroway</source>
        <translation>Légi folyosó</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Amenity</source>
        <translation>Szolgáltatás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Building</source>
        <translation>Épület</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Highway</source>
        <translation>Autópálya</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Historic</source>
        <translation>Történelmi</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Land use</source>
        <translation>Területhasználat</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Leisure</source>
        <translation>Szabadidő</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Man made</source>
        <translation>Mesterséges</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Natural</source>
        <translation>Természetes</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Place</source>
        <translation>Hely</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Railway</source>
        <translation>Vasút</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Shop</source>
        <translation>Bolt</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Tourism</source>
        <translation>Turizmus</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Waterway</source>
        <translation>Vízi út</translation>
    </message>
    <message>
        <location line="+238"/>
        <source>Network request error</source>
        <translation>Hálózati kérés hiba</translation>
    </message>
</context>
<context>
    <name>QPlaceSearchReplyMapbox</name>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/mapbox/qplacesearchreplymapbox.cpp" line="+174"/>
        <source>Response parse error</source>
        <translation type="unfinished">Válaszfeldolgozási hiba</translation>
    </message>
</context>
<context>
    <name>QPlaceSearchReplyOsm</name>
    <message>
        <source>Communication error</source>
        <translation type="vanished">Kommunikációs hiba</translation>
    </message>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/osm/qplacesearchreplyosm.cpp" line="+108"/>
        <source>Response parse error</source>
        <translation>Válaszfeldolgozási hiba</translation>
    </message>
</context>
<context>
    <name>QPlaceSearchSuggestionReplyMapbox</name>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/mapbox/qplacesearchsuggestionreplymapbox.cpp" line="+93"/>
        <source>Response parse error</source>
        <translation type="unfinished">Válaszfeldolgozási hiba</translation>
    </message>
</context>
<context>
    <name>QtLocationQML</name>
    <message>
        <location filename="../../qtlocation/src/location/declarativemaps/error_messages.cpp" line="+44"/>
        <source>Plugin property is not set.</source>
        <translation>Bővítménytulajdonság nincs beállítva.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Plugin Error (%1): %2</source>
        <translation>Bővítményhiba (%1): %2</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Plugin Error (%1): Could not instantiate provider</source>
        <translation>Bővítményhiba (%1): nem sikerült példányosítani a szolgáltatót</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Plugin is not valid</source>
        <translation>A bővítmény nem érvényes</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to initialize categories</source>
        <translation>Nem készíthetők elő a kategóriák</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to create request</source>
        <translation>Nem hozható létre a kérés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Index &apos;%1&apos; out of range</source>
        <translation>A(z) „%1” index kívül esik a tartományon</translation>
    </message>
    <message>
        <location filename="../../qtlocation/src/plugins/geoservices/nokia/qgeoerror_messages.cpp" line="+42"/>
        <source>Qt Location requires app_id and token parameters.
Please register at https://developer.here.com/ to get your personal application credentials.</source>
        <translation>A Qt helymeghatározáshoz az app_id és a token paraméterek szükségesek.
Regisztráljon a https://developer.here.com/ címen a személyes alkalmazás hitelesítési adatok beszerzéséhez.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Saving places is not supported.</source>
        <translation>A helyek mentése nem támogatott.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Removing places is not supported.</source>
        <translation>A helyek eltávolítása nem támogatott.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Saving categories is not supported.</source>
        <translation>A kategóriák mentése nem támogatott.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Removing categories is not supported.</source>
        <translation>A kategóriák eltávolítása nem támogatott.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Error parsing response.</source>
        <translation>Hiba a válasz feldolgozásakor.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Network error.</source>
        <translation>Hálózati hiba.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Request was canceled.</source>
        <translation>A kérést megszakították.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The response from the service was not in a recognizable format.</source>
        <translation>A szolgáltatástól érkező válasz nem felismerhető formátumban volt.</translation>
    </message>
</context>
</TS>
