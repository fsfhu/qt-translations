<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu_HU">
<context>
    <name>QBluetoothDeviceDiscoveryAgent</name>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/osx/uistrings.cpp" line="+48"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothdevicediscoveryagent_android.cpp" line="+147"/>
        <location line="+178"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothdevicediscoveryagent_bluez.cpp" line="+193"/>
        <location line="+62"/>
        <source>Device is powered off</source>
        <translation>Az eszköz ki van kapcsolva</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothdevicediscoveryagent_bluez.cpp" line="-12"/>
        <source>Cannot find valid Bluetooth adapter.</source>
        <translation>Nem található érvényes Bluetooth adapter.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Input Output Error</source>
        <translation>Bemeneti-kimeneti hiba</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bluetooth LE is not supported</source>
        <translation>A Bluetooth LE nem támogatott</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unknown error</source>
        <translation>Ismeretlen hiba</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot start device inquiry</source>
        <translation>Nem lehet elindítani az eszközvizsgálatot</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot start low energy device inquiry</source>
        <translation>Nem lehet elindítani az alacsony energiájú eszközlekérdezést</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothdevicediscoveryagent_android.cpp" line="-32"/>
        <source>Discovery cannot be stopped</source>
        <translation>A felderítést nem lehet leállítani</translation>
    </message>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothdevicediscoveryagent.cpp" line="+242"/>
        <source>Invalid Bluetooth adapter address</source>
        <translation>Érvénytelen Bluetooth adapter cím</translation>
    </message>
    <message>
        <location line="+144"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothdevicediscoveryagent_bluez.cpp" line="+36"/>
        <source>One or more device discovery methods are not supported on this platform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothdevicediscoveryagent_android.cpp" line="-164"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothdevicediscoveryagent_win.cpp" line="+373"/>
        <source>Device does not support Bluetooth</source>
        <translation>Az eszköznek nincs Bluetooth támogatása</translation>
    </message>
    <message>
        <location line="+10"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothdevicediscoveryagent_win.cpp" line="+15"/>
        <source>Passed address is not a local device.</source>
        <translation>Az átadott cím nem helyi eszköz.</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Missing Location permission. Search is not possible.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Location service turned off. Search is not possible.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Classic Discovery cannot be started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Low Energy Discovery not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Discovery cannot be started</source>
        <translation type="vanished">A felderítést nem lehet elindítani</translation>
    </message>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothdevicediscoveryagent_bluez.cpp" line="+358"/>
        <source>Bluetooth adapter error</source>
        <translation>Bluetooth adapter hiba</translation>
    </message>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothdevicediscoveryagent_p.cpp" line="+81"/>
        <source>Device discovery not supported on this platform</source>
        <translation>Az eszközfelderítés nem támogatott ezen a platformon</translation>
    </message>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothservicediscoveryagent_bluez.cpp" line="+150"/>
        <source>Cannot access adapter during service discovery</source>
        <translation>Nem lehet hozzáférni az adapterhez a szolgáltatásfelderítés közben</translation>
    </message>
</context>
<context>
    <name>QBluetoothServiceDiscoveryAgent</name>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/osx/uistrings.cpp" line="+3"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothservicediscoveryagent_bluez.cpp" line="+27"/>
        <source>Local device is powered off</source>
        <translation>A helyi eszköz ki van kapcsolva</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Minimal service discovery failed</source>
        <translation>A minimális szolgáltatásfelderítés sikertelen</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothservicediscoveryagent.cpp" line="+190"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothservicediscoveryagent_android.cpp" line="+134"/>
        <source>Invalid Bluetooth adapter address</source>
        <translation>Érvénytelen Bluetooth adapter cím</translation>
    </message>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothservicediscoveryagent_android.cpp" line="-5"/>
        <source>Platform does not support Bluetooth</source>
        <translation>A platformnak nincs Bluetooth támogatása</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Android API below v15 does not support SDP discovery</source>
        <translation>A 15. verzió alatti Android API nem támogatja az SDP felderítést</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Cannot create Android BluetoothDevice</source>
        <translation>Nem lehet létrehozni Android Bluetooth eszközt</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Cannot obtain service uuids</source>
        <translation>Nem lehet beszerezni a szolgáltatás uuid-jait</translation>
    </message>
    <message>
        <location line="+220"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothuuid.cpp" line="+748"/>
        <source>Serial Port Profile</source>
        <translation>Soros port profil</translation>
    </message>
    <message>
        <location line="+83"/>
        <source>Device is powered off</source>
        <translation>Az eszköz ki van kapcsolva</translation>
    </message>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothservicediscoveryagent_bluez.cpp" line="-68"/>
        <source>Unable to find appointed local adapter</source>
        <translation>Nem található kijelölt helyi adapter</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Cannot find local Bluetooth adapter</source>
        <translation>Nem található helyi Bluetooth adapter</translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Unable to find sdpscanner</source>
        <translation>Nem található SDP kereső</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Unable to perform SDP scan</source>
        <translation>Nem hajtható végre az SDP keresés</translation>
    </message>
    <message>
        <location line="+159"/>
        <location line="+44"/>
        <location line="+93"/>
        <source>Unable to access device</source>
        <translation>Nem lehet hozzáférni az eszközhöz</translation>
    </message>
    <message>
        <location line="-218"/>
        <location line="+410"/>
        <source>Custom Service</source>
        <translation>Egyéni szolgáltatás</translation>
    </message>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothuuid.cpp" line="-3"/>
        <source>Service Discovery</source>
        <translation>Szolgáltatásfelderítés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Browse Group Descriptor</source>
        <translation>Tallózási csoport leíró</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Public Browse Group</source>
        <translation>Nyilvános tallózási csoport</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>LAN Access Profile</source>
        <translation>LAN hozzáférési profil</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dial-Up Networking</source>
        <translation>Telefonos hálózat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Synchronization</source>
        <translation>Szinkronizáció</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Object Push</source>
        <translation>Objektum leküldése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>File Transfer</source>
        <translation>Fájlátvitel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Synchronization Command</source>
        <translation>Szinkronizálási parancs</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Headset</source>
        <translation>Fejhallgató</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Audio Source</source>
        <translation>Hangforrás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Audio Sink</source>
        <translation>Hangfogadó</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Audio/Video Remote Control Target</source>
        <translation>Hang/videó távvezérlési cél</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Advanced Audio Distribution</source>
        <translation>Speciális hangterjesztés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Audio/Video Remote Control</source>
        <translation>Hang/videó távvezérlés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Audio/Video Remote Control Controller</source>
        <translation>Hang/videó távvezérlés vezérlő</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Headset AG</source>
        <translation>Fejhallgató AG</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Personal Area Networking (PANU)</source>
        <translation>Személyes területi hálózat (PANU)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Personal Area Networking (NAP)</source>
        <translation>Személyes területi hálózat (NAP)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Personal Area Networking (GN)</source>
        <translation>Személyes területi hálózat (GN)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Basic Direct Printing (BPP)</source>
        <translation>Alap közvetlen nyomtatás (BPP)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Basic Reference Printing (BPP)</source>
        <translation>Alap hivatkozás nyomtatás (BPP)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Basic Imaging Profile</source>
        <translation>Alap képkezelési profil</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Basic Imaging Responder</source>
        <translation>Alap képkezelési válaszadó</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Basic Imaging Archive</source>
        <translation>Alap képkezelési archívum</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Basic Imaging Ref Objects</source>
        <translation>Alap képkezelési hivatkozási objektumok</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hands-Free</source>
        <translation>Kihangosító</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hands-Free AG</source>
        <translation>Kihangosító AG</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Basic Printing RefObject Service</source>
        <translation>Alap nyomtatási hivatkozási objektum szolgáltatás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Basic Printing Reflected UI</source>
        <translation>Alap nyomtatási visszatükröződő grafikus felület</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Basic Printing</source>
        <translation>Alap nyomtatás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Basic Printing Status</source>
        <translation>Alap nyomtatási állapot</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+46"/>
        <source>Human Interface Device</source>
        <translation>Külső kezelőeszköz</translation>
    </message>
    <message>
        <location line="-45"/>
        <source>Hardcopy Cable Replacement</source>
        <translation>Papírra nyomtatott kábelhelyettesítő</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hardcopy Cable Replacement Print</source>
        <translation>Papírra nyomtatott kábelhelyettesítő nyomtatás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hardcopy Cable Replacement Scan</source>
        <translation>Papírra nyomtatott kábelhelyettesítő keresés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>SIM Access Server</source>
        <translation>SIM elérést biztosító kiszolgáló</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Phonebook Access PCE</source>
        <translation>Telefonkönyv hozzáférés PCE</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Phonebook Access PSE</source>
        <translation>Telefonkönyv hozzáférés PSE</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Phonebook Access</source>
        <translation>Telefonkönyv hozzáférés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Headset HS</source>
        <translation>Fejhallgató HS</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Message Access Server</source>
        <translation>Üzenetelérési kiszolgáló</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Message Notification Server</source>
        <translation>Üzenetértesítési kiszolgáló</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Message Access</source>
        <translation>Üzenetelérés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Global Navigation Satellite System</source>
        <translation>Globális navigációs műholdrendszer</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Global Navigation Satellite System Server</source>
        <translation>Globális navigációs műholdrendszer kiszolgáló</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>3D Synchronization Display</source>
        <translation>3D szinkronizálási megjelenítő</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>3D Synchronization Glasses</source>
        <translation>3D szinkronizálási szemüveg</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>3D Synchronization</source>
        <translation>3D szinkronizálás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Multi-Profile Specification (Profile)</source>
        <translation>Több-profil meghatározás (profil)</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Multi-Profile Specification</source>
        <translation>Több-profil meghatározás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Device Identification</source>
        <translation>Eszközazonosítás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Generic Networking</source>
        <translation>Általános hálózatkezelés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Generic File Transfer</source>
        <translation>Általános fájlátvitel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Generic Audio</source>
        <translation>Általános hang</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Generic Telephony</source>
        <translation>Általános telefonos szolgáltatás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Video Source</source>
        <translation>Videoforrás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Video Sink</source>
        <translation>Videofogadó</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Video Distribution</source>
        <translation>Videoterjesztés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Health Device</source>
        <translation>Egészségügyi eszköz</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Health Device Source</source>
        <translation>Egészségügyi eszköz forrás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Health Device Sink</source>
        <translation>Egészségügyi eszköz fogadó</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Generic Access</source>
        <translation>Általános hozzáférés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Generic Attribute</source>
        <translation>Általános attribútum</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Immediate Alert</source>
        <translation>Azonnali riasztás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Link Loss</source>
        <translation>Kapcsolat elvesztése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Tx Power</source>
        <translation>TX energia</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Current Time Service</source>
        <translation>Jelenlegi időszolgáltatás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Reference Time Update Service</source>
        <translation>Viszonyítási idő frissítési szolgáltatás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Next DST Change Service</source>
        <translation>Következő DST változás szolgáltatás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Glucose</source>
        <translation>Vércukor</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Health Thermometer</source>
        <translation>Egészségügyi lázmérő</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Device Information</source>
        <translation>Eszközinformációk</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Heart Rate</source>
        <translation>Pulzus</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Phone Alert Status Service</source>
        <translation>Telefonos riasztási állapot szolgáltatás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Battery Service</source>
        <translation>Akkumulátorszolgáltatás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Blood Pressure</source>
        <translation>Vérnyomás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Alert Notification Service</source>
        <translation>Riasztásértesítő szolgáltatás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Scan Parameters</source>
        <translation>Keresési paraméterek</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Running Speed and Cadence</source>
        <translation>Futási sebesség és lépésszám</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cycling Speed and Cadence</source>
        <translation>Kerékpározási sebesség és lépésszám</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cycling Power</source>
        <translation>Kerékpározási teljesítmény</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Location and Navigation</source>
        <translation>Helyzet és navigáció</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Environmental Sensing</source>
        <translation>Környezeti érzékelés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Body Composition</source>
        <translation>Testfelépítés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>User Data</source>
        <translation>Felhasználói adatok</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Weight Scale</source>
        <translation>Mérleg</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Bond Management</source>
        <extracomment>Connection management (Bluetooth)</extracomment>
        <translation>Kötéskezelés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Continuous Glucose Monitoring</source>
        <translation>Folyamatos vércukorszint-figyelés</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Service Discovery Protocol</source>
        <translation>Szolgáltatásfelderítési protokoll</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>User Datagram Protocol</source>
        <translation>Felhasználói adatcsomag-protokoll</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Radio Frequency Communication</source>
        <translation>Rádiófrekvenciás kommunikáció</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transmission Control Protocol</source>
        <translation>Átvitelvezérlő protokoll</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Telephony Control Specification - Binary</source>
        <translation>Telefonos hálózati vezérlő specifikáció ‒ Bináris</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Telephony Control Specification - AT</source>
        <translation>Telefonos hálózati vezérlő specifikáció ‒ AT</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Attribute Protocol</source>
        <translation>Attribútumprotokoll</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Object Exchange Protocol</source>
        <translation>Objektumkicserélő protokoll</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Internet Protocol</source>
        <translation>Internetprotokoll</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>File Transfer Protocol</source>
        <translation>Fájlátviteli protokoll</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hypertext Transfer Protocol</source>
        <translation>Hiperszöveg-átviteli protokoll</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wireless Short Packet Protocol</source>
        <translation>Vezeték nélküli rövid csomag protokoll</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Bluetooth Network Encapsulation Protocol</source>
        <translation>Bluetooth hálózatbeágyazási protokoll</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Extended Service Discovery Protocol</source>
        <translation>Kiterjesztett szolgáltatásfelderítési protokoll</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Human Interface Device Protocol</source>
        <translation>Külső kezelőeszköz protokoll</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hardcopy Control Channel</source>
        <translation>Papírra nyomtatott vezérlőcsatorna</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hardcopy Data Channel</source>
        <translation>Papírra nyomtatott adatcsatorna</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hardcopy Notification</source>
        <translation>Papírra nyomtatott értesítés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Audio/Video Control Transport Protocol</source>
        <translation>Hang/videó vezérlésátviteli protokoll</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Audio/Video Distribution Transport Protocol</source>
        <translation>Hang/videó terjesztésátviteli protokoll</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Common ISDN Access Protocol</source>
        <translation>Közös ISDN-hozzáférési protokoll</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>UdiCPlain</source>
        <translation>UdiCPlain</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Multi-Channel Adaptation Protocol - Control</source>
        <translation>Többcsatornás adaptációs protokoll ‒ vezérlés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Multi-Channel Adaptation Protocol - Data</source>
        <translation>Többcsatornás adaptációs protokoll ‒ adat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Layer 2 Control Protocol</source>
        <translation>2. rétegbeli vezérlési protokoll</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>GAP Device Name</source>
        <extracomment>GAP: Generic Access Profile (Bluetooth)</extracomment>
        <translation>GAP eszköznév</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>GAP Appearance</source>
        <extracomment>GAP: Generic Access Profile (Bluetooth)</extracomment>
        <translation>GAP megjelenés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>GAP Peripheral Privacy Flag</source>
        <extracomment>GAP: Generic Access Profile (Bluetooth)</extracomment>
        <translation>GAP perifériás adatvédelmi jelző</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>GAP Reconnection Address</source>
        <extracomment>GAP: Generic Access Profile (Bluetooth)</extracomment>
        <translation>GAP újrakapcsolási cím</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>GAP Peripheral Preferred Connection Parameters</source>
        <translation>GAP előnyben részesített perifériás kapcsolati paraméterek</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>GATT Service Changed</source>
        <extracomment>GATT: _G_eneric _Att_ribute Profile (Bluetooth)</extracomment>
        <translation>Megváltozott GATT-szolgáltatás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Alert Level</source>
        <translation>Riasztási szint</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>TX Power</source>
        <translation>TX energia</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date Time</source>
        <translation>Dátum és idő</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Day Of Week</source>
        <translation>Hét napja</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Day Date Time</source>
        <translation>Nap, dátum és idő</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Exact Time 256</source>
        <translation>Pontos időpont 256</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>DST Offset</source>
        <translation>Nyári időszámítás-eltolás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Time Zone</source>
        <translation>Időzóna</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Local Time Information</source>
        <translation>Helyi időinformációk</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Time With DST</source>
        <translation>Idő nyári időszámítással</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Time Accuracy</source>
        <translation>Időpontosság</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Time Source</source>
        <translation>Időforrás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Reference Time Information</source>
        <translation>Referenciaidő-információk</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Time Update Control Point</source>
        <translation>Időfrissítési ellenőrző pont</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Time Update State</source>
        <translation>Időfrissítési állapot</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Glucose Measurement</source>
        <translation>Vércukorszint-mérés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Battery Level</source>
        <translation>Akkumulátorszint</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Temperature Measurement</source>
        <translation>Hőmérsékletmérés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Temperature Type</source>
        <translation>Hőmérséklettípus</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Intermediate Temperature</source>
        <translation>Köztes hőmérséklet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Measurement Interval</source>
        <translation>Mérési időköz</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Boot Keyboard Input Report</source>
        <translation>Indítóbillentyűzet bemeneti jelentés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>System ID</source>
        <translation>Rendszerazonosító</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Model Number String</source>
        <translation>Modellszám karakterlánca</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Serial Number String</source>
        <translation>Sorozatszám karakterlánca</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Firmware Revision String</source>
        <translation> Firmware-változat karakterlánca</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hardware Revision String</source>
        <translation>Hardverváltozat karakterlánca</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Software Revision String</source>
        <translation>Szoftverváltozat karakterlánca</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Manufacturer Name String</source>
        <translation>Gyártó nevének karakterlánca</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>IEEE 11073 20601 Regulatory Certification Data List</source>
        <translation>IEEE 11073 20601 szabályozó tanúsítvány adatlista</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Current Time</source>
        <translation>Jelenlegi idő</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scan Refresh</source>
        <translation>Keresésfrissítés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Boot Keyboard Output Report</source>
        <translation>Indítóbillentyűzet kimeneti jelentés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Boot Mouse Input Report</source>
        <translation>Indítóegér bemeneti jelentés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Glucose Measurement Context</source>
        <translation>Vércukorszint-mérési környezet</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Blood Pressure Measurement</source>
        <translation>Vérnyomás-mérés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Intermediate Cuff Pressure</source>
        <translation>Köztes mandzsetta nyomás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Heart Rate Measurement</source>
        <translation>Pulzusmérés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Body Sensor Location</source>
        <translation>Testszenzor helye</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Heart Rate Control Point</source>
        <translation>Pulzusellenőrző pont</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Alert Status</source>
        <translation>Riasztási állapot</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ringer Control Point</source>
        <translation>Csengőellenőrző pont</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ringer Setting</source>
        <translation>Csengőbeállítás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Alert Category ID Bit Mask</source>
        <translation>Riasztáskategória-azonosító bitmaszk</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Alert Category ID</source>
        <translation>Riasztáskategória-azonosító</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Alert Notification Control Point</source>
        <translation>Riasztásértesítő-ellenőrző pont</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unread Alert Status</source>
        <translation>Olvasatlan riasztási állapot</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>New Alert</source>
        <translation>Új riasztás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Supported New Alert Category</source>
        <translation>Támogatott új riasztáskategória</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Supported Unread Alert Category</source>
        <translation>Támogatott olvasatlan riasztáskategória</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Blood Pressure Feature</source>
        <translation>Vérnyomás-funkció</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>HID Information</source>
        <extracomment>HID: Human Interface Device Profile (Bluetooth)</extracomment>
        <translation>HID-információk</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Report Map</source>
        <translation>Jelentéstérkép</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>HID Control Point</source>
        <extracomment>HID: Human Interface Device Profile (Bluetooth)</extracomment>
        <translation>HID-ellenőrző pont</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Report</source>
        <translation>Jelentés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Protocol Mode</source>
        <translation>Protokollmód</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Scan Interval Window</source>
        <translation>Keresési időköz ablak</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>PnP ID</source>
        <translation>PnP azonosító</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Glucose Feature</source>
        <translation>Vércukor-funkció</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Record Access Control Point</source>
        <extracomment>Glucose Sensor patient record database.</extracomment>
        <translation>Rögzítés-hozzáférési ellenőrző pont</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>RSC Measurement</source>
        <extracomment>RSC: Running Speed and Cadence</extracomment>
        <translation>RSC-mérés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>RSC Feature</source>
        <extracomment>RSC: Running Speed and Cadence</extracomment>
        <translation>RSC-funkció</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>SC Control Point</source>
        <translation>SC-ellenőrző pont</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>CSC Measurement</source>
        <extracomment>CSC: Cycling Speed and Cadence</extracomment>
        <translation>CSC-mérés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>CSC Feature</source>
        <extracomment>CSC: Cycling Speed and Cadence</extracomment>
        <translation>CSC-funkció</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Sensor Location</source>
        <translation>Érzékelő helye</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cycling Power Measurement</source>
        <translation>Kerékpározási teljesítmény mérés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cycling Power Vector</source>
        <translation>Kerékpározási teljesítményvektor</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cycling Power Feature</source>
        <translation>Kerékpározási teljesítmény-funkció</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cycling Power Control Point</source>
        <translation>Kerékpározási teljesítmény ellenőrző pont</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Location And Speed</source>
        <translation>Hely és sebesség</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Navigation</source>
        <translation>Navigáció</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Position Quality</source>
        <translation>Helyzetminőség</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>LN Feature</source>
        <translation>LN-funkció</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>LN Control Point</source>
        <translation>LN-ellenőrző pont</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Magnetic Declination</source>
        <extracomment>Angle between geographic and magnetic north</extracomment>
        <translation>Mágneses elhajlás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Elevation</source>
        <extracomment>Above/below sea level</extracomment>
        <translation>Magasság</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pressure</source>
        <translation>Nyomás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Temperature</source>
        <translation>Hőmérséklet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Humidity</source>
        <translation>Páratartalom</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>True Wind Speed</source>
        <extracomment>Wind speed while standing</extracomment>
        <translation>Valós szélsebesség</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>True Wind Direction</source>
        <translation>Valós szélirány</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Apparent Wind Speed</source>
        <extracomment>Wind speed while observer is moving</extracomment>
        <translation>Látszólagos szélsebesség</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Apparent Wind Direction</source>
        <translation>Látszólagos szélirány</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Gust Factor</source>
        <extracomment>Factor by which wind gust is stronger than average wind</extracomment>
        <translation>Széllökési tényező</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Pollen Concentration</source>
        <translation>Pollenkoncentráció</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>UV Index</source>
        <translation>UV-index</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Irradiance</source>
        <translation>Besugárzás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rainfall</source>
        <translation>Csapadék</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Wind Chill</source>
        <translation>Szélérzet</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Heat Index</source>
        <translation>Hőindex</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Dew Point</source>
        <translation>Harmatpont</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Descriptor Value Changed</source>
        <extracomment>Environmental sensing related</extracomment>
        <translation>Megváltozott leíróérték</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Aerobic Heart Rate Lower Limit</source>
        <translation>Aerobik pulzus alsó korlátja</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Aerobic Heart Rate Upper Limit</source>
        <translation>Aerobik pulzus felső korlátja</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Aerobic Threshold</source>
        <translation>Aerobik-küszöb</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Age</source>
        <extracomment>Age of person</extracomment>
        <translation>Kor</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Anaerobic Heart Rate Lower Limit</source>
        <translation>Anaerob pulzus alsó korlátja</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Anaerobic Heart Rate Upper Limit</source>
        <translation>Anaerob pulzus felső korlátja</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Anaerobic Threshold</source>
        <translation>Anaerob-küszöb</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date Of Birth</source>
        <translation>Születési dátum</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Date Of Threshold Assessment</source>
        <translation>Küszöb-felmérés dátuma</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Email Address</source>
        <translation>E-mail cím</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fat Burn Heart Rate Lower Limit</source>
        <translation>Zsírégető pulzus alsó korlátja</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Fat Burn Heart Rate Upper Limit</source>
        <translation>Zsírégető pulzus felső korlátja</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>First Name</source>
        <translation>Utónév</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>5-Zone Heart Rate Limits</source>
        <translation>5-zónás pulzus korlátjai</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Gender</source>
        <translation>Nem</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Heart Rate Maximum</source>
        <translation>Pulzus maximuma</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Height</source>
        <extracomment>Height of a person</extracomment>
        <translation>Magasság</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Hip Circumference</source>
        <translation>Csípőkerület</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Last Name</source>
        <translation>Vezetéknév</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Maximum Recommended Heart Rate</source>
        <translation>Legnagyobb ajánlott pulzus</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Resting Heart Rate</source>
        <translation>Pihenő pulzus</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sport Type For Aerobic/Anaerobic Thresholds</source>
        <translation>Sporttípus az aerobik/anaerob küszöbökhöz</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>3-Zone Heart Rate Limits</source>
        <translation>3-zónás pulzus korlátjai</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>2-Zone Heart Rate Limits</source>
        <translation>2-zónás pulzus korlátjai</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Oxygen Uptake</source>
        <translation>Oxigénfelvétel</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Waist Circumference</source>
        <translation>Derékbőség</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Weight</source>
        <translation>Súly</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Database Change Increment</source>
        <extracomment>Environmental sensing related</extracomment>
        <translation>Adatbázisváltozás növelés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>User Index</source>
        <translation>Felhasználó-index</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Body Composition Feature</source>
        <translation>Testfelépítés-funkció</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Body Composition Measurement</source>
        <translation>Testfelépítés-mérés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Weight Measurement</source>
        <translation>Súlymérés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>User Control Point</source>
        <translation>Felhasználó-ellenőrző pont</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Magnetic Flux Density 2D</source>
        <translation>2D-s mágneses fluxussűrűség</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Magnetic Flux Density 3D</source>
        <translation>3D-s mágneses fluxussűrűség</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Language</source>
        <translation>Nyelv</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Barometric Pressure Trend</source>
        <translation>Barometrikus nyomás trend</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Characteristic Extended Properties</source>
        <translation>Karakterisztikus kiterjesztett tulajdonságok</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Characteristic User Description</source>
        <translation>Karakterisztikus felhasználói leírás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Client Characteristic Configuration</source>
        <translation>Kliens karakterisztikus beállításai</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Server Characteristic Configuration</source>
        <translation>Kiszolgáló karakterisztikus beállításai</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Characteristic Presentation Format</source>
        <translation>Karakterisztikus megjelenítési formátum</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Characteristic Aggregate Format</source>
        <translation>Karakterisztikus összegzési formátum</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Valid Range</source>
        <translation>Érvényes tartomány</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>External Report Reference</source>
        <translation>Külső jelentés-hivatkozás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Report Reference</source>
        <translation>Jelentés-hivatkozás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Environmental Sensing Configuration</source>
        <translation>Környezeti érzékelési beállítások</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Environmental Sensing Measurement</source>
        <translation>Környezeti érzékelés mérés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Environmental Sensing Trigger Setting</source>
        <translation>Környezeti érzékelési aktiváló beállítások</translation>
    </message>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qlowenergyservice.cpp" line="+552"/>
        <source>Unknown Service</source>
        <translation>Ismeretlen szolgáltatás</translation>
    </message>
</context>
<context>
    <name>QBluetoothSocket</name>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/osx/uistrings.cpp" line="+3"/>
        <source>Network Error</source>
        <translation>Hálózati hiba</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_android.cpp" line="+779"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_bluez.cpp" line="+601"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_bluezdbus.cpp" line="+455"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_dummy.cpp" line="+156"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_win.cpp" line="+441"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_winrt.cpp" line="+680"/>
        <source>Cannot write while not connected</source>
        <translation>Nem lehet írni, amíg nincs csatlakoztatva</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_android.cpp" line="-287"/>
        <location line="+47"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_bluez.cpp" line="-383"/>
        <location line="+54"/>
        <location line="+35"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_bluezdbus.cpp" line="-154"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_win.cpp" line="-289"/>
        <location line="+131"/>
        <location line="+35"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_winrt.cpp" line="-245"/>
        <location line="+69"/>
        <location line="+25"/>
        <source>Trying to connect while connection is in progress</source>
        <translation>Kapcsolódási kísérlet, miközben a kapcsolat folyamatban van</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket.cpp" line="+680"/>
        <source>Service cannot be found</source>
        <translation>A szolgáltatás nem található</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket.cpp" line="+72"/>
        <source>Invalid data/data size</source>
        <translation>Érvénytelen adat/adatméret</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_android.cpp" line="+269"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_bluez.cpp" line="+339"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_bluezdbus.cpp" line="+173"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_dummy.cpp" line="+15"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_win.cpp" line="+163"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_winrt.cpp" line="+164"/>
        <location line="+13"/>
        <source>Cannot read while not connected</source>
        <translation>Nem lehet olvasni, amíg nincs csatlakoztatva</translation>
    </message>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_android.cpp" line="-284"/>
        <location line="+23"/>
        <location line="+6"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_bluez.cpp" line="-418"/>
        <location line="+52"/>
        <location line="+20"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_bluezdbus.cpp" line="-165"/>
        <location line="+6"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_dummy.cpp" line="-89"/>
        <location line="+14"/>
        <location line="+14"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_win.cpp" line="-320"/>
        <location line="+130"/>
        <location line="+20"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_winrt.cpp" line="-263"/>
        <location line="+67"/>
        <location line="+25"/>
        <source>Socket type not supported</source>
        <translation>A foglalattípus nem támogatott</translation>
    </message>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_bluez.cpp" line="-169"/>
        <location line="+106"/>
        <location line="+10"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_win.cpp" line="-143"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_winrt.cpp" line="-159"/>
        <location line="+33"/>
        <location line="+45"/>
        <location line="+9"/>
        <location line="+16"/>
        <location line="+308"/>
        <location line="+29"/>
        <location line="+30"/>
        <source>Unknown socket error</source>
        <translation>Ismeretlen foglalathiba</translation>
    </message>
    <message>
        <location line="+103"/>
        <location line="+264"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_win.cpp" line="+36"/>
        <location line="+247"/>
        <source>Network Error: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connecting to port is not supported</source>
        <translation type="vanished">A porthoz való csatlakozás nem támogatott</translation>
    </message>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_android.cpp" line="-185"/>
        <source>Cannot connect to %1</source>
        <comment>%1 = uuid</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+46"/>
        <source>Device does not support Bluetooth</source>
        <translation>Az eszköznek nincs Bluetooth támogatása</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Device is powered off</source>
        <translation>Az eszköz ki van kapcsolva</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Cannot access address %1</source>
        <comment>%1 = Bt address e.g. 11:22:33:44:55:66</comment>
        <translation>Nem lehet elérni a(z) %1 címet</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Cannot connect to %1 on %2</source>
        <comment>%1 = uuid, %2 = Bt address</comment>
        <translation>Nem lehet csatlakozni: %1 ‒ %2</translation>
    </message>
    <message>
        <location line="+129"/>
        <location line="+300"/>
        <source>Obtaining streams for service failed</source>
        <translation>Az adatfolyamok beszerzése a szolgáltatáshoz sikertelen</translation>
    </message>
    <message>
        <location line="-279"/>
        <source>Input stream thread cannot be started</source>
        <translation>A bemeneti adatfolyamszálat nem lehet elindítani</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+23"/>
        <source>Connection to service failed</source>
        <translation>A szolgáltatáshoz való kapcsolódás sikertelen</translation>
    </message>
    <message>
        <location line="+120"/>
        <source>Error during write on socket.</source>
        <translation>Hiba a foglalatba való íráskor.</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Network error during read</source>
        <translation>Hálózati hiba olvasás közben</translation>
    </message>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_bluez.cpp" line="-462"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_win.cpp" line="+126"/>
        <location line="+13"/>
        <source>Cannot set connection security level</source>
        <translation>Nem lehet beállítani a kapcsolat biztonsági szintjét</translation>
    </message>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_bluezdbus.cpp" line="-120"/>
        <source>Cannot export profile on DBus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+27"/>
        <source>Cannot register profile on DBus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Cannot find remote device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot connect to remote profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+25"/>
        <location line="+23"/>
        <source>Missing serviceUuid or Serial Port service class uuid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-8"/>
        <source>Invalid Bluetooth address passed to connectToService()</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_win.cpp" line="-508"/>
        <source>Unsupported protocol. Win32 only supports RFCOMM sockets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Failed to create socket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Socket type not handled: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+90"/>
        <source>Logic error: more bytes sent than passed to ::send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_winrt.cpp" line="-65"/>
        <source>Network error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Remote host closed connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Connection timed out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Host not reachable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Host refused connection</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QBluetoothSocketPrivateAndroid</name>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_android.cpp" line="-252"/>
        <source>Connecting to port is not supported</source>
        <translation type="unfinished">A porthoz való csatlakozás nem támogatott</translation>
    </message>
</context>
<context>
    <name>QBluetoothSocketPrivateBluezDBus</name>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothsocket_bluezdbus.cpp" line="+45"/>
        <source>Connecting to port is not supported via Bluez DBus</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QBluetoothTransferReply</name>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/osx/uistrings.cpp" line="+3"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothtransferreply_bluez.cpp" line="+160"/>
        <location line="+57"/>
        <source>Invalid target address</source>
        <translation>Érvénytelen célcím</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothtransferreply_bluez.cpp" line="+29"/>
        <source>Push session cannot be started</source>
        <translation>A leküldés munkamenetet nem lehet elindítani</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Push session cannot connect</source>
        <translation>A leküldés munkamenethez nem lehet kapcsolódni</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothtransferreply_bluez.cpp" line="-96"/>
        <source>Source file does not exist</source>
        <translation>A forrásfájl nem létezik</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothtransferreply_bluez.cpp" line="-19"/>
        <source>QIODevice cannot be read. Make sure it is open for reading.</source>
        <translation>A QIODevice nem olvasható. Győződjön meg arról, hogy megnyitható-e olvasásra.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Push session failed</source>
        <translation>A leküldési munkamenet sikertelen</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothtransferreply_bluez.cpp" line="-51"/>
        <source>Invalid input device (null)</source>
        <translation>Érvénytelen bemeneti eszköz (null)</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothtransferreply_bluez.cpp" line="+313"/>
        <source>Operation canceled</source>
        <translation>A művelet megszakítva</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Transfer already started</source>
        <translation>Az átvitel már elindult</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Push service not found</source>
        <translation>A leküldési szolgáltatás nem található</translation>
    </message>
</context>
<context>
    <name>QBluetoothTransferReplyBluez</name>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qbluetoothtransferreply_bluez.cpp" line="-97"/>
        <source>Unknown Error</source>
        <translation>Ismeretlen hiba</translation>
    </message>
    <message>
        <location line="+54"/>
        <location line="+40"/>
        <source>Could not open file for sending</source>
        <translation>Nem sikerült megnyitni a fájlt a küldéshez</translation>
    </message>
    <message>
        <location line="-37"/>
        <source>The transfer was canceled</source>
        <translation>Az átvitelt megszakították</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>Operation canceled</source>
        <translation>A művelet megszakítva</translation>
    </message>
</context>
<context>
    <name>QLowEnergyController</name>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/osx/uistrings.cpp" line="+3"/>
        <location filename="../../qtconnectivity/src/bluetooth/qlowenergycontrollerbase.cpp" line="+92"/>
        <source>Remote device cannot be found</source>
        <translation>A távoli eszköz nem található</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../../qtconnectivity/src/bluetooth/qlowenergycontrollerbase.cpp" line="+3"/>
        <source>Cannot find local adapter</source>
        <translation>Nem található helyi adapter</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../../qtconnectivity/src/bluetooth/qlowenergycontrollerbase.cpp" line="+3"/>
        <source>Error occurred during connection I/O</source>
        <translation>Hiba történt a kapcsolat I/O során</translation>
    </message>
    <message>
        <location line="+1"/>
        <location filename="../../qtconnectivity/src/bluetooth/qlowenergycontrollerbase.cpp" line="+18"/>
        <source>Unknown Error</source>
        <translation>Ismeretlen hiba</translation>
    </message>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qlowenergycontrollerbase.cpp" line="-15"/>
        <source>Error occurred trying to connect to remote device.</source>
        <translation>Hiba történt a távoli eszközhöz való csatlakozás során.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Remote device closed the connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Failed to authorize on the remote device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../qtconnectivity/src/bluetooth/qlowenergycontroller_android.cpp" line="+867"/>
        <source>Advertisement data is larger than 31 bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Advertisement feature not supported on the platform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <location filename="../../qtconnectivity/src/bluetooth/qlowenergycontrollerbase.cpp" line="-6"/>
        <source>Error occurred trying to start advertising</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Failed due to too many advertisers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown advertisement error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
