<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu_HU">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../../qttools/src/linguist/linguist/mainwindow.cpp" line="+1371"/>
        <source>Qt Linguist</source>
        <translation>Qt Linguist</translation>
    </message>
</context>
<context>
    <name>BatchTranslationDialog</name>
    <message>
        <location filename="../../qttools/src/linguist/linguist/batchtranslation.ui"/>
        <source>Qt Linguist - Batch Translation</source>
        <translation>Qt Linguist – kötegelt fordítás</translation>
    </message>
    <message>
        <location/>
        <source>Options</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <location/>
        <source>Set translated entries to finished</source>
        <translation>Lefordított szövegek befejezettre állítása</translation>
    </message>
    <message>
        <location/>
        <source>Retranslate entries with existing translation</source>
        <translation>Bejegyzések újrafordítása a meglévő fordítással</translation>
    </message>
    <message>
        <location/>
        <source>Note that the modified entries will be reset to unfinished if &apos;Set translated entries to finished&apos; above is unchecked</source>
        <translation>Ne feledje, hogy a módosított bejegyzések befejezetlenre lesznek visszaállítva, ha a fenti „Lefordított szövegek befejezettre állítása” nincs bejelölve</translation>
    </message>
    <message>
        <location/>
        <source>Translate also finished entries</source>
        <translation>A befejezett bejegyzéseket is fordítsa le</translation>
    </message>
    <message>
        <location/>
        <source>Phrase book preference</source>
        <translation>Kifejezéstár sorrend</translation>
    </message>
    <message>
        <location/>
        <source>Move up</source>
        <translation>Mozgatás fel</translation>
    </message>
    <message>
        <location/>
        <source>Move down</source>
        <translation>Mozgatás le</translation>
    </message>
    <message>
        <location/>
        <source>The batch translator will search through the selected phrase books in the order given above</source>
        <translation>A kötegelt fordító a fent megadott sorrendben fogja átnézni a kijelölt kifejezéstárakat</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Run</source>
        <translation>&amp;Futtatás</translation>
    </message>
    <message>
        <location/>
        <source>Cancel</source>
        <translation>Mégse</translation>
    </message>
    <message>
        <location filename="../../qttools/src/linguist/linguist/batchtranslationdialog.cpp" line="+66"/>
        <source>Batch Translation of &apos;%1&apos; - Qt Linguist</source>
        <translation>„%1” kötegelt fordítása – Qt Linguist</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Searching, please wait...</source>
        <translation>Keresés, kérem várjon…</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Mégse</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Linguist batch translator</source>
        <translation>Linguist kötegelt fordító</translation>
    </message>
    <message numerus="yes">
        <location line="+1"/>
        <source>Batch translated %n entries</source>
        <translation>
            <numerusform>%n bejegyzés kötegelten lefordítva</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DataModel</name>
    <message>
        <location filename="../../qttools/src/linguist/linguist/messagemodel.cpp" line="+200"/>
        <source>The translation file &apos;%1&apos; will not be loaded because it is empty.</source>
        <translation>A(z) „%1” fordítási fájl nem lesz betöltve, mert üres.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&lt;qt&gt;Duplicate messages found in &apos;%1&apos;:</source>
        <translation>&lt;qt&gt;Kettőzött üzenetek találhatók a(z) „%1” fájlban:</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+8"/>
        <source>&lt;p&gt;[more duplicates omitted]</source>
        <translation>&lt;p&gt;[a többi előfordulás kihagyva]</translation>
    </message>
    <message>
        <location line="-5"/>
        <source>&lt;p&gt;* ID: %1</source>
        <translation>&lt;p&gt;* Azonosító: %1</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>&lt;p&gt;* Context: %1&lt;br&gt;* Source: %2</source>
        <translation>&lt;p&gt;* Környezet: %1&lt;br&gt;* Forrás: %2</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&lt;br&gt;* Comment: %3</source>
        <translation>&lt;br&gt;* Megjegyzés: %3</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Linguist does not know the plural rules for &apos;%1&apos;.
Will assume a single universal form.</source>
        <translation>A Linguist nem ismeri a többes szám képzést ennél: „%1”.
Egyes számú általános formát fog feltételezni.</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Cannot create &apos;%2&apos;: %1</source>
        <translation>A(z) „%2” létrehozása sikertelen: %1</translation>
    </message>
    <message>
        <location line="+50"/>
        <source>%1 (%2)</source>
        <extracomment>&lt;language&gt; (&lt;country&gt;)</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Universal Form</source>
        <translation>Általános alak</translation>
    </message>
</context>
<context>
    <name>ErrorsView</name>
    <message>
        <location filename="../../qttools/src/linguist/linguist/errorsview.cpp" line="+62"/>
        <source>Accelerator possibly superfluous in translation.</source>
        <translation>A gyorsbillentyű valószínűleg felesleges a fordításban.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Accelerator possibly missing in translation.</source>
        <translation>Egy gyorsbillentyű valószínűleg hiányzik a fordításból.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Translation does not have same leading and trailing whitespace as the source text.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Translation does not end with the same punctuation as the source text.</source>
        <translation>A fordítás nem ugyanazzal az írásjellel végződik mint a forrásszöveg.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>A phrase book suggestion for &apos;%1&apos; was ignored.</source>
        <translation>A(z) „%1” kifejezéstári javaslata figyelmen kívül lett hagyva.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Translation does not refer to the same place markers as in the source text.</source>
        <translation>A fordítás nem ugyanannyi helykitöltőre hivatkozik mint a forrásszöveg.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Translation does not contain the necessary %n/%Ln place marker.</source>
        <translation>A fordítás nem tartalmazza a szükséges %n/%Ln helyfoglalót.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Unknown error</source>
        <translation>Ismeretlen hiba</translation>
    </message>
</context>
<context>
    <name>FMT</name>
    <message>
        <location filename="../../qttools/src/linguist/shared/po.cpp" line="+893"/>
        <source>GNU Gettext localization files</source>
        <translation type="unfinished">GNU Gettext honosítási fájlok</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>GNU Gettext localization template files</source>
        <translation type="unfinished">GNU Gettext honosítási sablonfájlok</translation>
    </message>
    <message>
        <location filename="../../qttools/src/linguist/shared/qm.cpp" line="+735"/>
        <source>Compiled Qt translations</source>
        <translation type="unfinished">Lefordított Qt fordítások</translation>
    </message>
    <message>
        <location filename="../../qttools/src/linguist/shared/qph.cpp" line="+180"/>
        <source>Qt Linguist &apos;Phrase Book&apos;</source>
        <translation type="unfinished">Qt Linguist „kifejezéstár”</translation>
    </message>
    <message>
        <location filename="../../qttools/src/linguist/shared/ts.cpp" line="+689"/>
        <source>Qt translation sources</source>
        <translation type="unfinished">Qt fordítási források</translation>
    </message>
    <message>
        <location filename="../../qttools/src/linguist/shared/xliff.cpp" line="+838"/>
        <source>XLIFF localization files</source>
        <translation type="unfinished">XLIFF honosítási fájlok</translation>
    </message>
</context>
<context>
    <name>FindDialog</name>
    <message>
        <location filename="../../qttools/src/linguist/linguist/finddialog.ui"/>
        <source>Find</source>
        <translation>Keresés</translation>
    </message>
    <message>
        <location/>
        <source>This window allows you to search for some text in the translation source file.</source>
        <translation>Ez az ablak lehetővé teszi, hogy szöveget keressen a fordítás forrásfájljában.</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Find what:</source>
        <translation>Amit &amp;keres:</translation>
    </message>
    <message>
        <location/>
        <source>Type in the text to search for.</source>
        <translation>Gépelje be a keresendő szöveget.</translation>
    </message>
    <message>
        <location/>
        <source>Options</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <location/>
        <source>Source texts are searched when checked.</source>
        <translation>A forrásszövegekben is keresni fog, ha be van jelölve.</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Source texts</source>
        <translation>&amp;Forrásszövegek</translation>
    </message>
    <message>
        <location/>
        <source>Translations are searched when checked.</source>
        <translation>A fordításokban is keresni fog, ha be van jelölve.</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Translations</source>
        <translation>F&amp;ordítások</translation>
    </message>
    <message>
        <location/>
        <source>Texts such as &apos;TeX&apos; and &apos;tex&apos; are considered as different when checked.</source>
        <translation>Az olyan szövegek, mint például a „TeX” és „tex”, különbözőnek lesznek tekintve, ha be van jelölve.</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Match case</source>
        <translation>&amp;Kis- és nagybetűk megkülönböztetése</translation>
    </message>
    <message>
        <location/>
        <source>Comments and contexts are searched when checked.</source>
        <translation>A megjegyzésekben és a környezetekben is keresni fog, ha be van jelölve.</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Comments</source>
        <translation>&amp;Megjegyzések</translation>
    </message>
    <message>
        <location/>
        <source>Ignore &amp;accelerators</source>
        <translation>&amp;Gyorsbillentyűk mellőzése</translation>
    </message>
    <message>
        <location/>
        <source>Obsoleted messages are skipped when checked.</source>
        <translation>Az elavult üzenet kihagyásra kerülnek, ha be van jelölve.</translation>
    </message>
    <message>
        <location/>
        <source>Skip &amp;obsolete</source>
        <translation>&amp;Elavultak kihagyása</translation>
    </message>
    <message>
        <location/>
        <source>Click here to find the next occurrence of the text you typed in.</source>
        <translation>Kattintson ide a begépelt szöveg következő előfordulásának kereséséhez.</translation>
    </message>
    <message>
        <location/>
        <source>Find Next</source>
        <translation>Következő keresése</translation>
    </message>
    <message>
        <location/>
        <source>Click here to close this window.</source>
        <translation>Kattintson ide az ablak bezárásához.</translation>
    </message>
    <message>
        <location/>
        <source>Cancel</source>
        <translation>Mégse</translation>
    </message>
    <message>
        <source></source>
        <comment>Choose Edit|Find from the menu bar or press Ctrl+F to pop up the Find dialog</comment>
        <translation></translation>
    </message>
    <message>
        <location/>
        <source>Lets you use a Perl-compatible regular expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Regular &amp;expression</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FormMultiWidget</name>
    <message>
        <location filename="../../qttools/src/linguist/linguist/messageeditorwidgets.cpp" line="+301"/>
        <source>Alt+Delete</source>
        <extracomment>translate, but don&apos;t change</extracomment>
        <translation>Alt+Delete</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Shift+Alt+Insert</source>
        <extracomment>translate, but don&apos;t change</extracomment>
        <translation>Shift+Alt+Insert</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Alt+Insert</source>
        <extracomment>translate, but don&apos;t change</extracomment>
        <translation>Alt+Insert</translation>
    </message>
    <message>
        <location line="+166"/>
        <source>Confirmation - Qt Linguist</source>
        <translation>Megerősítés – Qt Linguist</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Delete non-empty length variant?</source>
        <translation>Törli a nem üres hosszváltozatot?</translation>
    </message>
</context>
<context>
    <name>LRelease</name>
    <message numerus="yes">
        <location filename="../../qttools/src/linguist/shared/qm.cpp" line="-29"/>
        <source>Dropped %n message(s) which had no ID.</source>
        <translation>
            <numerusform>%n üzenet eldobva, amelynek nem volt azonosítója.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>Excess context/disambiguation dropped from %n message(s).</source>
        <translation>
            <numerusform>Többlet környezet vagy egyértelműség eldobva %n üzenetből.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+9"/>
        <source>    Generated %n translation(s) (%1 finished and %2 unfinished)</source>
        <translation>
            <numerusform>%n fordítás előállítva (%1 befejezett és %2 befejezetlen)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location line="+4"/>
        <source>    Ignored %n untranslated source text(s)</source>
        <translation>
            <numerusform>%n lefordítatlan forrásszöveg figyelmen kívül hagyva</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>Linguist</name>
    <message>
        <source>GNU Gettext localization files</source>
        <translation type="vanished">GNU Gettext honosítási fájlok</translation>
    </message>
    <message>
        <source>GNU Gettext localization template files</source>
        <translation type="vanished">GNU Gettext honosítási sablonfájlok</translation>
    </message>
    <message>
        <source>Compiled Qt translations</source>
        <translation type="vanished">Lefordított Qt fordítások</translation>
    </message>
    <message>
        <source>Qt Linguist &apos;Phrase Book&apos;</source>
        <translation type="vanished">Qt Linguist „kifejezéstár”</translation>
    </message>
    <message>
        <source>Qt translation sources</source>
        <translation type="vanished">Qt fordítási források</translation>
    </message>
    <message>
        <source>XLIFF localization files</source>
        <translation type="vanished">XLIFF honosítási fájlok</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../qttools/src/linguist/linguist/mainwindow.ui"/>
        <source>MainWindow</source>
        <translation>Főablak</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Phrases</source>
        <translation>&amp;Kifejezések</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Close Phrase Book</source>
        <translation>Kifejezéstár &amp;bezárása</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Edit Phrase Book</source>
        <translation>Kifejezéstár &amp;szerkesztése</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Print Phrase Book</source>
        <translation>Kifejezéstár &amp;nyomtatása</translation>
    </message>
    <message>
        <location/>
        <source>V&amp;alidation</source>
        <translation>&amp;Ellenőrzés</translation>
    </message>
    <message>
        <location/>
        <source>&amp;View</source>
        <translation>&amp;Nézet</translation>
    </message>
    <message>
        <location/>
        <source>Vie&amp;ws</source>
        <translation>&amp;Nézetek</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Toolbars</source>
        <translation>&amp;Eszköztárak</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Zoom</source>
        <translation>&amp;Nagyítás</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Help</source>
        <translation>&amp;Súgó</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Translation</source>
        <translation>F&amp;ordítás</translation>
    </message>
    <message>
        <location/>
        <source>&amp;File</source>
        <translation>&amp;Fájl</translation>
    </message>
    <message>
        <location/>
        <source>Recently Opened &amp;Files</source>
        <translation>Legutóbb megnyitott &amp;fájlok</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Edit</source>
        <translation>S&amp;zerkesztés</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Open...</source>
        <translation>&amp;Megnyitás…</translation>
    </message>
    <message>
        <location/>
        <source>Open a Qt translation source file (TS file) for editing</source>
        <translation>Egy Qt fordítási forrásforrás (TS-fájl) megnyitása szerkesztésre</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location/>
        <source>E&amp;xit</source>
        <translation>&amp;Kilépés</translation>
    </message>
    <message>
        <location/>
        <source>Close this window and exit.</source>
        <translation>Ablak bezárása és kilépés.</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location/>
        <source>Save</source>
        <translation>Mentés</translation>
    </message>
    <message>
        <location/>
        <source>Save changes made to this Qt translation source file</source>
        <translation>Ezen a Qt fordítási forrásfájlon végzett változtatások mentése</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/linguist/linguist/mainwindow.cpp" line="+705"/>
        <location line="+11"/>
        <source>Save &amp;As...</source>
        <translation>Mentés má&amp;sként…</translation>
    </message>
    <message>
        <location/>
        <source>Save As...</source>
        <translation>Mentés másként…</translation>
    </message>
    <message>
        <location/>
        <source>Save changes made to this Qt translation source file into a new file.</source>
        <translation>Ezen a Qt fordítási forrásfájlon végzett változtatások elmentése egy új fájlba.</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/linguist/linguist/mainwindow.cpp" line="-1236"/>
        <location line="+1226"/>
        <source>Release</source>
        <translation>Kiadás</translation>
    </message>
    <message>
        <location/>
        <source>Create a Qt message file suitable for released applications from the current message file.</source>
        <translation>A kiadott alkalmazások számára megfelelő Qt üzenetfájl létrehozása a jelenlegi üzenetfájlból.</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Print...</source>
        <translation>&amp;Nyomtatás…</translation>
    </message>
    <message>
        <location/>
        <source>Print a list of all the translation units in the current translation source file.</source>
        <translation>A jelenlegi fordítási forrásfájlban lévő összes fordítási egység listájának kinyomtatása.</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+P</source>
        <translation>Ctrl+P</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Undo</source>
        <translation>&amp;Visszavonás</translation>
    </message>
    <message>
        <location/>
        <source>Undo the last editing operation performed on the current translation.</source>
        <translation>A jelenlegi fordításon elvégzett utolsó szerkesztési művelet visszavonása.</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+Z</source>
        <translation>Ctrl+Z</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Redo</source>
        <translation>Új&amp;ra</translation>
    </message>
    <message>
        <location/>
        <source>Redo an undone editing operation performed on the translation.</source>
        <translation>A fordításon elvégzett visszavont szerkesztési művelet megismétlése.</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+Y</source>
        <translation>Ctrl+Y</translation>
    </message>
    <message>
        <location/>
        <source>Cu&amp;t</source>
        <translation>&amp;Kivágás</translation>
    </message>
    <message>
        <location/>
        <source>Copy the selected translation text to the clipboard and deletes it.</source>
        <translation>A kijelölt fordítási szöveg másolása a vágólapra és annak törlése.</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Copy</source>
        <translation>&amp;Másolás</translation>
    </message>
    <message>
        <location/>
        <source>Copy the selected translation text to the clipboard.</source>
        <translation>A kijelölt fordítási szöveg másolása a vágólapra.</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Paste</source>
        <translation>&amp;Beillesztés</translation>
    </message>
    <message>
        <location/>
        <source>Paste the clipboard text into the translation.</source>
        <translation>A vágólapon lévő szöveg beillesztése a fordításba.</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location/>
        <source>Select &amp;All</source>
        <translation>Össz&amp;es kijelölése</translation>
    </message>
    <message>
        <location/>
        <source>Select the whole translation text.</source>
        <translation>A teljes fordítási szöveg kijelölése.</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Find...</source>
        <translation>K&amp;eresés…</translation>
    </message>
    <message>
        <location/>
        <source>Search for some text in the translation source file.</source>
        <translation>Szöveg keresése a fordítási forrásfájlban.</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location/>
        <source>Find &amp;Next</source>
        <translation>Kö&amp;vetkező keresése</translation>
    </message>
    <message>
        <location/>
        <source>Continue the search where it was left.</source>
        <translation>Annak a keresésnek a folytatása, ahol otthagyták.</translation>
    </message>
    <message>
        <location/>
        <source>F3</source>
        <translation>F3</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Prev Unfinished</source>
        <translation>&amp;Előző befejezetlen</translation>
    </message>
    <message>
        <location/>
        <source>Previous unfinished item</source>
        <translation>Előző befejezetlen elem</translation>
    </message>
    <message>
        <location/>
        <source>Move to the previous unfinished item.</source>
        <translation>Ugrás az előző befejezetlen elemre.</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+K</source>
        <translation>Ctrl+K</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Next Unfinished</source>
        <translation>Következő befejezetle&amp;n</translation>
    </message>
    <message>
        <location/>
        <source>Next unfinished item</source>
        <translation>Következő befejezetlen elem</translation>
    </message>
    <message>
        <location/>
        <source>Move to the next unfinished item.</source>
        <translation>Ugrás a következő befejezetlen elemre.</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+J</source>
        <translation>Ctrl+J</translation>
    </message>
    <message>
        <location/>
        <source>P&amp;rev</source>
        <translation>E&amp;lőző</translation>
    </message>
    <message>
        <location/>
        <source>Move to previous item</source>
        <translation>Ugrás az előző elemre</translation>
    </message>
    <message>
        <location/>
        <source>Move to the previous item.</source>
        <translation>Ugrás az előző elemre.</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+Shift+K</source>
        <translation>Ctrl+Shift+K</translation>
    </message>
    <message>
        <location/>
        <source>Ne&amp;xt</source>
        <translation>Kö&amp;vetkező</translation>
    </message>
    <message>
        <location/>
        <source>Next item</source>
        <translation>Következő elem</translation>
    </message>
    <message>
        <location/>
        <source>Move to the next item.</source>
        <translation>Ugrás a következő elemre.</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+Shift+J</source>
        <translation>Ctrl+Shift+J</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Done and Next</source>
        <translation>&amp;Kész és következő</translation>
    </message>
    <message>
        <location/>
        <source>Mark item as done and move to the next unfinished item</source>
        <translation>Az elem készre jelölése és ugrás a következő befejezetlen elemre</translation>
    </message>
    <message>
        <location/>
        <source>Mark this item as done and move to the next unfinished item.</source>
        <translation>Az elem készre jelölése és ugrás a következő befejezetlen elemre.</translation>
    </message>
    <message>
        <location/>
        <source>Copy from source text</source>
        <translation>Másolás a forrásszövegből</translation>
    </message>
    <message>
        <location/>
        <source>Copies the source text into the translation field</source>
        <translation>Átmásolja a forrásszöveget a fordítási mezőbe</translation>
    </message>
    <message>
        <location/>
        <source>Copies the source text into the translation field.</source>
        <translation>Átmásolja a forrásszöveget a fordítási mezőbe.</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+B</source>
        <translation>Ctrl+B</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Accelerators</source>
        <translation>&amp;Gyorsbillentyűk</translation>
    </message>
    <message>
        <source>Toggle the validity check of accelerators</source>
        <translation type="vanished">A gyorsbillentyűk érvényességi ellenőrzésének be- és kikapcsolása</translation>
    </message>
    <message>
        <source>Toggle the validity check of accelerators, i.e. whether the number of ampersands in the source and translation text is the same. If the check fails, a message is shown in the warnings window.</source>
        <translation type="vanished">A gyorsbillentyűk érvényességi ellenőrzésének be- és kikapcsolása, azaz ugyanannyi „és”-jel van-e a forrás és a fordítási szövegben. Ha az ellenőrzés sikertelen, akkor egy üzenet jelenik meg a figyelmeztetések ablakában.</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Ending Punctuation</source>
        <translation>&amp;Mondatvégi írásjelek</translation>
    </message>
    <message>
        <source>Toggle the validity check of ending punctuation</source>
        <translation type="vanished">A mondatvégi írásjelek érvényességi ellenőrzésének be- és kikapcsolása</translation>
    </message>
    <message>
        <source>Toggle the validity check of ending punctuation. If the check fails, a message is shown in the warnings window.</source>
        <translation type="vanished">A mondatvégi írásjelek érvényességi ellenőrzésének be- és kikapcsolása. Ha az ellenőrzés sikertelen, akkor egy üzenet jelenik meg a figyelmeztetések ablakában.</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Phrase matches</source>
        <translation>&amp;Kifejezésegyezések</translation>
    </message>
    <message>
        <source>Toggle checking that phrase suggestions are used</source>
        <translation type="vanished">Azon ellenőrzés be- és kikapcsolása, hogy a kifejezésjavaslatokat használják-e</translation>
    </message>
    <message>
        <source>Toggle checking that phrase suggestions are used. If the check fails, a message is shown in the warnings window.</source>
        <translation type="vanished">Azon ellenőrzés be- és kikapcsolása, hogy a kifejezésjavaslatokat használják-e. Ha az ellenőrzés sikertelen, akkor egy üzenet jelenik meg a figyelmeztetések ablakában.</translation>
    </message>
    <message>
        <location/>
        <source>Place &amp;Marker Matches</source>
        <translation>&amp;Helyfoglaló egyezések</translation>
    </message>
    <message>
        <source>Toggle the validity check of place markers</source>
        <translation type="vanished">A helyfoglalók érvényességi ellenőrzésének be- és kikapcsolása</translation>
    </message>
    <message>
        <source>Toggle the validity check of place markers, i.e. whether %1, %2, ... are used consistently in the source text and translation text. If the check fails, a message is shown in the warnings window.</source>
        <translation type="vanished">A helyfoglalók érvényességi ellenőrzésének be- és kikapcsolása, azaz a %1, %2, … következetesen vannak-e használva a forrásszövegben és a fordítási szövegben. Ha az ellenőrzés sikertelen, akkor egy üzenet jelenik meg a figyelmeztetések ablakában.</translation>
    </message>
    <message>
        <location/>
        <source>&amp;New Phrase Book...</source>
        <translation>Ú&amp;j kifejezéstár…</translation>
    </message>
    <message>
        <location/>
        <source>Create a new phrase book.</source>
        <translation>Egy új kifejezéstár létrehozása.</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Open Phrase Book...</source>
        <translation>Kifejezéstár meg&amp;nyitása…</translation>
    </message>
    <message>
        <location/>
        <source>Open a phrase book to assist translation.</source>
        <translation>Egy kifejezéstár megnyitása a fordítás segítéséhez.</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+H</source>
        <translation>Ctrl+H</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Reset Sorting</source>
        <translation>Rendezés &amp;visszaállítása</translation>
    </message>
    <message>
        <location/>
        <source>Sort the items back in the same order as in the message file.</source>
        <translation>Az elemek visszarendezése ugyanabba a sorrendbe, ahogy az üzenetfájlban van.</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Display guesses</source>
        <translation>&amp;Tippek megjelenítése</translation>
    </message>
    <message>
        <location/>
        <source>Set whether or not to display translation guesses.</source>
        <translation>Annak beállítása, hogy a fordítási tippek megjelenjenek-e vagy sem.</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Statistics</source>
        <translation>&amp;Statisztikák</translation>
    </message>
    <message>
        <location/>
        <source>Display translation statistics.</source>
        <translation>Fordítási statisztikák megjelenítése.</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Manual</source>
        <translation>&amp;Kézikönyv</translation>
    </message>
    <message>
        <location/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location/>
        <source>About Qt Linguist</source>
        <translation>A Qt Linguist névjegye</translation>
    </message>
    <message>
        <location/>
        <source>About Qt</source>
        <translation>A Qt névjegye</translation>
    </message>
    <message>
        <location/>
        <source>Display information about the Qt toolkit by Digia.</source>
        <translation>Információk megjelenítése a Qt eszközkészletről, a Digiától.</translation>
    </message>
    <message>
        <location/>
        <source>&amp;What&apos;s This?</source>
        <translation>Mi e&amp;z?</translation>
    </message>
    <message>
        <location/>
        <source>What&apos;s This?</source>
        <translation>Mi ez?</translation>
    </message>
    <message>
        <location/>
        <source>Enter What&apos;s This? mode.</source>
        <translation>Átlépés „Mi ez?” módba.</translation>
    </message>
    <message>
        <location/>
        <source>Shift+F1</source>
        <translation>Shift+F1</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Search And Translate...</source>
        <translation>Keresés és &amp;fordítás…</translation>
    </message>
    <message>
        <location/>
        <source>Replace the translation on all entries that matches the search source text.</source>
        <translation>A fordítás cseréje minden olyan bejegyzésnél, amely illeszkedik a keresési forrásszövegre.</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/linguist/linguist/mainwindow.cpp" line="+35"/>
        <source>&amp;Batch Translation...</source>
        <translation>&amp;Kötegelt fordítás…</translation>
    </message>
    <message>
        <location/>
        <source>Batch translate all entries using the information in the phrase books.</source>
        <translation>Az összes bejegyzés kötegelt fordítása a kifejezéstárakban lévő információk használatával.</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/linguist/linguist/mainwindow.cpp" line="-34"/>
        <location line="+10"/>
        <source>Release As...</source>
        <translation>Kiadás másként…</translation>
    </message>
    <message>
        <location/>
        <source>Create a Qt message file suitable for released applications from the current message file. The filename will automatically be determined from the name of the TS file.</source>
        <translation>A kiadott alkalmazások számára megfelelő Qt üzenetfájl létrehozása a jelenlegi üzenetfájlból. A fájlnév a TS-fájl nevéből lesz automatikusan meghatározva.</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/linguist/linguist/mainwindow.cpp" line="+76"/>
        <source>File</source>
        <translation>Fájl</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/linguist/linguist/mainwindow.cpp" line="+7"/>
        <source>Edit</source>
        <translation>Szerkesztés</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/linguist/linguist/mainwindow.cpp" line="+6"/>
        <source>Translation</source>
        <translation>Fordítás</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/linguist/linguist/mainwindow.cpp" line="+6"/>
        <source>Validation</source>
        <translation>Ellenőrzés</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/linguist/linguist/mainwindow.cpp" line="+7"/>
        <source>Help</source>
        <translation>Súgó</translation>
    </message>
    <message>
        <location/>
        <source>Open/Refresh Form &amp;Preview</source>
        <translation>Űrlap &amp;előnézet megnyitása és frissítése</translation>
    </message>
    <message>
        <location/>
        <source>Form Preview Tool</source>
        <translation>Űrlapelőnézet eszköz</translation>
    </message>
    <message>
        <location/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/linguist/linguist/mainwindow.cpp" line="-79"/>
        <source>Translation File &amp;Settings...</source>
        <translation>Fordítási fájl beállítá&amp;sai…</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Add to Phrase Book</source>
        <translation>Hozzá&amp;adás a kifejezéstárhoz</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location/>
        <source>Open Read-O&amp;nly...</source>
        <translation>Megnyitás csak &amp;olvasásra…</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Save All</source>
        <translation>Összes &amp;mentése</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+S</source>
        <translation>Ctrl+S</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/linguist/linguist/mainwindow.cpp" line="-28"/>
        <source>&amp;Release All</source>
        <translation>Összes k&amp;iadása</translation>
    </message>
    <message>
        <location/>
        <source>Close</source>
        <translation>Bezárás</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Close All</source>
        <translation>Összes &amp;bezárása</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <location/>
        <source>Length Variants</source>
        <translation>Hosszváltozatok</translation>
    </message>
    <message>
        <location/>
        <source>Visualize whitespace</source>
        <translation>Üres karakterek megjelenítése</translation>
    </message>
    <message>
        <source>Toggle visualize whitespace in editors</source>
        <translation type="vanished">Üres karakterek megjelenítésének be- és kikapcsolása a szerkesztőkben</translation>
    </message>
    <message>
        <location/>
        <source>Increase</source>
        <translation>Növelés</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl++</source>
        <translation>Ctrl++</translation>
    </message>
    <message>
        <location/>
        <source>Decrease</source>
        <translation>Csökkentés</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+-</source>
        <translation>Ctrl+-</translation>
    </message>
    <message>
        <location/>
        <source>Reset to default</source>
        <translation>Alapértelmezések visszaállítása</translation>
    </message>
    <message>
        <location/>
        <source>Ctrl+0</source>
        <translation>Ctrl+0</translation>
    </message>
    <message>
        <source></source>
        <comment>This is the application&apos;s main window.</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qttools/src/linguist/linguist/mainwindow.cpp" line="-1868"/>
        <source>Source text</source>
        <translation>Forrásszöveg</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+25"/>
        <source>Index</source>
        <translation>Index</translation>
    </message>
    <message>
        <location line="-2"/>
        <location line="+63"/>
        <source>Context</source>
        <translation>Környezet</translation>
    </message>
    <message>
        <location line="-62"/>
        <source>Items</source>
        <translation>Elemek</translation>
    </message>
    <message>
        <location line="+79"/>
        <source>This panel lists the source contexts.</source>
        <translation>Ez a panel sorolja fel a forráskörnyezeteket.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Strings</source>
        <translation>Karakterláncok</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Phrases and guesses</source>
        <translation>Kifejezések és tippek</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Sources and Forms</source>
        <translation>Források és űrlapok</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Warnings</source>
        <translation>Figyelmeztetések</translation>
    </message>
    <message>
        <location line="+61"/>
        <source> MOD </source>
        <comment>status bar: file(s) modified</comment>
        <translation> MOD </translation>
    </message>
    <message>
        <location line="+140"/>
        <source>Loading...</source>
        <translation>Betöltés…</translation>
    </message>
    <message>
        <location line="+32"/>
        <location line="+22"/>
        <source>Loading File - Qt Linguist</source>
        <translation>Fájl betöltése – Qt Linguist</translation>
    </message>
    <message>
        <location line="-21"/>
        <source>The file &apos;%1&apos; does not seem to be related to the currently open file(s) &apos;%2&apos;.

Close the open file(s) first?</source>
        <translation>A(z) „%1” fájl nem a jelenleg megnyitott „%2” fájlokhoz tartozónak tűnik.

Bezárja előbb a nyitott fájlokat?</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>The file &apos;%1&apos; does not seem to be related to the file &apos;%2&apos; which is being loaded as well.

Skip loading the first named file?</source>
        <translation>A(z) „%1” fájl nem a(z) „%2” fájlhoz tartozónak tűnik, amely szintén be lett töltve.

Kihagyja az elsőre megnevezett fájl betöltését?</translation>
    </message>
    <message numerus="yes">
        <location line="+61"/>
        <source>%n translation unit(s) loaded.</source>
        <translation>
            <numerusform>%n fordítási egység betöltve.</numerusform>
        </translation>
    </message>
    <message>
        <location line="+84"/>
        <source>Related files (%1);;</source>
        <translation>Kapcsolódó fájlok (%1);;</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Open Translation Files</source>
        <translation>Fordítási fájlok megnyitása</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+31"/>
        <source>File saved.</source>
        <translation>A fájl elmentve.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Qt message files for released applications (*.qm)
All files (*)</source>
        <translation>Qt üzenetfájlok a kiadott alkalmazásokhoz (*.qm)
Minden fájl (*)</translation>
    </message>
    <message>
        <location line="+3"/>
        <location line="+12"/>
        <source>File created.</source>
        <translation>A fájl létrehozva.</translation>
    </message>
    <message>
        <location line="+34"/>
        <location line="+348"/>
        <source>Printing...</source>
        <translation>Nyomtatás…</translation>
    </message>
    <message>
        <location line="-340"/>
        <source>Context: %1</source>
        <translation>Környezet: %1</translation>
    </message>
    <message>
        <location line="+32"/>
        <source>finished</source>
        <translation>befejezett</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>unresolved</source>
        <translation>megoldatlan</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>obsolete</source>
        <translation>elavult</translation>
    </message>
    <message>
        <location line="+15"/>
        <location line="+299"/>
        <source>Printing... (page %1)</source>
        <translation>Nyomtatás… (%1. oldal)</translation>
    </message>
    <message>
        <location line="-292"/>
        <location line="+299"/>
        <source>Printing completed</source>
        <translation>Nyomtatás befejezve</translation>
    </message>
    <message>
        <location line="-297"/>
        <location line="+299"/>
        <source>Printing aborted</source>
        <translation>Nyomtatás megszakítva</translation>
    </message>
    <message>
        <location line="-231"/>
        <source>Search wrapped.</source>
        <translation>A keresés körbeért.</translation>
    </message>
    <message>
        <location line="+17"/>
        <location line="+286"/>
        <location line="+35"/>
        <location line="+24"/>
        <location line="+22"/>
        <location line="+553"/>
        <location line="+1"/>
        <location line="+306"/>
        <location line="+40"/>
        <location line="+10"/>
        <source>Qt Linguist</source>
        <translation>Qt Linguist</translation>
    </message>
    <message>
        <location line="-1276"/>
        <location line="+101"/>
        <source>Cannot find the string &apos;%1&apos;.</source>
        <translation>Nem található a(z) „%1” karakterlánc.</translation>
    </message>
    <message>
        <location line="-82"/>
        <source>Search And Translate in &apos;%1&apos; - Qt Linguist</source>
        <translation>Keresés és fordítás itt: „%1” - Qt Linguist</translation>
    </message>
    <message>
        <location line="+34"/>
        <location line="+23"/>
        <location line="+24"/>
        <source>Translate - Qt Linguist</source>
        <translation>Fordítás – Qt Linguist</translation>
    </message>
    <message numerus="yes">
        <location line="-46"/>
        <source>Translated %n entry(s)</source>
        <translation>
            <numerusform>%n bejegyzés lefordítva</numerusform>
        </translation>
    </message>
    <message>
        <location line="+23"/>
        <source>No more occurrences of &apos;%1&apos;. Start over?</source>
        <translation>Nincs több találat erre: „%1”. Kezdje elölről?</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>Create New Phrase Book</source>
        <translation>Új kifejezéstár létrehozása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Qt phrase books (*.qph)
All files (*)</source>
        <translation>Qt kifejezéstárak (*.qph)
Minden fájl (*)</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Phrase book created.</source>
        <translation>A kifejezéstár létrehozva.</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Open Phrase Book</source>
        <translation>Kifejezéstár megnyitása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Qt phrase books (*.qph);;All files (*)</source>
        <translation>Qt kifejezéstárak (*.qph);;Minden fájl (*)</translation>
    </message>
    <message numerus="yes">
        <location line="+7"/>
        <source>%n phrase(s) loaded.</source>
        <translation>
            <numerusform>%n kifejezés betöltve.</numerusform>
        </translation>
    </message>
    <message>
        <location line="+91"/>
        <location line="+8"/>
        <location line="+7"/>
        <source>Add to phrase book</source>
        <translation>Hozzáadás a kifejezéstárhoz</translation>
    </message>
    <message>
        <location line="-14"/>
        <source>No appropriate phrasebook found.</source>
        <translation>Nem található megfelelő kifejezéstár.</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Adding entry to phrasebook %1</source>
        <translation>Bejegyzés hozzáadása a(z) %1 kifejezéstárhoz</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Select phrase book to add to</source>
        <translation>Válassza ki a bővítendő kifejezéstárat</translation>
    </message>
    <message>
        <location line="+35"/>
        <source>Unable to launch Qt Assistant (%1)</source>
        <translation>Nem indítható el a Qt Asszisztens (%1)</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Version %1</source>
        <translation>%1. verzió</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Qt Linguist is a tool for adding translations to Qt applications.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Copyright (C) %1 The Qt Company Ltd.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;center&gt;&lt;img src=&quot;:/images/splash.png&quot;/&gt;&lt;/img&gt;&lt;p&gt;%1&lt;/p&gt;&lt;/center&gt;&lt;p&gt;Qt Linguist is a tool for adding translations to Qt applications.&lt;/p&gt;&lt;p&gt;Copyright (C) %2 The Qt Company Ltd.</source>
        <translation type="vanished">&lt;center&gt;&lt;img src=&quot;:/images/splash.png&quot;/&gt;&lt;/img&gt;&lt;p&gt;%1&lt;/p&gt;&lt;/center&gt;&lt;p&gt;A Qt Linguist egy eszköz a fordítások Qt alkalmazásokhoz történő hozzáadásához.&lt;/p&gt;&lt;p&gt;Copyright (C) %2 The Qt Company Ltd.</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Do you want to save the modified files?</source>
        <translation>Szeretné menteni a módosított fájlokat?</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Do you want to save &apos;%1&apos;?</source>
        <translation>Szeretné menteni a(z) „%1” fájlt?</translation>
    </message>
    <message>
        <location line="+44"/>
        <source>Qt Linguist[*]</source>
        <translation>Qt Linguist[*]</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>%1[*] - Qt Linguist</source>
        <translation>%1[*] – Qt Linguist</translation>
    </message>
    <message>
        <location line="+257"/>
        <location line="+12"/>
        <source>No untranslated translation units left.</source>
        <translation>Nem maradt több lefordítatlan fordítási egység.</translation>
    </message>
    <message>
        <location line="+222"/>
        <source>&amp;Window</source>
        <translation>&amp;Ablak</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Minimize</source>
        <translation>Kis méret</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ctrl+M</source>
        <translation>Ctrl+M</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Display the manual for %1.</source>
        <translation>A(z) %1 kézikönyvének megjelenítése.</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Display information about %1.</source>
        <translation>Információk megjelenítése erről: %1.</translation>
    </message>
    <message>
        <location line="+91"/>
        <source>&amp;Save &apos;%1&apos;</source>
        <translation>„%1” menté&amp;se</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Save &apos;%1&apos; &amp;As...</source>
        <translation>„%1” me&amp;ntése másként…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Release &apos;%1&apos;</source>
        <translation>„%1” kiadása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Release &apos;%1&apos; As...</source>
        <translation>„%1” kiadása másként…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Close &apos;%1&apos;</source>
        <translation>„%1” be&amp;zárása</translation>
    </message>
    <message>
        <location line="+2"/>
        <location line="+15"/>
        <source>&amp;Save</source>
        <translation>Menté&amp;s</translation>
    </message>
    <message>
        <location line="-11"/>
        <location line="+13"/>
        <source>&amp;Close</source>
        <translation>&amp;Bezárás</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Save All</source>
        <translation>Összes mentése</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close All</source>
        <translation>Összes bezárása</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Release</source>
        <translation>Kia&amp;dás</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Translation File &amp;Settings for &apos;%1&apos;...</source>
        <translation>Fordítási fájl &amp;beállításai ehhez: „%1”…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Batch Translation of &apos;%1&apos;...</source>
        <translation>„%1” &amp;kötegelt fordítása…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Search And &amp;Translate in &apos;%1&apos;...</source>
        <translation>Keresés és &amp;fordítás itt: „%1”…</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Search And &amp;Translate...</source>
        <translation>Keresés és &amp;fordítás…</translation>
    </message>
    <message>
        <location line="+172"/>
        <source>Cannot read from phrase book &apos;%1&apos;.</source>
        <translation>Nem lehet olvasni a(z) „%1” kifejezéstárból.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Close this phrase book.</source>
        <translation>A kifejezéstár bezárása.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Enables you to add, modify, or delete entries in this phrase book.</source>
        <translation>Lehetővé teszi bejegyzések hozzáadását, módosítását és törlését ebben a kifejezéstárban.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Print the entries in this phrase book.</source>
        <translation>A kifejezéstárban lévő bejegyzések nyomtatása.</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Cannot create phrase book &apos;%1&apos;.</source>
        <translation>Nem lehet létrehozni a(z) „%1” kifejezéstárat.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Do you want to save phrase book &apos;%1&apos;?</source>
        <translation>Szeretné menteni a(z) „%1” kifejezéstárat?</translation>
    </message>
    <message numerus="yes">
        <location line="+34"/>
        <source>%n unfinished message(s) left.</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location line="+348"/>
        <source>All</source>
        <translation>Összes</translation>
    </message>
    <message>
        <location filename="../../qttools/src/linguist/linguist/mainwindow.ui"/>
        <source>Guesses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Toggles the validity check of accelerators</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Toggles the validity check of accelerators, i.e. whether the number of ampersands in the source and translation text is the same. If the check fails, a message is shown in the warnings window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Surrounding &amp;Whitespace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Toggles the validity check of surrounding whitespace.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Toggles the validity check of surrounding whitespace. If the check fails, a message is shown in the warnings window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Toggles the validity check of ending punctuation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Toggles the validity check of ending punctuation. If the check fails, a message is shown in the warnings window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Toggles checking that phrase suggestions are used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Toggles checking that phrase suggestions are used. If the check fails, a message is shown in the warnings window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Toggles the validity check of place markers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Toggles the validity check of place markers, i.e. whether %1, %2, ... are used consistently in the source text and translation text. If the check fails, a message is shown in the warnings window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Toggles visualize whitespace in editors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Show more</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Alt++</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Show fewer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Alt+-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Alt+0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>D&amp;one</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Mark item as done</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Mark this item as done.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageEditor</name>
    <message>
        <source></source>
        <comment>This is the right panel of the main window.</comment>
        <translation></translation>
    </message>
    <message>
        <source>Russian</source>
        <translation type="vanished">Orosz</translation>
    </message>
    <message>
        <source>German</source>
        <translation type="vanished">Német</translation>
    </message>
    <message>
        <source>Japanese</source>
        <translation type="vanished">Japán</translation>
    </message>
    <message>
        <source>French</source>
        <translation type="vanished">Francia</translation>
    </message>
    <message>
        <source>Polish</source>
        <translation type="vanished">Lengyel</translation>
    </message>
    <message>
        <source>Chinese</source>
        <translation type="vanished">Kínai</translation>
    </message>
    <message>
        <location filename="../../qttools/src/linguist/linguist/messageeditor.cpp" line="+104"/>
        <source>This whole panel allows you to view and edit the translation of some source text.</source>
        <translation>Ez az egész panel lehetővé teszi a forrásszövegek fordításainak megtekintését és szerkesztését.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Source text</source>
        <translation>Forrásszöveg</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>This area shows the source text.</source>
        <translation>Ez a terület a forrásszöveget jeleníti meg.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Source text (Plural)</source>
        <translation>Forrásszöveg (többes szám)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>This area shows the plural form of the source text.</source>
        <translation>Ez a terület a forrásszöveg többesszámát jeleníti meg.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Developer comments</source>
        <translation>Fejlesztői megjegyzések</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>This area shows a comment that may guide you, and the context in which the text occurs.</source>
        <translation>Ez a terület olyan egy megjegyzést jelenít meg, amely a segítségére lehet, és azt a környezetet, amelyben a szöveg előfordul.</translation>
    </message>
    <message>
        <location line="+61"/>
        <source>Here you can enter comments for your own use. They have no effect on the translated applications.</source>
        <translation>Itt írhat be saját használatú megjegyzéseket. Ezek nincsenek hatással a lefordított alkalmazásokra.</translation>
    </message>
    <message>
        <location line="+262"/>
        <source>Translation to %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Translation to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Translator comments for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 translation (%2)</source>
        <translation type="vanished">%1 fordítás (%2)</translation>
    </message>
    <message>
        <location line="-6"/>
        <source>This is where you can enter or modify the translation of the above source text.</source>
        <translation>Ez az a terület, ahol a fenti forrásszöveg fordítását beírhatja vagy módosíthatja.</translation>
    </message>
    <message>
        <source>%1 translation</source>
        <translation type="vanished">%1 fordítás</translation>
    </message>
    <message>
        <source>%1 translator comments</source>
        <translation type="vanished">%1 fordítói megjegyzés</translation>
    </message>
    <message>
        <location line="+130"/>
        <source>&apos;%1&apos;
Line: %2</source>
        <translation>„%1”
Sor: %2</translation>
    </message>
</context>
<context>
    <name>MessageModel</name>
    <message>
        <location filename="../../qttools/src/linguist/linguist/messagemodel.cpp" line="+862"/>
        <source>Completion status for %1</source>
        <translation>Befejezettségi állapot ehhez: %1</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>&lt;file header&gt;</source>
        <translation>&lt;fájlfejléc&gt;</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&lt;context comment&gt;</source>
        <translation>&lt;környezeti megjegyzés&gt;</translation>
    </message>
    <message>
        <location line="+71"/>
        <source>&lt;unnamed context&gt;</source>
        <translation>&lt;névtelen környezet&gt;</translation>
    </message>
    <message numerus="yes">
        <location line="+6"/>
        <source>%n unfinished message(s) left.</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>PhraseBook</name>
    <message>
        <location filename="../../qttools/src/linguist/linguist/phrase.cpp" line="+181"/>
        <source>Parse error at line %1, column %2 (%3).</source>
        <translation>Feldolgozási hiba a(z) %1. sor, %2. oszlopában (%3).</translation>
    </message>
</context>
<context>
    <name>PhraseBookBox</name>
    <message>
        <location filename="../../qttools/src/linguist/linguist/phrasebookbox.ui"/>
        <source>Edit Phrase Book</source>
        <translation>Kifejezéstár szerkesztése</translation>
    </message>
    <message>
        <location/>
        <source>This window allows you to add, modify, or delete entries in a phrase book.</source>
        <translation>Ez az ablak lehetővé teszi bejegyzések hozzáadását, módosítását és törlését egy kifejezéstárban.</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Translation:</source>
        <translation>&amp;Fordítás:</translation>
    </message>
    <message>
        <location/>
        <source>This is the phrase in the target language corresponding to the source phrase.</source>
        <translation>Ez a kifejezés a célnyelven a forrásnyelv megfelelője.</translation>
    </message>
    <message>
        <location/>
        <source>S&amp;ource phrase:</source>
        <translation>F&amp;orrás kifejezés:</translation>
    </message>
    <message>
        <location/>
        <source>This is a definition for the source phrase.</source>
        <translation>Ez a forráskifejezés meghatározása.</translation>
    </message>
    <message>
        <location/>
        <source>This is the phrase in the source language.</source>
        <translation>Ez a kifejezés a forrásnyelven.</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Definition:</source>
        <translation>&amp;Definíció:</translation>
    </message>
    <message>
        <location/>
        <source>Click here to add the phrase to the phrase book.</source>
        <translation>Kattintson ide a kifejezés hozzáadásához a kifejezéstárba.</translation>
    </message>
    <message>
        <location/>
        <source>&amp;New Entry</source>
        <translation>Ú&amp;j bejegyzés</translation>
    </message>
    <message>
        <location/>
        <source>Click here to remove the entry from the phrase book.</source>
        <translation>Kattintson ide a bejegyzés eltávolításához a kifejezéstárból.</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Remove Entry</source>
        <translation>Bejegyzés &amp;eltávolítása</translation>
    </message>
    <message>
        <location/>
        <source>Settin&amp;gs...</source>
        <translation>&amp;Beállítások…</translation>
    </message>
    <message>
        <location/>
        <source>Click here to save the changes made.</source>
        <translation>Kattintson ide az elvégzett változtatások mentéséhez.</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Save</source>
        <translation>Menté&amp;s</translation>
    </message>
    <message>
        <location/>
        <source>Click here to close this window.</source>
        <translation>Kattintson ide az ablak bezárásához.</translation>
    </message>
    <message>
        <location/>
        <source>Close</source>
        <translation>Bezárás</translation>
    </message>
    <message>
        <source></source>
        <comment>Go to Phrase &gt; Edit Phrase Book... The dialog that pops up is a PhraseBookBox.</comment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../qttools/src/linguist/linguist/phrasebookbox.cpp" line="+53"/>
        <source>(New Entry)</source>
        <translation>(Új bejegyzés)</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>%1[*] - Qt Linguist</source>
        <translation>%1[*] – Qt Linguist</translation>
    </message>
    <message>
        <location line="+90"/>
        <source>Qt Linguist</source>
        <translation>Qt Linguist</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Cannot save phrase book &apos;%1&apos;.</source>
        <translation>Nem lehet elmenteni a(z) „%1” kifejezéstárat.</translation>
    </message>
</context>
<context>
    <name>PhraseModel</name>
    <message>
        <location filename="../../qttools/src/linguist/linguist/phrasemodel.cpp" line="+105"/>
        <source>Source phrase</source>
        <translation>Forráskifejezés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Translation</source>
        <translation>Fordítás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Definition</source>
        <translation>Definíció</translation>
    </message>
</context>
<context>
    <name>PhraseView</name>
    <message>
        <location filename="../../qttools/src/linguist/linguist/phraseview.cpp" line="+106"/>
        <source>Insert</source>
        <translation>Beszúrás</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Edit</source>
        <translation>Szerkesztés</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Go to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+144"/>
        <source>Guess from &apos;%1&apos; (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Guess from &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Guess (%1)</source>
        <translation type="vanished">Tipp (%1)</translation>
    </message>
    <message>
        <source>Guess</source>
        <translation type="vanished">Tipp</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../qttools/src/linguist/linguist/mainwindow.cpp" line="-1947"/>
        <source>Translation files (%1);;</source>
        <translation>Fordítási fájlok (%1);;</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>All files (*)</source>
        <translation>Minden fájl (*)</translation>
    </message>
    <message>
        <location filename="../../qttools/src/linguist/linguist/messagemodel.cpp" line="-1173"/>
        <location line="+5"/>
        <location line="+29"/>
        <location line="+66"/>
        <location line="+39"/>
        <location line="+17"/>
        <location line="+15"/>
        <location filename="../../qttools/src/linguist/linguist/phrase.cpp" line="+4"/>
        <source>Qt Linguist</source>
        <translation>Qt Linguist</translation>
    </message>
</context>
<context>
    <name>SourceCodeView</name>
    <message>
        <location filename="../../qttools/src/linguist/linguist/sourcecodeview.cpp" line="+57"/>
        <source>&lt;i&gt;Source code not available&lt;/i&gt;</source>
        <translation>&lt;i&gt;A forráskód nem érhető el&lt;/i&gt;</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>&lt;i&gt;File %1 not available&lt;/i&gt;</source>
        <translation>&lt;i&gt;A(z) %1 nem érhető el&lt;/i&gt;</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&lt;i&gt;File %1 not readable&lt;/i&gt;</source>
        <translation>&lt;i&gt;A(z) %1 fájl nem olvasható&lt;/i&gt;</translation>
    </message>
</context>
<context>
    <name>Statistics</name>
    <message>
        <location filename="../../qttools/src/linguist/linguist/statistics.ui"/>
        <source>Statistics</source>
        <translation>Statisztikák</translation>
    </message>
    <message>
        <location/>
        <source>Close</source>
        <translation>Bezárás</translation>
    </message>
    <message>
        <location/>
        <source>Translation</source>
        <translation>Fordítás</translation>
    </message>
    <message>
        <location/>
        <source>Source</source>
        <translation>Forrás</translation>
    </message>
    <message>
        <location/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location/>
        <source>Words:</source>
        <translation>Szavak:</translation>
    </message>
    <message>
        <location/>
        <source>Characters:</source>
        <translation>Karakterek:</translation>
    </message>
    <message>
        <location/>
        <source>Characters (with spaces):</source>
        <translation>Karakterek (szóközökkel):</translation>
    </message>
</context>
<context>
    <name>TranslateDialog</name>
    <message>
        <location filename="../../qttools/src/linguist/linguist/translatedialog.ui"/>
        <source>This window allows you to search for some text in the translation source file.</source>
        <translation>Ez az ablak lehetővé teszi, hogy szöveget keressen a fordítás forrásfájljában.</translation>
    </message>
    <message>
        <location/>
        <source>Type in the text to search for.</source>
        <translation>Gépelje be a keresendő szöveget.</translation>
    </message>
    <message>
        <location/>
        <source>Find &amp;source text:</source>
        <translation>&amp;Forrásszöveg keresése:</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Translate to:</source>
        <translation>&amp;Fordítás erre:</translation>
    </message>
    <message>
        <location/>
        <source>Search options</source>
        <translation>Keresési beállítások</translation>
    </message>
    <message>
        <location/>
        <source>Texts such as &apos;TeX&apos; and &apos;tex&apos; are considered as different when checked.</source>
        <translation>Az olyan szövegek, mint például a „TeX” és „tex”, különbözőnek lesznek tekintve, ha be van jelölve.</translation>
    </message>
    <message>
        <location/>
        <source>Match &amp;case</source>
        <translation>&amp;Kis- és nagybetűk megkülönböztetése</translation>
    </message>
    <message>
        <location/>
        <source>Mark new translation as &amp;finished</source>
        <translation>Új fordítás megjelölése be&amp;fejezettként</translation>
    </message>
    <message>
        <location/>
        <source>Click here to find the next occurrence of the text you typed in.</source>
        <translation>Kattintson ide a begépelt szöveg következő előfordulásának kereséséhez.</translation>
    </message>
    <message>
        <location/>
        <source>Find Next</source>
        <translation>Következő keresése</translation>
    </message>
    <message>
        <location/>
        <source>Translate</source>
        <translation>Fordítás</translation>
    </message>
    <message>
        <location/>
        <source>Translate All</source>
        <translation>Összes fordítása</translation>
    </message>
    <message>
        <location/>
        <source>Click here to close this window.</source>
        <translation>Kattintson ide az ablak bezárásához.</translation>
    </message>
    <message>
        <location/>
        <source>Cancel</source>
        <translation>Mégse</translation>
    </message>
</context>
<context>
    <name>TranslationSettingsDialog</name>
    <message>
        <location filename="../../qttools/src/linguist/linguist/translationsettings.ui"/>
        <source>Source language</source>
        <translation>Forrásnyelv</translation>
    </message>
    <message>
        <location/>
        <source>Language</source>
        <translation>Nyelv</translation>
    </message>
    <message>
        <location/>
        <source>Country/Region</source>
        <translation>Ország vagy régió</translation>
    </message>
    <message>
        <location/>
        <source>Target language</source>
        <translation>Célnyelv</translation>
    </message>
    <message>
        <location filename="../../qttools/src/linguist/linguist/translationsettingsdialog.cpp" line="+50"/>
        <location line="+38"/>
        <source>%1 (%2)</source>
        <extracomment>&lt;english&gt; (&lt;endonym&gt;) (language and country names)</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-22"/>
        <location line="+8"/>
        <source>Settings for &apos;%1&apos; - Qt Linguist</source>
        <translation>„%1” beállításai – Qt Linguist</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Any Country</source>
        <translation>Bármely ország</translation>
    </message>
</context>
</TS>
