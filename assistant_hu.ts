<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu_HU">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/aboutdialog.cpp" line="+105"/>
        <source>&amp;Close</source>
        <translation>&amp;Bezárás</translation>
    </message>
</context>
<context>
    <name>AboutLabel</name>
    <message>
        <location line="-15"/>
        <source>Warning</source>
        <translation>Figyelmeztetés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to launch external application.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to launch external application.
</source>
        <translation type="vanished">Nem indítható el a külső alkalmazás.
</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>Assistant</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/main.cpp" line="+153"/>
        <source>Error registering documentation file &apos;%1&apos;: %2</source>
        <translation>Hiba a(z) „%1” dokumentációfájl regisztrálásakor: %2</translation>
    </message>
    <message>
        <source>Error: %1</source>
        <translation type="vanished">Hiba: %1</translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Could not register documentation file
%1

Reason:
%2</source>
        <translation>Nem sikerült regisztrálni a dokumentációfájlt
%1

Oka:
%2</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Documentation successfully registered.</source>
        <translation>A dokumentáció sikeresen regisztrálva.</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Could not unregister documentation file
%1

Reason:
%2</source>
        <translation>Nem sikerült a dokumentációfájl regisztrálásának eltávolítása
%1

Oka:
%2</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Documentation successfully unregistered.</source>
        <translation>A dokumentáció regisztrálása sikeresen eltávolítva.</translation>
    </message>
    <message>
        <location line="+53"/>
        <location line="+18"/>
        <source>Error reading collection file &apos;%1&apos;: %2.</source>
        <translation>Hiba a(z) „%1” gyűjteményfájl olvasásakor: %2.</translation>
    </message>
    <message>
        <location line="-7"/>
        <source>Error creating collection file &apos;%1&apos;: %2.</source>
        <translation>Hiba a(z) „%1 ”gyűjteményfájl létrehozásakor: %2.</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>Cannot load sqlite database driver!</source>
        <translation>Nem lehet betölteni az sqlite adatbázis-meghajtót!</translation>
    </message>
</context>
<context>
    <name>BookmarkDialog</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/bookmarkdialog.ui"/>
        <source>Add Bookmark</source>
        <translation>Könyvjelző hozzáadása</translation>
    </message>
    <message>
        <location/>
        <source>Bookmark:</source>
        <translation>Könyvjelző:</translation>
    </message>
    <message>
        <location/>
        <source>Add in Folder:</source>
        <translation>Hozzáadás mappába:</translation>
    </message>
    <message>
        <location/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location/>
        <source>New Folder</source>
        <translation>Új mappa</translation>
    </message>
    <message>
        <location filename="../../qttools/src/assistant/assistant/bookmarkdialog.cpp" line="+212"/>
        <source>Rename Folder</source>
        <translation>Mappa átnevezése</translation>
    </message>
</context>
<context>
    <name>BookmarkItem</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/bookmarkitem.cpp" line="+139"/>
        <source>New Folder</source>
        <translation>Új mappa</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Untitled</source>
        <translation>Névtelen</translation>
    </message>
</context>
<context>
    <name>BookmarkManager</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/bookmarkmanager.cpp" line="+149"/>
        <source>Untitled</source>
        <translation>Névtelen</translation>
    </message>
    <message>
        <location line="+76"/>
        <source>Remove</source>
        <translation>Eltávolítás</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>You are going to delete a Folder, this will also&lt;br&gt;remove it&apos;s content. Are you sure to continue?</source>
        <translation>Egy mappa törlésére készül, de ez el fogja&lt;br&gt;távolítani annak tartalmát is. Biztosan folytatja?</translation>
    </message>
    <message>
        <location line="+157"/>
        <source>Manage Bookmarks...</source>
        <translation>Könyvjelzők kezelése…</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Add Bookmark...</source>
        <translation>Könyvjelző hozzáadása…</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Delete Folder</source>
        <translation>Mappa törlése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rename Folder</source>
        <translation>Mappa átnevezése</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Bookmark</source>
        <translation>Könyvjelző megjelenítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Bookmark in New Tab</source>
        <translation>Könyvjelző megjelenítése új lapon</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Delete Bookmark</source>
        <translation>Könyvjelző törlése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rename Bookmark</source>
        <translation>Könyvjelző átnevezése</translation>
    </message>
</context>
<context>
    <name>BookmarkManagerWidget</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/bookmarkmanagerwidget.ui"/>
        <source>Manage Bookmarks</source>
        <translation>Könyvjelzők kezelése</translation>
    </message>
    <message>
        <location/>
        <source>Search:</source>
        <translation>Keresés:</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/assistant/assistant/bookmarkmanagerwidget.cpp" line="+250"/>
        <source>Remove</source>
        <translation>Eltávolítás</translation>
    </message>
    <message>
        <location/>
        <source>Import and Backup</source>
        <translation>Importálás és biztonsági mentés</translation>
    </message>
    <message>
        <location/>
        <location filename="../../qttools/src/assistant/assistant/bookmarkmanagerwidget.cpp" line="-30"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../qttools/src/assistant/assistant/bookmarkmanagerwidget.cpp" line="-148"/>
        <source>Import...</source>
        <translation>Importálás…</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Export...</source>
        <translation>Exportálás…</translation>
    </message>
    <message>
        <location line="+117"/>
        <source>Open File</source>
        <translation>Fájl megnyitása</translation>
    </message>
    <message>
        <location line="+1"/>
        <location line="+16"/>
        <source>Files (*.xbel)</source>
        <translation>Fájlok (*.xbel)</translation>
    </message>
    <message>
        <location line="-1"/>
        <source>Save File</source>
        <translation>Fájl mentése</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Qt Assistant</source>
        <translation>Qt Asszisztens</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Unable to save bookmarks.</source>
        <translation>Nem menthetők a könyvjelzők.</translation>
    </message>
    <message>
        <location line="+30"/>
        <source>You are goingto delete a Folder, this will also&lt;br&gt; remove it&apos;s content. Are you sure to continue?</source>
        <translation>Egy mappa törlésére készül, de ez el fogja&lt;br&gt;távolítani annak tartalmát is. Biztosan folytatja?</translation>
    </message>
    <message>
        <location line="+28"/>
        <source>Delete Folder</source>
        <translation>Mappa törlése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rename Folder</source>
        <translation>Mappa átnevezése</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Show Bookmark</source>
        <translation>Könyvjelző megjelenítése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Show Bookmark in New Tab</source>
        <translation>Könyvjelző megjelenítése új lapon</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Delete Bookmark</source>
        <translation>Könyvjelző törlése</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Rename Bookmark</source>
        <translation>Könyvjelző átnevezése</translation>
    </message>
</context>
<context>
    <name>BookmarkModel</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/bookmarkmodel.cpp" line="+77"/>
        <source>Name</source>
        <translation>Név</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Address</source>
        <translation>Cím</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Bookmarks Toolbar</source>
        <translation>Könyvjelző eszköztár</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Bookmarks Menu</source>
        <translation>Könyvjelzők menü</translation>
    </message>
</context>
<context>
    <name>BookmarkWidget</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/bookmarkwidget.ui"/>
        <source>Bookmarks</source>
        <translation>Könyvjelzők</translation>
    </message>
    <message>
        <location/>
        <source>Filter:</source>
        <translation>Szűrő:</translation>
    </message>
    <message>
        <location/>
        <source>Add</source>
        <translation>Hozzáadás</translation>
    </message>
    <message>
        <location/>
        <source>Remove</source>
        <translation>Eltávolítás</translation>
    </message>
</context>
<context>
    <name>CentralWidget</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/centralwidget.cpp" line="+402"/>
        <source>Print Document</source>
        <translation>Dokumentum nyomtatása</translation>
    </message>
</context>
<context>
    <name>CmdLineParser</name>
    <message>
        <source>Usage: assistant [Options]

-collectionFile file       Uses the specified collection
                           file instead of the default one
-showUrl url               Shows the document with the
                           url.
-enableRemoteControl       Enables Assistant to be
                           remotely controlled.
-show widget               Shows the specified dockwidget
                           which can be &quot;contents&quot;, &quot;index&quot;,
                           &quot;bookmarks&quot; or &quot;search&quot;.
-activate widget           Activates the specified dockwidget
                           which can be &quot;contents&quot;, &quot;index&quot;,
                           &quot;bookmarks&quot; or &quot;search&quot;.
-hide widget               Hides the specified dockwidget
                           which can be &quot;contents&quot;, &quot;index&quot;
                           &quot;bookmarks&quot; or &quot;search&quot;.
-register helpFile         Registers the specified help file
                           (.qch) in the given collection
                           file.
-unregister helpFile       Unregisters the specified help file
                           (.qch) from the give collection
                           file.
-setCurrentFilter filter   Set the filter as the active filter.
-remove-search-index       Removes the full text search index.
-rebuild-search-index      Re-builds the full text search index (potentially slow).
-quiet                     Does not display any error or
                           status message.
-help                      Displays this help.
</source>
        <translation type="vanished">Használat: assistant [kapcsolók]

-collectionFile fájl       A megadott gyűjteményfájl
                           használata az alapértelmezett helyett
-showUrl url               Megjeleníti az url-ben
                           megadott dokumentumot.
-enableRemoteControl       Engedélyezi, hogy az Aszisztenst
                           távvezéreljék.
-show widget               Megjeleníti a megadott dokk felületi elemet,
                           amely „contents”, „index”,
                           „bookmarks” vagy „search” lehet.
-activate widget           Aktiválja a megadott dokk felületi elemet,
                           amely „contents”, „index”,
                           „bookmarks” vagy „search” lehet.
-hide widget               Elrejti a megadott dokk felületi elemet,
                           amely „contents”, „index”
                           „bookmarks” vagy „search” lehet.
-register súgófájl         Regisztrálja a megadott súgófájlt
                           (.qch) az adott gyűjteményfájlba.
-unregister súgófájl       Eltávolítja a megadott súgófájlt
                           (.qch) az adott gyűjteményfájlból.
-setCurrentFilter szűrő    Beállítja a szűrőt aktív szűrőként.
-remove-search-index       Törli a szabad-szavas szöveg keresési indexet.
-rebuild-search-index      Újra előállítja a szabad-szavas szöveg keresési
                           indexet (többnyire lassú).
-quiet                     Nem jelenít meg semmilyen hiba- vagy
                           állapotüzenetet.
-help                      Megjeleníti ezt a súgót.</translation>
    </message>
    <message>
        <location filename="../../qttools/src/assistant/assistant/cmdlineparser.cpp" line="+38"/>
        <source>Usage: assistant [Options]

-collectionFile file       Uses the specified collection
                           file instead of the default one
-showUrl url               Shows the document with the
                           url.
-enableRemoteControl       Enables Assistant to be
                           remotely controlled.
-show widget               Shows the specified dockwidget
                           which can be &quot;contents&quot;, &quot;index&quot;,
                           &quot;bookmarks&quot; or &quot;search&quot;.
-activate widget           Activates the specified dockwidget
                           which can be &quot;contents&quot;, &quot;index&quot;,
                           &quot;bookmarks&quot; or &quot;search&quot;.
-hide widget               Hides the specified dockwidget
                           which can be &quot;contents&quot;, &quot;index&quot;
                           &quot;bookmarks&quot; or &quot;search&quot;.
-register helpFile         Registers the specified help file
                           (.qch) in the given collection
                           file.
-unregister helpFile       Unregisters the specified help file
                           (.qch) from the give collection
                           file.
-setCurrentFilter filter   Set the filter as the active filter.
-remove-search-index       Removes the full text search index.
-rebuild-search-index      Obsolete. Use -remove-search-index instead.
                           Removes the full text search index.
                           It will be rebuilt on next Assistant run.
-quiet                     Does not display any error or
                           status message.
-help                      Displays this help.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Unknown option: %1</source>
        <translation>Ismeretlen kapcsoló: %1</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>The collection file &apos;%1&apos; does not exist.</source>
        <translation>A(z) „%1” gyűjteményfájl nem létezik.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Missing collection file.</source>
        <translation>Hiányzó gyűjteményfájl.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Invalid URL &apos;%1&apos;.</source>
        <translation>Érvénytelen URL: „%1”.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Missing URL.</source>
        <translation>Hiányzó URL.</translation>
    </message>
    <message>
        <location line="+36"/>
        <source>Unknown widget: %1</source>
        <translation>Ismeretlen felületi elem: %1</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Missing widget.</source>
        <translation>Hiányzó felületi elem.</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>The Qt help file &apos;%1&apos; does not exist.</source>
        <translation>A(z) „%1” Qt súgófájl nem létezik.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Missing help file.</source>
        <translation>Hiányzó súgófájl.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Missing filter argument.</source>
        <translation>Hiányzó szűrőargumentum.</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>Error</source>
        <translation>Hiba</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Notice</source>
        <translation>Értesítés</translation>
    </message>
</context>
<context>
    <name>ContentWindow</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/contentwindow.cpp" line="+163"/>
        <source>Open Link</source>
        <translation>Hivatkozás megnyitása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open Link in New Tab</source>
        <translation>Hivatkozás megnyitása új lapon</translation>
    </message>
</context>
<context>
    <name>FilterNameDialogClass</name>
    <message>
        <source>Add Filter Name</source>
        <translation type="vanished">Szűrőnév hozzáadása</translation>
    </message>
    <message>
        <source>Filter Name:</source>
        <translation type="vanished">Szűrő neve:</translation>
    </message>
</context>
<context>
    <name>FindWidget</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/findwidget.cpp" line="+71"/>
        <source>Previous</source>
        <translation>Előző</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Next</source>
        <translation>Következő</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Case Sensitive</source>
        <translation>Kis- és nagybetű érzékeny</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&lt;img src=&quot;:/qt-project.org/assistant/images/wrap.png&quot;&gt;&amp;nbsp;Search wrapped</source>
        <translation>&lt;img src=&quot;:/qt-project.org/assistant/images/wrap.png&quot;&gt;&amp;nbsp;A keresés körbeért</translation>
    </message>
</context>
<context>
    <name>FontPanel</name>
    <message>
        <location filename="../../qttools/src/shared/fontpanel/fontpanel.cpp" line="+61"/>
        <source>Font</source>
        <translation>Betűkészlet</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Writing system</source>
        <translation>Írás&amp;rendszer</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Family</source>
        <translation>Betűcs&amp;alád</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Style</source>
        <translation>&amp;Stílus</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>&amp;Point size</source>
        <translation>&amp;Pontméret</translation>
    </message>
</context>
<context>
    <name>GlobalActions</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/globalactions.cpp" line="+63"/>
        <source>&amp;Back</source>
        <translation>&amp;Vissza</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Forward</source>
        <translation>&amp;Előre</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Home</source>
        <translation>&amp;Kezdőlap</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>ALT+Home</source>
        <translation>Alt+Home</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Zoom &amp;in</source>
        <translation>&amp;Nagyítás</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Zoom &amp;out</source>
        <translation>&amp;Kicsinyítés</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>&amp;Copy selected Text</source>
        <translation>A kijelölt szöveg &amp;másolása</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>&amp;Print...</source>
        <translation>&amp;Nyomtatás…</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>&amp;Find in Text...</source>
        <translation>Ke&amp;resés a szövegben…</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>&amp;Find</source>
        <translation>&amp;Keresés</translation>
    </message>
</context>
<context>
    <name>HelpDocSettingsWidget</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/helpdocsettingswidget.ui"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Registered Documentation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>&lt;Filter&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location/>
        <source>Add...</source>
        <translation type="unfinished">Hozzáadás…</translation>
    </message>
    <message>
        <location/>
        <source>Remove</source>
        <translation type="unfinished">Eltávolítás</translation>
    </message>
</context>
<context>
    <name>HelpDocSettingsWidgetPrivate</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/helpdocsettingswidget.cpp" line="+72"/>
        <source>Add Documentation</source>
        <translation type="unfinished">Dokumentáció hozzáadása</translation>
    </message>
    <message>
        <location line="+0"/>
        <source>Qt Compressed Help Files (*.qch)</source>
        <translation type="unfinished">Qt tömörített súgófájlok (*.qch)</translation>
    </message>
</context>
<context>
    <name>HelpEngineWrapper</name>
    <message>
        <source>Unfiltered</source>
        <translation type="vanished">Szűretlen</translation>
    </message>
</context>
<context>
    <name>HelpViewer</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/helpbrowsersupport.cpp" line="+59"/>
        <source>Error 404...</source>
        <translation>404-es hiba…</translation>
    </message>
    <message>
        <source>The page could not be found!</source>
        <translation type="vanished">Az oldal nem található!</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The page could not be found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Please make sure that you have all documentation sets installed.</source>
        <translation>Győződjön meg róla, hogy minden dokumentációs készlet telepítve van-e.</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Error loading: %1</source>
        <translation>Hiba a betöltéskor: %1</translation>
    </message>
    <message>
        <location filename="../../qttools/src/assistant/assistant/helpviewer.cpp" line="+49"/>
        <source>&lt;title&gt;about:blank&lt;/title&gt;</source>
        <translation>&lt;title&gt;about:blank&lt;/title&gt;</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&lt;title&gt;Error 404...&lt;/title&gt;&lt;div align=&quot;center&quot;&gt;&lt;br&gt;&lt;br&gt;&lt;h1&gt;The page could not be found.&lt;/h1&gt;&lt;br&gt;&lt;h3&gt;&apos;%1&apos;&lt;/h3&gt;&lt;/div&gt;</source>
        <translation>&lt;title&gt;404-es hiba…&lt;/title&gt;&lt;div align=&quot;center&quot;&gt;&lt;br&gt;&lt;br&gt;&lt;h1&gt;A lap nem található.&lt;/h1&gt;&lt;br&gt;&lt;h3&gt;„%1”&lt;/h3&gt;&lt;/div&gt;</translation>
    </message>
    <message>
        <location filename="../../qttools/src/assistant/assistant/helpviewer_qtb.cpp" line="+355"/>
        <source>Open Link</source>
        <translation>Hivatkozás megnyitása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open Link in New Tab	Ctrl+LMB</source>
        <translation>Hivatkozás megnyitása új lapon	Ctrl+bal egérgomb</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Copy &amp;Link Location</source>
        <translation>&amp;Hivatkozás helyének másolása</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Copy</source>
        <translation>Másolás</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Reload</source>
        <translation>Újratöltés</translation>
    </message>
    <message>
        <location filename="../../qttools/src/assistant/assistant/helpviewer_qwv.cpp" line="+158"/>
        <source>Open Link in New Page</source>
        <translation>Hivatkozás megnyitása új oldalon</translation>
    </message>
</context>
<context>
    <name>IndexWindow</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/indexwindow.cpp" line="+59"/>
        <source>&amp;Look for:</source>
        <translation>&amp;Keresés erre:</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>Open Link</source>
        <translation>Hivatkozás megnyitása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Open Link in New Tab</source>
        <translation>Hivatkozás megnyitása új lapon</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/mainwindow.cpp" line="+122"/>
        <location line="+468"/>
        <source>Index</source>
        <translation>Index</translation>
    </message>
    <message>
        <location line="-462"/>
        <location line="+460"/>
        <source>Contents</source>
        <translation>Tartalom</translation>
    </message>
    <message>
        <location line="-452"/>
        <location line="+458"/>
        <source>Search</source>
        <translation>Keresés</translation>
    </message>
    <message>
        <location line="-453"/>
        <location line="+451"/>
        <source>Bookmarks</source>
        <translation>Könyvjelzők</translation>
    </message>
    <message>
        <location line="-445"/>
        <location line="+449"/>
        <source>Open Pages</source>
        <translation>Oldalak megnyitása</translation>
    </message>
    <message>
        <location line="-427"/>
        <location line="+746"/>
        <location line="+281"/>
        <source>Qt Assistant</source>
        <translation>Qt Asszisztens</translation>
    </message>
    <message>
        <location line="-996"/>
        <source>Bookmark Toolbar</source>
        <translation>Könyvjelző eszköztár</translation>
    </message>
    <message>
        <location line="+262"/>
        <source>Looking for Qt Documentation...</source>
        <translation>Qt dokumentáció keresése…</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>&amp;File</source>
        <translation>&amp;Fájl</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>New &amp;Tab</source>
        <translation>Új &amp;lap</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&amp;Close Tab</source>
        <translation>Lap be&amp;zárása</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Page Set&amp;up...</source>
        <translation>&amp;Oldalbeállítás…</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Print Preview...</source>
        <translation>Nyomtatási kép…</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>E&amp;xit</source>
        <translation>&amp;Kilépés</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>CTRL+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Quit</source>
        <translation>&amp;Kilépés</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>&amp;Edit</source>
        <translation>S&amp;zerkesztés</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Find &amp;Next</source>
        <translation>Kö&amp;vetkező keresése</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Find &amp;Previous</source>
        <translation>&amp;Előző keresése</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Preferences...</source>
        <translation>Beállítások…</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;View</source>
        <translation>&amp;Nézet</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Normal &amp;Size</source>
        <translation>Normá&amp;l méret</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Ctrl+0</source>
        <translation>Ctrl+0</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>ALT+C</source>
        <translation>Alt+C</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>ALT+I</source>
        <translation>Alt+I</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>ALT+O</source>
        <translation>Alt+O</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>ALT+S</source>
        <translation>Alt+S</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>ALT+P</source>
        <translation>Alt+P</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Go</source>
        <translation>&amp;Ugrás</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Sync with Table of Contents</source>
        <translation>Szinkronizálás a tartalomjegyzékkel</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Sync</source>
        <translation>Szinkronizálás</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Next Page</source>
        <translation>Következő oldal</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl+Alt+Right</source>
        <translation>Ctrl+Alt+jobb</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Previous Page</source>
        <translation>Előző oldal</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl+Alt+Left</source>
        <translation>Ctrl+Alt+bal</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>&amp;Bookmarks</source>
        <translation>&amp;Könyvjelzők</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&amp;Help</source>
        <translation>&amp;Súgó</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>About...</source>
        <translation>Névjegy…</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Navigation Toolbar</source>
        <translation>Navigációs eszköztár</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>&amp;Window</source>
        <translation>&amp;Ablak</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Zoom</source>
        <translation>Nagyítás</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Minimize</source>
        <translation>Kis méret</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Ctrl+M</source>
        <translation>Ctrl+M</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Toolbars</source>
        <translation>Eszköztárak</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Filter Toolbar</source>
        <translation>Szűrő eszköztár</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Filtered by:</source>
        <translation>Szűrés alapja: </translation>
    </message>
    <message>
        <location line="+26"/>
        <source>Address Toolbar</source>
        <translation>Cím eszköztár</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Address:</source>
        <translation>Cím:</translation>
    </message>
    <message>
        <location line="+94"/>
        <source>Could not find the associated content item.</source>
        <translation>Nem található a hozzárendelt tartalomelem.</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>&lt;center&gt;&lt;h3&gt;%1&lt;/h3&gt;&lt;p&gt;Version %2&lt;/p&gt;&lt;p&gt;Browser: %3&lt;/p&gt;&lt;/center&gt;&lt;p&gt;Copyright (C) %4 The Qt Company Ltd.&lt;/p&gt;</source>
        <translation>&lt;center&gt;&lt;h3&gt;%1&lt;/h3&gt;&lt;p&gt;%2. verzió&lt;/p&gt;&lt;p&gt;Böngésző: %3&lt;/p&gt;&lt;/center&gt;&lt;p&gt;Copyright (C) %4 The Qt Company Ltd.&lt;/p&gt;</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>About %1</source>
        <translation>%1 névjegye</translation>
    </message>
    <message>
        <location line="+139"/>
        <source>Unfiltered</source>
        <translation type="unfinished">Szűretlen</translation>
    </message>
    <message>
        <location line="+37"/>
        <source>Updating search index</source>
        <translation>Keresési index frissítése</translation>
    </message>
    <message>
        <location line="+100"/>
        <source>Could not register file &apos;%1&apos;: %2</source>
        <translation>Nem sikerült a(z) „%1” fájl regisztrálása: %2</translation>
    </message>
</context>
<context>
    <name>OpenPagesWidget</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/openpageswidget.cpp" line="+148"/>
        <source>Close %1</source>
        <translation>%1 bezárása</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Close All Except %1</source>
        <translation>Összes bezárása, kivéve: %1</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <source>Add Documentation</source>
        <translation type="vanished">Dokumentáció hozzáadása</translation>
    </message>
    <message>
        <source>Qt Compressed Help Files (*.qch)</source>
        <translation type="vanished">Qt tömörített súgófájlok (*.qch)</translation>
    </message>
    <message>
        <source>The namespace %1 is already registered!</source>
        <translation type="vanished">A(z) %1 névtér már regisztrálva van!</translation>
    </message>
    <message>
        <source>The specified file is not a valid Qt Help File!</source>
        <translation type="vanished">A megadott fájl nem érvényes Qt súgófájl!</translation>
    </message>
    <message>
        <source>Remove Documentation</source>
        <translation type="vanished">Dokumentáció eltávolítása</translation>
    </message>
    <message>
        <source>Some documents currently opened in Assistant reference the documentation you are attempting to remove. Removing the documentation will close those documents.</source>
        <translation type="vanished">Az Asszisztensben jelenleg néhány megnyitott dokumentum hivatkozik arra dokumentációra, amelyet eltávolítani próbál. A dokumentáció eltávolítása be fogja zárni azokat a dokumentumokat.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Mégse</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">OK</translation>
    </message>
    <message>
        <location filename="../../qttools/src/assistant/assistant/preferencesdialog.cpp" line="+175"/>
        <source>Use custom settings</source>
        <translation>Egyéni beállítások használata</translation>
    </message>
</context>
<context>
    <name>PreferencesDialogClass</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/preferencesdialog.ui"/>
        <source>Preferences</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <location/>
        <source>Fonts</source>
        <translation>Betűkészletek</translation>
    </message>
    <message>
        <location/>
        <source>Font settings:</source>
        <translation>Betűkészlet beállításai:</translation>
    </message>
    <message>
        <location/>
        <source>Browser</source>
        <translation>Böngésző</translation>
    </message>
    <message>
        <location/>
        <source>Application</source>
        <translation>Alkalmazás</translation>
    </message>
    <message>
        <location/>
        <source>Filters</source>
        <translation>Szűrők</translation>
    </message>
    <message>
        <source>Filter:</source>
        <translation type="vanished">Szűrő:</translation>
    </message>
    <message>
        <source>Attributes:</source>
        <translation type="vanished">Attribútumok:</translation>
    </message>
    <message>
        <source>1</source>
        <translation type="vanished">1</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="vanished">Hozzáadás</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="vanished">Eltávolítás</translation>
    </message>
    <message>
        <location/>
        <source>Documentation</source>
        <translation>Dokumentáció</translation>
    </message>
    <message>
        <source>Registered Documentation:</source>
        <translation type="vanished">Regisztrált dokumentáció:</translation>
    </message>
    <message>
        <source>Add...</source>
        <translation type="vanished">Hozzáadás…</translation>
    </message>
    <message>
        <location/>
        <source>Options</source>
        <translation>Beállítások</translation>
    </message>
    <message>
        <location/>
        <source>On help start:</source>
        <translation>A súgó indításakor:</translation>
    </message>
    <message>
        <location/>
        <source>Show my home page</source>
        <translation>Saját kezdőlap megjelenítése</translation>
    </message>
    <message>
        <location/>
        <source>Show a blank page</source>
        <translation>Üres lap megjelenítése</translation>
    </message>
    <message>
        <location/>
        <source>Show my tabs from last session</source>
        <translation>Saját lapok megjelenítése a legutóbbi munkamenetből</translation>
    </message>
    <message>
        <location/>
        <source>Homepage</source>
        <translation>Kezdőlap</translation>
    </message>
    <message>
        <location/>
        <source>Current Page</source>
        <translation>Jelenlegi oldal</translation>
    </message>
    <message>
        <location/>
        <source>Blank Page</source>
        <translation>Üres oldal</translation>
    </message>
    <message>
        <location/>
        <source>Restore to default</source>
        <translation>Alapértelmezés visszaállítása</translation>
    </message>
    <message>
        <location/>
        <source>Appearance</source>
        <translation>Megjelenés</translation>
    </message>
    <message>
        <location/>
        <source>Show tabs for each individual page</source>
        <translation>Lapok megjelenítése az egyéni oldalakhoz</translation>
    </message>
</context>
<context>
    <name>RemoteControl</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/remotecontrol.cpp" line="+82"/>
        <source>Debugging Remote Control</source>
        <translation>Hibakeresési távvezérlés</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Received Command: %1 %2</source>
        <translation>A kapott parancs: %1 %2</translation>
    </message>
</context>
<context>
    <name>SearchWidget</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/searchwidget.cpp" line="+189"/>
        <source>&amp;Copy</source>
        <translation>&amp;Másolás</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Copy &amp;Link Location</source>
        <translation>&amp;Hivatkozás helyének másolása</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Open Link in New Tab</source>
        <translation>Hivatkozás megnyitása új lapon</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Select All</source>
        <translation>Összes kijelölése</translation>
    </message>
</context>
<context>
    <name>TabBar</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/centralwidget.cpp" line="-275"/>
        <source>(Untitled)</source>
        <translation>(Névtelen)</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>New &amp;Tab</source>
        <translation>Új &amp;lap</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>&amp;Close Tab</source>
        <translation>Lap be&amp;zárása</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Close Other Tabs</source>
        <translation>A többi lap bezárása</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Add Bookmark for this Page...</source>
        <translation>Könyvjelző hozzáadása ehhez az oldalhoz…</translation>
    </message>
</context>
<context>
    <name>TopicChooser</name>
    <message>
        <location filename="../../qttools/src/assistant/assistant/topicchooser.ui"/>
        <source>Choose Topic</source>
        <translation>Téma választása </translation>
    </message>
    <message>
        <location/>
        <source>&amp;Topics</source>
        <translation>&amp;Témák</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Display</source>
        <translation>Meg&amp;jelenítés</translation>
    </message>
    <message>
        <location/>
        <source>&amp;Close</source>
        <translation>&amp;Bezárás</translation>
    </message>
    <message>
        <location filename="../../qttools/src/assistant/assistant/topicchooser.cpp" line="+51"/>
        <source>Filter</source>
        <translation>Szűrő</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Choose a topic for &lt;b&gt;%1&lt;/b&gt;:</source>
        <translation>Téma választása ehhez: &lt;b&gt;%1&lt;/b&gt;:</translation>
    </message>
</context>
</TS>
