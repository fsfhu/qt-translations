<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu_HU">
<context>
    <name>QScriptBreakpointsModel</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptbreakpointsmodel.cpp" line="+453"/>
        <source>ID</source>
        <translation>Azonosító</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Location</source>
        <translation>Hely</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Condition</source>
        <translation>Feltétel</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ignore-count</source>
        <translation>Kihagyások száma</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Single-shot</source>
        <translation>Egyetlen kép</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Hit-count</source>
        <translation>Találatszám</translation>
    </message>
</context>
<context>
    <name>QScriptBreakpointsWidget</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptbreakpointswidget.cpp" line="+296"/>
        <source>New</source>
        <translation>Új</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Delete</source>
        <translation>Törlés</translation>
    </message>
</context>
<context>
    <name>QScriptDebugger</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptdebugger.cpp" line="+885"/>
        <location line="+1033"/>
        <source>Go to Line</source>
        <translation>Ugrás sorra</translation>
    </message>
    <message>
        <location line="-1032"/>
        <source>Line:</source>
        <translation>Sor:</translation>
    </message>
    <message>
        <location line="+791"/>
        <source>Interrupt</source>
        <translation>Megszakítás</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Shift+F5</source>
        <translation>Shift+F5</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Continue</source>
        <translation>Folytatás</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Step Into</source>
        <translation>Belelépés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>F11</source>
        <translation>F11</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Step Over</source>
        <translation>Átlépés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>F10</source>
        <translation>F10</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Step Out</source>
        <translation>Kilépés</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Shift+F11</source>
        <translation>Shift+F11</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Run to Cursor</source>
        <translation>Futtatás a kurzorig</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Ctrl+F10</source>
        <translation>Ctrl+F10</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Run to New Script</source>
        <translation>Futtatás új parancsfájlig</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Toggle Breakpoint</source>
        <translation>Töréspont be- és kikapcsolása</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>F9</source>
        <translation>F9</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Clear Debug Output</source>
        <translation>Hibakeresési kimenet törlése</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Clear Error Log</source>
        <translation>Hibanapló törlése</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Clear Console</source>
        <translation>Konzol törlése</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>&amp;Find in Script...</source>
        <translation>&amp;Keresés a parancsfájlban…</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Find &amp;Next</source>
        <translation>&amp;Következő keresése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>F3</source>
        <translation>F3</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Find &amp;Previous</source>
        <translation>&amp;Előző keresése</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Shift+F3</source>
        <translation>Shift+F3</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Ctrl+G</source>
        <translation>Ctrl+G</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Debug</source>
        <translation>Hibakeresés</translation>
    </message>
</context>
<context>
    <name>QScriptDebuggerCodeFinderWidget</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptdebuggercodefinderwidget.cpp" line="+139"/>
        <source>Close</source>
        <translation>Bezárás</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Previous</source>
        <translation>Előző</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Next</source>
        <translation>Következő</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Case Sensitive</source>
        <translation>Kis- és nagybetű érzékeny</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Whole words</source>
        <translation>Teljes szavak</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>&lt;img src=&quot;:/qt/scripttools/debugging/images/wrap.png&quot;&gt;&amp;nbsp;Search wrapped</source>
        <translation>&lt;img src=&quot;:/qt/scripttools/debugging/images/wrap.png&quot;&gt;&amp;nbsp;A keresés körbeért</translation>
    </message>
</context>
<context>
    <name>QScriptDebuggerLocalsModel</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptdebuggerlocalsmodel.cpp" line="+895"/>
        <source>Name</source>
        <translation>Név</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Value</source>
        <translation>Érték</translation>
    </message>
</context>
<context>
    <name>QScriptDebuggerStackModel</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptdebuggerstackmodel.cpp" line="+159"/>
        <source>Level</source>
        <translation>Szint</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Name</source>
        <translation>Név</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Location</source>
        <translation>Hely</translation>
    </message>
</context>
<context>
    <name>QScriptEdit</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptedit.cpp" line="+419"/>
        <source>Toggle Breakpoint</source>
        <translation>Töréspont be- és kikapcsolása</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Disable Breakpoint</source>
        <translation>Töréspont letiltása</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Enable Breakpoint</source>
        <translation>Töréspont engedélyezése</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Breakpoint Condition:</source>
        <translation>Töréspont feltétele:</translation>
    </message>
</context>
<context>
    <name>QScriptEngineDebugger</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptenginedebugger.cpp" line="+522"/>
        <source>Loaded Scripts</source>
        <translation>Betöltött parancsfájlok</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Breakpoints</source>
        <translation>Töréspontok</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Stack</source>
        <translation>Verem</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Locals</source>
        <translation>Helyi változók</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Console</source>
        <translation>Konzol</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Debug Output</source>
        <translation>Hibakeresési kimenet</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Error Log</source>
        <translation>Hibanapló</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Search</source>
        <translation>Keresés</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>View</source>
        <translation>Nézet</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Qt Script Debugger</source>
        <translation>Qt parancsfájl hibakereső</translation>
    </message>
</context>
<context>
    <name>QScriptNewBreakpointWidget</name>
    <message>
        <location filename="../../qtscript/src/scripttools/debugging/qscriptbreakpointswidget.cpp" line="-223"/>
        <source>Close</source>
        <translation>Bezárás</translation>
    </message>
</context>
</TS>
