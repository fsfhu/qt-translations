<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hu_HU">
<context>
    <name>Object</name>
    <message>
        <source>Duplicate method name</source>
        <translation type="vanished">Kettőzött metódusnév</translation>
    </message>
    <message>
        <source>Method names cannot begin with an upper case letter</source>
        <translation type="vanished">Metódusnevek nem kezdődhetnek nagybetűvel</translation>
    </message>
    <message>
        <source>Illegal method name</source>
        <translation type="vanished">Szabálytalan metódusnév</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/qml/compiler/qqmlirbuilder.cpp" line="+230"/>
        <source>Duplicate scoped enum name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Duplicate signal name</source>
        <translation>Kettőzött jelzésnév</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Duplicate property name</source>
        <translation>Kettőzött tulajdonságnév</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Property names cannot begin with an upper case letter</source>
        <translation>Tulajdonságnevek nem kezdődhetnek nagybetűvel</translation>
    </message>
    <message>
        <location line="+6"/>
        <location line="+27"/>
        <source>Duplicate default property</source>
        <translation>Kettőzött alapértelmezett tulajdonság</translation>
    </message>
    <message>
        <location line="-10"/>
        <source>Duplicate alias name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Alias names cannot begin with an upper case letter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+43"/>
        <source>Property value set multiple times</source>
        <translation>A tulajdonság értéke többször lett beállítva</translation>
    </message>
</context>
<context>
    <name>QInputMethod</name>
    <message>
        <source>InputMethod is an abstract class</source>
        <translation type="vanished">Az InputMethod egy absztrakt osztály</translation>
    </message>
</context>
<context>
    <name>QQmlAnonymousComponentResolver</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmltypecompiler.cpp" line="+919"/>
        <source>Component objects cannot declare new functions.</source>
        <translation>Az összetevő objektumok nem deklarálhatnak új függvényeket.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Component objects cannot declare new properties.</source>
        <translation>Az összetevő objektumok nem deklarálhatnak új tulajdonságokat.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Component objects cannot declare new signals.</source>
        <translation>Az összetevő objektumok nem deklarálhatnak új jelzéseket.</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot create empty component specification</source>
        <translation>Nem lehet üres összetevő-specifikációt létrehozni</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Component elements may not contain properties other than id</source>
        <translation>Az összetevő elemek nem tartalmazhatnak az azonosítón kívül más tulajdonságokat</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Invalid component body specification</source>
        <translation>Érvénytelen összetevő-törzs megadás</translation>
    </message>
    <message>
        <location line="+52"/>
        <source>id is not unique</source>
        <translation>az azonosító nem egyedi</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>Circular alias reference detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+31"/>
        <source>Invalid alias reference. Unable to find id &quot;%1&quot;</source>
        <translation>Érvénytelen álnév-hivatkozás. A(z) „%1” azonosító nem található</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+37"/>
        <location line="+28"/>
        <location line="+10"/>
        <source>Invalid alias target location: %1</source>
        <translation>Érvénytelen álnév célhely: %1</translation>
    </message>
</context>
<context>
    <name>QQmlCodeGenerator</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/compiler/qqmlirbuilder.cpp" line="+278"/>
        <location line="+854"/>
        <source>Property value set multiple times</source>
        <translation>A tulajdonság értéke többször lett beállítva</translation>
    </message>
    <message>
        <location line="-794"/>
        <location line="+820"/>
        <source>Expected type name</source>
        <translation>Típusnév szükséges</translation>
    </message>
    <message>
        <location line="-579"/>
        <source>Signal names cannot begin with an upper case letter</source>
        <translation>A jelzésnevek nem kezdődhetnek nagybetűvel</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Illegal signal name</source>
        <translation>Szabálytalan jelzésnév</translation>
    </message>
    <message>
        <location line="+446"/>
        <source>No property alias location</source>
        <translation>Nincs tulajdonságálnév hely</translation>
    </message>
    <message>
        <location line="+18"/>
        <location line="+6"/>
        <location line="+4"/>
        <source>Invalid alias reference. An alias reference must be specified as &lt;id&gt;, &lt;id&gt;.&lt;property&gt; or &lt;id&gt;.&lt;value property&gt;.&lt;property&gt;</source>
        <translation>Érvénytelen álnévhivatkozás. Egy álnévhivatkozást a következőként kell megadni: &lt;azonosító&gt;, &lt;azonosító&gt;.&lt;tulajdonság&gt; vagy &lt;azonosító&gt;.&lt;érték tulajdonság&gt;.&lt;tulajdonság&gt;</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>Invalid alias location</source>
        <translation>Érvénytelen álnévhely</translation>
    </message>
    <message>
        <location line="-411"/>
        <location line="+432"/>
        <source>Illegal property name</source>
        <translation>Szabálytalan tulajdonságnév</translation>
    </message>
    <message>
        <location line="-1199"/>
        <location line="+6"/>
        <source>Duplicate method name</source>
        <translation type="unfinished">Kettőzött metódusnév</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Method names cannot begin with an upper case letter</source>
        <translation type="unfinished">Metódusnevek nem kezdődhetnek nagybetűvel</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Illegal method name</source>
        <translation type="unfinished">Szabálytalan metódusnév</translation>
    </message>
    <message>
        <location line="+617"/>
        <source>Scoped enum names must begin with an upper case letter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Enum names must begin with an upper case letter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Enum value must be an integer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Enum value out of range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+456"/>
        <source>Invalid component id specification</source>
        <translation>Érvénytelen összetevő-azonosító megadás</translation>
    </message>
    <message>
        <location line="+134"/>
        <source>Invalid empty ID</source>
        <translation>Érvénytelen üres azonosító</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>IDs cannot start with an uppercase letter</source>
        <translation>Az azonosítók nem kezdődhetnek nagybetűvel</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>IDs must start with a letter or underscore</source>
        <translation>Az azonosítóknak betűvel vagy aláhúzással kell kezdődniük</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>IDs must contain only letters, numbers, and underscores</source>
        <translation>Az azonosítók csak betűket, számokat és aláhúzást tartalmazhatnak</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>ID illegally masks global JavaScript property</source>
        <translation>Az azonosító szabálytalanul maszkol egy globális JavaScript tulajdonságot</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>Invalid use of id property</source>
        <translation>Az azonosító tulajdonság érvénytelen használata</translation>
    </message>
</context>
<context>
    <name>QQmlComponent</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlcomponent.cpp" line="+701"/>
        <source>Invalid empty URL</source>
        <translation>Érvénytelen üres URL</translation>
    </message>
    <message>
        <location line="+753"/>
        <location line="+127"/>
        <source>createObject: value is not an object</source>
        <translation>objektumlétrehozás: az érték nem egy objektum</translation>
    </message>
    <message>
        <source>Object destroyed during incubation</source>
        <translation type="vanished">Az objektum megsemmisült az inkubáció során</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlincubator.cpp" line="+286"/>
        <source>Object or context destroyed during incubation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQmlConnections</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/types/qqmlconnections.cpp" line="+246"/>
        <location line="+127"/>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation>Nem lehet hozzárendelni a nem létező „%1” tulajdonsághoz</translation>
    </message>
    <message>
        <location line="-120"/>
        <source>Connections: nested objects not allowed</source>
        <translation>Kapcsolatok: az egymásba ágyazott objektumok nem engedélyezettek</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Connections: syntax error</source>
        <translation>Kapcsolatok: szintaktikai hiba</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Connections: script expected</source>
        <translation>Kapcsolatok: parancsfájl szükséges</translation>
    </message>
    <message>
        <location line="+25"/>
        <source>Implicitly defined onFoo properties in Connections are deprecated. Use this syntax instead: function onFoo(&lt;arguments&gt;) { ... }</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+56"/>
        <source>Detected function &quot;%1&quot; in Connections element. This is probably intended to be a signal handler but no signal of the target matches the name.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQmlDebugServerImpl</name>
    <message>
        <location filename="../../qtdeclarative/src/plugins/qmltooling/qmldbg_server/qqmldebugserver.cpp" line="+378"/>
        <source>QML Debugger: Invalid argument &quot;%1&quot; detected. Ignoring the same.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <source>QML Debugger: Ignoring &quot;-qmljsdebugger=%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The format is &quot;-qmljsdebugger=[file:&lt;file&gt;|port:&lt;port_from&gt;][,&lt;port_to&gt;][,host:&lt;ip address&gt;][,block][,services:&lt;service&gt;][,&lt;service&gt;]*&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&quot;file:&quot; can be used to specify the name of a file the debugger will try to connect to using a QLocalSocket. If &quot;file:&quot; is given any &quot;host:&quot; and&quot;port:&quot; arguments will be ignored.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&quot;host:&quot; and &quot;port:&quot; can be used to specify an address and a single port or a range of ports the debugger will try to bind to with a QTcpServer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>&quot;block&quot; makes the debugger and some services wait for clients to be connected and ready before the first QML engine starts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&quot;services:&quot; can be used to specify which debug services the debugger should load. Some debug services interact badly with others. The V4 debugger should not be loaded when using the QML profiler as it will force any V4 engines to use the JavaScript interpreter rather than the JIT. The following debug services are available by default:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>The QML debugger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The V4 debugger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The QML inspector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+1"/>
        <source>The QML profiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Allows the client to delay the starting and stopping of
		  QML engines until other services are ready. QtCreator
		  uses this service with the QML profiler in order to
		  profile multiple QML engines at the same time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Sends qDebug() and similar messages over the QML debug
		  connection. QtCreator uses this for showing debug
		  messages in the debugger console.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>helps to see if a translated text
		  will result in an elided text
		  in QML elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Other services offered by qmltooling plugins that implement QQmlDebugServiceFactory and which can be found in the standard plugin paths will also be available and can be specified. If no &quot;services&quot; argument is given, all services found this way, including the default ones, are loaded.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQmlDelegateModel</name>
    <message>
        <location filename="../../qtdeclarative/src/qmlmodels/qqmldelegatemodel.cpp" line="+466"/>
        <source>The delegate of a DelegateModel cannot be changed within onUpdated.</source>
        <translation>Egy DelegateModel delegáltját nem lehet megváltoztatni az onUpdated eseményen belül.</translation>
    </message>
    <message>
        <location line="+227"/>
        <source>The maximum number of supported DelegateModelGroups is 8</source>
        <translation>A támogatott DelegateModelGroups csoportok legnagyobb száma 8.</translation>
    </message>
    <message>
        <location line="+110"/>
        <source>The group of a DelegateModel cannot be changed within onChanged</source>
        <translation>Egy DelegateModel csoportját nem lehet megváltoztatni az onChanged eseményen belül</translation>
    </message>
    <message>
        <location line="+1020"/>
        <source>The delegates of a DelegateModel cannot be changed within onUpdated.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQmlDelegateModelGroup</name>
    <message>
        <location line="-1503"/>
        <source>Group names must start with a lower case letter</source>
        <translation>A csoportneveknek kisbetűvel kell kezdődniük</translation>
    </message>
    <message>
        <location line="+2555"/>
        <source>get: index out of range</source>
        <translation>lekérés: az index kívül esik a tartományon</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>insert: index out of range</source>
        <translation>beszúrás: az index kívül esik a tartományon</translation>
    </message>
    <message>
        <location line="+85"/>
        <source>create: index out of range</source>
        <translation>létrehozás: az index kívül esik a tartományon</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>resolve: from index out of range</source>
        <translation>feloldás: a kezdő index kívül esik a tartományon</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>resolve: from index invalid</source>
        <translation>feloldás: a kezdő index érvénytelen</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>resolve: to index out of range</source>
        <translation>feloldás: a befejező index kívül esik a tartományon</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>resolve: to index invalid</source>
        <translation>feloldás: a befejező index érvénytelen</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>resolve: from is not an unresolved item</source>
        <translation>feloldás: a kezdő elem nem egy feloldatlan elem</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>resolve: to is not a model item</source>
        <translation>feloldás: az befejező elem nem egy modellelem</translation>
    </message>
    <message>
        <location line="+70"/>
        <source>remove: invalid index</source>
        <translation>eltávolítás: érvénytelen index</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>remove: index out of range</source>
        <translation>eltávolítás: az index kívül esik a tartományon</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>remove: invalid count</source>
        <translation>eltávolítás: érvénytelen darabszám</translation>
    </message>
    <message>
        <location line="+55"/>
        <source>addGroups: index out of range</source>
        <translation>csoportok hozzáadása: az index kívül esik a tartományon</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>addGroups: invalid count</source>
        <translation>csoportok hozzáadása: érvénytelen darabszám</translation>
    </message>
    <message>
        <location line="+26"/>
        <source>removeGroups: index out of range</source>
        <translation>csoportok eltávolítása: az index kívül esik a tartományon</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>removeGroups: invalid count</source>
        <translation>csoportok eltávolítása: érvénytelen darabszám</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>setGroups: index out of range</source>
        <translation>csoportok beállítása: az index kívül esik a tartományon</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>setGroups: invalid count</source>
        <translation>csoportok beállítása: érvénytelen darabszám</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>move: invalid from index</source>
        <translation>áthelyezés: érvénytelen kezdő index</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>move: invalid to index</source>
        <translation>áthelyezés: érvénytelen befejező index</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>move: invalid count</source>
        <translation>áthelyezés: érvénytelen darabszám</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>move: from index out of range</source>
        <translation>áthelyezés: a kezdő index kívül esik a tartományon</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>move: to index out of range</source>
        <translation>áthelyezés: a befejező index kívül esik a tartományon</translation>
    </message>
</context>
<context>
    <name>QQmlEngine</name>
    <message>
        <source>Locale cannot be instantiated.  Use Qt.locale()</source>
        <translation type="vanished">A területi beállítás nem példányosítható. Helyette a Qt.locale() használandó</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlengine.cpp" line="+223"/>
        <source>Locale cannot be instantiated. Use Qt.locale()</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+445"/>
        <source>There are still &quot;%1&quot; items in the process of being created at engine destruction.</source>
        <translation>Még mindig „%1” elem van a motormegsemmisítéskor létrehozott folyamatban.</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/imports/localstorage/qquicklocalstorage.cpp" line="+279"/>
        <source>executeSql called outside transaction()</source>
        <translation>Az executeSql a transaction() függvényen kívülről lett meghívva</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Read-only Transaction</source>
        <translation>Csak olvasható tranzakció</translation>
    </message>
    <message>
        <location line="+112"/>
        <source>Version mismatch: expected %1, found %2</source>
        <translation>Verzióeltérés: várt: %1, talált: %2</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>SQL transaction failed</source>
        <translation>Az SQL tranzakció sikertelen</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>transaction: missing callback</source>
        <translation>tranzakció: hiányzó visszahívás</translation>
    </message>
    <message>
        <location line="+267"/>
        <source>SQL: can&apos;t create database, offline storage is disabled.</source>
        <translation>SQL: nem lehet adatbázist létrehozni, a kapcsolat nélküli tároló le van tiltva.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>LocalStorage: can&apos;t create path %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+14"/>
        <source>SQL: database version mismatch</source>
        <translation>SQL: adatbázisverzió-eltérés</translation>
    </message>
</context>
<context>
    <name>QQmlEnumTypeResolver</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmltypecompiler.cpp" line="-645"/>
        <source>Invalid property assignment: Enum value &quot;%1&quot; cannot start with a lowercase letter</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: a(z) „%1” felsorolásérték nem kezdődhet kisbetűvel</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Invalid property assignment: &quot;%1&quot; is a read-only property</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: a(z) „%1” egy csak olvasható tulajdonság</translation>
    </message>
</context>
<context>
    <name>QQmlImportDatabase</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlimport.cpp" line="+645"/>
        <source>&quot;%1&quot; is ambiguous. Found in %2 and in %3</source>
        <translation>A(z) „%1” nem egyértelmű. Ezekben található meg: %2 és %3</translation>
    </message>
    <message>
        <location line="+270"/>
        <source>- %1 is neither a type nor a namespace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>- %1 is not a namespace</source>
        <translation>- %1 nem egy névtér</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>- %1 is not a type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+12"/>
        <source>- nested namespaces not allowed</source>
        <translation>- egymásba ágyazott névterek nem engedélyezettek</translation>
    </message>
    <message>
        <location line="+59"/>
        <location line="+4"/>
        <source>local directory</source>
        <translation>helyi könyvtár</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>is ambiguous. Found in %1 and in %2</source>
        <translation>nem egyértelmű. Ezekben található meg: %1 és %2</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>is ambiguous. Found in %1 in version %2.%3 and %4.%5</source>
        <translation>nem egyértelmű. Megtalálható ebben: %1 a következő verziókban: %2.%3 és %4.%5</translation>
    </message>
    <message>
        <location line="+17"/>
        <source>is instantiated recursively</source>
        <translation>rekurzívan van példányosítva</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>is not a type</source>
        <translation>nem egy típus</translation>
    </message>
    <message>
        <location line="+92"/>
        <source>static plugin for module &quot;%1&quot; with name &quot;%2&quot; has no metadata URI</source>
        <translation>a(z) „%2” nevű „%1” modulhoz tartozó statikus bővítménynek nincs metaadat URI-ja</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>module does not support the designer &quot;%1&quot;</source>
        <translation>a modul nem támogatja a(z) „%1” tervezőt</translation>
    </message>
    <message>
        <location line="+46"/>
        <source>plugin cannot be loaded for module &quot;%1&quot;: %2</source>
        <translation>nem tölthető be bővítmény a(z) „%1” modulhoz: %2</translation>
    </message>
    <message>
        <location line="+34"/>
        <source>static plugin for module &quot;%1&quot; with name &quot;%2&quot; cannot be loaded: %3</source>
        <translation>a(z) „%2” nevű „%1” modulhoz tartozó statikus bővítmény nem tölthető be: %3</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>could not resolve all plugins for module &quot;%1&quot;</source>
        <translation>nem sikerült feloldani minden bővítményt a(z) „%1” modulhoz</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>module &quot;%1&quot; plugin &quot;%2&quot; not found</source>
        <translation>a(z) „%1” modul  „%2” bővítménye nem található</translation>
    </message>
    <message>
        <location line="+173"/>
        <location line="+24"/>
        <source>&quot;%1&quot; version %2.%3 is defined more than once in module &quot;%4&quot;</source>
        <translation>a(z) %2.%3 verziójú „%1” egynél többször van meghatározva a(z) „%4” modulban</translation>
    </message>
    <message>
        <location line="+15"/>
        <location line="+88"/>
        <location line="+139"/>
        <source>module &quot;%1&quot; version %2.%3 is not installed</source>
        <translation>a(z) %2.%3 verziójú „%1” modul nincs telepítve</translation>
    </message>
    <message>
        <location line="-137"/>
        <location line="+139"/>
        <source>module &quot;%1&quot; is not installed</source>
        <translation>a(z) „%1” modul nincs telepítve</translation>
    </message>
    <message>
        <location line="-94"/>
        <source>&quot;%1&quot;: no such directory</source>
        <translation>„%1”: nincs ilyen könyvtár</translation>
    </message>
    <message>
        <location line="+20"/>
        <source>import &quot;%1&quot; has no qmldir and no namespace</source>
        <translation>a(z) „%1” importálásnak nincs qmldir könyvtára és névtere</translation>
    </message>
    <message>
        <source>Module loaded for URI &apos;%1&apos; does not implement QQmlTypesExtensionInterface</source>
        <translation type="vanished">A(z) „%1” URI-hoz betöltött modul nem valósítja meg a QQmlTypesExtensionInterface felületet</translation>
    </message>
    <message>
        <source>Module namespace &apos;%1&apos; does not match import URI &apos;%2&apos;</source>
        <translation type="vanished">A(z) „%1” modulnévtér nem egyezik az importált „%2” URI-val</translation>
    </message>
    <message>
        <source>Namespace &apos;%1&apos; has already been used for type registration</source>
        <translation type="vanished">A(z) „%1” névtér már használatban van a típusregisztrációhoz</translation>
    </message>
    <message>
        <source>Module &apos;%1&apos; does not contain a module identifier directive - it cannot be protected from external registrations.</source>
        <translation type="vanished">A(z) „%1” modul nem tartalmaz modulazonosító irányelvet - nem lehet megvédeni a külső regisztrációktól.</translation>
    </message>
    <message>
        <location line="+599"/>
        <source>File name case mismatch for &quot;%1&quot;</source>
        <translation>Kis- és nagybetű eltérés a(z) „%1” fájlnévben</translation>
    </message>
</context>
<context>
    <name>QQmlListModel</name>
    <message>
        <source>unable to enable dynamic roles as this model is not empty!</source>
        <translation type="vanished">nem engedélyezhetők a dinamikus szerepek, mivel ez a modell nem üres!</translation>
    </message>
    <message>
        <source>unable to enable static roles as this model is not empty!</source>
        <translation type="vanished">nem engedélyezhetők a statikus szerepek, mivel ez a modell nem üres!</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/qmlmodels/qqmllistmodel.cpp" line="+2262"/>
        <source>unable to enable dynamic roles as this model is not empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>unable to enable static roles as this model is not empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>dynamic role setting must be made from the main thread, before any worker scripts are created</source>
        <translation>a dinamikus szerep beállítását a fő szálból kell megtenni, mielőtt bármilyen feldolgozó parancsfájl létrejönne </translation>
    </message>
    <message>
        <location line="+42"/>
        <source>remove: indices [%1 - %2] out of range [0 - %3]</source>
        <translation>eltávolítás: az indexek [%1 - %2] kívül esnek a tartományon [0 - %3]</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>remove: incorrect number of arguments</source>
        <translation>eltávolítás: helytelen argumentumszám</translation>
    </message>
    <message>
        <location line="+59"/>
        <source>insert: index %1 out of range</source>
        <translation>beszúrás: a(z) %1 index kívül esik a tartományon</translation>
    </message>
    <message>
        <location line="+32"/>
        <location line="+3"/>
        <source>insert: value is not an object</source>
        <translation>beszúrás: az érték nem objektum</translation>
    </message>
    <message>
        <location line="+23"/>
        <source>move: out of range</source>
        <translation>áthelyezés: kívül esik a tartományon</translation>
    </message>
    <message>
        <location line="+92"/>
        <location line="+3"/>
        <source>append: value is not an object</source>
        <translation>hozzáfűzés: az érték nem objektum</translation>
    </message>
    <message>
        <location line="+83"/>
        <source>set: value is not an object</source>
        <translation>beállítás: az érték nem objektum</translation>
    </message>
    <message>
        <location line="+4"/>
        <location line="+46"/>
        <source>set: index %1 out of range</source>
        <translation>beállítás: a(z) %1 index kívül esik a tartományon</translation>
    </message>
    <message>
        <location line="+42"/>
        <location line="+15"/>
        <source>ListElement: cannot contain nested elements</source>
        <translation>listaelem: nem tartalmazhat egymásba ágyazott elemeket</translation>
    </message>
    <message>
        <location line="-8"/>
        <source>ListElement: cannot use reserved &quot;id&quot; property</source>
        <translation>listaelem: nem használható a foglalt azonosító tulajdonság</translation>
    </message>
    <message>
        <location line="+21"/>
        <source>ListElement: cannot use script for property value</source>
        <translation>listaelem: nem lehet parancsfájlt használni tulajdonság értéknél</translation>
    </message>
    <message>
        <location line="+100"/>
        <source>ListModel: undefined property &apos;%1&apos;</source>
        <translation>listamodell: meghatározatlan „%1” tulajdonság</translation>
    </message>
</context>
<context>
    <name>QQmlObjectCreator</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlobjectcreator.cpp" line="+687"/>
        <source>Cannot assign value %1 to property %2</source>
        <translation>Nem lehet hozzárendelni a(z) %1 értéket a(z) %2 tulajdonsághoz</translation>
    </message>
    <message>
        <location line="+180"/>
        <location line="+12"/>
        <source>Cannot set properties on %1 as it is null</source>
        <translation>Nem lehet beállítani a tulajdonságokat ezen: %1, mivel az null</translation>
    </message>
    <message>
        <location line="+140"/>
        <source>Cannot assign an object to signal property %1</source>
        <translation>Nem lehet egy objektumot hozzárendelni a(z) %1 jelzéstulajdonsághoz</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cannot assign object type %1 with no default method</source>
        <translation>Nem lehet hozzárendelni az alapértelmezett metódus nélküli %1 objektumtípust</translation>
    </message>
    <message>
        <source>Cannot connect mismatched signal/slot %1 %vs. %2</source>
        <translation type="vanished">Nem lehet csatlakoztatni eltérő jelzést és tárolóhelyet: %1 ↔ %2</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot connect mismatched signal/slot %1 vs %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+21"/>
        <source>Cannot assign object to interface property</source>
        <translation>Nem lehet objektumot hozzárendelni a felület tulajdonsághoz</translation>
    </message>
    <message>
        <location line="+39"/>
        <source>Cannot assign object to read only list</source>
        <translation>Nem lehet objektumot hozzárendelni egy csak olvasható listához</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot assign primitives to lists</source>
        <translation>Nem lehet primitív típusokat listákhoz rendelni</translation>
    </message>
    <message>
        <location line="+87"/>
        <source>Unable to create object of type %1</source>
        <translation>A(z) %1 típusú objektum nem hozható létre</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Composite Singleton Type %1 is not creatable</source>
        <translation>A(z) %1 összetett egyke típus nem létrehozható</translation>
    </message>
</context>
<context>
    <name>QQmlObjectModel</name>
    <message>
        <location filename="../../qtdeclarative/src/qmlmodels/qqmlobjectmodel.cpp" line="+386"/>
        <source>insert: index %1 out of range</source>
        <translation>beszúrás: a(z) %1 index kívül esik a tartományon</translation>
    </message>
    <message>
        <location line="+27"/>
        <source>move: out of range</source>
        <translation>áthelyezés: kívül esik a tartományon</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>remove: indices [%1 - %2] out of range [0 - %3]</source>
        <translation>eltávolítás: az indexek [%1 - %2] kívül esnek a tartományon [0 - %3]</translation>
    </message>
</context>
<context>
    <name>QQmlParser</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/compiler/qqmlirbuilder.cpp" line="-1004"/>
        <source>Unexpected object definition</source>
        <translation>Váratlan objektum-meghatározás</translation>
    </message>
    <message>
        <location line="+260"/>
        <source>Invalid import qualifier ID</source>
        <translation>Érvénytelen importminősítő-azonosító</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Reserved name &quot;Qt&quot; cannot be used as an qualifier</source>
        <translation>A „Qt” foglalt név nem használható minősítőként</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Script import qualifiers must be unique.</source>
        <translation>A parancsfájl importminősítőknek egyedinek kell lenniük.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Script import requires a qualifier</source>
        <translation>A parancsfájl importálás egy minősítőt igényel</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Library import requires a version</source>
        <translation>A függvénykönyvtár-importáláshoz egy verzió szükséges</translation>
    </message>
    <message>
        <location line="+30"/>
        <location line="+4"/>
        <source>Pragma requires a valid qualifier</source>
        <translation>A pragma egy érvényes minősítőt igényel</translation>
    </message>
    <message>
        <location line="+93"/>
        <source>Expected parameter type</source>
        <translation>Paramétertípus szükséges</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid signal parameter type: </source>
        <translation>Érvénytelen jelzésparaméter-típus:</translation>
    </message>
    <message>
        <location line="+49"/>
        <source>Invalid property type modifier</source>
        <translation>Érvénytelen tulajdonságtípus módosító</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Unexpected property type modifier</source>
        <translation>Váratlan tulajdonságtípus módosító</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Expected property type</source>
        <translation>Tulajdonságtípus szükséges</translation>
    </message>
    <message>
        <location line="+73"/>
        <source>JavaScript declaration outside Script element</source>
        <translation>JavaScript deklaráció egy Script elemen kívül</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/qml/parser/qqmljslexer.cpp" line="+1197"/>
        <source>Illegal syntax for exponential number</source>
        <translation>Érvénytelen szintaxisú exponenciális szám</translation>
    </message>
    <message>
        <location line="-298"/>
        <source>Stray newline in string literal</source>
        <translation>Bitang új sor a karakterlánc konstansértékben</translation>
    </message>
    <message>
        <location line="-503"/>
        <source>Illegal unicode escape sequence</source>
        <translation>Szabálytalan Unicode elfedési sorrend</translation>
    </message>
    <message>
        <location line="+606"/>
        <source>Illegal hexadecimal escape sequence</source>
        <translation>Szabálytalan hexadecimális elfedési sorrend</translation>
    </message>
    <message>
        <location line="+33"/>
        <source>Octal escape sequences are not allowed</source>
        <translation>Oktális elfedési sorrend nem engedélyezett</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Unclosed string at end of line</source>
        <translation>Lezáratlan karakterlánc a sor végénél</translation>
    </message>
    <message>
        <location line="+83"/>
        <source>Decimal numbers can&apos;t start with &apos;0&apos;</source>
        <translation>Decimális számok nem kezdődhetnek „0”-val</translation>
    </message>
    <message>
        <location line="-68"/>
        <source>At least one hexadecimal digit is required after &apos;0%1&apos;</source>
        <translation>Legalább egy hexadecimális számjegy szükséges a „0%1” után</translation>
    </message>
    <message>
        <location line="-409"/>
        <source>Unexpected token &apos;.&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+309"/>
        <source>End of file reached at escape sequence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+124"/>
        <source>At least one octal digit is required after &apos;0%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+24"/>
        <source>At least one binary digit is required after &apos;0%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+121"/>
        <source>Invalid regular expression flag &apos;%0&apos;</source>
        <translation>Érvénytelen reguláris kifejezés jelző: „%0”</translation>
    </message>
    <message>
        <location line="+17"/>
        <location line="+22"/>
        <source>Unterminated regular expression backslash sequence</source>
        <translation>Lezáratlan reguláris kifejezés fordított perjel sorozat</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Unterminated regular expression class</source>
        <translation>Lezáratlan reguláris kifejezés osztály</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Unterminated regular expression literal</source>
        <translation>Lezáratlan reguláris kifejezés konstansérték</translation>
    </message>
    <message>
        <location line="+203"/>
        <location line="+8"/>
        <location line="+110"/>
        <source>Syntax error</source>
        <translation>Szintaktikai hiba</translation>
    </message>
    <message>
        <location line="-88"/>
        <source>Imported file must be a script</source>
        <translation>Az importált fájlnak parancsfájlnak kell lennie</translation>
    </message>
    <message>
        <location line="+8"/>
        <location line="+8"/>
        <location line="+10"/>
        <source>Invalid module URI</source>
        <translation>Érvénytelen modul URI</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Module import requires a version</source>
        <translation>A modulimportálás egy verziót igényel</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Module import requires a minor version (missing dot)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Module import requires a minor version (missing number)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+12"/>
        <source>File import requires a qualifier</source>
        <translation>A fájlimportálás egy minősítőt igényel</translation>
    </message>
    <message>
        <location line="-10"/>
        <location line="+12"/>
        <source>Module import requires a qualifier</source>
        <translation>A modulimportálás egy minősítőt igényel</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid import qualifier</source>
        <translation>Érvénytelen importminősítő</translation>
    </message>
    <message>
        <source>Unexpected token `%1&apos;</source>
        <translation type="vanished">Váratlan vezérjel: „%1”</translation>
    </message>
    <message>
        <source>Expected token `%1&apos;</source>
        <translation type="vanished">„%1” vezérjel szükséges</translation>
    </message>
</context>
<context>
    <name>QQmlPartsModel</name>
    <message>
        <location filename="../../qtdeclarative/src/qmlmodels/qqmldelegatemodel.cpp" line="+56"/>
        <source>The group of a DelegateModel cannot be changed within onChanged</source>
        <translation>Egy DelegateModel csoportját nem lehet megváltoztatni az onChanged eseményen belül</translation>
    </message>
    <message>
        <location line="+113"/>
        <source>Delegate component must be Package type.</source>
        <translation>A delegált összetevőnek csomag típusúnak kell lennie.</translation>
    </message>
</context>
<context>
    <name>QQmlPropertyCacheCreator</name>
    <message>
        <source>Fully dynamic types cannot declare new properties.</source>
        <translation type="vanished">A teljesen dinamikus típusok nem deklarálhatnak új tulajdonságokat.</translation>
    </message>
    <message>
        <source>Fully dynamic types cannot declare new signals.</source>
        <translation type="vanished">A teljesen dinamikus típusok nem deklarálhatnak új jelzéseket.</translation>
    </message>
    <message>
        <source>Fully Dynamic types cannot declare new functions.</source>
        <translation type="vanished">A teljesen dinamikus típusok nem deklarálhatnak új függvényeket.</translation>
    </message>
    <message>
        <source>Non-existent attached object</source>
        <translation type="vanished">Nem létező csatolt objektum</translation>
    </message>
    <message>
        <source>Cannot override FINAL property</source>
        <translation type="vanished">Nem lehet FINAL tulajdonságot felülbírálni</translation>
    </message>
    <message>
        <source>Invalid signal parameter type: %1</source>
        <translation type="vanished">Érvénytelen jelzésparaméter-típus: %1</translation>
    </message>
    <message>
        <source>Duplicate signal name: invalid override of property change signal or superclass signal</source>
        <translation type="vanished">Kettőzött jelzésnév: tulajdonságváltoztató jelzés vagy szuperosztály jelzés érvénytelen felülbírálása</translation>
    </message>
    <message>
        <source>Duplicate method name: invalid override of property change signal or superclass signal</source>
        <translation type="vanished">Kettőzött metódusnév: tulajdonságváltoztató jelzés vagy szuperosztály jelzés érvénytelen felülbírálása</translation>
    </message>
    <message>
        <source>Invalid property type</source>
        <translation type="vanished">Érvénytelen tulajdonságtípus</translation>
    </message>
</context>
<context>
    <name>QQmlPropertyCacheCreatorBase</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlpropertycachecreator_p.h" line="+315"/>
        <source>Fully dynamic types cannot declare new properties.</source>
        <translation type="unfinished">A teljesen dinamikus típusok nem deklarálhatnak új tulajdonságokat.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Fully dynamic types cannot declare new signals.</source>
        <translation type="unfinished">A teljesen dinamikus típusok nem deklarálhatnak új jelzéseket.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Fully Dynamic types cannot declare new functions.</source>
        <translation type="unfinished">A teljesen dinamikus típusok nem deklarálhatnak új függvényeket.</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Non-existent attached object</source>
        <translation type="unfinished">Nem létező csatolt objektum</translation>
    </message>
    <message>
        <location line="+45"/>
        <location line="+9"/>
        <source>Cannot override FINAL property</source>
        <translation type="unfinished">Nem lehet FINAL tulajdonságot felülbírálni</translation>
    </message>
    <message>
        <location line="+89"/>
        <source>Invalid signal parameter type: %1</source>
        <translation type="unfinished">Érvénytelen jelzésparaméter-típus: %1</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Duplicate signal name: invalid override of property change signal or superclass signal</source>
        <translation type="unfinished">Kettőzött jelzésnév: tulajdonságváltoztató jelzés vagy szuperosztály jelzés érvénytelen felülbírálása</translation>
    </message>
    <message>
        <location line="+16"/>
        <source>Duplicate method name: invalid override of property change signal or superclass signal</source>
        <translation type="unfinished">Kettőzött metódusnév: tulajdonságváltoztató jelzés vagy szuperosztály jelzés érvénytelen felülbírálása</translation>
    </message>
    <message>
        <location line="+53"/>
        <source>Invalid property type</source>
        <translation type="unfinished">Érvénytelen tulajdonságtípus</translation>
    </message>
    <message>
        <location line="+252"/>
        <source>Cyclic alias</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Invalid alias target</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQmlPropertyValidator</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlpropertyvalidator.cpp" line="+138"/>
        <source>Property assignment expected</source>
        <translation>Tulajdonság-hozzárendelés szükséges</translation>
    </message>
    <message>
        <location line="+57"/>
        <source>&quot;%1.%2&quot; is not available in %3 %4.%5.</source>
        <translation>A(z) „%1.%2” nem érhető ebben: %3 %4.%5.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&quot;%1.%2&quot; is not available due to component versioning.</source>
        <translation>A(z) „%1.%2” nem érhető el az összetevő-verziózás miatt.</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+54"/>
        <location line="+23"/>
        <source>Cannot assign a value directly to a grouped property</source>
        <translation>Nem lehet egy értéket közvetlenül hozzárendelni egy csoportosított tulajdonsághoz</translation>
    </message>
    <message>
        <location line="-62"/>
        <source>Invalid use of namespace</source>
        <translation>Érvénytelen névtérhasználat</translation>
    </message>
    <message>
        <location line="+1"/>
        <source>Invalid attached object assignment</source>
        <translation>Érvénytelen csatolt objektum hozzárendelés</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>Attached properties cannot be used here</source>
        <translation>A csatolt tulajdonságok itt nem használhatók</translation>
    </message>
    <message>
        <location line="+17"/>
        <location line="+37"/>
        <source>Invalid property assignment: &quot;%1&quot; is a read-only property</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: a(z) „%1” egy csak olvasható tulajdonság</translation>
    </message>
    <message>
        <location line="-31"/>
        <source>Cannot assign multiple values to a script property</source>
        <translation>Nem lehet több értéket hozzárendelni egy parancsfájl tulajdonsághoz</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cannot assign multiple values to a singular property</source>
        <translation>Nem lehet több értéket hozzárendelni egy egyedülálló tulajdonsághoz</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Property has already been assigned a value</source>
        <translation>A tulajdonsághoz már hozzárendeltek egy értéket</translation>
    </message>
    <message>
        <location line="+19"/>
        <source>Invalid grouped property access</source>
        <translation>Érvénytelen csoportos tulajdonság-hozzáférés</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Invalid grouped property access: Property &quot;%1&quot; with primitive type &quot;%2&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Invalid grouped property access: Property &quot;%1&quot; with type &quot;%2&quot;, which is not a value type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot assign to non-existent default property</source>
        <translation>Nem lehet hozzárendelni egy nem létező alapértelmezett tulajdonsághoz</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation>Nem lehet hozzárendelni a nem létező „%1” tulajdonsághoz</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Invalid use of id property with a value type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Cannot assign primitives to lists</source>
        <translation>Nem lehet primitív típusokat listákhoz rendelni</translation>
    </message>
    <message>
        <location line="+18"/>
        <source>Invalid property assignment: unknown enumeration</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: ismeretlen felsorolás</translation>
    </message>
    <message>
        <location line="+11"/>
        <source> - Assigning null to incompatible properties in QML is deprecated. This will become a compile error in future versions of Qt.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Invalid property assignment: string expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: karakterlánc szükséges</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid property assignment: string or string list expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: karakterlánc vagy karakterlánclista szükséges</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid property assignment: byte array expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: bájttömb szükséges</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid property assignment: url expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: URL szükséges</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Invalid property assignment: unsigned int expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: előjel nélküli egész szám szükséges</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Invalid property assignment: int expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: egész szám szükséges</translation>
    </message>
    <message>
        <location line="+5"/>
        <location line="+6"/>
        <source>Invalid property assignment: number expected</source>
        <translation>Érvénytelen tulajdonság hozzárendelés: szám szükséges</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Invalid property assignment: color expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: szín szükséges</translation>
    </message>
    <message>
        <location line="+9"/>
        <source>Invalid property assignment: date expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: dátum szükséges</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Invalid property assignment: time expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: idő szükséges</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Invalid property assignment: datetime expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: dátum és idő szükséges</translation>
    </message>
    <message>
        <location line="+9"/>
        <location line="+8"/>
        <location line="+32"/>
        <source>Invalid property assignment: point expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: pont szükséges</translation>
    </message>
    <message>
        <location line="-24"/>
        <location line="+8"/>
        <source>Invalid property assignment: size expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: méret szükséges</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Invalid property assignment: rect expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: téglalap szükséges</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Invalid property assignment: boolean expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: logikai érték szükséges</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Invalid property assignment: 2D vector expected</source>
        <translation type="unfinished">Érvénytelen tulajdonság-hozzárendelés: 4D vektor szükséges {2D?}</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Invalid property assignment: 3D vector expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: 3D vektor szükséges</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Invalid property assignment: 4D vector expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: 4D vektor szükséges</translation>
    </message>
    <message>
        <location line="+12"/>
        <source>Invalid property assignment: quaternion expected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Invalid property assignment: regular expression expected; use /pattern/ syntax</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: reguláris kifejezés szükséges, használja a /minta/ szintaxist</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Invalid property assignment: number or array of numbers expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: szám vagy számok tömbje szükséges</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Invalid property assignment: int or array of ints expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: egész szám vagy egész számok tömbje szükséges</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Invalid property assignment: bool or array of bools expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: logikai érték vagy logikai értékek tömbje szükséges</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Invalid property assignment: url or array of urls expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: URL vagy URL-ek tömbje szükséges</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Invalid property assignment: string or array of strings expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: karakterlánc vagy karakterláncok tömbje szükséges</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>Invalid property assignment: unsupported type &quot;%1&quot;</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: nem támogatott „%1” típus</translation>
    </message>
    <message>
        <location line="+77"/>
        <source>&quot;%1&quot; cannot operate on &quot;%2&quot;</source>
        <translation>A(z) „%1” nem működik ezen: „%2”</translation>
    </message>
    <message>
        <location line="+24"/>
        <source>Cannot assign object to list property &quot;%1&quot;</source>
        <translation>Nem lehet objektumot hozzárendelni a(z) „%1” listatulajdonsághoz</translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Cannot assign value of type &quot;%1&quot; to property &quot;%2&quot;, expecting &quot;%3&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+5"/>
        <source>Cannot assign value of type &quot;%1&quot; to property &quot;%2&quot;, expecting an object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+34"/>
        <source>Cannot assign object of type &quot;%1&quot; to property of type &quot;%2&quot; as the former is neither the same as the latter nor a sub-class of it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Cannot assign to property of unknown type &quot;%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unexpected object assignment</source>
        <translation type="vanished">Váratlan objektum-hozzárendelés</translation>
    </message>
    <message>
        <location line="-35"/>
        <source>Invalid property assignment: script expected</source>
        <translation>Érvénytelen tulajdonság-hozzárendelés: parancsfájl szükséges</translation>
    </message>
    <message>
        <source>Cannot assign object to property</source>
        <translation type="vanished">Nem lehet objektumot hozzárendelni a tulajdonsághoz</translation>
    </message>
</context>
<context>
    <name>QQmlRewrite</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlpropertycache.cpp" line="+899"/>
        <source>Signal uses unnamed parameter followed by named parameter.</source>
        <translation>A jelzés névtelen paramétert használ, amelyet egy elnevezett paraméter követ.</translation>
    </message>
    <message>
        <location line="+4"/>
        <source>Signal parameter &quot;%1&quot; hides global variable.</source>
        <translation>A(z) „%1” jelzésparaméter elrejti a globális változót.</translation>
    </message>
</context>
<context>
    <name>QQmlTypeCompiler</name>
    <message>
        <source>Composite Singleton Type %1 is not creatable.</source>
        <translation type="vanished">A(z) %1 összetett egyke típus nem létrehozható.</translation>
    </message>
    <message>
        <source>Element is not creatable.</source>
        <translation type="vanished">Az elem nem létrehozható.</translation>
    </message>
</context>
<context>
    <name>QQmlTypeData</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmltypedata.cpp" line="+925"/>
        <source>Composite Singleton Type %1 is not creatable.</source>
        <translation type="unfinished">A(z) %1 összetett egyke típus nem létrehozható.</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Element is not creatable.</source>
        <translation type="unfinished">Az elem nem létrehozható.</translation>
    </message>
</context>
<context>
    <name>QQmlTypeLoader</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlimport.cpp" line="-509"/>
        <source>Cannot update qmldir content for &apos;%1&apos;</source>
        <translation>Nem lehet frissíteni a qmldir tartalmát ennél:„%1”</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmltypedata.cpp" line="-483"/>
        <source>No matching type found, pragma Singleton files cannot be used by QQmlComponent.</source>
        <translation>Nem található illeszkedő típus, a QQmlComponent nem tudja használni a pragma egyke fájlokat.</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>pragma Singleton used with a non composite singleton type %1</source>
        <translation>pragma egyke lett használva egy nem összetett %1 egyke típussal</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlscriptblob.cpp" line="+86"/>
        <location filename="../../qtdeclarative/src/qml/qml/qqmltypedata.cpp" line="+99"/>
        <source>File was compiled ahead of time with an incompatible version of Qt and the original file cannot be found. Please recompile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+2"/>
        <location filename="../../qtdeclarative/src/qml/qml/qqmltypedata.cpp" line="+2"/>
        <source>No such file or directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+85"/>
        <location filename="../../qtdeclarative/src/qml/qml/qqmltypedata.cpp" line="-255"/>
        <source>Script %1 unavailable</source>
        <translation>A(z) %1 parancsfájl nem érhető el</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmltypedata.cpp" line="+24"/>
        <source>Type %1 has no inline component type called %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+16"/>
        <location line="+19"/>
        <source>Type %1 unavailable</source>
        <translation>A(z) %1 típus nem érhető el</translation>
    </message>
    <message>
        <location line="+104"/>
        <source>qmldir defines type as singleton, but no pragma Singleton found in type %1.</source>
        <translation>a qmldir egykeként határozza meg a típust, de nem található pragma egyke a(z) %1 típusban.</translation>
    </message>
    <message>
        <location line="+94"/>
        <source>File is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+138"/>
        <source>module &quot;%1&quot; is not installed</source>
        <translation>a(z) „%1” modul nincs telepítve</translation>
    </message>
    <message>
        <location line="+285"/>
        <source>Namespace %1 cannot be used as a type</source>
        <translation>A(z) %1 névtér nem használható típusként</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unreported error adding script import to import database</source>
        <translation>Nem jelentett hiba a parancsfájl importálásának hozzáadásakor az importálási adatbázisba</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
</context>
<context>
    <name>QQuickAbstractAnimation</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickanimation.cpp" line="+188"/>
        <source>Cannot animate non-existent property &quot;%1&quot;</source>
        <translation>Nem lehet animálni a nem létező „%1&quot; tulajdonságot</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Cannot animate read-only property &quot;%1&quot;</source>
        <translation>Nem lehet animálni a csak olvasható „%1” tulajdonságot</translation>
    </message>
    <message>
        <source>Animation is an abstract class</source>
        <translation type="vanished">Az animáció egy absztrakt osztály</translation>
    </message>
    <message>
        <source>Animator is an abstract class</source>
        <translation type="vanished">Az Animator egy absztrakt osztály</translation>
    </message>
</context>
<context>
    <name>QQuickAccessibleAttached</name>
    <message>
        <source>Accessible is only available via attached properties</source>
        <translation type="vanished">Az Accessible csak csatolt tulajdonságokon keresztül érhető el</translation>
    </message>
</context>
<context>
    <name>QQuickAnchorAnimation</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemanimation.cpp" line="+477"/>
        <source>Cannot set a duration of &lt; 0</source>
        <translation>Nem állítható be 0-nál kisebb időtartam</translation>
    </message>
</context>
<context>
    <name>QQuickAnchors</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickanchors.cpp" line="+217"/>
        <source>Possible anchor loop detected on fill.</source>
        <translation>Lehetséges horgony hurok észlelhető a kitöltésen.</translation>
    </message>
    <message>
        <location line="+29"/>
        <source>Possible anchor loop detected on centerIn.</source>
        <translation>Lehetséges horgony hurok észlelhető a középvonalon.</translation>
    </message>
    <message>
        <location line="+286"/>
        <location line="+36"/>
        <location line="+794"/>
        <location line="+38"/>
        <source>Cannot anchor to an item that isn&apos;t a parent or sibling.</source>
        <translation>Nem lehet egy olyan elemhez horgonyozni, amely nem szülő vagy testvér.</translation>
    </message>
    <message>
        <location line="-778"/>
        <source>Possible anchor loop detected on vertical anchor.</source>
        <translation>Lehetséges horgony hurok észlelhető egy függőleges horgonyon.</translation>
    </message>
    <message>
        <location line="+175"/>
        <source>Possible anchor loop detected on horizontal anchor.</source>
        <translation>Lehetséges horgony hurok észlelhető egy vízszintes horgonyon.</translation>
    </message>
    <message>
        <location line="+548"/>
        <source>Cannot specify left, right, and horizontalCenter anchors at the same time.</source>
        <translation>Nem lehet a bal, jobb és vízszintes közép horgonyokat egyszerre megadni.</translation>
    </message>
    <message>
        <location line="+10"/>
        <location line="+38"/>
        <source>Cannot anchor to a null item.</source>
        <translation>Nem lehet egy null elemhez horgonyozni.</translation>
    </message>
    <message>
        <location line="-35"/>
        <source>Cannot anchor a horizontal edge to a vertical edge.</source>
        <translation>Nem lehet egy vízszintes szélt egy függőleges szélhez horgonyozni.</translation>
    </message>
    <message>
        <location line="+7"/>
        <location line="+38"/>
        <source>Cannot anchor item to self.</source>
        <translation>Nem lehet egy elemet önmagához horgonyozni.</translation>
    </message>
    <message>
        <location line="-26"/>
        <source>Cannot specify top, bottom, and verticalCenter anchors at the same time.</source>
        <translation>Nem lehet a felső, alsó és függőleges közép horgonyokat egyszerre megadni.</translation>
    </message>
    <message>
        <location line="+6"/>
        <source>Baseline anchor cannot be used in conjunction with top, bottom, or verticalCenter anchors.</source>
        <translation>Alapvonali horgonyt nem lehet együtt használni a felső, alsó vagy függőleges közép horgonyokkal.</translation>
    </message>
    <message>
        <location line="+13"/>
        <source>Cannot anchor a vertical edge to a horizontal edge.</source>
        <translation>Nem lehet egy függőleges szélt egy vízszintes szélhez horgonyozni.</translation>
    </message>
</context>
<context>
    <name>QQuickAnimatedImage</name>
    <message>
        <source>Qt was built without support for QMovie</source>
        <translation type="vanished">A Qt a QMovie támogatás nélkül lett lefordítva</translation>
    </message>
</context>
<context>
    <name>QQuickApplication</name>
    <message>
        <source>Application is an abstract class</source>
        <translation type="vanished">Az alkalmazás egy absztrakt osztály</translation>
    </message>
</context>
<context>
    <name>QQuickBehavior</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickbehavior.cpp" line="+132"/>
        <source>Cannot change the animation assigned to a Behavior.</source>
        <translation>Nem lehet egy viselkedéshez hozzárendelt animációt módosítani.</translation>
    </message>
</context>
<context>
    <name>QQuickDragAttached</name>
    <message>
        <source>Drag is only available via attached properties</source>
        <translation type="vanished">A Drag csak csatolt tulajdonságokon keresztül érhető el</translation>
    </message>
</context>
<context>
    <name>QQuickEnterKeyAttached</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitem.cpp" line="+1663"/>
        <source>EnterKey attached property only works with Items</source>
        <translation>Az EnterKey csatolt tulajdonság csak elemekkel működik</translation>
    </message>
    <message>
        <source>EnterKey is only available via attached properties</source>
        <translation type="vanished">Az EnterKey csak csatolt tulajdonságokon keresztül érhető el</translation>
    </message>
</context>
<context>
    <name>QQuickFlipable</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickflipable.cpp" line="+156"/>
        <source>front is a write-once property</source>
        <translation>a front csak egyszer írható tulajdonság</translation>
    </message>
    <message>
        <location line="+22"/>
        <source>back is a write-once property</source>
        <translation>a back csak egyszer írható tulajdonság</translation>
    </message>
</context>
<context>
    <name>QQuickItemView</name>
    <message>
        <source>ItemView is an abstract base class</source>
        <translation type="vanished">Az ItemView egy absztrakt-alapú osztály</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemview.cpp" line="+2365"/>
        <source>Delegate must be of Item type</source>
        <translation>A delegáltnak elem típusúnak kell lennie</translation>
    </message>
</context>
<context>
    <name>QQuickKeyNavigationAttached</name>
    <message>
        <source>KeyNavigation is only available via attached properties</source>
        <translation type="vanished">A KeyNavigation csak csatolt tulajdonságokon keresztül érhető el</translation>
    </message>
</context>
<context>
    <name>QQuickKeysAttached</name>
    <message>
        <source>Keys is only available via attached properties</source>
        <translation type="vanished">A Keys csak csatolt tulajdonságokon keresztül érhető el</translation>
    </message>
</context>
<context>
    <name>QQuickLayoutMirroringAttached</name>
    <message>
        <source>LayoutDirection attached property only works with Items</source>
        <translation type="vanished">A LayoutDirection csatolt tulajdonság csak elemekkel működik</translation>
    </message>
    <message>
        <source>LayoutMirroring is only available via attached properties</source>
        <translation type="vanished">A LayoutMirroring csak csatolt tulajdonságokon keresztül érhető el</translation>
    </message>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitem.cpp" line="-150"/>
        <source>LayoutDirection attached property only works with Items and Windows</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQuickLoader</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickloader.cpp" line="+990"/>
        <source>setSource: value is not an object</source>
        <translation>forrásbeállítás: az érték nem objektum</translation>
    </message>
</context>
<context>
    <name>QQuickOpenGLInfo</name>
    <message>
        <source>OpenGLInfo is only available via attached properties</source>
        <translation type="vanished">Az OpenGLInfo csak csatolt tulajdonságokon keresztül érhető el</translation>
    </message>
</context>
<context>
    <name>QQuickPaintedItem</name>
    <message>
        <source>Cannot create instance of abstract class PaintedItem</source>
        <translation type="vanished">Nem lehet létrehozni a PaintedItem absztrakt osztály példányát</translation>
    </message>
</context>
<context>
    <name>QQuickParentAnimation</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemanimation.cpp" line="-170"/>
        <source>Unable to preserve appearance under complex transform</source>
        <translation>Nem lehet a kinézetet megőrizni összetett transzformáció alatt</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+7"/>
        <source>Unable to preserve appearance under non-uniform scale</source>
        <translation>Nem lehet a kinézetet megőrizni a nem egyenletes méretezés alatt</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to preserve appearance under scale of 0</source>
        <translation>Nem lehet a kinézetet megőrizni 0-ás méretezés alatt</translation>
    </message>
</context>
<context>
    <name>QQuickParentChange</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickstateoperations.cpp" line="+85"/>
        <source>Unable to preserve appearance under complex transform</source>
        <translation>Nem lehet a kinézetet megőrizni összetett transzformáció alatt</translation>
    </message>
    <message>
        <location line="+11"/>
        <location line="+7"/>
        <source>Unable to preserve appearance under non-uniform scale</source>
        <translation>Nem lehet a kinézetet megőrizni a nem egyenletes méretezés alatt</translation>
    </message>
    <message>
        <location line="+7"/>
        <source>Unable to preserve appearance under scale of 0</source>
        <translation>Nem lehet a kinézetet megőrizni 0-ás méretezés alatt</translation>
    </message>
</context>
<context>
    <name>QQuickPathAnimation</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickitemanimation.cpp" line="+284"/>
        <source>Cannot set a duration of &lt; 0</source>
        <translation>Nem állítható be 0-nál kisebb időtartam</translation>
    </message>
</context>
<context>
    <name>QQuickPathView</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickpathview.cpp" line="+141"/>
        <source>Delegate must be of Item type</source>
        <translation>A delegáltnak elem típusúnak kell lennie</translation>
    </message>
</context>
<context>
    <name>QQuickPauseAnimation</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickanimation.cpp" line="+541"/>
        <source>Cannot set a duration of &lt; 0</source>
        <translation>Nem állítható be 0-nál kisebb időtartam</translation>
    </message>
</context>
<context>
    <name>QQuickPixmap</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickpixmapcache.cpp" line="+466"/>
        <source>Error decoding: %1: %2</source>
        <translation>Hiba a dekódoláskor: %1: %2</translation>
    </message>
    <message>
        <location line="+315"/>
        <location line="+581"/>
        <source>Invalid image provider: %1</source>
        <translation>Érvénytelen képszolgáltató: %1</translation>
    </message>
    <message>
        <location line="-551"/>
        <location line="+21"/>
        <location line="+576"/>
        <source>Failed to get image from provider: %1</source>
        <translation>Nem sikerült lekérni a képet a szolgáltatótól: %1</translation>
    </message>
    <message>
        <location line="-555"/>
        <source>Failed to get texture from provider: %1</source>
        <translation>Nem sikerült lekérni a textúrát a szolgáltatótól: %1</translation>
    </message>
    <message>
        <location line="+57"/>
        <location line="+518"/>
        <source>Error decoding: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="-495"/>
        <location line="+512"/>
        <source>Cannot open: %1</source>
        <translation>Nem nyitható meg: %1</translation>
    </message>
</context>
<context>
    <name>QQuickPropertyAnimation</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickanimation.cpp" line="+1403"/>
        <source>Cannot set a duration of &lt; 0</source>
        <translation>Nem állítható be 0-nál kisebb időtartam</translation>
    </message>
</context>
<context>
    <name>QQuickPropertyChanges</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/util/qquickpropertychanges.cpp" line="+243"/>
        <source>PropertyChanges does not support creating state-specific objects.</source>
        <translation>A PropertyChanges nem támogatja az állapotspecifikus objektumokat.</translation>
    </message>
    <message>
        <location line="+176"/>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation>Nem lehet hozzárendelni a nem létező „%1” tulajdonsághoz</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Cannot assign to read-only property &quot;%1&quot;</source>
        <translation>Nem lehet hozzárendelni a csak olvasható „%1” tulajdonsághoz</translation>
    </message>
</context>
<context>
    <name>QQuickRepeater</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickrepeater.cpp" line="+435"/>
        <source>Delegate must be of Item type</source>
        <translation>A delegáltnak elem típusúnak kell lennie</translation>
    </message>
</context>
<context>
    <name>QQuickShaderEffectMesh</name>
    <message>
        <source>Cannot create instance of abstract class ShaderEffectMesh.</source>
        <translation type="vanished">Nem lehet létrehozni a ShaderEffectMesh absztrakt osztály példányát.</translation>
    </message>
</context>
<context>
    <name>QQuickTextUtil</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquicktextutil.cpp" line="+64"/>
        <source>%1 does not support loading non-visual cursor delegates.</source>
        <translation>A(z) %1 nem támogatja a nem vizuális kurzor delegáltak betöltését.</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Could not load cursor delegate</source>
        <translation>Nem sikerült betölteni a kurzor delegáltat</translation>
    </message>
</context>
<context>
    <name>QQuickViewTransitionAttached</name>
    <message>
        <source>ViewTransition is only available via attached properties</source>
        <translation type="vanished">A ViewTransition csak csatolt tulajdonságokon keresztül érhető el</translation>
    </message>
</context>
<context>
    <name>QQuickWindow</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickwindow.cpp" line="+3465"/>
        <source>Failed to create %1 context for format %2.
This is most likely caused by not having the necessary graphics drivers installed.

Install a driver providing OpenGL 2.0 or higher, or, if this is not possible, make sure the ANGLE Open GL ES 2.0 emulation libraries (%3, %4 and d3dcompiler_*.dll) are available in the application executable&apos;s directory or in a location listed in PATH.</source>
        <extracomment>%1 Context type (Open GL, EGL), %2 format, ANGLE %3, %4 library names</extracomment>
        <translation>Nem sikerült %1 környezetet létrehozni a(z) %2 formátumhoz.
Nagy valószínűség szerint ezt az okozza, hogy a szükséges grafikai illesztőprogramok nem lettek telepítve.

Telepítsen egy olyan illesztőprogramot, amely OpenGL 2.0-t vagy újabbat nyújt, vagy ha ez nem lehetséges, akkor győződjön meg arról, hogy az ANGLE Open GL ES 2.0 emulációs függvénykönyvtárak (%3, %4 és d3dcompiler_*.dll) elérhetők-e az alkalmazás végrehajtható könyvtárában vagy a PATH környezeti változóban felsorolt egyik helyen.</translation>
    </message>
    <message>
        <location line="+10"/>
        <source>Failed to create %1 context for format %2</source>
        <extracomment>%1 Context type (Open GL, EGL), %2 format specification</extracomment>
        <translation>Nem sikerült %1 környezetet létrehozni a(z) %2 formátumhoz</translation>
    </message>
    <message>
        <location line="+11"/>
        <source>Failed to initialize graphics backend for %1.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QQuickWindowQmlImpl</name>
    <message>
        <location filename="../../qtdeclarative/src/quick/items/qquickwindowmodule.cpp" line="+172"/>
        <source>Conflicting properties &apos;visible&apos; and &apos;visibility&apos; for Window &apos;%1&apos;</source>
        <translation>Ütköző „visible” és „visibility” tulajdonságok a(z) „%1” ablaknál</translation>
    </message>
    <message>
        <location line="+3"/>
        <source>Conflicting properties &apos;visible&apos; and &apos;visibility&apos;</source>
        <translation>Ütköző „visible” és „visibility” tulajdonságok</translation>
    </message>
</context>
<context>
    <name>QQuickXmlListModel</name>
    <message>
        <source>&quot;%1&quot; duplicates a previous role name and will be disabled.</source>
        <translation type="vanished">A(z) „%1” kettőz egy korábbi szerepnevet, és le lesz tiltva.</translation>
    </message>
    <message>
        <source>invalid query: &quot;%1&quot;</source>
        <translation type="vanished">érvénytelen lekérdezés: „%1”</translation>
    </message>
</context>
<context>
    <name>QQuickXmlListModelRole</name>
    <message>
        <source>An XmlRole query must not start with &apos;/&apos;</source>
        <translation type="vanished">Egy XmlRole lekérdezés nem kezdődhet „/” karakterrel</translation>
    </message>
</context>
<context>
    <name>QQuickXmlRoleList</name>
    <message>
        <source>An XmlListModel query must start with &apos;/&apos; or &quot;//&quot;</source>
        <translation type="vanished">Egy XmlListModel lekérdezésnek „/” vagy „//” karakterekkel kell kezdődnie</translation>
    </message>
</context>
<context>
    <name>SignalHandlerConverter</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmltypecompiler.cpp" line="-218"/>
        <source>Non-existent attached object</source>
        <translation>Nem létező csatolt objektum</translation>
    </message>
    <message>
        <location line="+40"/>
        <source>Signal uses unnamed parameter followed by named parameter.</source>
        <translation>A jelzés névtelen paramétert használ, amelyet egy elnevezett paraméter követ.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Signal parameter &quot;%1&quot; hides global variable.</source>
        <translation>A(z) „%1” jelzésparaméter elrejti a globális változót.</translation>
    </message>
    <message>
        <location line="+15"/>
        <source>&quot;%1.%2&quot; is not available in %3 %4.%5.</source>
        <translation>A(z) „%1.%2” nem érhető ebben: %3 %4.%5.</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>&quot;%1.%2&quot; is not available due to component versioning.</source>
        <translation>A(z) „%1.%2” nem érhető el az összetevő-verziózás miatt.</translation>
    </message>
    <message>
        <location line="+42"/>
        <source>Cannot assign a value to a signal (expecting a script to be run)</source>
        <translation>Nem lehet értéket hozzárendelni egy jelzéshez (egy parancsfájl szükséges a futtatáshoz)</translation>
    </message>
    <message>
        <location line="+2"/>
        <source>Incorrectly specified signal assignment</source>
        <translation>Helytelenül megadott jelzés-hozzárendelés</translation>
    </message>
</context>
<context>
    <name>SignalTransition</name>
    <message>
        <location filename="../../qtdeclarative/src/imports/statemachine/signaltransition.cpp" line="+129"/>
        <source>Specified signal does not exist.</source>
        <translation>A megadott jelzés nem létezik.</translation>
    </message>
    <message>
        <location line="+68"/>
        <source>Cannot assign to non-existent property &quot;%1&quot;</source>
        <translation>Nem lehet hozzárendelni a nem létező „%1” tulajdonsághoz</translation>
    </message>
    <message>
        <location line="+5"/>
        <source>SignalTransition: script expected</source>
        <translation>jelzésátmenet: parancsfájl szükséges</translation>
    </message>
</context>
<context>
    <name>qmlRegisterType</name>
    <message>
        <location filename="../../qtdeclarative/src/qml/qml/qqmlmetatype.cpp" line="+393"/>
        <source>Invalid QML %1 name &quot;%2&quot;; type names must begin with an uppercase letter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location line="+8"/>
        <source>Invalid QML %1 name &quot;%2&quot;</source>
        <translation>Érvénytelen „%2” QML %1 név</translation>
    </message>
    <message>
        <source>Cannot install %1 &apos;%2&apos; into unregistered namespace &apos;%3&apos;</source>
        <translation type="vanished">Nem lehetséges a(z) %1 „%2” telepítése a nem regisztrált „%3” névtérbe</translation>
    </message>
    <message>
        <source>Cannot install %1 &apos;%2&apos; into protected namespace &apos;%3&apos;</source>
        <translation type="vanished">Nem lehetséges a(z) %1 „%2” telepítése a védett „%3” névtérbe</translation>
    </message>
    <message>
        <location line="+14"/>
        <source>Cannot install %1 &apos;%2&apos; into protected module &apos;%3&apos; version &apos;%4&apos;</source>
        <translation>Nem lehetséges a(z) %1 „%2” telepítése a védett „%4” verziójú „%3” modulba</translation>
    </message>
</context>
</TS>
